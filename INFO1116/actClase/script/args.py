#!/usr/bin/python3

import sys

def main() -> None:
    print(f"Yo soy: {__file__}")
    for i in range(len(sys.argv[1:])):
        print(f"Este es el argumento {1+i}: {sys.argv[1+i]}")

    return


if __name__ == '__main__':
    main()
