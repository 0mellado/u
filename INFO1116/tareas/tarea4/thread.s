	.file	"thread.c"
	.text
	.globl	num
	.bss
	.align 4
	.type	num, @object
	.size	num, 4
num:
	.zero	4
	.globl	results
	.align 8
	.type	results, @object
	.size	results, 8
results:
	.zero	8
	.globl	var
	.align 32
	.type	var, @object
	.size	var, 40
var:
	.zero	40
	.globl	file
	.align 32
	.type	file, @object
	.size	file, 40
file:
	.zero	40
	.section	.rodata
.LC0:
	.string	"primer hilo: escribiendo %d\n"
.LC1:
	.string	"r"
.LC2:
	.string	"resultados.txt"
.LC3:
	.string	"%s"
	.text
	.globl	createRandNum
	.type	createRandNum, @function
createRandNum:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	call	getpid@PLT
	movl	%eax, %edi
	call	srand@PLT
	movl	$0, -84(%rbp)
	jmp	.L2
.L3:
	leaq	var(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_lock@PLT
	call	rand@PLT
	movl	%eax, %ecx
	movslq	%ecx, %rax
	imulq	$1717986919, %rax, %rax
	shrq	$32, %rax
	movl	%eax, %edx
	sarl	$2, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movl	%edx, %eax
	sall	$2, %eax
	addl	%edx, %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %edx
	leal	1(%rdx), %eax
	movl	%eax, num(%rip)
	movl	num(%rip), %eax
	movl	%eax, %esi
	leaq	.LC0(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	leaq	var(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_unlock@PLT
	movl	$200, %edi
	call	usleep@PLT
	addl	$1, -84(%rbp)
.L2:
	cmpl	$9, -84(%rbp)
	jle	.L3
	movl	$200, %edi
	call	usleep@PLT
	leaq	file(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_lock@PLT
	leaq	.LC1(%rip), %rax
	movq	%rax, %rsi
	leaq	.LC2(%rip), %rax
	movq	%rax, %rdi
	call	fopen@PLT
	movq	%rax, results(%rip)
	movq	results(%rip), %rdx
	leaq	-80(%rbp), %rax
	movl	$64, %esi
	movq	%rax, %rdi
	call	fgets@PLT
	leaq	-80(%rbp), %rax
	movq	%rax, %rsi
	leaq	.LC3(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movq	results(%rip), %rax
	movq	%rax, %rdi
	call	fclose@PLT
	leaq	file(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_unlock@PLT
	nop
	movq	-8(%rbp), %rax
	subq	%fs:40, %rax
	je	.L5
	call	__stack_chk_fail@PLT
.L5:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	createRandNum, .-createRandNum
	.section	.rodata
.LC4:
	.string	"segundo hilo: leyendo %d\n"
.LC7:
	.string	"w"
	.align 8
.LC8:
	.string	"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, promedio: %f\n"
	.text
	.globl	leer
	.type	leer, @function
leer:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$100, %edi
	call	usleep@PLT
	movl	$0, -92(%rbp)
	jmp	.L7
.L8:
	leaq	var(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_lock@PLT
	movl	num(%rip), %eax
	movl	%eax, %esi
	leaq	.LC4(%rip), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	num(%rip), %edx
	movl	-92(%rbp), %eax
	cltq
	movl	%edx, -80(%rbp,%rax,4)
	leaq	var(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_unlock@PLT
	movl	$200, %edi
	call	usleep@PLT
	addl	$1, -92(%rbp)
.L7:
	cmpl	$9, -92(%rbp)
	jle	.L8
	leaq	file(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_lock@PLT
	movb	$0, -93(%rbp)
	pxor	%xmm0, %xmm0
	movss	%xmm0, -88(%rbp)
	jmp	.L9
.L10:
	movsbl	-93(%rbp), %eax
	cltq
	movl	-80(%rbp,%rax,4), %eax
	pxor	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	-88(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, -88(%rbp)
	movzbl	-93(%rbp), %eax
	addl	$1, %eax
	movb	%al, -93(%rbp)
.L9:
	cmpb	$9, -93(%rbp)
	jle	.L10
	movss	-88(%rbp), %xmm0
	movss	.LC6(%rip), %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, -84(%rbp)
	leaq	.LC7(%rip), %rax
	movq	%rax, %rsi
	leaq	.LC2(%rip), %rax
	movq	%rax, %rdi
	call	fopen@PLT
	movq	%rax, results(%rip)
	pxor	%xmm2, %xmm2
	cvtss2sd	-84(%rbp), %xmm2
	movq	%xmm2, %rsi
	movl	-44(%rbp), %ebx
	movl	-48(%rbp), %r11d
	movl	-52(%rbp), %r10d
	movl	-56(%rbp), %r9d
	movl	-60(%rbp), %r8d
	movl	-64(%rbp), %edi
	movl	-68(%rbp), %r13d
	movl	-72(%rbp), %r12d
	movl	-76(%rbp), %ecx
	movl	-80(%rbp), %edx
	movq	results(%rip), %rax
	pushq	%rbx
	pushq	%r11
	pushq	%r10
	pushq	%r9
	pushq	%r8
	pushq	%rdi
	movq	%rsi, %xmm0
	movl	%r13d, %r9d
	movl	%r12d, %r8d
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movl	$1, %eax
	call	fprintf@PLT
	addq	$48, %rsp
	movq	results(%rip), %rax
	movq	%rax, %rdi
	call	fclose@PLT
	leaq	file(%rip), %rax
	movq	%rax, %rdi
	call	pthread_mutex_unlock@PLT
	nop
	movq	-40(%rbp), %rax
	subq	%fs:40, %rax
	je	.L12
	call	__stack_chk_fail@PLT
.L12:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	leer, .-leer
	.globl	main
	.type	main, @function
main:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-24(%rbp), %rax
	movl	$0, %ecx
	leaq	createRandNum(%rip), %rdx
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_create@PLT
	leaq	-16(%rbp), %rax
	movl	$0, %ecx
	leaq	leer(%rip), %rdx
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_create@PLT
	movq	-24(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_join@PLT
	movq	-16(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_join@PLT
	movl	$0, %eax
	movq	-8(%rbp), %rdx
	subq	%fs:40, %rdx
	je	.L15
	call	__stack_chk_fail@PLT
.L15:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC6:
	.long	1092616192
	.ident	"GCC: (GNU) 12.2.0"
	.section	.note.GNU-stack,"",@progbits
