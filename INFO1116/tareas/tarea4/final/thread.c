#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int num;
FILE *results;
pthread_mutex_t var = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t file = PTHREAD_MUTEX_INITIALIZER;

void createRandNum(void *args) {
    srand(getpid());

    for (int i = 0; i < 10; i++) {
        pthread_mutex_lock(&var);
        num = rand() % 10 + 1;
        printf("primer hilo: escribiendo %d\n", num);
        pthread_mutex_unlock(&var );
        sleep(2);
    }

    sleep(2);
    pthread_mutex_lock(&file);
    char dataFile[64];
    results = fopen("resultados.txt", "r"); 

    fgets(dataFile, 64, results);
    printf("%s", dataFile);

    fclose(results);
    pthread_mutex_unlock(&file);
    return;
}


void leer(void *args) {
    int nums[10];

    sleep(1);
    for (int i = 0; i < 10; i++) {
        pthread_mutex_lock(&var);
        printf("segundo hilo: leyendo %d\n", num);
        nums[i] = num;
        pthread_mutex_unlock(&var);
        sleep(2);
    }

    pthread_mutex_lock(&file);
    char i = 0;
    float sum = 0;
    while (i < 10) {
        sum = sum + nums[i];
        i++;
    }
    float media = sum / 10;

    results = fopen("resultados.txt", "w");

    fprintf(results, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, promedio: %f\n", 
            nums[0], nums[1], nums[2], nums[3], nums[4], 
            nums[5], nums[6], nums[7], nums[8], nums[9], media);

    fclose(results);
    pthread_mutex_unlock(&file);

    return;
}


int main() {
    pthread_t thread0, thread1;

    pthread_create(&thread0, NULL, (void *) &createRandNum, NULL);
    pthread_create(&thread1, NULL, (void *) &leer, NULL);

    pthread_join(thread0, NULL);
    pthread_join(thread1, NULL);

    return 0;
}
