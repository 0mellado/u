\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Resumen}{2}{section.1}%
\contentsline {section}{\numberline {2}Introducci\IeC {\'o}n}{2}{section.2}%
\contentsline {section}{\numberline {3}Marco te\IeC {\'o}rico}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Partici\IeC {\'o}n de disco}{3}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Esquema de partici\IeC {\'o}n MBR}{3}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Tipos de particiones}{5}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}Sistemas de archivos}{5}{subsubsection.3.1.3}%
\contentsline {subsection}{\numberline {3.2}Kernel}{6}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Arranque del sistema}{6}{subsection.3.3}%
\contentsline {section}{\numberline {4}Configuraci\IeC {\'o}n e instalaci\IeC {\'o}n de los diferentes sistemas operativos}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Sistema archlinux}{7}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Requerimientos m\IeC {\'\i }nimos, recomendados y usados}{7}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Descripci\IeC {\'o}n del proceso b\IeC {\'a}sico de la instalaci\IeC {\'o}n}{7}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Pre-instalaci\IeC {\'o}n}{8}{subsubsection.4.1.3}%
\contentsline {subsubsection}{\numberline {4.1.4}Instalaci\IeC {\'o}n}{13}{subsubsection.4.1.4}%
\contentsline {subsubsection}{\numberline {4.1.5}Configuraci\IeC {\'o}n}{13}{subsubsection.4.1.5}%
\contentsline {subsubsection}{\numberline {4.1.6}Administraci\IeC {\'o}n de usuarios}{16}{subsubsection.4.1.6}%
\contentsline {subsubsection}{\numberline {4.1.7}Instalaci\IeC {\'o}n de la interfaz gr\IeC {\'a}fica}{18}{subsubsection.4.1.7}%
\contentsline {subsubsection}{\numberline {4.1.8}Administrador de tareas}{20}{subsubsection.4.1.8}%
\contentsline {subsubsection}{\numberline {4.1.9}Controlador de dispositivos}{21}{subsubsection.4.1.9}%
\contentsline {subsubsection}{\numberline {4.1.10}Interfaz de comandos}{21}{subsubsection.4.1.10}%
\contentsline {subsubsection}{\numberline {4.1.11}Visor de sucesos}{22}{subsubsection.4.1.11}%
\contentsline {subsubsection}{\numberline {4.1.12}Directorios importantes del sistema}{22}{subsubsection.4.1.12}%
\contentsline {subsection}{\numberline {4.2}Sistema openBSD}{23}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Requerimientos m\IeC {\'\i }nimos, recomendados y usados}{23}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Descripci\IeC {\'o}n del proceso b\IeC {\'a}sico de instalaci\IeC {\'o}n}{23}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Pre-instalaci\IeC {\'o}n}{24}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}Instalaci\IeC {\'o}n}{25}{subsubsection.4.2.4}%
\contentsline {subsubsection}{\numberline {4.2.5}Creaci\IeC {\'o}n de un usuario con permisos de administrador}{26}{subsubsection.4.2.5}%
\contentsline {subsubsection}{\numberline {4.2.6}Administrador de tareas}{27}{subsubsection.4.2.6}%
\contentsline {subsubsection}{\numberline {4.2.7}Controlador de dispositivos}{27}{subsubsection.4.2.7}%
\contentsline {subsubsection}{\numberline {4.2.8}Interfaz de comandos}{28}{subsubsection.4.2.8}%
\contentsline {subsubsection}{\numberline {4.2.9}Visor de sucesos}{28}{subsubsection.4.2.9}%
\contentsline {subsubsection}{\numberline {4.2.10}Directorios importantes del sistema}{29}{subsubsection.4.2.10}%
\contentsline {section}{\numberline {5}Conclusiones}{30}{section.5}%
