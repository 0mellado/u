# Instalación del sistema operativo archlinux
# sistema BIOS
loadkeys es
ping archlinux.org
timedatectl set-ntp true

cfdisk
mkfs.ext4 /dev/sda2
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda3

mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
swapon /dev/sda3

pacman -Sy archlinux-keyring

pacstrap /mnt base linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab 

arch-chroot /mnt 

ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
hwclock --systohc
pacman -S vim
vim /etc/locale.gen
locale-gen
echo "LANG=es_CL.UTF-8" > /etc/locale.conf
echo "KEYMAP=es" > /etc/vconsole.conf
echo "arch" > /etc/hostname
vim /etc/hosts
passwd
pacman -S networkmanager
systemctl enable NetworkManager
pacman -S grub 
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
useradd -m usuario
passwd usuario
usermod -aG wheel,audio,video,storage usuario
pacman -S sudo
vim /etc/sudoers
exit

umount -R /mnt
reboot
