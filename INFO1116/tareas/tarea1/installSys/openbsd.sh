# Instalación del sistema openBSD

Welcome to the OpenBSD/amd64 7.1 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell? I

Choose your keyboard layout ('?', or 'L' for list) la
System hostname? (short form, e.g 'foo') bsd
which network interface do you wish to configure? (or 'done') em0
IPv4 address for em0? (or 'autoconf' or 'none') autoconf
IPv6 address for em0? (or 'autoconf' or 'none') none
which network interface do you wish to configure? (or 'done') done
Password for root account? (will not echo) 1234
Password for root account? (again) 1234
Start sshd(8) by default? yes
Do you expect to run the X Window System? yes
Do you want the X Window System to be started by xenodm(1)? yes
Setup a user? (enter a lower-case loginname, or 'no') no
Allow root ssh login? (yes, no, prohibit-password) no
What timezone are you in? ('?' for list) America/Santiago
Which disk is the root disk? ('?' for details) wd0
Use (W)hole disk MBR, whole disk (G)PT or (E)dit? whole
Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? a
Location of sets? (cd0 disk http nfs or 'done') cd0
Pathname to the sets? (or 'done') 7.1/amd64
Set name(s)? (or 'abort' or 'done') done
Directory does not contain SHA256.sig. Continue without verification? yes
Location of sets? (cd0 disk http nfs or 'done') done
Time appears wrong. Set to 'fecha y hora actual'? yes
Exit to (S)hell, (H)alt or (R)eboot? reboot

# Crear un usuario con permisos de administrador

useradd -m usuario
usermod -G wheel usuario

# activar el comando doas

echo "permit :wheel" > /etc/doas.conf

# Instalación de la interfaz grafica

doas pkg_add git dmenu bash
git clone https://git.suckless.org/dwm
doas make clean install
echo "dwm" > ~/.xsession
