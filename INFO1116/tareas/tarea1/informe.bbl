\begin{thebibliography}{}

\bibitem[AMD, 2012]{amd64}
AMD (2012).
\newblock Amd64 architecture programmer’s manual.
\newblock 2(24593):13--14.
\newblock \\ Consultado el 28 de agosto del 2022.

\bibitem[{Bellevue Linux Users Group}, 2006]{kernel}
{Bellevue Linux Users Group} (2006).
\newblock Kernel definition.
\newblock \\ \url{http://www.linfo.org/kernel.html}.
\newblock \\ Consultado el 2 de septiembre del 2022.

\bibitem[{Equipo archlinux}, 2022a]{fstab}
{Equipo archlinux} (2022a).
\newblock fstab.
\newblock \\ \url{https://wiki.archlinux.org/title/Fstab}.
\newblock \\ Consultado el 5 de septiembre del 2022.

\bibitem[{Equipo archlinux}, 2022b]{guidearch}
{Equipo archlinux} (2022b).
\newblock Installation guide.
\newblock \\ \url{https://wiki.archlinux.org/title/installation_guide}.
\newblock \\ Consultado el 5 de septiembre del 2022.

\bibitem[{Equipo de openBSD}, 2022]{openbsd}
{Equipo de openBSD} (2022).
\newblock Openbsd.
\newblock \url{https://www.openbsd.org/}.
\newblock \\ Consultado el 28 de agosto del 2022.

\bibitem[Gillis, 2020]{driver}
Gillis, A.~S. (2020).
\newblock device driver.
\newblock \\
  \url{https://www.techtarget.com/searchenterprisedesktop/definition/device-driver}.
\newblock \\ Consultado el 5 de septiembre del 2022.

\bibitem[Han, 2022]{2tera}
Han, D. (2022).
\newblock Soporte de windows para discos duros de mas de 2tb.
\newblock \\
  \url{https://docs.microsoft.com/es-ES/troubleshoot/windows-server/backup-and-storage/support-for-hard-disks-exceeding-2-tb}.
\newblock \\ Consultado el 3 de septiembre del 2022.

\bibitem[Juncos, 2008]{fsystem}
Juncos, R. (2008).
\newblock Sistema de ficheros gnu/linux.
\newblock \\
  \url{https://web.archive.org/web/20081214104329/http://observatorio.cnice.mec.es/modules.php?op=modload\&name=News\&file=article\&sid=549}.
\newblock \\ Consultado el 3 de septiembre del 2022.

\bibitem[Levi, 2002]{unix}
Levi, B. (2002).
\newblock {\em UNIX Administration}.
\newblock \\ Consultado el 28 de agosto del 2022.

\bibitem[Lissot, 2005]{typepartition}
Lissot, A. (2005).
\newblock Partition types.
\newblock \\ \url{https://tldp.org/HOWTO/Partition/partition-types.html}.
\newblock \\ Consultado el 7 de septiembre del 2022.

\bibitem[{openbsdhandbook}, ]{setsopen}
{openbsdhandbook}.
\newblock File sets.
\newblock \\
  \url{https://www.openbsdhandbook.com/package_management/#file-sets}.
\newblock \\ Consultado el 7 de septiembre del 2022.

\bibitem[Stallings, 2005]{op}
Stallings, W. (2005).
\newblock Operating systems. internals and design principles.
\newblock 7.
\newblock \\ Consultado el 7 de septiembre del 2022.

\bibitem[Tanenbaum, 2003]{tanenbaum}
Tanenbaum, A. (2003).
\newblock {\em Sistemas Operativos Modernos}, volume~3.
\newblock \\ Consultado el 2 de septiembre del 2022.

\bibitem[{Universidad de Indiana}, 2017]{posix}
{Universidad de Indiana} (2017).
\newblock What is posix.
\newblock \\
  \url{https://web.archive.org/web/20180614172556/https://kb.iu.edu/d/agjv}.
\newblock \\ Consultado el 7 de septiembre del 2022.

\end{thebibliography}
