# **************  GNU/Linux ***********************
# limpieza de reglas anteriores
# Para IPTables

iptables -t filter -F
iptables -t nat -F


iptables -t filter -P INPUT ACCEPT
iptables -t filter -P FORWARD ACCEPT
iptables -t filter -P OUTPUT ACCEPT

# iptables -t nat -A POSTROUTING -o enp4s0f4u2u1 -j MASQUERADE
iptables -t nat -A POSTROUTING -o wlp2s0 -j MASQUERADE

# No olvidar el ipforwarding en el /etc/sysctl.conf
# Sin reiniciar 
# se usa
#
# echo 1 > /proc/sys/net/ipv4/ip_forward
