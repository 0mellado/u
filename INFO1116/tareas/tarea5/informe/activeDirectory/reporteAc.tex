\documentclass[11pt]{article}

    \usepackage[breakable]{tcolorbox}
    \usepackage{parskip} % Stop auto-indenting (to mimic markdown behaviour)
    

    % Basic figure setup, for now with no caption control since it's done
    % automatically by Pandoc (which extracts ![](path) syntax from Markdown).
    \usepackage{graphicx}
    % Maintain compatibility with old templates. Remove in nbconvert 6.0
    \let\Oldincludegraphics\includegraphics
    % Ensure that by default, figures have no caption (until we provide a
    % proper Figure object with a Caption API and a way to capture that
    % in the conversion process - todo).
    \usepackage{caption}
    \DeclareCaptionFormat{nocaption}{}
    \captionsetup{format=nocaption,aboveskip=0pt,belowskip=0pt}

    \usepackage{float}
    \floatplacement{figure}{H} % forces figures to be placed at the correct location
    \usepackage{xcolor} % Allow colors to be defined
    \usepackage{enumerate} % Needed for markdown enumerations to work
    \usepackage[a4paper]{geometry} % Used to adjust the document margins
    \usepackage{amsmath} % Equations
    \usepackage{amssymb} % Equations
    \usepackage{textcomp} % defines textquotesingle
    % Hack from http://tex.stackexchange.com/a/47451/13684:
    \AtBeginDocument{%
        \def\PYZsq{\textquotesingle}% Upright quotes in Pygmentized code
    }
    \usepackage{upquote} % Upright quotes for verbatim code
    \usepackage{eurosym} % defines \euro

    \usepackage{iftex}
    \ifPDFTeX
        \usepackage[T1]{fontenc}
        \IfFileExists{alphabeta.sty}{
              \usepackage{alphabeta}
          }{
              \usepackage[mathletters]{ucs}
              \usepackage[utf8x]{inputenc}
          }
    \else
        \usepackage{fontspec}
        \usepackage{unicode-math}
    \fi

    \usepackage{fancyvrb} % verbatim replacement that allows latex
    \usepackage{grffile} % extends the file name processing of package graphics 
                         % to support a larger range
    \makeatletter % fix for old versions of grffile with XeLaTeX
    \@ifpackagelater{grffile}{2019/11/01}
    {
      % Do nothing on new versions
    }
    {
      \def\Gread@@xetex#1{%
        \IfFileExists{"\Gin@base".bb}%
        {\Gread@eps{\Gin@base.bb}}%
        {\Gread@@xetex@aux#1}%
      }
    }
    \makeatother
    \usepackage[Export]{adjustbox} % Used to constrain images to a maximum size
    \adjustboxset{max size={0.9\linewidth}{0.9\paperheight}}

    % The hyperref package gives us a pdf with properly built
    % internal navigation ('pdf bookmarks' for the table of contents,
    % internal cross-reference links, web links for URLs, etc.)
    \usepackage{hyperref}
    % The default LaTeX title has an obnoxious amount of whitespace. By default,
    % titling removes some of it. It also provides customization options.
    \usepackage{titling}
    \usepackage{longtable} % longtable support required by pandoc >1.10
    \usepackage{booktabs}  % table support for pandoc > 1.12.2
    \usepackage{array}     % table support for pandoc >= 2.11.3
    \usepackage{calc}      % table minipage width calculation for pandoc >= 2.11.1
    \usepackage[inline]{enumitem} % IRkernel/repr support (it uses the enumerate* environment)
    \usepackage[normalem]{ulem} % ulem is needed to support strikethroughs (\sout)
                                % normalem makes italics be italics, not underlines
    \usepackage{mathrsfs}
    

    
    % Colors for the hyperref package
    \definecolor{urlcolor}{rgb}{0,.145,.698}
    \definecolor{linkcolor}{rgb}{.71,0.21,0.01}
    \definecolor{citecolor}{rgb}{.12,.54,.11}

    % ANSI colors
    \definecolor{ansi-black}{HTML}{3E424D}
    \definecolor{ansi-black-intense}{HTML}{282C36}
    \definecolor{ansi-red}{HTML}{E75C58}
    \definecolor{ansi-red-intense}{HTML}{B22B31}
    \definecolor{ansi-green}{HTML}{00A250}
    \definecolor{ansi-green-intense}{HTML}{007427}
    \definecolor{ansi-yellow}{HTML}{DDB62B}
    \definecolor{ansi-yellow-intense}{HTML}{B27D12}
    \definecolor{ansi-blue}{HTML}{208FFB}
    \definecolor{ansi-blue-intense}{HTML}{0065CA}
    \definecolor{ansi-magenta}{HTML}{D160C4}
    \definecolor{ansi-magenta-intense}{HTML}{A03196}
    \definecolor{ansi-cyan}{HTML}{60C6C8}
    \definecolor{ansi-cyan-intense}{HTML}{258F8F}
    \definecolor{ansi-white}{HTML}{C5C1B4}
    \definecolor{ansi-white-intense}{HTML}{A1A6B2}
    \definecolor{ansi-default-inverse-fg}{HTML}{FFFFFF}
    \definecolor{ansi-default-inverse-bg}{HTML}{000000}

    % common color for the border for error outputs.
    \definecolor{outerrorbackground}{HTML}{FFDFDF}

    % commands and environments needed by pandoc snippets
    % extracted from the output of `pandoc -s`
    \providecommand{\tightlist}{%
      \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
    \DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
    % Add ',fontsize=\small' for more characters per line
    \newenvironment{Shaded}{}{}
    \newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.56,0.13,0.00}{{#1}}}
    \newcommand{\DecValTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\FloatTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\CharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\StringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\CommentTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textit{{#1}}}}
    \newcommand{\OtherTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{{#1}}}
    \newcommand{\AlertTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.02,0.16,0.49}{{#1}}}
    \newcommand{\RegionMarkerTok}[1]{{#1}}
    \newcommand{\ErrorTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\NormalTok}[1]{{#1}}
    
    % Additional commands for more recent versions of Pandoc
    \newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.53,0.00,0.00}{{#1}}}
    \newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.73,0.40,0.53}{{#1}}}
    \newcommand{\ImportTok}[1]{{#1}}
    \newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.73,0.13,0.13}{\textit{{#1}}}}
    \newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\VariableTok}[1]{\textcolor[rgb]{0.10,0.09,0.49}{{#1}}}
    \newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.40,0.40,0.40}{{#1}}}
    \newcommand{\BuiltInTok}[1]{{#1}}
    \newcommand{\ExtensionTok}[1]{{#1}}
    \newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.74,0.48,0.00}{{#1}}}
    \newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.49,0.56,0.16}{{#1}}}
    \newcommand{\InformationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\WarningTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    
    
    % Define a nice break command that doesn't care if a line doesn't already
    % exist.
    \def\br{\hspace*{\fill} \\* }
    % Math Jax compatibility definitions
    \def\gt{>}
    \def\lt{<}
    \let\Oldtex\TeX
    \let\Oldlatex\LaTeX
    \renewcommand{\TeX}{\textrm{\Oldtex}}
    \renewcommand{\LaTeX}{\textrm{\Oldlatex}}
    
    
% Pygments definitions
\makeatletter
\def\PY@reset{\let\PY@it=\relax \let\PY@bf=\relax%
    \let\PY@ul=\relax \let\PY@tc=\relax%
    \let\PY@bc=\relax \let\PY@ff=\relax}
\def\PY@tok#1{\csname PY@tok@#1\endcsname}
\def\PY@toks#1+{\ifx\relax#1\empty\else%
    \PY@tok{#1}\expandafter\PY@toks\fi}
\def\PY@do#1{\PY@bc{\PY@tc{\PY@ul{%
    \PY@it{\PY@bf{\PY@ff{#1}}}}}}}
\def\PY#1#2{\PY@reset\PY@toks#1+\relax+\PY@do{#2}}

\@namedef{PY@tok@w}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.73,0.73}{##1}}}
\@namedef{PY@tok@c}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cp}{\def\PY@tc##1{\textcolor[rgb]{0.61,0.40,0.00}{##1}}}
\@namedef{PY@tok@k}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kt}{\def\PY@tc##1{\textcolor[rgb]{0.69,0.00,0.25}{##1}}}
\@namedef{PY@tok@o}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ow}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@nb}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nf}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@ne}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.80,0.25,0.22}{##1}}}
\@namedef{PY@tok@nv}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@no}{\def\PY@tc##1{\textcolor[rgb]{0.53,0.00,0.00}{##1}}}
\@namedef{PY@tok@nl}{\def\PY@tc##1{\textcolor[rgb]{0.46,0.46,0.00}{##1}}}
\@namedef{PY@tok@ni}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@na}{\def\PY@tc##1{\textcolor[rgb]{0.41,0.47,0.13}{##1}}}
\@namedef{PY@tok@nt}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nd}{\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@s}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sd}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@si}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@se}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.36,0.12}{##1}}}
\@namedef{PY@tok@sr}{\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@ss}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sx}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@m}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@gh}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@gu}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.50,0.00,0.50}{##1}}}
\@namedef{PY@tok@gd}{\def\PY@tc##1{\textcolor[rgb]{0.63,0.00,0.00}{##1}}}
\@namedef{PY@tok@gi}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.52,0.00}{##1}}}
\@namedef{PY@tok@gr}{\def\PY@tc##1{\textcolor[rgb]{0.89,0.00,0.00}{##1}}}
\@namedef{PY@tok@ge}{\let\PY@it=\textit}
\@namedef{PY@tok@gs}{\let\PY@bf=\textbf}
\@namedef{PY@tok@gp}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@go}{\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@gt}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.27,0.87}{##1}}}
\@namedef{PY@tok@err}{\def\PY@bc##1{{\setlength{\fboxsep}{\string -\fboxrule}\fcolorbox[rgb]{1.00,0.00,0.00}{1,1,1}{\strut ##1}}}}
\@namedef{PY@tok@kc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kd}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kr}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@bp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@fm}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@vc}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vg}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vi}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vm}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sa}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sb}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sc}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@dl}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s2}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sh}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s1}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@mb}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mf}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mh}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mi}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@il}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mo}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ch}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cm}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cpf}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@c1}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cs}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}

\def\PYZbs{\char`\\}
\def\PYZus{\char`\_}
\def\PYZob{\char`\{}
\def\PYZcb{\char`\}}
\def\PYZca{\char`\^}
\def\PYZam{\char`\&}
\def\PYZlt{\char`\<}
\def\PYZgt{\char`\>}
\def\PYZsh{\char`\#}
\def\PYZpc{\char`\%}
\def\PYZdl{\char`\$}
\def\PYZhy{\char`\-}
\def\PYZsq{\char`\'}
\def\PYZdq{\char`\"}
\def\PYZti{\char`\~}
% for compatibility with earlier versions
\def\PYZat{@}
\def\PYZlb{[}
\def\PYZrb{]}
\makeatother


    % For linebreaks inside Verbatim environment from package fancyvrb. 
    \makeatletter
        \newbox\Wrappedcontinuationbox 
        \newbox\Wrappedvisiblespacebox 
        \newcommand*\Wrappedvisiblespace {\textcolor{red}{\textvisiblespace}} 
        \newcommand*\Wrappedcontinuationsymbol {\textcolor{red}{\llap{\tiny$\m@th\hookrightarrow$}}} 
        \newcommand*\Wrappedcontinuationindent {3ex } 
        \newcommand*\Wrappedafterbreak {\kern\Wrappedcontinuationindent\copy\Wrappedcontinuationbox} 
        % Take advantage of the already applied Pygments mark-up to insert 
        % potential linebreaks for TeX processing. 
        %        {, <, #, %, $, ' and ": go to next line. 
        %        _, }, ^, &, >, - and ~: stay at end of broken line. 
        % Use of \textquotesingle for straight quote. 
        \newcommand*\Wrappedbreaksatspecials {% 
            \def\PYGZus{\discretionary{\char`\_}{\Wrappedafterbreak}{\char`\_}}% 
            \def\PYGZob{\discretionary{}{\Wrappedafterbreak\char`\{}{\char`\{}}% 
            \def\PYGZcb{\discretionary{\char`\}}{\Wrappedafterbreak}{\char`\}}}% 
            \def\PYGZca{\discretionary{\char`\^}{\Wrappedafterbreak}{\char`\^}}% 
            \def\PYGZam{\discretionary{\char`\&}{\Wrappedafterbreak}{\char`\&}}% 
            \def\PYGZlt{\discretionary{}{\Wrappedafterbreak\char`\<}{\char`\<}}% 
            \def\PYGZgt{\discretionary{\char`\>}{\Wrappedafterbreak}{\char`\>}}% 
            \def\PYGZsh{\discretionary{}{\Wrappedafterbreak\char`\#}{\char`\#}}% 
            \def\PYGZpc{\discretionary{}{\Wrappedafterbreak\char`\%}{\char`\%}}% 
            \def\PYGZdl{\discretionary{}{\Wrappedafterbreak\char`\$}{\char`\$}}% 
            \def\PYGZhy{\discretionary{\char`\-}{\Wrappedafterbreak}{\char`\-}}% 
            \def\PYGZsq{\discretionary{}{\Wrappedafterbreak\textquotesingle}{\textquotesingle}}% 
            \def\PYGZdq{\discretionary{}{\Wrappedafterbreak\char`\"}{\char`\"}}% 
            \def\PYGZti{\discretionary{\char`\~}{\Wrappedafterbreak}{\char`\~}}% 
        } 
        % Some characters . , ; ? ! / are not pygmentized. 
        % This macro makes them "active" and they will insert potential linebreaks 
        \newcommand*\Wrappedbreaksatpunct {% 
            \lccode`\~`\.\lowercase{\def~}{\discretionary{\hbox{\char`\.}}{\Wrappedafterbreak}{\hbox{\char`\.}}}% 
            \lccode`\~`\,\lowercase{\def~}{\discretionary{\hbox{\char`\,}}{\Wrappedafterbreak}{\hbox{\char`\,}}}% 
            \lccode`\~`\;\lowercase{\def~}{\discretionary{\hbox{\char`\;}}{\Wrappedafterbreak}{\hbox{\char`\;}}}% 
            \lccode`\~`\:\lowercase{\def~}{\discretionary{\hbox{\char`\:}}{\Wrappedafterbreak}{\hbox{\char`\:}}}% 
            \lccode`\~`\?\lowercase{\def~}{\discretionary{\hbox{\char`\?}}{\Wrappedafterbreak}{\hbox{\char`\?}}}% 
            \lccode`\~`\!\lowercase{\def~}{\discretionary{\hbox{\char`\!}}{\Wrappedafterbreak}{\hbox{\char`\!}}}% 
            \lccode`\~`\/\lowercase{\def~}{\discretionary{\hbox{\char`\/}}{\Wrappedafterbreak}{\hbox{\char`\/}}}% 
            \catcode`\.\active
            \catcode`\,\active 
            \catcode`\;\active
            \catcode`\:\active
            \catcode`\?\active
            \catcode`\!\active
            \catcode`\/\active 
            \lccode`\~`\~ 	
        }
    \makeatother

    \let\OriginalVerbatim=\Verbatim
    \makeatletter
    \renewcommand{\Verbatim}[1][1]{%
        %\parskip\z@skip
        \sbox\Wrappedcontinuationbox {\Wrappedcontinuationsymbol}%
        \sbox\Wrappedvisiblespacebox {\FV@SetupFont\Wrappedvisiblespace}%
        \def\FancyVerbFormatLine ##1{\hsize\linewidth
            \vtop{\raggedright\hyphenpenalty\z@\exhyphenpenalty\z@
                \doublehyphendemerits\z@\finalhyphendemerits\z@
                \strut ##1\strut}%
        }%
        % If the linebreak is at a space, the latter will be displayed as visible
        % space at end of first line, and a continuation symbol starts next line.
        % Stretch/shrink are however usually zero for typewriter font.
        \def\FV@Space {%
            \nobreak\hskip\z@ plus\fontdimen3\font minus\fontdimen4\font
            \discretionary{\copy\Wrappedvisiblespacebox}{\Wrappedafterbreak}
            {\kern\fontdimen2\font}%
        }%
        
        % Allow breaks at special characters using \PYG... macros.
        \Wrappedbreaksatspecials
        % Breaks at punctuation characters . , ; ? ! and / need catcode=\active 	
        \OriginalVerbatim[#1,codes*=\Wrappedbreaksatpunct]%
    }
    \makeatother

    % Exact colors from NB
    \definecolor{incolor}{HTML}{303F9F}
    \definecolor{outcolor}{HTML}{D84315}
    \definecolor{cellborder}{HTML}{CFCFCF}
    \definecolor{cellbackground}{HTML}{F7F7F7}
    
    % prompt
    \makeatletter
    \newcommand{\boxspacing}{\kern\kvtcb@left@rule\kern\kvtcb@boxsep}
    \makeatother
    \newcommand{\prompt}[4]{
        {\ttfamily\llap{{\color{#2}[#3]:\hspace{3pt}#4}}\vspace{-\baselineskip}}
    }
    

    
    % Prevent overflowing lines due to hard-to-break entities
    \sloppy 
    % Setup hyperref package
    \hypersetup{
      breaklinks=true,  % so long urls are correctly broken across lines
      colorlinks=true,
      urlcolor=urlcolor,
      linkcolor=linkcolor,
      citecolor=black,
      }
    % Slightly bigger margins than the latex defaults
    \renewcommand{\sfdefault}{phv}
    \renewcommand{\rmfamily}{phv}
    \usepackage[spanish]{babel}
    \geometry{verbose,tmargin=2cm,bmargin=2cm,lmargin=2cm,rmargin=2cm}
    \graphicspath{{img/}}

\begin{document}
\sffamily
 \begin{titlepage}
\centering
     {\includegraphics[scale=0.3]{UCT_logo.png}\par}
\vspace{1cm}
{\bfseries\LARGE Universidad Católica de Temuco\par}
\vspace{1cm}
{\scshape\Large Facultad de Ingeniería \\ Ingeniería civil en Informática \par}
\vspace{3cm}
{\scshape\Huge Instalación, funcionamiento, y características del servicio de dominio de Active Directory \par}
\vspace{3cm}
{\itshape\Large Reporte de mantención de sistemas \par}
\vfill
{   \Large Oscar Mellado Bustos \\
    \large Ayudante de laboratorio \\
     Estudiante de segundo semestre \par}
\vfill
{\Large Noviembre 2022 \par}
\end{titlepage}   

\newpage
\section*{Resumen}

En este reporte se recopila información relativa a la parte teórica detrás de un servicio de
dominio como Active Directory, además se va a explicar paso a paso el método de instalación
gráfico, y configuración del servicio de dominio Active Directory.

\section*{Introducción}
Un sistema de directorios es una estructura jerárquica que almacena información sobre objetos
en la red. Active Directory almacena información acerca de los objetos de una red y facilita
su búsqueda y uso por parte de los usuarios y administradores. Active Directory usa un almacén
de datos estructurado como base para una organización jerárquica lógica de la información del
directorio~\cite{ac}.

\newpage
\section*{Marco teórico}
\subsection*{Controlador de dominio}
Un controlador de dominio es una computadora servidor, que se encarga de responder las peticiones
de autenticación de seguridad dentro de un dominio. El concepto de \textit{dominio} fue
introducido en Windows NT, con el cual se le otorga al usuario acceso de diferentes recursos
informáticos con el uso de una única contraseña y nombre de usuario~\cite{DC}.

En Windows NT 4, un DC sirve como controlador de dominio primario (PDC). Otros, si existen,
suelen ser un controlador de dominio de respaldo (BDC). El PDC se suele designar como el
"primero". El "Gestor de Usuarios para Dominios", es una utilidad para mantener la información
de usuarios/grupos. Utiliza la base de datos de seguridad del dominio en el controlador primario.
El PDC tiene la copia maestra de la base de datos de cuentas de usuario a la que puede acceder
y modificar. Las computadoras BDC tienen una copia de esta base de datos, pero estas copias son
de solo lectura. El PDC replicará la base de datos de su cuenta en los BDC periódicamente~\cite{dbDC}.

En las versiones modernas de Windows, los dominios se han complementado con el uso de los
servicios de Active Directory. En los dominios de Active Directory, ya no se aplica el concepto
de relaciones de controlador de dominio principal y secundario. Los emuladores de PDC contienen
las bases de datos de cuentas y las herramientas administrativas. Como resultado, una gran carga
de trabajo puede ralentizar el sistema. El servicio DNS se puede instalar en una máquina
emuladora secundaria para aliviar la carga de trabajo en el emulador de PDC~\cite{workload}.

\subsection*{Estructura lógica}
\begin{itemize}
    \item \textbf{Objetos:} Las estructuras de Active Directory son arreglos de información 
        sobre objetos. Los objetos se dividen en dos grandes categorías: recursos (por ejemplo,
        impresoras) y responsables de seguridad (cuentas y grupos de usuarios o equipos). A los
        responsables de seguridad se les asignan identificadores de seguridad únicos.

        Cada objeto representa una sola entidad, ya sea un usuario, una computadora, una
        impresora o un grupo, y sus atributos. Ciertos objetos pueden contener otro objetos.
        Un objeto se identifica únicamente por su nombre, y tiene un conjunto de atributos:
        las características, junto con la información que representa el objeto, definidos por 
        un esquema, que también determina los tipos de objetos que se pueden almacenar en un
        servicio de directorio activo.

    \item \textbf{Bosques, árboles, y dominios:} Los objetos dentro de Active Directory se pueden
        ver en varios niveles. El bosque, el árbol, y el dominio son divisiones lógicas en una
        red de Active Directory. Dentro de una implementación, los objetos se agrupan en los
        dominios. Un dominio se define como un grupo lógico de objetos de red (computadoras,
        usuarios, dispositivos) que comparten la misma base de datos de Active Directory.
        Un árbol es una colección de uno o más dominios y árboles de dominio en un namespace
        contiguo y está vinculado en una jerarquía de confianza transitiva. En la parte superior
        de la estructura está el bosque. Un bosque es una colección de árboles que comparten un
        catálogo global común, un esquema de directorio, una estructura lógica y una
        configuración de directorio. El bosque representa el límite de seguridad dentro del
        cual los usuarios, ordenadores, grupos y otros objetos son accesibles~\cite{estructuraAc}.
\end{itemize}
\newpage

\subsection*{Protocolo NetBIOS}
NetBIOS es del acrónimo Network Basic Input/Output System. Proporciona servicios relacionados
con la capa de sesión del modelo OSI\footnote{El modelo de interconexión de sistemas abiertos
(modelo OSI) es un modelo conceptual que proporciona una base común para la coordinación del
desarrollo de normas ISO con el propósito de la interconexión de sistemas.} que permite que 
las aplicaciones en computadoras separadas se comuniquen a través de la red de área local, como
estrictamente una API, NetBIOS no es un protocolo. En las redes modernas, NetBIOS se ejecuta
normalmente sobre el protocolo TCP/IP. Dentro del datagrama va encapsulado.
\newpage

\section*{Instalación y configuración}
La instalación del servicio de directorio activo se va a ser implementada en el sistema 
Windows server 2019, y el cliente va a tener el sistema Windows 10. El proceso de instalación
se debe llevar a cabo en una misma red para que se puedan conectar correctamente el cliente,
y el servidor.

\subsection*{Instalación en el servidor}
Se inicia la instalación cambiando el nombre del equipo del servidor. Para llevar a cabo esa 
configuración, se tiene que abrir el programa ''Administrador del servidor'', e irse a la pestaña
de ''Servidor local'', y hacer doble click en el nombre del equipo. Esto se hace para asignar un 
nombre que sea más fácil de recordar.
\begin{center}
    \includegraphics{1.png}
\end{center}
Se abrirá una ventana con las propiedades del sistema y se hace un click en el botón que dice
cambiar.
\begin{center}
    \includegraphics{2.png}
\end{center}
\newpage
En la entrada de texto se escribe en nuevo nombre de la máquina, y se termina aceptando en todas
las ventanas que se abrieron.
\begin{center}
    \includegraphics{3.png}
\end{center}
El sistema solicitará reiniciar la máquina para aceptar los cambios.

Para instalar el servicio de Active Directory, se tiene que clickear en la pestaña de
''administrar'', y en el menú desplegado se clickea en el apartado de ''Agregar roles y
características''.
\begin{center}
    \includegraphics{4.png}
\end{center}
\newpage
En la primera página se explica que es un asistente para facilitar la instalación de role,
servicios, y características, además de dar alguna recomendaciones, para mantener la seguridad
del servidor. Se pasa a la siguiente página.
\begin{center}
    \includegraphics{5.png}
\end{center}
En esta página se selecciona la opción predeterminada, porque no se va a instalar un servicio 
de escritorio remoto, por lo que se pasa a la siguiente página.
\begin{center}
    \includegraphics{6.png}
\end{center}
\newpage
En esta página con en el entorno de simulación solo hay un servidor en la misma red, no se puede
seleccionar otro, por lo que se cambia a la siguiente página.
\begin{center}
    \includegraphics{7.png}
\end{center}
Dentro de los servicios se selecciona ''Servicios de dominio de Active Directory''. Al clickear
en esa opción se desplegará una ventana para agregar las características del servicio que se
va a instalar.
\begin{center}
    \includegraphics{8.png}
\end{center}
\newpage
En la siguiente página se le da a siguiente, porque no se va a agregar más características al 
servidor.
\begin{center}
    \includegraphics{9.png}
\end{center}
En la página se explica de forma resumida cual es la función de un servicio de dominio de
Active Directory, por lo que se pasa a la siguiente página.
\begin{center}
    \includegraphics{10.png}
\end{center}
\newpage
Se termina confirmando las selecciones de la instalación, también seleccionando la casilla para
que el sistema se reinicie automáticamente al terminar el proceso de instalación, y de
configuración. Cuando la instalación termina se cierra la ventana del asistente de instalación.
\begin{center}
    \includegraphics{11.png}
\end{center}
Después de cerrar el asistente de instalación se tiene que dar click en la bandera al lado
izquierdo del apartado de ''Administrar''. Se desplegará un menú para configurar la
implementación del servicio de directorio activo.
\begin{center}
    \includegraphics{12.png}
\end{center}
\newpage
Se abrirá el ''Asistente para configuración de Servicio de dominio Active Directory''. En las 
opciones que se muestran el la página, se seleccionarán: Agregar un nuevo bosque, y en la
entrada de texto, se ingresará el nombre de la raíz del dominio. Una vez seleccionada la opción
y asignado el dominio se pasa a la siguiente página.
\begin{center}
    \includegraphics{13.png}
\end{center}
En esta página se le asigna una contraseña de restauración al servicio de directorio.
\begin{center}
    \includegraphics{14.png}
\end{center}
\newpage
En esta página se confirma el nombre del dominio, por lo que se le da a siguiente.
\begin{center}
    \includegraphics{15.png}
\end{center}
En las siguientes dos páginas se revisan las configuraciones y las rutas de instalación, ya sea
la de la base de datos, o la de registros, por lo que en las dos se clickea en el botón para
ir a la siguiente página.

Se comprueban los requisitos para la instalación, y se termina de instalar el servicio de
directorio activo. Al terminar la instalación se reiniciará la máquina automáticamente. Al
volver a iniciar el sistema el servicio de dominio de Active Directory estará completamente
instalado, y listo para recibir peticiones de los usuarios.
\begin{center}
    \includegraphics{16.png}
\end{center}
\newpage
\subsection*{Instalación en el cliente}
Desde el cliente se comprueba la conexión al servidor utilizando el comando ping.
\begin{center}
    \includegraphics{17.png}
\end{center}
Una vez que se comprueba que el cliente tiene conexión al servidor, se abren las propiedades del
equipo.
\begin{center}
    \includegraphics{18.png}
\end{center}
\newpage
Dentro de las propiedades del equipo se hace click en el apartado de ''Configuración avanzada
del sistema''
\begin{center}
    \includegraphics{19.png}
\end{center}
En la ventana que se despliega se va a la pestaña de ''Nombre del equipo'', y se clickea en
el botón que dice cambiar.
\begin{center}
    \includegraphics{20.png}
\end{center}
\newpage
En el nombre del equipo ingresa un nombre más recordable e identificativo de la máquina, y en el
dominio se escribe el mismo dominio del servidor.
\begin{center}
    \includegraphics{21.png}
\end{center}
Cuando se aceptan las configuraciones, se despliega otra ventana consultando las credenciales del
servicio de directorio activo. Al terminar la conexión se le solicitará reiniciar la computadora
para que se apliquen los cambios.
\begin{center}
    \includegraphics{22.png}
\end{center}
\newpage
Al volver a iniciar el sistema sale la opción de autenticarse con otro usuario, osea con un
usuario que esté registrado en el servicio de directorio activo.
\begin{center}
    \includegraphics{23.png}
\end{center}
\newpage
\section*{Pruebas de funcionamiento}
Para crear un usuario se tiene que ir el apartado de herramientas, en la opción ''Usuarios
y equipos de Active Directory''.
\begin{center}
    \includegraphics{24.png}
\end{center}
En esta ventana se hace click en el nombre del dominio y en la parte de arriba se hace click en 
el botón de añadir usuarios.
\begin{center}
    \includegraphics{25.png}
\end{center}
\newpage
En esta ventana se ingresan los datos del nuevo usuario. Se ingresa el nombre de usuario, el
apellido, y el nombre con el que se va a iniciar sesión, se le da a siguiente.
\begin{center}
    \includegraphics{26.png}
\end{center}
En la siguiente página se le asigna una contraseña al usuario, se pasa a la siguiente página
para finalizar la creación del usuario.
\begin{center}
    \includegraphics{27.png}
\end{center}
\newpage
Con el usuario creado se puede autenticar en el cliente.
\begin{center}
    \includegraphics{23.png}
\end{center}
\newpage
\section*{Conclusiones}
Teniendo en cuenta el método de instalación basado en interfaz gráfica, su funcionamiento, y
características. Se concluye que el método de instalación por interfaz gráfica lo hace bastante
sencillo de llevar a cabo, sin embargo, al ser tan sencillo, el administrador no va a tener en 
conocimiento pleno de las instrucciones que realmente se ejecutan para instalar el servicio de 
dominio de Active Directory. En cuanto a su funcionamiento, y características se concluye que
la centralización de los usuarios facilita la administración de los mismo, por lo que el
servidor controla totalmente los computadores de los usuarios, tanto como el flujo de los
recursos entre el usuario y el servidor, por otro lado, la API NetBIOS hace vulnerable la
transmisión de datos, dejando inseguro el servidor~\cite{netbiosvuln}.
\newpage

\bibliographystyle{apalike}
\bibliography{referencias}
    
\end{document}
