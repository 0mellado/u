\documentclass[11pt]{article}

    \usepackage[breakable]{tcolorbox}
    \usepackage{parskip} % Stop auto-indenting (to mimic markdown behaviour)
    

    % Basic figure setup, for now with no caption control since it's done
    % automatically by Pandoc (which extracts ![](path) syntax from Markdown).
    \usepackage{graphicx}
    % Maintain compatibility with old templates. Remove in nbconvert 6.0
    \let\Oldincludegraphics\includegraphics
    % Ensure that by default, figures have no caption (until we provide a
    % proper Figure object with a Caption API and a way to capture that
    % in the conversion process - todo).
    \usepackage{caption}
    \DeclareCaptionFormat{nocaption}{}
    \captionsetup{format=nocaption,aboveskip=0pt,belowskip=0pt}

    \usepackage{float}
    \floatplacement{figure}{H} % forces figures to be placed at the correct location
    \usepackage{xcolor} % Allow colors to be defined
    \usepackage{enumerate} % Needed for markdown enumerations to work
    \usepackage[a4paper]{geometry} % Used to adjust the document margins
    \usepackage{amsmath} % Equations
    \usepackage{amssymb} % Equations
    \usepackage{textcomp} % defines textquotesingle
    % Hack from http://tex.stackexchange.com/a/47451/13684:
    \AtBeginDocument{%
        \def\PYZsq{\textquotesingle}% Upright quotes in Pygmentized code
    }
    \usepackage{upquote} % Upright quotes for verbatim code
    \usepackage{eurosym} % defines \euro

    \usepackage{iftex}
    \ifPDFTeX
        \usepackage[T1]{fontenc}
        \IfFileExists{alphabeta.sty}{
              \usepackage{alphabeta}
          }{
              \usepackage[mathletters]{ucs}
              \usepackage[utf8x]{inputenc}
          }
    \else
        \usepackage{fontspec}
        \usepackage{unicode-math}
    \fi

    \usepackage{fancyvrb} % verbatim replacement that allows latex
    \usepackage{grffile} % extends the file name processing of package graphics 
                         % to support a larger range
    \makeatletter % fix for old versions of grffile with XeLaTeX
    \@ifpackagelater{grffile}{2019/11/01}
    {
      % Do nothing on new versions
    }
    {
      \def\Gread@@xetex#1{%
        \IfFileExists{"\Gin@base".bb}%
        {\Gread@eps{\Gin@base.bb}}%
        {\Gread@@xetex@aux#1}%
      }
    }
    \makeatother
    \usepackage[Export]{adjustbox} % Used to constrain images to a maximum size
    \adjustboxset{max size={0.9\linewidth}{0.9\paperheight}}

    % The hyperref package gives us a pdf with properly built
    % internal navigation ('pdf bookmarks' for the table of contents,
    % internal cross-reference links, web links for URLs, etc.)
    \usepackage{hyperref}
    % The default LaTeX title has an obnoxious amount of whitespace. By default,
    % titling removes some of it. It also provides customization options.
    \usepackage{titling}
    \usepackage{longtable} % longtable support required by pandoc >1.10
    \usepackage{booktabs}  % table support for pandoc > 1.12.2
    \usepackage{array}     % table support for pandoc >= 2.11.3
    \usepackage{calc}      % table minipage width calculation for pandoc >= 2.11.1
    \usepackage[inline]{enumitem} % IRkernel/repr support (it uses the enumerate* environment)
    \usepackage[normalem]{ulem} % ulem is needed to support strikethroughs (\sout)
                                % normalem makes italics be italics, not underlines
    \usepackage{mathrsfs}
    

    
    % Colors for the hyperref package
    \definecolor{urlcolor}{rgb}{0,.145,.698}
    \definecolor{linkcolor}{rgb}{.71,0.21,0.01}
    \definecolor{citecolor}{rgb}{.12,.54,.11}

    % ANSI colors
    \definecolor{ansi-black}{HTML}{3E424D}
    \definecolor{ansi-black-intense}{HTML}{282C36}
    \definecolor{ansi-red}{HTML}{E75C58}
    \definecolor{ansi-red-intense}{HTML}{B22B31}
    \definecolor{ansi-green}{HTML}{00A250}
    \definecolor{ansi-green-intense}{HTML}{007427}
    \definecolor{ansi-yellow}{HTML}{DDB62B}
    \definecolor{ansi-yellow-intense}{HTML}{B27D12}
    \definecolor{ansi-blue}{HTML}{208FFB}
    \definecolor{ansi-blue-intense}{HTML}{0065CA}
    \definecolor{ansi-magenta}{HTML}{D160C4}
    \definecolor{ansi-magenta-intense}{HTML}{A03196}
    \definecolor{ansi-cyan}{HTML}{60C6C8}
    \definecolor{ansi-cyan-intense}{HTML}{258F8F}
    \definecolor{ansi-white}{HTML}{C5C1B4}
    \definecolor{ansi-white-intense}{HTML}{A1A6B2}
    \definecolor{ansi-default-inverse-fg}{HTML}{FFFFFF}
    \definecolor{ansi-default-inverse-bg}{HTML}{000000}

    % common color for the border for error outputs.
    \definecolor{outerrorbackground}{HTML}{FFDFDF}

    % commands and environments needed by pandoc snippets
    % extracted from the output of `pandoc -s`
    \providecommand{\tightlist}{%
      \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
    \DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
    % Add ',fontsize=\small' for more characters per line
    \newenvironment{Shaded}{}{}
    \newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.56,0.13,0.00}{{#1}}}
    \newcommand{\DecValTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\FloatTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\CharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\StringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\CommentTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textit{{#1}}}}
    \newcommand{\OtherTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{{#1}}}
    \newcommand{\AlertTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.02,0.16,0.49}{{#1}}}
    \newcommand{\RegionMarkerTok}[1]{{#1}}
    \newcommand{\ErrorTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\NormalTok}[1]{{#1}}
    
    % Additional commands for more recent versions of Pandoc
    \newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.53,0.00,0.00}{{#1}}}
    \newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.73,0.40,0.53}{{#1}}}
    \newcommand{\ImportTok}[1]{{#1}}
    \newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.73,0.13,0.13}{\textit{{#1}}}}
    \newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\VariableTok}[1]{\textcolor[rgb]{0.10,0.09,0.49}{{#1}}}
    \newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.40,0.40,0.40}{{#1}}}
    \newcommand{\BuiltInTok}[1]{{#1}}
    \newcommand{\ExtensionTok}[1]{{#1}}
    \newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.74,0.48,0.00}{{#1}}}
    \newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.49,0.56,0.16}{{#1}}}
    \newcommand{\InformationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\WarningTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    
    
    % Define a nice break command that doesn't care if a line doesn't already
    % exist.
    \def\br{\hspace*{\fill} \\* }
    % Math Jax compatibility definitions
    \def\gt{>}
    \def\lt{<}
    \let\Oldtex\TeX
    \let\Oldlatex\LaTeX
    \renewcommand{\TeX}{\textrm{\Oldtex}}
    \renewcommand{\LaTeX}{\textrm{\Oldlatex}}
    
    
% Pygments definitions
\makeatletter
\def\PY@reset{\let\PY@it=\relax \let\PY@bf=\relax%
    \let\PY@ul=\relax \let\PY@tc=\relax%
    \let\PY@bc=\relax \let\PY@ff=\relax}
\def\PY@tok#1{\csname PY@tok@#1\endcsname}
\def\PY@toks#1+{\ifx\relax#1\empty\else%
    \PY@tok{#1}\expandafter\PY@toks\fi}
\def\PY@do#1{\PY@bc{\PY@tc{\PY@ul{%
    \PY@it{\PY@bf{\PY@ff{#1}}}}}}}
\def\PY#1#2{\PY@reset\PY@toks#1+\relax+\PY@do{#2}}

\@namedef{PY@tok@w}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.73,0.73}{##1}}}
\@namedef{PY@tok@c}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cp}{\def\PY@tc##1{\textcolor[rgb]{0.61,0.40,0.00}{##1}}}
\@namedef{PY@tok@k}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kt}{\def\PY@tc##1{\textcolor[rgb]{0.69,0.00,0.25}{##1}}}
\@namedef{PY@tok@o}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ow}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@nb}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nf}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@ne}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.80,0.25,0.22}{##1}}}
\@namedef{PY@tok@nv}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@no}{\def\PY@tc##1{\textcolor[rgb]{0.53,0.00,0.00}{##1}}}
\@namedef{PY@tok@nl}{\def\PY@tc##1{\textcolor[rgb]{0.46,0.46,0.00}{##1}}}
\@namedef{PY@tok@ni}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@na}{\def\PY@tc##1{\textcolor[rgb]{0.41,0.47,0.13}{##1}}}
\@namedef{PY@tok@nt}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nd}{\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@s}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sd}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@si}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@se}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.36,0.12}{##1}}}
\@namedef{PY@tok@sr}{\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@ss}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sx}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@m}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@gh}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@gu}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.50,0.00,0.50}{##1}}}
\@namedef{PY@tok@gd}{\def\PY@tc##1{\textcolor[rgb]{0.63,0.00,0.00}{##1}}}
\@namedef{PY@tok@gi}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.52,0.00}{##1}}}
\@namedef{PY@tok@gr}{\def\PY@tc##1{\textcolor[rgb]{0.89,0.00,0.00}{##1}}}
\@namedef{PY@tok@ge}{\let\PY@it=\textit}
\@namedef{PY@tok@gs}{\let\PY@bf=\textbf}
\@namedef{PY@tok@gp}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@go}{\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@gt}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.27,0.87}{##1}}}
\@namedef{PY@tok@err}{\def\PY@bc##1{{\setlength{\fboxsep}{\string -\fboxrule}\fcolorbox[rgb]{1.00,0.00,0.00}{1,1,1}{\strut ##1}}}}
\@namedef{PY@tok@kc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kd}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kr}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@bp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@fm}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@vc}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vg}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vi}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vm}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sa}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sb}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sc}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@dl}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s2}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sh}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s1}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@mb}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mf}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mh}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mi}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@il}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mo}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ch}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cm}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cpf}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@c1}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cs}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}

\def\PYZbs{\char`\\}
\def\PYZus{\char`\_}
\def\PYZob{\char`\{}
\def\PYZcb{\char`\}}
\def\PYZca{\char`\^}
\def\PYZam{\char`\&}
\def\PYZlt{\char`\<}
\def\PYZgt{\char`\>}
\def\PYZsh{\char`\#}
\def\PYZpc{\char`\%}
\def\PYZdl{\char`\$}
\def\PYZhy{\char`\-}
\def\PYZsq{\char`\'}
\def\PYZdq{\char`\"}
\def\PYZti{\char`\~}
% for compatibility with earlier versions
\def\PYZat{@}
\def\PYZlb{[}
\def\PYZrb{]}
\makeatother


    % For linebreaks inside Verbatim environment from package fancyvrb. 
    \makeatletter
        \newbox\Wrappedcontinuationbox 
        \newbox\Wrappedvisiblespacebox 
        \newcommand*\Wrappedvisiblespace {\textcolor{red}{\textvisiblespace}} 
        \newcommand*\Wrappedcontinuationsymbol {\textcolor{red}{\llap{\tiny$\m@th\hookrightarrow$}}} 
        \newcommand*\Wrappedcontinuationindent {3ex } 
        \newcommand*\Wrappedafterbreak {\kern\Wrappedcontinuationindent\copy\Wrappedcontinuationbox} 
        % Take advantage of the already applied Pygments mark-up to insert 
        % potential linebreaks for TeX processing. 
        %        {, <, #, %, $, ' and ": go to next line. 
        %        _, }, ^, &, >, - and ~: stay at end of broken line. 
        % Use of \textquotesingle for straight quote. 
        \newcommand*\Wrappedbreaksatspecials {% 
            \def\PYGZus{\discretionary{\char`\_}{\Wrappedafterbreak}{\char`\_}}% 
            \def\PYGZob{\discretionary{}{\Wrappedafterbreak\char`\{}{\char`\{}}% 
            \def\PYGZcb{\discretionary{\char`\}}{\Wrappedafterbreak}{\char`\}}}% 
            \def\PYGZca{\discretionary{\char`\^}{\Wrappedafterbreak}{\char`\^}}% 
            \def\PYGZam{\discretionary{\char`\&}{\Wrappedafterbreak}{\char`\&}}% 
            \def\PYGZlt{\discretionary{}{\Wrappedafterbreak\char`\<}{\char`\<}}% 
            \def\PYGZgt{\discretionary{\char`\>}{\Wrappedafterbreak}{\char`\>}}% 
            \def\PYGZsh{\discretionary{}{\Wrappedafterbreak\char`\#}{\char`\#}}% 
            \def\PYGZpc{\discretionary{}{\Wrappedafterbreak\char`\%}{\char`\%}}% 
            \def\PYGZdl{\discretionary{}{\Wrappedafterbreak\char`\$}{\char`\$}}% 
            \def\PYGZhy{\discretionary{\char`\-}{\Wrappedafterbreak}{\char`\-}}% 
            \def\PYGZsq{\discretionary{}{\Wrappedafterbreak\textquotesingle}{\textquotesingle}}% 
            \def\PYGZdq{\discretionary{}{\Wrappedafterbreak\char`\"}{\char`\"}}% 
            \def\PYGZti{\discretionary{\char`\~}{\Wrappedafterbreak}{\char`\~}}% 
        } 
        % Some characters . , ; ? ! / are not pygmentized. 
        % This macro makes them "active" and they will insert potential linebreaks 
        \newcommand*\Wrappedbreaksatpunct {% 
            \lccode`\~`\.\lowercase{\def~}{\discretionary{\hbox{\char`\.}}{\Wrappedafterbreak}{\hbox{\char`\.}}}% 
            \lccode`\~`\,\lowercase{\def~}{\discretionary{\hbox{\char`\,}}{\Wrappedafterbreak}{\hbox{\char`\,}}}% 
            \lccode`\~`\;\lowercase{\def~}{\discretionary{\hbox{\char`\;}}{\Wrappedafterbreak}{\hbox{\char`\;}}}% 
            \lccode`\~`\:\lowercase{\def~}{\discretionary{\hbox{\char`\:}}{\Wrappedafterbreak}{\hbox{\char`\:}}}% 
            \lccode`\~`\?\lowercase{\def~}{\discretionary{\hbox{\char`\?}}{\Wrappedafterbreak}{\hbox{\char`\?}}}% 
            \lccode`\~`\!\lowercase{\def~}{\discretionary{\hbox{\char`\!}}{\Wrappedafterbreak}{\hbox{\char`\!}}}% 
            \lccode`\~`\/\lowercase{\def~}{\discretionary{\hbox{\char`\/}}{\Wrappedafterbreak}{\hbox{\char`\/}}}% 
            \catcode`\.\active
            \catcode`\,\active 
            \catcode`\;\active
            \catcode`\:\active
            \catcode`\?\active
            \catcode`\!\active
            \catcode`\/\active 
            \lccode`\~`\~ 	
        }
    \makeatother

    \let\OriginalVerbatim=\Verbatim
    \makeatletter
    \renewcommand{\Verbatim}[1][1]{%
        %\parskip\z@skip
        \sbox\Wrappedcontinuationbox {\Wrappedcontinuationsymbol}%
        \sbox\Wrappedvisiblespacebox {\FV@SetupFont\Wrappedvisiblespace}%
        \def\FancyVerbFormatLine ##1{\hsize\linewidth
            \vtop{\raggedright\hyphenpenalty\z@\exhyphenpenalty\z@
                \doublehyphendemerits\z@\finalhyphendemerits\z@
                \strut ##1\strut}%
        }%
        % If the linebreak is at a space, the latter will be displayed as visible
        % space at end of first line, and a continuation symbol starts next line.
        % Stretch/shrink are however usually zero for typewriter font.
        \def\FV@Space {%
            \nobreak\hskip\z@ plus\fontdimen3\font minus\fontdimen4\font
            \discretionary{\copy\Wrappedvisiblespacebox}{\Wrappedafterbreak}
            {\kern\fontdimen2\font}%
        }%
        
        % Allow breaks at special characters using \PYG... macros.
        \Wrappedbreaksatspecials
        % Breaks at punctuation characters . , ; ? ! and / need catcode=\active 	
        \OriginalVerbatim[#1,codes*=\Wrappedbreaksatpunct]%
    }
    \makeatother

    % Exact colors from NB
    \definecolor{incolor}{HTML}{303F9F}
    \definecolor{outcolor}{HTML}{D84315}
    \definecolor{cellborder}{HTML}{CFCFCF}
    \definecolor{cellbackground}{HTML}{F7F7F7}
    
    % prompt
    \makeatletter
    \newcommand{\boxspacing}{\kern\kvtcb@left@rule\kern\kvtcb@boxsep}
    \makeatother
    \newcommand{\prompt}[4]{
        {\ttfamily\llap{{\color{#2}[#3]:\hspace{3pt}#4}}\vspace{-\baselineskip}}
    }
    

    
    % Prevent overflowing lines due to hard-to-break entities
    \sloppy 
    % Setup hyperref package
    \hypersetup{
      breaklinks=true,  % so long urls are correctly broken across lines
      colorlinks=true,
      urlcolor=urlcolor,
      linkcolor=linkcolor,
      citecolor=black,
      }
    % Slightly bigger margins than the latex defaults
    \renewcommand{\sfdefault}{phv}
    \renewcommand{\rmfamily}{phv}
    \usepackage[spanish]{babel}
    \geometry{verbose,tmargin=2cm,bmargin=2cm,lmargin=2cm,rmargin=2cm}
    \graphicspath{{img/}}

\begin{document}
\sffamily
 \begin{titlepage}
\centering
     {\includegraphics[scale=0.3]{UCT_logo.png}\par}
\vspace{1cm}
{\bfseries\LARGE Universidad Católica de Temuco\par}
\vspace{1cm}
{\scshape\Large Facultad de Ingeniería \\ Ingeniería civil en Informática \par}
\vspace{3cm}
{\scshape\Huge Instalación, funcionamiento, y características del servicio de directorio LDAP\par}
\vspace{3cm}
{\itshape\Large Reporte de mantención de sistemas \par}
\vfill
{   \Large Oscar Mellado Bustos \\
    \large Ayudante de laboratorio \\
     Estudiante de segundo semestre \par}
\vfill
{\Large Noviembre 2022 \par}
\end{titlepage}   

\newpage
\section*{Resumen}
En el presente reporte se explica de manéra teórica el funcionamiento,
además de el método de instalación y configuración basado en terminal
del servicio de directorio LDAP.

\section*{Introducción}
LDAP o protocolo ligero de acceso a directorios (en ingles: 
\textbf{L}ightweight \textbf{D}irectory \textbf{A}ccess \textbf{P}rotocol)
es un protocolo de aplicación estándar, abierto e independiente del
proveedor, para acceder y mantener servicios de información de directorio
distribuidos a través del protocolo IP~\cite{ldap}. Los servicios de 
directorio desempeñan un papel importante en el desarrollo de 
aplicaciones de intranet e Internet al permitir compartir información 
sobre usuarios, sistemas, redes, servicios y aplicaciones en toda la
red ~\cite{ldap2}.

\newpage
\section*{Marco teórico}
\subsection*{Network file system}
Network file system es un protocolo de sistema de archivos distribuidos
originalmente desarrollado por Sun Microsystems en el año 1984~\cite{nfs},
permitiendo que usuarios desde un computador cliente, acceda a archivos
a través de una red Informática, de manera muy similar a como se accede al
almacenamiento local.
\begin{itemize}
    \item El protocolo NFS está basado en lógica de cliente/servidor, en
        donde, una máquina llamada servidor provee los servicios a los
        diferentes clientes que se pueden conectar de manera remota al
        servidor.

    \item Los datos de todos los usuarios se encuentran centralizados
        en un mismo equipo servidor en el cual los usuarios pueden
        acceder a sus recursos, esto hace que en las estaciones de
        trabajo locales se utilizen menos almacenamiento en disco, porque
        la información no se replica en los clientes.

    \item También se pueden compartir a través de la red dispositivos de
        almacenamiento como disqueteras, CD-ROM y unidades ZIP. Esto
        puede reducir la inversión en dichos dispositivos y mejorar el
        aprovechamiento del hardware existente en la organización.
\end{itemize}

Todas las operaciones sobre ficheros son síncronas. Esto significa que
la operación solo retorna cuando el servidor ha completado todo el
trabajo asociado para esa operación. En caso de una solicitud de
escritura, el servidor escribirá físicamente los datos en el disco, y si
es necesario, actualizará la estructura de directorios, antes de
devolver una respuesta al cliente. Esto garantiza la integridad de los
ficheros.

\subsubsection*{Cliente NFS}
El cliente simula las funcionalidades del sistema de archivos de UNIX,
integrado directamente en el kernel. Se encarga de controlar las
peticiones del VFS al servidor. Manda los bloques o archivos desde el
servidor y hasta el servidor. Cuando es posible almacena en la caché
local los bloques.

\subsubsection*{Servidor NFS}
El servidor NFS es parte del núcleo Linux, en los núcleos que Debian
provee está compilado como un módulo de núcleo. Su interfaz está
definida en el RFC 1813.

Se encarga de recibir las peticiones, que pueden ser similares a las de
modelo de archivos plano o pueden simular las del sistema de UNIX.

El servidor también ofrece servicios de montaje, de control de
autenticación y acceso y una caché.

\subsection*{OpenLDAP}
OpenLDAP es una implementación libre y de código abierto del protocolo 
Lightweight Directory Access Protocol (LDAP) desarrollada por el proyecto OpenLDAP.

Muchas distribuciones GNU/Linux incluyen el software OpenLDAP para el 
soporte LDAP. Este software también corre en plataformas BSD, AIX, HP-UX,
Mac OS X, Solaris, Microsoft Windows (NT y derivados, incluyendo 2000,
XP, Vista), y z/OS.
\newpage
\subsubsection*{Componentes}
OpenLDAP posee tres componentes principales:
\begin{itemize}
    \item slapd -Demonio del servidor y herramientas
    \item Bibliotecas que implementan el protocolo LDAP
    \item Programas cliente: ldapsearch, ldapadd, ldapdelete, entre otros
\end{itemize}

\subsubsection*{Backends}
Históricamente la arquitectura del servidor OpenLDAP (slapd, Standalone
LDAP Daemon) fue dividida entre una sección frontal que maneja las
conexiones de redes y el procesamiento del protocolo, y una base de
datos dorsal o de segundo plano (backend) que trata únicamente con el
almacenamiento de datos. La arquitectura es modular y una variedad de
backends está disponible para interactuar con otras tecnologías, no solo
bases de datos tradicionales.

\subsubsection*{Overlays}
Generalmente una petición LDAP es recibida por el frontend, decodificada
y luego transferida a un backend para procesamiento. Cuando el backend
completa la petición, devuelve un resultado al frontend, quien luego
envía el resultado al cliente LDAP. Un overlay es una pieza de código
que puede ser insertada entre el frontend y el backend. Es entonces
capaz de interceptar peticiones y lanzar otras acciones en ellas antes
de que el backend las reciba, y puede también actuar sobre los
resultados del backend antes de que estos alcancen el frontend. Overlays
tiene acceso completo a las interfaces de programación (APIs) internas
del servidor slapd, y por tanto pueden invocar cualquier llamada que
podrían realizar el frontend u otros backends. Múltiples overlays
pueden ser usados a la vez, formando una pila de módulos entre el
frontend y el backend.

\newpage
\section*{Instalación y configuración}
\subsection*{Instalación en el servidor}
La instalación parte desde un sistema GNU/Linux con el kernel 
5.15.0-53-generic, con la distribución Ubuntu server 22.04. El proceso 
se inícia después de actualizar todos los paquetes del sistema.

En la tabla de hosts se agrega la IP del servidor con el nombre del host
y el dominio.

Se agrega de la siguiente forma:
\vspace{0.3cm}
\begin{center}
    <address>\hspace{1cm}<hostname>.<dominio>\hspace{1cm}<hostname>
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{1.png}
\end{center}
Con el siguiente comando se descarga e instala el servicio slapd y las
herramientas del LDAP.
\vspace{0.3cm}
\begin{center}
    sudo apt install slapd ldap-utils
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics[scale=1.1]{2.png}
\end{center}
\newpage
Durante la instalación del servicio y las herramientas se va a configurar
la contraseña del administrador del servicio de directorios.
\begin{center}
    \includegraphics{3.png}
\end{center}
Se va a crear un archivo \textbf{content.ldif} con el siguiente contenido
para agregar un grupo, y un usuario al servicio de directorio.
\vspace{0.3cm}
\begin{center}
    vim content.ldif
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{4.png}
\end{center}
\newpage
El contenido se añade a LDAP con el siguiente comando:
\vspace{0.3cm}
\begin{center}
    ldapadd -x -D cn=admin,dc=ldap,dc=cl -W -f content.ldif
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{5.png}
\end{center}
Con el siguiente comando se instala el servicio NFS en el lado del 
servidor:
\vspace{0.3cm}
\begin{center}
    sudo apt install nfs-kernel-server
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{6.png}
\end{center}
Se crea el directorio donde van a estar los directorios de trabajo de los
usuarios dentro de LDAP.
\vspace{0.3cm}
\begin{center}
    sudo mkdir /home/users
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{7.png}
\end{center}
En el archivo /etc/exports se agrega el siguiente contenido, para asignar
permisos de escritura y lectura, la sincronía, se desactiva la
verificación de los sub arboles.
\vspace{0.3cm}
\begin{center}
    /home/users     *(rw,sync,no\_subtree\_check)
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{8.png}
\end{center}
Se reinicia el servicio NFS.
\begin{center}
    \includegraphics{9.png}
\end{center}
Con el siguiente comando se comprueba que el servicio de NFS este 
funcionando correctamente.
\vspace{0.3cm}
\begin{center}
    showmount -e <address>
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{10.png}
\end{center}
Dentro de el directorio /home/users se crea un sub directorio con el
nombre de uno de los usuarios.
\vspace{0.3cm}
\begin{center}
    cd /home/users \\
    sudo mkdir <username>
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{11.png}
\end{center}
Con el siguiente comando se instala y configura el cliente del servicio
slapd. Esto se hace para probar dentro del servidor el servicio LDAP.
\vspace{0.3cm}
\begin{center}
    sudo apt install libnss-ldap
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{12.png}
\end{center}
Durante el proceso de instalación del cliente, se va a solicitar la URI
del servicio de directorio.
\begin{center}
    \includegraphics[scale=0.9]{13.png}
\end{center}
En la siguiente página se solicita solo el dominio del servicio.
\begin{center}
    \includegraphics[scale=0.9]{14.png}
\end{center}
Las siguiente configuraciones son las predeterminadas.

En esta pantalla se vuelve a ingresar el dominio, además de el usuario 
administrador.
\begin{center}
    \includegraphics{15.png}
\end{center}
Se por último ingresa la contraseña del administrador.
\begin{center}
    \includegraphics{16.png}
\end{center}
Se edita el archivo /etc/nsswitch.conf para que quede con el siguiente
contenido.
\vspace{0.3cm}
\begin{center}
    sudo vim /etc/nsswitch.conf
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{17.png}
\end{center}
Se cambia el propietario y el grupo del directorio del usuario.
\begin{center}
    \includegraphics{18.png}
\end{center}
\newpage
El usuario configurado funciona correctamente del lado del mismo servidor.
\begin{center}
    \includegraphics{19.png}
\end{center}
\subsection*{Instalación en el cliente}
La instalación del cliente de lleva a cabo en una máquina GNU/Linux con 
el kernel 5.15.0-53-generic, con la distribución Ubuntu desktop 22.04. El
sistema tiene todos sus paquetes actualizados.

La tabla de hosts se modifica añadiendo, la IP con el dominio del
servidor, y la IP con el dominio del cliente.
\vspace{0.3cm}
\begin{center}
    sudo vim /etc/hosts
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{20.png}
\end{center}
Se instala el cliente del servicio NFS.
\vspace{0.3cm}
\begin{center}
    sudo apt install nfs-common
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{21.png}
\end{center}
\newpage
Después de instalar el cliente del servicio NFS se puede utilizar el 
comando \textbf{showmount} para comprobar el directorio del servidor
compartido a través de la red.
\begin{center}
    \includegraphics{22.png}
\end{center}
Dentro del cliente se crea el directorio /home/users.
\begin{center}
    \includegraphics{23.png}
\end{center}
El directorio del servidor que se comparte por red, se monta en el
directorio creado en el cliente.
\vspace{0.3cm}
\begin{center}
    sudo mount <address>:/home/users /home/users
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics{24.png}
\end{center}
Se instala el cliente del servicio LDAP.
\begin{center}
    \includegraphics{25.png}
\end{center}
Durante la instalación solicita los mismo datos y configuraciones que la
instalación del lado del cliente, osea ingresar el dominio junto con el
nombre del host del servidor, además del usuarios administrador y su 
contraseña para realizar la conexión.

Por último se edita el archivo /etc/nsswitch.conf de la misma forma que
en el servidor.
\begin{center}
    \includegraphics{26.png}
\end{center}
\newpage
\section*{Pruebas de funcionamiento}
Dentro del cliente se puede autenticar con el usuarios creado desde LDAP.
\begin{center}
    \includegraphics{27.png}
\end{center}
De igual forma se puede conectar al usuario creado en LDAP con una 
conexión SSH a la máquina cliente de LDAP.
\begin{center}
    \includegraphics{28.png}
\end{center}
\newpage
Con el comando \textbf{slapcat} se transforman la información de la 
base de datos de LDAP a un formato .ldif.
\vspace{0.3cm}
\begin{center}
    sudo slapcat
\end{center}
\vspace{0.5cm}
\begin{center}
    \includegraphics[scale=1.1]{29.png}
\end{center}
\begin{center}
    \includegraphics[scale=1.1]{30.png}
\end{center}
\newpage
\section*{Conclusiones}
En relación a el servicio de directorio LDAP, además, de el método de 
instalación, el funcionamiento, y las características, se conluye que 
una instalación, junto con la configuración basada en el uso de un
interprete de comandos, facilita y agiliza el proceso, ya que no se 
necesita de un gran conocimiento parar poder ejecutar la instalación.

En cuanto a su funcionamiento y características, la centralización de 
los recursos, junto con la sincronía de los datos entre el cliente y el
servidor, esto debido al uso del servicio NFS, es positiva porque permite
usar las operaciones de leer, escribir, editar, y eliminar en el sistema
de archivos, como si los recursos estubieran de manera local.


\newpage

\bibliographystyle{apalike}
\bibliography{referencias}
    
\end{document}
