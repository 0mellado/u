-- MariaDB dump 10.19  Distrib 10.8.3-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: newgym
-- ------------------------------------------------------
-- Server version	10.8.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idemp` int(4) NOT NULL AUTO_INCREMENT,
  `rutper` int(10) DEFAULT NULL,
  `idlabor` int(4) DEFAULT NULL,
  `idturno` int(2) DEFAULT NULL,
  `idjefe` int(2) DEFAULT NULL,
  `sueldo` int(32) DEFAULT NULL,
  `fechaHoraContra` datetime DEFAULT NULL,
  PRIMARY KEY (`idemp`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES
(1,14691134,1,1,1,800000,'2022-01-04 12:00:00'),
(2,11316131,1,1,1,1200000,'2022-01-04 12:00:00'),
(3,11021813,1,1,1,1200000,'2018-01-04 12:00:00'),
(4,14077683,1,1,1,1200000,'2018-01-04 12:00:00'),
(5,11740777,1,1,1,800000,'2018-01-04 12:00:00'),
(6,13168654,1,1,1,1200000,'2022-01-04 12:00:00'),
(7,11732905,1,3,1,800000,'2021-06-04 12:00:00'),
(8,12553863,1,3,1,1200000,'2021-06-04 14:00:00'),
(9,14416479,1,3,1,1200000,'2021-06-04 14:00:00'),
(10,13216342,1,3,1,1200000,'2022-06-04 14:00:00'),
(11,13264146,1,3,1,800000,'2002-06-04 14:00:00'),
(12,11794184,1,3,1,800000,'2002-06-04 14:00:00'),
(13,12362346,2,3,1,1200000,'2002-06-04 14:00:00'),
(14,10225091,2,3,1,1200000,'2002-06-04 14:00:00'),
(15,14407010,2,4,1,1200000,'2022-01-04 18:00:00'),
(16,10504455,2,4,1,1200000,'2022-01-04 18:00:00'),
(17,13723643,2,4,1,1200000,'2022-01-04 18:00:00'),
(18,13469491,2,4,1,1200000,'2010-01-28 18:00:00'),
(19,13819718,2,4,1,1200000,'2010-01-28 18:00:00'),
(20,13285369,2,4,1,1200000,'2010-06-28 18:00:00'),
(21,14025088,2,4,1,1200000,'2010-06-28 18:00:00'),
(22,10564084,2,4,1,1200000,'2010-06-28 18:00:00'),
(23,14261436,2,4,1,1200000,'2010-06-28 18:00:00'),
(24,11594230,2,4,1,1200000,'2000-06-28 18:00:00'),
(25,11623955,3,4,1,1200000,'2000-06-28 18:00:00'),
(26,14151758,3,2,1,1200000,'2000-06-28 18:00:00'),
(27,13541846,3,2,1,1200000,'2003-06-28 18:00:00'),
(28,11932925,3,2,1,1200000,'2003-06-28 18:00:00'),
(29,13228022,3,2,1,1200000,'2003-06-28 08:00:00'),
(30,13272561,3,2,1,1200000,'2013-06-28 08:00:00'),
(31,10627614,3,2,1,1200000,'2013-01-28 08:00:00'),
(32,12827749,3,2,1,1200000,'2022-01-28 08:00:00'),
(33,13631648,3,2,1,1200000,'2012-01-28 08:00:00'),
(34,14238621,3,2,1,1200000,'2012-01-28 08:00:00'),
(35,12888102,3,2,1,1200000,'2012-01-28 08:00:00'),
(36,13561203,3,2,1,1200000,'2012-01-28 08:00:00'),
(37,13347829,2,3,1,3489999,'2022-01-28 08:00:00');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jefe`
--

DROP TABLE IF EXISTS `jefe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jefe` (
  `idjefe` int(2) NOT NULL,
  `nojefe` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jeferut` int(10) DEFAULT NULL,
  `jefedigitv` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidop` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidom` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idjefe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jefe`
--

LOCK TABLES `jefe` WRITE;
/*!40000 ALTER TABLE `jefe` DISABLE KEYS */;
INSERT INTO `jefe` VALUES
(1,'Juan',18437473,'4','Perez','Gonsalez');
/*!40000 ALTER TABLE `jefe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labores`
--

DROP TABLE IF EXISTS `labores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labores` (
  `idlabor` int(4) NOT NULL,
  `labor` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idlabor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labores`
--

LOCK TABLES `labores` WRITE;
/*!40000 ALTER TABLE `labores` DISABLE KEYS */;
INSERT INTO `labores` VALUES
(1,'profesor'),
(2,'servicio al cliente'),
(3,'personal de aseo');
/*!40000 ALTER TABLE `labores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logNombre`
--

DROP TABLE IF EXISTS `logNombre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logNombre` (
  `idlog` int(9) NOT NULL AUTO_INCREMENT,
  `newName` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oldName` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fechaHora` datetime DEFAULT NULL,
  PRIMARY KEY (`idlog`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logNombre`
--

LOCK TABLES `logNombre` WRITE;
/*!40000 ALTER TABLE `logNombre` DISABLE KEYS */;
INSERT INTO `logNombre` VALUES
(3,'Pablo','Oscar','2022-07-02 20:27:13'),
(4,'Julia','Pamela','2022-07-02 20:40:50');
/*!40000 ALTER TABLE `logNombre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logPerson`
--

DROP TABLE IF EXISTS `logPerson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logPerson` (
  `idlogper` int(9) NOT NULL AUTO_INCREMENT,
  `logrut` int(10) DEFAULT NULL,
  `fechaHoraIn` datetime DEFAULT NULL,
  PRIMARY KEY (`idlogper`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logPerson`
--

LOCK TABLES `logPerson` WRITE;
/*!40000 ALTER TABLE `logPerson` DISABLE KEYS */;
INSERT INTO `logPerson` VALUES
(4,94389334,'2022-07-02 20:08:19'),
(5,7483537,'2022-07-02 20:23:15'),
(6,47382943,'2022-07-02 20:26:25'),
(7,74328947,'2022-07-02 20:38:14'),
(8,94389334,'2022-07-02 21:10:43');
/*!40000 ALTER TABLE `logPerson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `rutper` int(10) NOT NULL,
  `nomper` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digitvery` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidop` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidom` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rutper`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES
(34243,'persona2','4','apellidopa1','apellidoma2'),
(5435435,'gfdsgfds','g','gfdsg','fgsdgds'),
(7483537,'Pepe','5','Perez','Gonzales'),
(10225091,'Sebastian','6','Carrasco','Becerra'),
(10504455,'Ariel','6','Buarque','Muriel'),
(10564084,'Laura','6','Cameron','Contreras'),
(10627614,'Sebastian','5','Brooks','Rovira'),
(11021813,'Trinidad','3','Font','Quiñones'),
(11316131,'Matias','k','Gonzalez','Franco'),
(11594230,'Melyssa','7','Ware','Colomer'),
(11623955,'personaPrueba','5','Alcaide','Amado'),
(11732905,'Renato','9','Serrano','Sala'),
(11740777,'Fernanda','5','Fuente','Padilla'),
(11794184,'Rocio','6','Mellado','Mariño'),
(11932925,'Benjamin','2','Vera','Laguna'),
(12362346,'Darius','5','Ketterer','Llorens'),
(12553863,'Araya','k','Patel','Oliver'),
(12827749,'Felipe','4','Foley','Royo'),
(12888102,'Nash','3','Morgan','Carvajal'),
(13168654,'Jose','6','Segarra','Cañizares'),
(13216342,'Hilary','4','Cervantes','Valles'),
(13228022,'Ignatius','3','Boix','Estrada'),
(13264146,'Natalie','5','Pratt','Mora'),
(13272561,'Victoria','k','Vizcaino','Valero'),
(13285369,'Wallace','6','Cruz','Salcedo'),
(13347829,'Alberto','4','Carrasco','Vera'),
(13469491,'Raymond','7','Prado','Agudo'),
(13541846,'Edan','6','Gonzalez','Cuesta'),
(13561203,'Monserrat','k','Ratliff','Pastor'),
(13631648,'Maximiliano','6','Salazar','Carreras'),
(13723643,'Warren','2','Plaza','Gonzales'),
(13819718,'Jorden','5','Bolton','Tovar'),
(14025088,'Beck','4','Wiley','Patiño'),
(14077683,'Barbara','4','Vitar','Peñalver'),
(14151758,'Carolina','4','Perez','Melgar'),
(14238621,'Cristobal','2','Alston','Moral'),
(14261436,'Pia','2','Macias','Lago'),
(14407010,'Emmanuel','4','Bustos','Alcaide'),
(14416479,'Joaquin','3','Franco','Marrero'),
(14691134,'Maximo','9','Carranza','Fariña'),
(42383979,'Pedrito','3','Gonzales','Tapia'),
(47382943,'Pablo','4','Mellado','Bustos'),
(74328947,'Julia','3','Jerez','Novoa'),
(94389334,'Pedro','k','Perez','Castro');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`oscar`@`localhost`*/ /*!50003 TRIGGER logInsertPerson AFTER INSERT ON persona
FOR EACH ROW
    INSERT INTO `logPerson` (logrut, fechaHoraIn)
    VALUES (new.rutper, now()) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`oscar`@`localhost`*/ /*!50003 TRIGGER logCambioNom AFTER UPDATE ON persona
FOR EACH ROW
    INSERT INTO `logNombre` (newName, oldName, fechaHora)
    VALUES (new.nomper, old.nomper, now()) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `turnos`
--

DROP TABLE IF EXISTS `turnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turnos` (
  `idturno` int(2) NOT NULL,
  `turno` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idturno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turnos`
--

LOCK TABLES `turnos` WRITE;
/*!40000 ALTER TABLE `turnos` DISABLE KEYS */;
INSERT INTO `turnos` VALUES
(1,'8:00 - 13:00'),
(2,'10:00 - 13:00'),
(3,'17:00 - 20:00'),
(4,'17:00 - 22:00');
/*!40000 ALTER TABLE `turnos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-02 22:40:03
