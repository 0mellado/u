

/* INSERT */
CREATE TRIGGER logInsertPerson AFTER INSERT ON persona
FOR EACH ROW
    INSERT INTO `logPerson` (logrut, fechaHoraIn)
    VALUES (new.rutper, now());





/* UPDATE */
CREATE TRIGGER logCambioNom AFTER UPDATE ON persona
FOR EACH ROW
    INSERT INTO `logNombre` (newName, oldName, fechaHora)
    VALUES (new.nomper, old.nomper, now());
