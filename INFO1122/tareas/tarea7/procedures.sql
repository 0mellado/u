/* Primer procedimento */
/* contratar */
CREATE PROCEDURE contratar (
    rutper INT(10), 
    nomper VARCHAR(64), 
    apellidop VARCHAR(64),
    apellidom VARCHAR(64),
    digitvery VARCHAR(2)
)

INSERT INTO `persona` (rutper, nomper, digitvery, apellidop, apellidom)
VALUES (rutper, nomper, digitvery, apellidop, apellidom);

/* Segundo procedimiento */
/* cambiar el nombre */
CREATE PROCEDURE cambiarNombre (rut INT(10), nomNuevo VARCHAR(64))
UPDATE persona SET nomper = nomNuevo WHERE rutper = rut;

/* Tercer procedimiento */
/* listar toda la tabla persona */
CREATE PROCEDURE selectTodoPer ()
select * from persona;
