/* CREATE TABLE `persona` ( */
/*   `rutper` int(10), */
/*   `nomper` varchar(64), */
/*   `digitvery` varchar(2), */
/*   `apellidop` varchar(64), */
/*   `apellidom` varchar(64), */
/*   PRIMARY KEY (`rutper`) */
/* ); */

/* INSERT INTO `persona` (rutper, nomper, digitvery, apellidop, apellidom) */
/* VALUES */
/* (13347829, 'Alberto'    , '4', 'Carrasco' , 'Vera'     ); */
/* (14691134, 'Maximo'     , '9', 'Carranza' , 'Fariña'   ), */
/* (11316131, 'Matias'     , 'k', 'Gonzalez' , 'Franco'   ), */
/* (11021813, 'Trinidad'   , '3', 'Font'     , 'Quiñones' ), */
/* (14077683, 'Barbara'    , '4', 'Vitar'    , 'Peñalver' ), */
/* (11740777, 'Fernanda'   , '5', 'Fuente'   , 'Padilla'  ), */
/* (13168654, 'Jose'       , '6', 'Segarra'  , 'Cañizares'), */
/* (11732905, 'Renato'     , '9', 'Serrano'  , 'Sala'     ), */
/* (12553863, 'Araya'      , 'k', 'Patel'    , 'Oliver'   ), */
/* (14416479, 'Joaquin'    , '3', 'Franco'   , 'Marrero'  ), */
/* (13216342, 'Hilary'     , '4', 'Cervantes', 'Valles'   ), */
/* (13264146, 'Natalie'    , '5', 'Pratt'    , 'Mora'     ), */
/* (11794184, 'Rocio'      , '6', 'Mellado'  , 'Mariño'   ), */
/* (12362346, 'Darius'     , '5', 'Ketterer' , 'Llorens'  ), */
/* (10225091, 'Wilma'      , '6', 'Carrasco' , 'Becerra'  ), */
/* (14407010, 'Emmanuel'   , '4', 'Bustos'   , 'Alcaide'  ), */
/* (10504455, 'Ariel'      , '6', 'Buarque'  , 'Muriel'   ), */
/* (13723643, 'Warren'     , '2', 'Plaza'    , 'Gonzales' ), */
/* (13469491, 'Raymond'    , '7', 'Prado'    , 'Agudo'    ), */
/* (13819718, 'Jorden'     , '5', 'Bolton'   , 'Tovar'    ), */
/* (13285369, 'Wallace'    , '6', 'Cruz'     , 'Salcedo'  ), */
/* (14025088, 'Beck'       , '4', 'Wiley'    , 'Patiño'   ), */
/* (10564084, 'Laura'      , '6', 'Cameron'  , 'Contreras'), */
/* (14261436, 'Pia'        , '2', 'Macias'   , 'Lago'     ), */
/* (11594230, 'Melyssa'    , '7', 'Ware'     , 'Colomer'  ), */
/* (11623955, 'Florencia'  , '5', 'Alcaide'  , 'Amado'    ), */
/* (14151758, 'Carolina'   , '4', 'Perez'    , 'Melgar'   ), */
/* (13541846, 'Edan'       , '6', 'Gonzalez' , 'Cuesta'   ), */
/* (11932925, 'Benjamin'   , '2', 'Vera'     , 'Laguna'   ), */
/* (13228022, 'Ignatius'   , '3', 'Boix'     , 'Estrada'  ), */
/* (13272561, 'Victoria'   , 'k', 'Vizcaino' , 'Valero'   ), */
/* (10627614, 'Sebastian'  , '5', 'Brooks'   , 'Rovira'   ), */
/* (12827749, 'Felipe'     , '4', 'Foley'    , 'Royo'     ), */
/* (13631648, 'Maximiliano', '6', 'Salazar'  , 'Carreras' ), */
/* (14238621, 'Cristobal'  , '2', 'Alston'   , 'Moral'    ), */
/* (12888102, 'Nash'       , '3', 'Morgan'   , 'Carvajal' ), */
/* (13561203, 'Monserrat'  , 'k', 'Ratliff'  , 'Pastor'   ); */



/* CREATE TABLE `labores` ( */
/*   `idlabor` int(4), */
/*   `labor` varchar(32), */
/*   PRIMARY KEY (`idlabor`) */
/* ); */

/* INSERT INTO `labores` (idlabor, labor) */
/* VALUES */
/* (1, "profesor"), */
/* (2, "servicio al cliente"), */
/* (3, "profesor"); */


/* CREATE TABLE `turnos` ( */
/*   `idturno` int(2), */
/*   `turno` varchar(32), */
/*   PRIMARY KEY (`idturno`) */
/* ); */

/* INSERT INTO `turnos` (idturno, turno) */
/* VALUES */
/* (1, "8:00 - 13:00"), */
/* (2, "10:00 - 13:00"), */
/* (3, "17:00 - 20:00"), */
/* (4, "17:00 - 22:00"); */


/* CREATE TABLE `jefe` ( */
/*   `idjefe` int(2), */
/*   `nojefe` varchar(64), */
/*   `jeferut` int(10), */
/*   `jefedigitv` varchar(2), */
/*   `apellidop` varchar(64), */
/*   `apellidom` varchar(64), */
/*   PRIMARY KEY (`idjefe`) */
/* ); */

/* INSERT INTO `jefe` (idjefe, nojefe, apellidop, apellidom, jefedigitv, jeferut) */
/* VALUES */
/* (1, 'Juan', 'Perez', 'Gonsalez', '4', 18437473); */

CREATE TABLE `empleado` (
  `idemp` int(4) NOT NULL AUTO_INCREMENT,
  `rutper` int(10),
  `idlabor` int(4),
  `idturno` int(2),
  `idjefe` int(2),
  `sueldo` int(32),
  `fechaHoraContra` datetime,
  PRIMARY KEY (`idemp`)
);


/* INSERT INTO `empleado` (idemp, rutper, idlabor, idturno, idjefe, sueldo, fechacontra) */
INSERT INTO `empleado` (rutper, idlabor, idturno, idjefe, sueldo, fechaHoraContra)
VALUES
(14691134, 1, 1, 1,  800000 , '2022-01-04 12:00:00'),
(11316131, 1, 1, 1, 1200000 , '2022-01-04 12:00:00'),
(11021813, 1, 1, 1, 1200000 , '2018-01-04 12:00:00'),
(14077683, 1, 1, 1, 1200000 , '2018-01-04 12:00:00'),
(11740777, 1, 1, 1,  800000 , '2018-01-04 12:00:00'),
(13168654, 1, 1, 1, 1200000 , '2022-01-04 12:00:00'),
(11732905, 1, 3, 1,  800000 , '2021-06-04 12:00:00'),
(12553863, 1, 3, 1, 1200000 , '2021-06-04 14:00:00'),
(14416479, 1, 3, 1, 1200000 , '2021-06-04 14:00:00'),
(13216342, 1, 3, 1, 1200000 , '2022-06-04 14:00:00'),
(13264146, 1, 3, 1,  800000 , '2002-06-04 14:00:00'),
(11794184, 1, 3, 1,  800000 , '2002-06-04 14:00:00'),
(12362346, 2, 3, 1, 1200000 , '2002-06-04 14:00:00'),
(10225091, 2, 3, 1, 1200000 , '2002-06-04 14:00:00'),
(14407010, 2, 4, 1, 1200000 , '2022-01-04 18:00:00'),
(10504455, 2, 4, 1, 1200000 , '2022-01-04 18:00:00'),
(13723643, 2, 4, 1, 1200000 , '2022-01-04 18:00:00'),
(13469491, 2, 4, 1, 1200000 , '2010-01-28 18:00:00'),
(13819718, 2, 4, 1, 1200000 , '2010-01-28 18:00:00'),
(13285369, 2, 4, 1, 1200000 , '2010-06-28 18:00:00'),
(14025088, 2, 4, 1, 1200000 , '2010-06-28 18:00:00'),
(10564084, 2, 4, 1, 1200000 , '2010-06-28 18:00:00'),
(14261436, 2, 4, 1, 1200000 , '2010-06-28 18:00:00'),
(11594230, 2, 4, 1, 1200000 , '2000-06-28 18:00:00'),
(11623955, 3, 4, 1, 1200000 , '2000-06-28 18:00:00'),
(14151758, 3, 2, 1, 1200000 , '2000-06-28 18:00:00'),
(13541846, 3, 2, 1, 1200000 , '2003-06-28 18:00:00'),
(11932925, 3, 2, 1, 1200000 , '2003-06-28 18:00:00'),
(13228022, 3, 2, 1, 1200000 , '2003-06-28 08:00:00'),
(13272561, 3, 2, 1, 1200000 , '2013-06-28 08:00:00'),
(10627614, 3, 2, 1, 1200000 , '2013-01-28 08:00:00'),
(12827749, 3, 2, 1, 1200000 , '2022-01-28 08:00:00'),
(13631648, 3, 2, 1, 1200000 , '2012-01-28 08:00:00'),
(14238621, 3, 2, 1, 1200000 , '2012-01-28 08:00:00'),
(12888102, 3, 2, 1, 1200000 , '2012-01-28 08:00:00'),
(13561203, 3, 2, 1, 1200000 , '2012-01-28 08:00:00'),
(13347829, 2, 3, 1, 3489999 , '2022-01-28 08:00:00');
