\documentclass[11pt]{article}

    \usepackage[breakable]{tcolorbox}
    \usepackage{parskip} % Stop auto-indenting (to mimic markdown behaviour)
    

    % Basic figure setup, for now with no caption control since it's done
    % automatically by Pandoc (which extracts ![](path) syntax from Markdown).
    \usepackage{graphicx}
    % Maintain compatibility with old templates. Remove in nbconvert 6.0
    \let\Oldincludegraphics\includegraphics
    % Ensure that by default, figures have no caption (until we provide a
    % proper Figure object with a Caption API and a way to capture that
    % in the conversion process - todo).
    \usepackage{caption}
    \DeclareCaptionFormat{nocaption}{}
    \captionsetup{format=nocaption,aboveskip=0pt,belowskip=0pt}

    \usepackage{float}
    \floatplacement{figure}{H} % forces figures to be placed at the correct location
    \usepackage{xcolor} % Allow colors to be defined
    \usepackage{enumerate} % Needed for markdown enumerations to work
    \usepackage[a4paper]{geometry} % Used to adjust the document margins
    \usepackage{amsmath} % Equations
    \usepackage{amssymb} % Equations
    \usepackage{textcomp} % defines textquotesingle
    % Hack from http://tex.stackexchange.com/a/47451/13684:
    \AtBeginDocument{%
        \def\PYZsq{\textquotesingle}% Upright quotes in Pygmentized code
    }
    \usepackage{upquote} % Upright quotes for verbatim code
    \usepackage{eurosym} % defines \euro

    \usepackage{iftex}
    \ifPDFTeX
        \usepackage[T1]{fontenc}
        \IfFileExists{alphabeta.sty}{
              \usepackage{alphabeta}
          }{
              \usepackage[mathletters]{ucs}
              \usepackage[utf8x]{inputenc}
          }
    \else
        \usepackage{fontspec}
        \usepackage{unicode-math}
    \fi

    \usepackage{fancyvrb} % verbatim replacement that allows latex
    \usepackage{grffile} % extends the file name processing of package graphics 
                         % to support a larger range
    \makeatletter % fix for old versions of grffile with XeLaTeX
    \@ifpackagelater{grffile}{2019/11/01}
    {
      % Do nothing on new versions
    }
    {
      \def\Gread@@xetex#1{%
        \IfFileExists{"\Gin@base".bb}%
        {\Gread@eps{\Gin@base.bb}}%
        {\Gread@@xetex@aux#1}%
      }
    }
    \makeatother
    \usepackage[Export]{adjustbox} % Used to constrain images to a maximum size
    \adjustboxset{max size={0.9\linewidth}{0.9\paperheight}}

    % The hyperref package gives us a pdf with properly built
    % internal navigation ('pdf bookmarks' for the table of contents,
    % internal cross-reference links, web links for URLs, etc.)
    \usepackage{hyperref}
    % The default LaTeX title has an obnoxious amount of whitespace. By default,
    % titling removes some of it. It also provides customization options.
    \usepackage{titling}
    \usepackage{longtable} % longtable support required by pandoc >1.10
    \usepackage{booktabs}  % table support for pandoc > 1.12.2
    \usepackage{array}     % table support for pandoc >= 2.11.3
    \usepackage{calc}      % table minipage width calculation for pandoc >= 2.11.1
    \usepackage[inline]{enumitem} % IRkernel/repr support (it uses the enumerate* environment)
    \usepackage[normalem]{ulem} % ulem is needed to support strikethroughs (\sout)
                                % normalem makes italics be italics, not underlines
    \usepackage{mathrsfs}
    

    
    % Colors for the hyperref package
    \definecolor{urlcolor}{rgb}{0,.145,.698}
    \definecolor{linkcolor}{rgb}{.71,0.21,0.01}
    \definecolor{citecolor}{rgb}{.12,.54,.11}

    % ANSI colors
    \definecolor{ansi-black}{HTML}{3E424D}
    \definecolor{ansi-black-intense}{HTML}{282C36}
    \definecolor{ansi-red}{HTML}{E75C58}
    \definecolor{ansi-red-intense}{HTML}{B22B31}
    \definecolor{ansi-green}{HTML}{00A250}
    \definecolor{ansi-green-intense}{HTML}{007427}
    \definecolor{ansi-yellow}{HTML}{DDB62B}
    \definecolor{ansi-yellow-intense}{HTML}{B27D12}
    \definecolor{ansi-blue}{HTML}{208FFB}
    \definecolor{ansi-blue-intense}{HTML}{0065CA}
    \definecolor{ansi-magenta}{HTML}{D160C4}
    \definecolor{ansi-magenta-intense}{HTML}{A03196}
    \definecolor{ansi-cyan}{HTML}{60C6C8}
    \definecolor{ansi-cyan-intense}{HTML}{258F8F}
    \definecolor{ansi-white}{HTML}{C5C1B4}
    \definecolor{ansi-white-intense}{HTML}{A1A6B2}
    \definecolor{ansi-default-inverse-fg}{HTML}{FFFFFF}
    \definecolor{ansi-default-inverse-bg}{HTML}{000000}

    % common color for the border for error outputs.
    \definecolor{outerrorbackground}{HTML}{FFDFDF}

    % commands and environments needed by pandoc snippets
    % extracted from the output of `pandoc -s`
    \providecommand{\tightlist}{%
      \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
    \DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
    % Add ',fontsize=\small' for more characters per line
    \newenvironment{Shaded}{}{}
    \newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.56,0.13,0.00}{{#1}}}
    \newcommand{\DecValTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\FloatTok}[1]{\textcolor[rgb]{0.25,0.63,0.44}{{#1}}}
    \newcommand{\CharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\StringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\CommentTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textit{{#1}}}}
    \newcommand{\OtherTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{{#1}}}
    \newcommand{\AlertTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.02,0.16,0.49}{{#1}}}
    \newcommand{\RegionMarkerTok}[1]{{#1}}
    \newcommand{\ErrorTok}[1]{\textcolor[rgb]{1.00,0.00,0.00}{\textbf{{#1}}}}
    \newcommand{\NormalTok}[1]{{#1}}
    
    % Additional commands for more recent versions of Pandoc
    \newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.53,0.00,0.00}{{#1}}}
    \newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.25,0.44,0.63}{{#1}}}
    \newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.73,0.40,0.53}{{#1}}}
    \newcommand{\ImportTok}[1]{{#1}}
    \newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.73,0.13,0.13}{\textit{{#1}}}}
    \newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\VariableTok}[1]{\textcolor[rgb]{0.10,0.09,0.49}{{#1}}}
    \newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.00,0.44,0.13}{\textbf{{#1}}}}
    \newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.40,0.40,0.40}{{#1}}}
    \newcommand{\BuiltInTok}[1]{{#1}}
    \newcommand{\ExtensionTok}[1]{{#1}}
    \newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.74,0.48,0.00}{{#1}}}
    \newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.49,0.56,0.16}{{#1}}}
    \newcommand{\InformationTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    \newcommand{\WarningTok}[1]{\textcolor[rgb]{0.38,0.63,0.69}{\textbf{\textit{{#1}}}}}
    
    
    % Define a nice break command that doesn't care if a line doesn't already
    % exist.
    \def\br{\hspace*{\fill} \\* }
    % Math Jax compatibility definitions
    \def\gt{>}
    \def\lt{<}
    \let\Oldtex\TeX
    \let\Oldlatex\LaTeX
    \renewcommand{\TeX}{\textrm{\Oldtex}}
    \renewcommand{\LaTeX}{\textrm{\Oldlatex}}
    % Document parameters
    % Document title
    \title{
        \huge REPORTE COMERCIAL \\ THE BACKYARDIGANS GYM
        \copyright
    }
    \author{Reporte hecho por Oscar Mellado Bustos}
    \date{Junio 2022}
    
% Pygments definitions
\makeatletter
\def\PY@reset{\let\PY@it=\relax \let\PY@bf=\relax%
    \let\PY@ul=\relax \let\PY@tc=\relax%
    \let\PY@bc=\relax \let\PY@ff=\relax}
\def\PY@tok#1{\csname PY@tok@#1\endcsname}
\def\PY@toks#1+{\ifx\relax#1\empty\else%
    \PY@tok{#1}\expandafter\PY@toks\fi}
\def\PY@do#1{\PY@bc{\PY@tc{\PY@ul{%
    \PY@it{\PY@bf{\PY@ff{#1}}}}}}}
\def\PY#1#2{\PY@reset\PY@toks#1+\relax+\PY@do{#2}}

\@namedef{PY@tok@w}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.73,0.73}{##1}}}
\@namedef{PY@tok@c}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cp}{\def\PY@tc##1{\textcolor[rgb]{0.61,0.40,0.00}{##1}}}
\@namedef{PY@tok@k}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kt}{\def\PY@tc##1{\textcolor[rgb]{0.69,0.00,0.25}{##1}}}
\@namedef{PY@tok@o}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ow}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@nb}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nf}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@nn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@ne}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.80,0.25,0.22}{##1}}}
\@namedef{PY@tok@nv}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@no}{\def\PY@tc##1{\textcolor[rgb]{0.53,0.00,0.00}{##1}}}
\@namedef{PY@tok@nl}{\def\PY@tc##1{\textcolor[rgb]{0.46,0.46,0.00}{##1}}}
\@namedef{PY@tok@ni}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@na}{\def\PY@tc##1{\textcolor[rgb]{0.41,0.47,0.13}{##1}}}
\@namedef{PY@tok@nt}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@nd}{\def\PY@tc##1{\textcolor[rgb]{0.67,0.13,1.00}{##1}}}
\@namedef{PY@tok@s}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sd}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@si}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@se}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.67,0.36,0.12}{##1}}}
\@namedef{PY@tok@sr}{\def\PY@tc##1{\textcolor[rgb]{0.64,0.35,0.47}{##1}}}
\@namedef{PY@tok@ss}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sx}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@m}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@gh}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@gu}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.50,0.00,0.50}{##1}}}
\@namedef{PY@tok@gd}{\def\PY@tc##1{\textcolor[rgb]{0.63,0.00,0.00}{##1}}}
\@namedef{PY@tok@gi}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.52,0.00}{##1}}}
\@namedef{PY@tok@gr}{\def\PY@tc##1{\textcolor[rgb]{0.89,0.00,0.00}{##1}}}
\@namedef{PY@tok@ge}{\let\PY@it=\textit}
\@namedef{PY@tok@gs}{\let\PY@bf=\textbf}
\@namedef{PY@tok@gp}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,0.50}{##1}}}
\@namedef{PY@tok@go}{\def\PY@tc##1{\textcolor[rgb]{0.44,0.44,0.44}{##1}}}
\@namedef{PY@tok@gt}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.27,0.87}{##1}}}
\@namedef{PY@tok@err}{\def\PY@bc##1{{\setlength{\fboxsep}{\string -\fboxrule}\fcolorbox[rgb]{1.00,0.00,0.00}{1,1,1}{\strut ##1}}}}
\@namedef{PY@tok@kc}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kd}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kn}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@kr}{\let\PY@bf=\textbf\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@bp}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.50,0.00}{##1}}}
\@namedef{PY@tok@fm}{\def\PY@tc##1{\textcolor[rgb]{0.00,0.00,1.00}{##1}}}
\@namedef{PY@tok@vc}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vg}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vi}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@vm}{\def\PY@tc##1{\textcolor[rgb]{0.10,0.09,0.49}{##1}}}
\@namedef{PY@tok@sa}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sb}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sc}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@dl}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s2}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@sh}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@s1}{\def\PY@tc##1{\textcolor[rgb]{0.73,0.13,0.13}{##1}}}
\@namedef{PY@tok@mb}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mf}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mh}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mi}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@il}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@mo}{\def\PY@tc##1{\textcolor[rgb]{0.40,0.40,0.40}{##1}}}
\@namedef{PY@tok@ch}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cm}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cpf}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@c1}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}
\@namedef{PY@tok@cs}{\let\PY@it=\textit\def\PY@tc##1{\textcolor[rgb]{0.24,0.48,0.48}{##1}}}

\def\PYZbs{\char`\\}
\def\PYZus{\char`\_}
\def\PYZob{\char`\{}
\def\PYZcb{\char`\}}
\def\PYZca{\char`\^}
\def\PYZam{\char`\&}
\def\PYZlt{\char`\<}
\def\PYZgt{\char`\>}
\def\PYZsh{\char`\#}
\def\PYZpc{\char`\%}
\def\PYZdl{\char`\$}
\def\PYZhy{\char`\-}
\def\PYZsq{\char`\'}
\def\PYZdq{\char`\"}
\def\PYZti{\char`\~}
% for compatibility with earlier versions
\def\PYZat{@}
\def\PYZlb{[}
\def\PYZrb{]}
\makeatother


    % For linebreaks inside Verbatim environment from package fancyvrb. 
    \makeatletter
        \newbox\Wrappedcontinuationbox 
        \newbox\Wrappedvisiblespacebox 
        \newcommand*\Wrappedvisiblespace {\textcolor{red}{\textvisiblespace}} 
        \newcommand*\Wrappedcontinuationsymbol {\textcolor{red}{\llap{\tiny$\m@th\hookrightarrow$}}} 
        \newcommand*\Wrappedcontinuationindent {3ex } 
        \newcommand*\Wrappedafterbreak {\kern\Wrappedcontinuationindent\copy\Wrappedcontinuationbox} 
        % Take advantage of the already applied Pygments mark-up to insert 
        % potential linebreaks for TeX processing. 
        %        {, <, #, %, $, ' and ": go to next line. 
        %        _, }, ^, &, >, - and ~: stay at end of broken line. 
        % Use of \textquotesingle for straight quote. 
        \newcommand*\Wrappedbreaksatspecials {% 
            \def\PYGZus{\discretionary{\char`\_}{\Wrappedafterbreak}{\char`\_}}% 
            \def\PYGZob{\discretionary{}{\Wrappedafterbreak\char`\{}{\char`\{}}% 
            \def\PYGZcb{\discretionary{\char`\}}{\Wrappedafterbreak}{\char`\}}}% 
            \def\PYGZca{\discretionary{\char`\^}{\Wrappedafterbreak}{\char`\^}}% 
            \def\PYGZam{\discretionary{\char`\&}{\Wrappedafterbreak}{\char`\&}}% 
            \def\PYGZlt{\discretionary{}{\Wrappedafterbreak\char`\<}{\char`\<}}% 
            \def\PYGZgt{\discretionary{\char`\>}{\Wrappedafterbreak}{\char`\>}}% 
            \def\PYGZsh{\discretionary{}{\Wrappedafterbreak\char`\#}{\char`\#}}% 
            \def\PYGZpc{\discretionary{}{\Wrappedafterbreak\char`\%}{\char`\%}}% 
            \def\PYGZdl{\discretionary{}{\Wrappedafterbreak\char`\$}{\char`\$}}% 
            \def\PYGZhy{\discretionary{\char`\-}{\Wrappedafterbreak}{\char`\-}}% 
            \def\PYGZsq{\discretionary{}{\Wrappedafterbreak\textquotesingle}{\textquotesingle}}% 
            \def\PYGZdq{\discretionary{}{\Wrappedafterbreak\char`\"}{\char`\"}}% 
            \def\PYGZti{\discretionary{\char`\~}{\Wrappedafterbreak}{\char`\~}}% 
        } 
        % Some characters . , ; ? ! / are not pygmentized. 
        % This macro makes them "active" and they will insert potential linebreaks 
        \newcommand*\Wrappedbreaksatpunct {% 
            \lccode`\~`\.\lowercase{\def~}{\discretionary{\hbox{\char`\.}}{\Wrappedafterbreak}{\hbox{\char`\.}}}% 
            \lccode`\~`\,\lowercase{\def~}{\discretionary{\hbox{\char`\,}}{\Wrappedafterbreak}{\hbox{\char`\,}}}% 
            \lccode`\~`\;\lowercase{\def~}{\discretionary{\hbox{\char`\;}}{\Wrappedafterbreak}{\hbox{\char`\;}}}% 
            \lccode`\~`\:\lowercase{\def~}{\discretionary{\hbox{\char`\:}}{\Wrappedafterbreak}{\hbox{\char`\:}}}% 
            \lccode`\~`\?\lowercase{\def~}{\discretionary{\hbox{\char`\?}}{\Wrappedafterbreak}{\hbox{\char`\?}}}% 
            \lccode`\~`\!\lowercase{\def~}{\discretionary{\hbox{\char`\!}}{\Wrappedafterbreak}{\hbox{\char`\!}}}% 
            \lccode`\~`\/\lowercase{\def~}{\discretionary{\hbox{\char`\/}}{\Wrappedafterbreak}{\hbox{\char`\/}}}% 
            \catcode`\.\active
            \catcode`\,\active 
            \catcode`\;\active
            \catcode`\:\active
            \catcode`\?\active
            \catcode`\!\active
            \catcode`\/\active 
            \lccode`\~`\~ 	
        }
    \makeatother

    \let\OriginalVerbatim=\Verbatim
    \makeatletter
    \renewcommand{\Verbatim}[1][1]{%
        %\parskip\z@skip
        \sbox\Wrappedcontinuationbox {\Wrappedcontinuationsymbol}%
        \sbox\Wrappedvisiblespacebox {\FV@SetupFont\Wrappedvisiblespace}%
        \def\FancyVerbFormatLine ##1{\hsize\linewidth
            \vtop{\raggedright\hyphenpenalty\z@\exhyphenpenalty\z@
                \doublehyphendemerits\z@\finalhyphendemerits\z@
                \strut ##1\strut}%
        }%
        % If the linebreak is at a space, the latter will be displayed as visible
        % space at end of first line, and a continuation symbol starts next line.
        % Stretch/shrink are however usually zero for typewriter font.
        \def\FV@Space {%
            \nobreak\hskip\z@ plus\fontdimen3\font minus\fontdimen4\font
            \discretionary{\copy\Wrappedvisiblespacebox}{\Wrappedafterbreak}
            {\kern\fontdimen2\font}%
        }%
        
        % Allow breaks at special characters using \PYG... macros.
        \Wrappedbreaksatspecials
        % Breaks at punctuation characters . , ; ? ! and / need catcode=\active 	
        \OriginalVerbatim[#1,codes*=\Wrappedbreaksatpunct]%
    }
    \makeatother

    % Exact colors from NB
    \definecolor{incolor}{HTML}{303F9F}
    \definecolor{outcolor}{HTML}{D84315}
    \definecolor{cellborder}{HTML}{CFCFCF}
    \definecolor{cellbackground}{HTML}{F7F7F7}
    
    % prompt
    \makeatletter
    \newcommand{\boxspacing}{\kern\kvtcb@left@rule\kern\kvtcb@boxsep}
    \makeatother
    \newcommand{\prompt}[4]{
        {\ttfamily\llap{{\color{#2}[#3]:\hspace{3pt}#4}}\vspace{-\baselineskip}}
    }
    

    
    % Prevent overflowing lines due to hard-to-break entities
    \sloppy 
    % Setup hyperref package
    \hypersetup{
      breaklinks=true,  % so long urls are correctly broken across lines
      colorlinks=true,
      urlcolor=urlcolor,
      linkcolor=linkcolor,
      citecolor=citecolor,
      }
    % Slightly bigger margins than the latex defaults
    
    \geometry{verbose,tmargin=2cm,bmargin=2cm,lmargin=2cm,rmargin=2cm}
    \newcommand{\gym}{\textbf{THE BACKYARDIGANS GYM \copyright}}
    \setlength{\parindent}{12pt}
    
    

\begin{document}
    
    \maketitle
    

\hypertarget{descripcion}{
\subsection*{Descripción:}\label{descripcion}}

\hspace{12pt}En este reporte redactado por Oscar Mellado Bustos
se encuentran diversos datos relativos a la información de los
empleados que se encuentran trabajando en \gym. En la primera
tabla se encuentra información relativa a los sueldos de algunos
de los trabajadores, en la segunda tabla se profundiza en el tema 
de los sueldos de los trabajadores, en la tercera tabla habla
de el nombre del trabajador, su turno de trabajo, su labor y
su sueldo en la empresa, en la cuarta tabla se habla de los
años promedio de contratación ordenados por labor, en la quinta
tabla se listan la personas de cierta edad, y el la sexta tabla 
los datos que se encuentran son relativos a los labores con un
sueldo mayor a un cierto límite, y el nombre de la persona que
supervisa esas labores.


\vspace{1.5cm}

\begin{center}
    \subsubsection*{Tabla 1 - Promedio de sueldo}
    \begin{tabular}{  c  c  }
    \hline
        \textbf{Media de sueldos} & 
        \textbf{Nombres} 
        \\ [0.5ex] \hline\hline
        \textdollar 1,166,666.667 &
        Maximo, Matias, Maximiliano \\ \hline
    \end{tabular}
\end{center}

En la primera tabla se muestra el promedio de los sueldos de los
trabajadores cuyo nombre empiece por m mayúscula le sigan 2 
caracteres cualquiera, para continuar con la letra i y que le sigan
cualquier cantidad de caracteres.

\begin{center}
    \subsubsection*{Tabla 2 - Finanzas por labor}
    \begin{tabular}{  c  c  c  c  c  }
    \hline
        \textbf{suma de sueldos} & 
        \textbf{cantidad} &
        \textbf{media} &
        \textbf{sueldo máximo} &
        \textbf{sueldo mínimo} 
        \\ [0.5ex] \hline\hline
        \textdollar 18000000 & 12 & \textdollar 1500000 & \textdollar 1500000 & \textdollar 1500000 \\  \hline
        \textdollar 16800000 & 12 & \textdollar 1400000 & \textdollar 1400000 & \textdollar 1400000 \\  \hline
        \textdollar 11600000 & 12 & \textdollar 966666.6 & \textdollar 1200000 & \textdollar 800000 \\ 
    \hline
    \end{tabular}
\end{center}

En la segunda tabla se listan la sumatoria de todos los sueldo 
de cada labor, la cantidad de trabajadores que estan en cada 
labor, el promedio de sueldo por cada labor, el sueldo máximo y
el sueldo mínimo.


\begin{center}
    \subsubsection*{Tabla 3 - }
    \begin{tabular}{  c  c  c  c  }
    \hline
        \textbf{nombre} & \textbf{turno} &
        \textbf{labor} & \textbf{sueldo} 
        \\ [0.5ex] \hline\hline
        Maximo     & 8:00 - 13:00  & Profesor             &   800000 \\ \hline
        Matias     & 8:00 - 13:00  & Profesor             &  1200000 \\ \hline
        Trinidad   & 8:00 - 13:00  & Profesor             &  1000000 \\ \hline 
        Barbara    & 8:00 - 13:00  & Profesor             &  1000000 \\ \hline
        Fernanda   & 8:00 - 13:00  & Profesor             &   800000 \\ \hline
        Jose       & 8:00 - 13:00  & Profesor             &  1200000 \\ \hline
        Renato     & 17:00 - 20:00 & Profesor             &   800000 \\ \hline
        Araya      & 17:00 - 20:00 & Profesor             &  1200000 \\ \hline
        Joaquin    & 17:00 - 20:00 & Profesor             &  1000000 \\ \hline
        Hilary     & 17:00 - 20:00 & Profesor             &  1000000 \\ \hline
        Natalie    & 17:00 - 20:00 & Profesor             &   800000 \\ \hline
        Rocio      & 17:00 - 20:00 & Profesor             &   800000 \\ \hline
        Darius     & 17:00 - 20:00 & Servicio al cliente  &  1400000 \\ \hline
        Wilma      & 17:00 - 20:00 & Servicio al cliente  &  1400000 \\ \hline
        Emmanuel   & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Ariel      & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Warren     & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Raymond    & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Jorden     & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Wallace    & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Beck       & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Laura      & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Pia        & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
        Melyssa    & 17:00 - 22:00 & Servicio al cliente  &  1400000 \\ \hline
    \end{tabular}
\end{center}

En la tercera tabla se listan los nombre, el turno,
el labor por el que fueron contratados y el sueldo de los empleados
que con un ingreso menor a \textdollar 1400001. Con esta tabla se
puede ver la claramente que labor y turno cumple cada funcionario
de la empresa y el sueldo por el cual están contratados.

\vspace{1.5cm}

\begin{center}
    \subsubsection*{Tabla 4 - Año promedio de ingreso por labor}
    \begin{tabular}{  c  c  }
    \hline
        \textbf{Año} & 
        \textbf{labor} \\ [0.5ex] \hline\hline
        2022 & NULL  \\  \hline
        2009 & Personal de aseo \\  \hline
        2017 & Profesor  \\ \hline
        2011 & Servicio al cliente \\
        \hline
    \end{tabular}
\end{center}

En la cuarta tabla se listan la media de los años de ingreso
por labor y la labor. En el año 2022 como es un año contemporaneo,
el trabajador no tiene una labor asignada, entonces se rellena con
un valor nulo.

\vspace{1.5cm}

\begin{center}
    \subsubsection*{Tabla 5 - La id, el rut de la persona y el nombre del jefe}
    \begin{tabular}{  c  c  c  }
    \hline
        \textbf{Id del empleado} & 
        \textbf{Rut del empleado} & 
        \textbf{Nombre del jefe} \\ [0.5ex] \hline\hline
          2 & 11316131 & Juan \\ \hline 
          3 & 11021813 & Juan \\ \hline 
          5 & 11740777 & Juan \\ \hline 
          7 & 11732905 & Juan \\ \hline 
          8 & 12553863 & Juan \\ \hline 
         12 & 11794184 & Juan \\ \hline 
         13 & 12362346 & Juan \\ \hline 
         14 & 10225091 & Juan \\ \hline 
         16 & 10504455 & Juan \\ \hline 
         22 & 10564084 & Juan \\ \hline 
         24 & 11594230 & Juan \\ \hline 
         25 & 11623955 & Juan \\ \hline 
         28 & 11932925 & Juan \\ \hline 
         31 & 10627614 & Juan \\ \hline 
         32 & 12827749 & Juan \\ \hline 
         35 & 12888102 & Juan \\ \hline 
    \end{tabular}
\end{center}

En la quinta tabla se listan las identificaciones de los empleados
dentro de la empresa, sus ruts, el nombre de los trabajadores y el
nombre de la persona que está a cargo de los trabajadores, osea su jefe.
\vspace{1.5cm}

\begin{center}
    \subsubsection*{Tabla 6 - Año promedio de ingreso por labor}
    \begin{tabular}{  c  c  }
    \hline
        \textbf{Labores con un sueldo sobre \textdollar 1300000} & 
        \textbf{Nombre del jefe} \\ [0.5ex] \hline\hline
        Personal de aseo, Servicio al cliente & Juan \\
        \hline
    \end{tabular}
\end{center}

En la sexta tabla se listan labores cuyo ingreso sea superior 
a \textdollar 1300000, y el nombre del jefe.

    % Add a bibliography block to the postdoc

    \newpage
\setlength{\parindent}{0cm}
\section*{ANEXO TECNICO - Consultas SQL}

\subsection*{Tabla 1}
Para la tabla 1 se usó la siguiente consulta: 

\color{red}SELECT \color{orange}FORMAT\color{black}(\color{orange}AVG\color{black}(sueldo), 3),
\color{orange}GROUP\_CONCAT\color{black}(nomper)\\
\color{red}FROM \color{black}empleado \\
\color{red}NATURAL \color{orange}JOIN \color{black}persona \\
\color{red}WHERE \color{black}nomper \color{orange}LIKE \color{black}"M\_\_i\%";

\hspace{12pt}Con esta consulta listo el promedio de los sueldo con una precisión
de 3 decimales, y los nombre de las personas con las que se calculó
el promedio. Como en la tabla empleado almaceno en su mayoría 
llaves foráneas, necesito unirla con la tabla persona para 
obtener los nombres, con la condición de que el nombre empieze por
la letra m mayúscula, le siguen 2 caracteres cualesquiera, la letra i
y que termine con cualquier combinación de caracteres.


\subsection*{Tabla 2}
Para la tabla 2 se usó la siguiente consulta: 

\color{red}SELECT \color{orange}SUM\color{black}(sueldo)
\color{orange}AS \color{black}"suma de sueldos", \\
\color{orange}COUNT\color{black}(idlabor)
\color{orange}AS \color{black}"cantidad", \\
\color{orange}AVG\color{black}(sueldo)
\color{orange}AS \color{black}"media", \\
\color{orange}MAX\color{black}(sueldo)
\color{orange}AS \color{black}"sueldo máximo", \\
\color{orange}MIN\color{black}(sueldo)
\color{orange}AS \color{black}"sueldo mínimo" \\
\color{red}FROM \color{black}empleado \\
\color{red}GROUP \color{orange}BY \color{black}idlabor \\
\color{red}ORDER \color{orange}BY \color{black}sum(sueldo)
\color{orange}DESC\color{black}; \\

\hspace{12pt}Con esta consulta se calculan y listán la sumatoria,
la cantidad, el promedio, el sueldo más alto, y el sueldo más bajo
entre los labores que existen en la empresa. Para hacer esto se 
necesito agrupar los datos por las id's de las labores, y los datos
se ordenaron de mayor a menor dependiendo de la sumatoria de los 
sueldos de cada labor.

\subsection*{Tabla 3}
Para la tabla 3 se usó la siguiente consulta: 

\color{red}SELECT \color{black}nomper, turno, labor, sueldo \\
\color{red}FROM \color{black}(((empleado 
\color{red}INNER \color{orange}JOIN \color{black} persona
\color{orange}ON \color{black} empleado.rutper = persona.rutper) \\
\color{red}INNER \color{orange}JOIN \color{black}turnos
\color{orange}ON \color{black} empleado.idturno = persona.idturno) \\
\color{red}INNER \color{orange}JOIN \color{black}labores
\color{orange}ON \color{black} empleado.idlabor = labores.idlabor) \\
\color{red}WHERE \color{black}sueldo > 1400001;

\hspace{12pt}Para esta consulta se necesito unir 4 tablas para dar 
una información más general de cada empleado, información como el 
nombre, el turno que debe trabajar el empleado, su labor, y su 
sueldo. Cada uno de estos datos están en una de las diferentes tablas
de la base de datos, y como condición solo listo los datos de las
personas que tiene un sueldo superior a los \textdollar 1400001.

\newpage
\subsection*{Tabla 4}
Para la tabla 4 se usó la siguiente consulta: 

\color{red}SELECT \color{orange}AVG\color{black}(\color{orange}YEAR\color{black}(fechacontra)), labor\\
\color{red}FROM \color{black}empleado \\
\color{red}LEFT \color{orange}JOIN \color{black}labores 
\color{orange}ON \color{black}empleado.idlabor = labores.idlabor \\
\color{red}GROUP \color{orange}BY \color{black}idlabor;

\hspace{12pt}En esta consulta con la función YEAR() se
obtuvo el año en el que los empleados fueron contratados, con
la función AVG() se obtuvo el promedio de esos años, y para que 
el promedio lo liste por labor se agruparon los datos por las id's
de las labores.

\subsection*{Tabla 5}
Para la tabla 5 se usó la siguiente consulta: 

\color{red}SELECT \color{black}idemp, rutper, nojefe \\
\color{red}FROM \color{black}empleado \\
\color{red}NATURAL \color{orange}JOIN \color{black}jefe \\
\color{red}WHERE \color{black}rutper < 13000000;

\hspace{12pt}Para esta consulta listo las id's de los empleados,
sus rut, y el nombre de su superior, uno la tabla empleado con
la tabla jefe, para tener el nombre del jefe. Con la condición
rutper < 13000000, listo solo los datos de las personas con un rut
superior a 13000000.

\subsection*{Tabla 6}
Para la tabla 6 se usó la siguiente consulta: 

\color{red}SELECT \color{orange}GROUP\_CONCAT\color{black}(\color{orange}DISTINCT \color{black}labor \color{orange}SEPARATOR\color{black}', '), nojefe \\
\color{red}FROM \color{black}(empleado 
\color{red}INNER \color{orange}JOIN \color{black}labores 
\color{orange}ON \color{black}empleado.idlabor = labores.idlabor) \\
\color{red}NATURAL \color{orange}JOIN \color{black}jefe \\
\color{red}WHERE \color{black}sueldo > 1300000;

\hspace{12pt}En esta consulta con la función GROUP\_CONCAT()
agrupo las labores donde el sueldo es mayor a \textdollar 1300000.
Necesito unir 3 tablas porque en este caso, el nombre del jefe 
está en la tabla jefe, y las diferentes labores en su respectiva 
tabla, pero ambas tablas no tienen una relación directa, por lo
que también necesito la tabla empleado para poder relacionar entre
si datos de la tabla labores, y la tabla jefe.

\end{document}
