
CREATE TABLE `empleados` (
  `idemple` int(10) NOT NULL,
  `labor` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rutnum` int(10) DEFAULT NULL,
  `digitveri` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidop` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sueldo` int(10) DEFAULT NULL,
  `idhorario` int(10) DEFAULT NULL,
  `fechacontra` date DEFAULT NULL,
  PRIMARY KEY (`idemple`),
  KEY `idhorario` (`idhorario`),
  CONSTRAINT `empleados_ibfk_1` FOREIGN KEY (`idhorario`) REFERENCES `horarios` (`idhorario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `empleados` VALUES
(1,'profesor',14691134,'9','Maximo','Carranza',800000,1,'2022-01-04'),
(2,'profesor',11316131,'k','Matias','Gonzalez',1200000,7,'2022-01-04'),
(3,'profesor',11021813,'3','Trinidad','Font',1000000,2,'2018-01-04'),
(4,'profesor',14077683,'4','Barbara','Vitar',1000000,8,'2018-01-04'),
(5,'profesor',11740777,'5','Fernanda','Fuente',800000,3,'2018-01-04'),
(6,'profesor',13168654,'6','Jose','Segarra',1200000,9,'2022-01-04'),
(7,'profesor',11732905,'9','Renato','Serrano',800000,4,'2021-06-04'),
(8,'profesor',12553863,'k','Araya','Patel',1200000,10,'2021-06-04'),
(9,'profesor',14416479,'3','Joaquin','Franco',1000000,5,'2021-06-04'),
(10,'profesor',13216342,'4','Hilary','Cervantes',1000000,11,'2022-06-04'),
(11,'profesor',13264146,'5','Natalie','Pratt',800000,6,'2002-06-04'),
(12,'profesor',11794184,'6','Rocio','Mellado',800000,12,'2002-06-04'),
(13,'servicio al cliente',12362346,'5','Darius','Ketterer',1400000,1,'2002-06-04'),
(14,'servicio al cliente',10225091,'6','Wilma','Carrasco',1400000,2,'2002-06-04'),
(15,'servicio al cliente',14407010,'4','Emmanuel','Bustos',1400000,3,'2022-01-04'),
(16,'servicio al cliente',10504455,'6','Ariel','Buarque',1400000,4,'2022-01-04'),
(17,'servicio al cliente',13723643,'2','Warren','Plaza',1400000,5,'2022-01-04'),
(18,'servicio al cliente',13469491,'7','Raymond','Prado',1400000,6,'2010-01-28'),
(19,'servicio al cliente',13819718,'5','Jorden','Bolton',1400000,7,'2010-01-28'),
(20,'servicio al cliente',13285369,'6','Wallace','Cruz',1400000,8,'2010-06-28'),
(21,'servicio al cliente',14025088,'4','Beck','Wiley',1400000,9,'2010-06-28'),
(22,'servicio al cliente',10564084,'6','Laura','Cameron',1400000,10,'2010-06-28'),
(23,'servicio al cliente',14261436,'2','Pia','Macias',1400000,11,'2010-06-28'),
(24,'servicio al cliente',11594230,'7','Melyssa','Ware',1400000,12,'2000-06-28'),
(25,'personal de aseo',11623955,'5','Florencia','Alcaide',1500000,1,'2000-06-28'),
(26,'personal de aseo',14151758,'4','Carolina','Perez',1500000,2,'2000-06-28'),
(27,'personal de aseo',13541846,'6','Edan','Gonzalez',1500000,3,'2003-06-28'),
(28,'personal de aseo',11932925,'2','Benjamin','Vera',1500000,4,'2003-06-28'),
(29,'personal de aseo',13228022,'3','Ignatius','Boix',1500000,5,'2003-06-28'),
(30,'personal de aseo',13272561,'k','Victoria','Vizcaino',1500000,6,'2013-06-28'),
(31,'personal de aseo',10627614,'5','Sebastian','Brooks',1500000,7,'2013-01-28'),
(32,'personal de aseo',12827749,'4','Felipe','Foley',1500000,8,'2022-01-28'),
(33,'personal de aseo',13631648,'6','Maximiliano','Salazar',1500000,9,'2012-01-28'),
(34,'personal de aseo',14238621,'2','Cristobal','Alston',1500000,10,'2012-01-28'),
(35,'personal de aseo',12888102,'3','Nash','Morgan',1500000,11,'2012-01-28'),
(36,'personal de aseo',13561203,'k','Monserrat','Ratliff',1500000,12,'2012-01-28');


CREATE TABLE `gerencia` (
  `idgerente` int(10) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidop` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidom` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digitveri` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rutnum` int(10) DEFAULT NULL,
  `direccion` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sueldo` int(10) DEFAULT NULL,
  PRIMARY KEY (`idgerente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `gerencia` VALUES
(1,'Juan','Perez','Gonsalez','4',18437473,'Los pedrolos 0123',1000000);


CREATE TABLE `gimnasio` (
  `idgim` int(10) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idemple` int(10) DEFAULT NULL,
  `direc` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disciplina` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgerente` int(10) DEFAULT NULL,
  PRIMARY KEY (`idgim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `gimnasio` VALUES
(1,'Gymsick',1,'Los escultores 0438','Pesas',1);

CREATE TABLE `horarios` (
  `idhorario` int(10) NOT NULL,
  `jornada` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dia` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inicio` int(10) DEFAULT NULL,
  `termino` int(10) DEFAULT NULL,
  `duracion` int(10) DEFAULT NULL,
  PRIMARY KEY (`idhorario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `horarios` VALUES
(1,'mañana','lunes',800,1300,5),
(2,'mañana','martes',1000,1300,3),
(3,'mañana','miercoles',800,1300,5),
(4,'mañana','jueves',1000,1300,3),
(5,'mañana','viernes',800,1300,5),
(6,'mañana','sabado',1000,1300,3),
(7,'tarde','lunes',1700,2000,3),
(8,'tarde','martes',1700,2200,5),
(9,'tarde','miercoles',1700,2000,3),
(10,'tarde','jueves',1700,2200,5),
(11,'tarde','viernes',1700,2000,3),
(12,'tarde','sabado',1700,2200,5);
