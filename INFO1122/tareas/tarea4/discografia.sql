CREATE TABLE canciones (
     id int(4) not null auto_increment,
     id_art int(4),
     album varchar(25),
     cancion varchar(25),
     fecha_lanzamiento date,
     vendida int(8),
     valor int(5),
     primary key(id)
);

insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"Californication","Around the World",date("1999-01-01"),353,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"Californication","Scar Tissue",date("1999-01-01"),470,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"Californication","Otherside",date("1999-01-01"),365,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"Californication","Californication",date("1999-01-01"),530,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"Californication","Road Trippin",date("1999-01-01"),370,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"By the way","By the way",date("2002-01-01"),850,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"By the way","Dosed",date("2002-01-01"),450,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"By the way","The Zephyr Song",date("2002-01-01"),890,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"By the way","Can't Stop",date("2002-01-01"),780,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(1,"By the way","I Could Die for You",date("2002-01-01"),200,355);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer","Let Down",date("1997-01-01"),550,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer","Karma Police",date("1997-01-01"),642,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer","Electioneering",date("1997-01-01"),760,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer","No Surprises",date("1997-01-01"),723,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer","Lucky",date("1997-01-01"),723,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(2,"OK Computer", "thetourist",date("1997-01-01"),872,420);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(3,"En Boca de Tantos","En Boca de Tantos",date("2008-01-01"),920,170);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(3,"En Boca de Tantos","Suben al cielo – Con Aid",date("2008-01-01"),645,170);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(3,"En Boca de Tantos","Aprecia lo que tienes",date("2008-01-01"),890,170);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(3,"En Boca de Tantos","Sobre el famoso tema",date("2008-01-01"),1040,170);
insert into canciones (id_art, album, cancion, fecha_lanzamiento, vendida, valor) values(3,"En Boca de Tantos","Sin ti",date("2008-01-01"),220,170);

CREATE TABLE artistas (
   `id_art` int(1) not null,
   `nombre` varchar(21),
   `genero` varchar(9),
   `ano_nac` int(4),
   Primary Key(id_art)
);


INSERT INTO `artistas` (`id_art`, `nombre`, `genero`, `ano_nac`)
VALUES 
(1, 'Red Hot Chili Peppers', 'Funk Rock', 1983),
(2, 'Radiohead', 'Art Rock', 1985),
(3, 'Porta', 'Hip Hop', 1988);

