#!/usr/bin/python3
import numpy as np

# Función que compara dos ecuaciones en un formato especifico para
# determinar si las ecuaciones coinciden en los coeficientes a, pero distintos en los coeficientes b
def sinSoluciones(a, b):
    if a[0] == b[0] \
    and a[1] == b[1] \
    and a[2] == b[2] \
    and a[3] != b[3]:
        print("No tiene soluciones")
        exit(0)


# Función que compara dos ecuaciones en un formato especifico para
# determinar si las ecuaciones son equivalentes
def infinitasSoluciones(a, b):
    if a[0] == b[0] \
    and a[1] == b[1] \
    and a[2] == b[2] \
    and a[3] == b[3]:
        print("Las soluciones son infinitas")
        exit(0)

# Función principal que recibe los datos de entrada,
# los almacena en arreglos y procede a operar con los datos
def main():
    print("Se debe reemplazar aij, bij por valores numéricos")
    eqs = np.zeros((3, 3))
    results = np.zeros(3)

    for i in range(3):
        for j in range(3):
            eq = float(input(f"Ingrese el valor numérico de a{i+1}{j+1}\n> "))
            eqs[i][j] = eq
        res = float(input(f"Ingrese el valor numérico de b{i+1}\n> "))
        results[i] = res

    # En esta parte se comprueba que el sistema tenga soluciones infinitas
    eq1 = np.array([eqs[0][0], eqs[0][1], eqs[0][2], results[0]]) * results[1]
    eq2 = np.array([eqs[1][0], eqs[1][1], eqs[1][2], results[1]]) * results[0]

    infinitasSoluciones(eq1, eq2)

    eq1 = np.array([eqs[0][0], eqs[0][1], eqs[0][2], results[0]]) * results[2]
    eq2 = np.array([eqs[2][0], eqs[2][1], eqs[2][2], results[2]]) * results[0]

    infinitasSoluciones(eq1, eq2)

    eq1 = np.array([eqs[1][0], eqs[1][1], eqs[1][2], results[1]]) * results[2]
    eq2 = np.array([eqs[2][0], eqs[2][1], eqs[2][2], results[2]]) * results[1]

    infinitasSoluciones(eq1, eq2)

    eq1 = np.array([eqs[0][0], eqs[0][1], eqs[0][2], results[0]])
    eq2 = np.array([eqs[1][0], eqs[1][1], eqs[1][2], results[1]])

    # En esta parte se comprueba que el sistema no tenga soluciones
    sinSoluciones(eq1, eq2)

    eq1 = np.array([eqs[0][0], eqs[0][1], eqs[0][2], results[0]])
    eq2 = np.array([eqs[2][0], eqs[2][1], eqs[2][2], results[2]])

    sinSoluciones(eq1, eq2)

    eq1 = np.array([eqs[1][0], eqs[1][1], eqs[1][2], results[1]])
    eq2 = np.array([eqs[2][0], eqs[2][1], eqs[2][2], results[2]])

    sinSoluciones(eq1, eq2)

    # Con esta línea de código se calcula las soluciones del sistema
    csol = np.linalg.solve(eqs, results)

    print(f"x = {csol[0]}, y = {csol[1]}, z = {csol[2]}")


if __name__ == '__main__':
    main()
