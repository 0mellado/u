#!/usr/bin/python3
import matplotlib.pyplot as plt # se importa la librería matplotlib para graficar.
import numpy as np # se importa la librería numpy para poder tener datos que evaluar 
                   # en las ecuaciones.

def firstSis():
    sis = np.array([[3, 5], [0, 2]])
    sols = np.array([8, 8])
    return np.linalg.solve(sis, sols)


def secondSis():
    sis = np.array([[3, 5], [0, 1]])
    sols = np.array([8, -2])
    return np.linalg.solve(sis, sols)


def threeSis():
    sis = np.array([[3, -2], [0, 1]])
    sols = np.array([30, -2])
    return np.linalg.solve(sis, sols)


def fourSis():
    sis = np.array([[3, -2], [0, 2]])
    sols = np.array([30, 8])
    return np.linalg.solve(sis, sols)


def graficaPuntos():
    ver1 = firstSis()
    ver2 = secondSis()
    ver3 = threeSis()
    ver4 = fourSis()

    vertx = [ver1[0], ver2[0], ver3[0], ver4[0]]
    verty = [ver1[1], ver2[1], ver3[1], ver4[1]]

    plt.fill(vertx, verty, 'c')

    for i in range(len(vertx)):
        plt.scatter(vertx[i], verty[i])
        plt.text(vertx[i]-1, verty[i]+0.3, f"{np.around(vertx[i], 2), verty[i]}")


def main():
    x = np.linspace(-20, 40, 2) # con el método linspace se crea un arreglo con
                              # números desde el 0 hasta el 40 con 2
    y1 = (8 - 3*x) / 5
    y2 = np.linspace(-2, -2, 2)
    y3 = np.linspace(4, 4, 2)
    y4 = (3*x - 30) / 2

    plt.plot(x, y1, label=r'$3x+5y > 8$', ls='dashed')
    plt.plot(x, y2, label=r'$y+2 \geq 0$')
    plt.plot(x, y3, label=r'$2y-8 < 0$', ls='dashed')
    plt.plot(x, y4, label=r'$3x-2y \leq 30$')

    graficaPuntos()

    plt.legend(bbox_to_anchor=(0.5, 1.1), loc=2, borderaxespad=0.)
    plt.xlabel(r'$x$')
    plt.ylabel(r'$y$')
    plt.axis([-6, 15, -5, 7])
    plt.show()

if __name__ == '__main__':
    main()
