import java.util.List;
import java.util.Vector;

public class ConectivosLogicos
{
    public static void main(String []args)
    {
        boolean[] bool = new boolean[2];
        bool[0] = true;
        bool[1] = false;

        System.out.println("Negación: p ~p");
        System.out.println("p\t~p");
        for (boolean p: bool) {
            boolean P = !p;
            System.out.println(p + "\t" + P);
        }

        System.out.println("\nConjunción: p AND q");
        System.out.println("p\tq\tp AND q");
        for (boolean p: bool) {
            for (boolean q: bool) {
                boolean P = p && q;
                System.out.println(p+"\t"+q+"\t"+P);
            }
        }

        System.out.println("\nDisyunción inclusiva: p OR q");
        System.out.println("p\tq\tp OR q");
        for (boolean p: bool) {
            for (boolean q: bool) {
                boolean P = p || q;
                System.out.println(p+"\t"+q+"\t"+P);
            }
        }

        System.out.println("\nDisyunción exclusiva: p XOR q");
        System.out.println("p\tq\tp XOR q");
        for (boolean p: bool) {
            for (boolean q: bool) {
                boolean P = p != q;
                System.out.println(p+"\t"+q+"\t"+P);
            }
        }

        System.out.println("\nCondicional: p -> q");
        System.out.println("p\tq\tp -> q");
        for (boolean p: bool) {
            for (boolean q: bool) {
                boolean P = !p || q;
                System.out.println(p+"\t"+q+"\t"+P);
            }
        }

        System.out.println("\nBicondicional: p <-> q");
        System.out.println("p\tq\tp <-> q");
        for (boolean p: bool) {
            for (boolean q: bool) {
                boolean P = p == q;
                System.out.println(p+"\t"+q+"\t"+P);
            }
        }
    }
}


