# Conectivos Logicos R ; Grupo 4

bool <- c(T, F) #Asignacion de arreglo(True,False)

print("Negación: p ~p")
cat("p\t~p\n")
for (p in bool) {
#ciclo for que verifica los booleanos del arreglo
    P <- !p
    cat(p,"\t", P, "\n")
#Se imprimen los resultados por pantalla
}
#Conjución
#Para este conectivo logico se utiliza "&"
cat("\n")
print("Conjunción: p AND q")
cat("p\tq\tp AND q\n")
for (p in bool) {
    for (q in bool) {
#ciclo for que verifica los booleanos del arreglo
        P <- p & q
        cat(p,"\t",q,"\t", P, "\n")
#Se imprimen los resultados por pantalla
    }
}
#Disyucion inclusiva
#Para este conectivo logico se utiliza "|"
cat("\n")
print("Disyunción inclusiva: p OR q")
cat("p\tq\tp OR q\n")
for (p in bool) {
    for (q in bool) {
#ciclo for que verifica los booleanos del arreglo
        P <- p | q
        cat(p,"\t",q,"\t", P, "\n")
#Se imprimen los resultados por pantalla
    }
}
#Disyucion exclusiva
#Para este conectivo logico se utiliza "!="
cat("\n")
print("Disyunción exclusiva: p XOR q")
cat("p\tq\tp XOR q\n")
for (p in bool) {
    for (q in bool) {
#ciclo for que verifica los booleanos del arreglo
        P <- p != q
        cat(p,"\t",q,"\t", P, "\n")
#Se imprimen los resultados por pantalla
    }
}
#Condicional
#Para este conectivo logico se utiliza "!" y "|"
cat("\n")
print("Condicional: p -> q")
cat("p\tq\tp -> q\n")
for (p in bool) {
    for (q in bool) {
#ciclo for que verifica los booleanos del arreglo
        P <- !p | q
        cat(p,"\t",q,"\t", P, "\n")
#Se imprimen los resultados por pantalla
    }
}
#Bicondicional
#Para este conectivo logico se utiliza "=="
cat("\n")
print("Bicondicional: p <-> q")
cat("p\tq\tp <-> q\n")
for (p in bool) {
    for (q in bool) {
#ciclo for que verifica los booleanos del arreglo
        P <- p == q
        cat(p,"\t",q,"\t", P, "\n")
#Se imprimen los resultados por pantalla
    }
}
