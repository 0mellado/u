<?php
$bool = array(1, 0);

echo "Negación:\np\t~p\n";
foreach ($bool as $p) {
    $P = !$p;
    echo "$p\t$P\n";
}

echo "\nConjunción:\np AND q\n";
foreach ($bool as $p) {
    foreach ($bool as $q) {
        $P = $p && $q;
        echo "$p\t$q\t$P\n";
    }
}

echo "\nDisyunción inclusiva:\np OR q\n";
foreach ($bool as $p) {
    foreach ($bool as $q) {
        $P = $p || $q;
        echo "$p\t$q\t$P\n";
    }
}

echo "\nDisyunción exclusiva:\np XOR q\n";
foreach ($bool as $p) {
    foreach ($bool as $q) {
        $P = $p != $q;
        echo "$p\t$q\t$P\n";
    }
}

echo "\nCondicional:\np -> q\n";
foreach ($bool as $p) {
    foreach ($bool as $q) {
        $P = !($p) || $q;
        echo "$p\t$q\t$P\n";
    }
}

echo "\nBicondicional:\np <-> q\n";
foreach ($bool as $p) {
    foreach ($bool as $q) {
        $P = $p == $q;
        echo "$p\t$q\t$P\n";
    }
}
?>
