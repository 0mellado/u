bool <- c(T, F)

print("Negación: p ~p")
cat("p\t~p\n")
for (p in bool) {
    P <- !p
    cat(p,"\t", P, "\n")
}

cat("\n")
print("Conjunción: p AND q")
cat("p\tq\tp AND q\n")
for (p in bool) {
    for (q in bool) {
        P <- p & q
        cat(p,"\t",q,"\t", P, "\n")
    }
}

cat("\n")
print("Disyunción inclusiva: p OR q")
cat("p\tq\tp OR q\n")
for (p in bool) {
    for (q in bool) {
        P <- p | q
        cat(p,"\t",q,"\t", P, "\n")
    }
}

cat("\n")
print("Disyunción exclusiva: p XOR q")
cat("p\tq\tp XOR q\n")
for (p in bool) {
    for (q in bool) {
        P <- p != q
        cat(p,"\t",q,"\t", P, "\n")
    }
}

cat("\n")
print("Condicional: p -> q")
cat("p\tq\tp -> q\n")
for (p in bool) {
    for (q in bool) {
        P <- !p | q
        cat(p,"\t",q,"\t", P, "\n")
    }
}

cat("\n")
print("Bicondicional: p <-> q")
cat("p\tq\tp <-> q\n")
for (p in bool) {
    for (q in bool) {
        P <- p == q
        cat(p,"\t",q,"\t", P, "\n")
    }
}
