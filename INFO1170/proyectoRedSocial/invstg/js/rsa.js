const {
    generateKeyPairSync
} = require('node:crypto');

const {
    publicKey,
    privateKey
} = generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
    },
    privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem',
        cipher: 'aes-256-cbc',
        passphrase: 'alguna wea'
    }
});

console.log(privateKey)
