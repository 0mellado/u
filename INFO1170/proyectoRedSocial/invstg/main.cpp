#include "rsa.h"
#include "primos.h"


int main(int argc, char** argv) {

    /* int a = 93248392; */

    /* /1* bool is = Primos::isprimo(a); *1/ */
    /* Primos::isprimo(a); */

    Primos::createPrimos();

    while (1) {
        cout << "1 -> generar claves, 2 -> encriptar, 3 -> desencritptar, 4 -> salir\n";

        char opt;
        cin >> opt;

        if (opt == '1') {
            /* generar claves */
            RSA::generar();
        } else if (opt == '2') {
            /* encriptar */
            string msg;
            InfInt e, n;

            cout << "Mensage: ";
            cin.ignore();
            getline(cin, msg);

            cout << "e: ";
            cin >> e;

            cout << "n: ";
            cin >> n;

            auto r = RSA::encriptar(msg, e, n);
            for (auto n: r) {
                cout << n << "-";
            }

        } else if (opt == '3') {
            /* desencriptar */
            string msg;
            InfInt d, n;

            cout << "Mensage: ";
            cin.ignore();
            getline(cin, msg);

            cout << "d: ";
            cin >> d;

            cout << "n: ";
            cin >> n;

            auto r = RSA::desencriptar(msg, d, n);
            cout << r << "-";

        } else if (opt == '4') {
            break; // salir
        }
        cout << "\n";
    }

    return 0;
}
