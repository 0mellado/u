#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <tuple>
#include "InfInt.h"

using namespace std;

inline InfInt mcd(InfInt a, InfInt b) {
    if (b != 0) return mcd(b, a % b);
    return a;
}

inline tuple<InfInt, InfInt, InfInt> euclides(InfInt a, InfInt b) {
    if (a == 0) return make_tuple(b, 0, 1);

    InfInt mcd, x, y;
    tie(mcd, x, y) = euclides(b % a, a);

    return make_tuple(mcd, y - (b / a) * x, x);
}

struct RSA {
    static void generar();
    static vector<InfInt> encriptar(string, InfInt, InfInt);
    static string desencriptar(string, InfInt, InfInt);
};

inline void RSA::generar() {
    InfInt p, q, n, phi, e, d;

    cout << "Escribe p: ";
    cin >> p;

    cout << "Escribe q: ";
    cin >> q;

    n = p * q;
    phi = (p - 1) * (q - 1);

    do {
        cout << "Escribe e (menor y coprimo con" << phi << "): ";
        cin >> e;
    } while (!(e < phi && mcd(e, phi) == 1));

    d = get<1>(euclides(e, phi));

    while (d < 0) d += phi;

    cout << "Modulo: " << n << endl;
    cout << "Clave publica: " << e << endl;
    cout << "Clave privada: " << d << endl;
}


inline InfInt modpow(InfInt base, InfInt exp, InfInt mod) {
    InfInt r = 1;

    while (--exp >= 0) {
        r *= base;
        r %= mod;
    }
    return r;
}


inline vector<InfInt> RSA::encriptar(string msg, InfInt e, InfInt n) {
    vector<InfInt> result;

    for (auto c: msg) {
        result.push_back(modpow((int)c, e, n));
    }
    return result;
}


inline string RSA::desencriptar(string msg, InfInt d, InfInt n) {
    vector<InfInt> message;

    string temp("");
    for (auto c: msg) {
        if (c == '-') {
            message.push_back(temp);
            temp = "";
        } else temp += c;
    }

    if (temp.length() > 0) message.push_back(temp);

    string result("");
    for (auto c: message) {
        auto num = modpow(c, d, n);
        result += (char)(num.toInt());
    }
    return result;
}
