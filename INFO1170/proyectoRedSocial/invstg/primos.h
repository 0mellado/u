#include <iostream>
#include <vector>
#include "InfInt.h"
#include <pthread.h>

using namespace std;


struct Primos {
    static bool isprimo(InfInt);
    static void createPrimos();
};


inline bool Primos::isprimo(InfInt n) {

/*     int firstQ = n * 0.25; */
/*     int secondQ = n * 0.5; */
/*     int thirdQ = n * 0.75; */

/*     cout << firstQ << endl; */
/*     cout << secondQ << endl; */
/*     cout << thirdQ << endl; */
/*     cout << n << endl; */


    for (InfInt i = 2; i < n; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return n != 1;
}


inline void Primos::createPrimos() {
    InfInt max = 100000;
    InfInt min = 65538;
    InfInt cant = 2;

    InfInt i = min;

    while (cant > 0) {
        if (Primos::isprimo(i)) {
            cout << i << endl;
            cant--;
        }
        i++;
    }
}

