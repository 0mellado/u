# By Alberto Caro
# Simulacion de Estaciones de Material Particulado
# que generan niveles de contaminacion segun ICA
# 
#       ICA -> Normal        [001 - 079] ug/m3
#       ICA -> Alerta        [080 - 109] ug/m3
#       ICA -> PreEmergencia [110 - 169] ug/m3
#       ICA -> Emergencia    [170 - 999] ug/m3
#
#       MP = 2.5 um 
# mediante Cadenas de Markov 1er Orden
# Trama de datos incluye -> GPS + MP2.5um [ug/m3]
# Asignatura: INFO1157 
# Sistemas Inteligentes
#-------------------------------------------------

import numpy as np, random as RA

# RA.seed(1234)

nSTATE_0 = 0 ; nSTATE_1 = 1 ; nSTATE_2 = 2 ; nSTATE_3 = 3


aT = {       # B     A     P     E
      'B': [[0.80, 0.10, 0.05, 0.05],
            [0.70, 0.20, 0.05, 0.05],
            [0.60, 0.30, 0.05, 0.05],
            [0.50, 0.10, 0.20, 0.20]],

             # B     A     P     E
	  'A': [[0.20, 0.70, 0.05, 0.05],
	        [0.10, 0.80, 0.05, 0.05],
	        [0.10, 0.60, 0.20, 0.10],
	        [0.10, 0.50, 0.10, 0.30]],

             # B     A     P     E
      'P': [[0.05, 0.15, 0.60, 0.20],
	        [0.05, 0.10, 0.70, 0.15],
	        [0.05, 0.10, 0.70, 0.15],
	        [0.10, 0.10, 0.60, 0.20]],

             # B     A     P     E
      'E': [[0.05, 0.05, 0.20, 0.70],
	        [0.10, 0.20, 0.10, 0.60],
	        [0.05, 0.15, 0.20, 0.60],
	        [0.05, 0.10, 0.15, 0.70]]
     }

dICA = {
         'B': 'Normal',
         'A': 'Alerta',
         'P': 'Pre-Emergencia',
         'E': 'Emergencia'
       }

def Markov_Dist(cAQI) -> list:
    nSamples = 5
    nE = nSTATE_0
    aX = [ 0 for _ in range(nSamples) ]   
    aU = [ RA.random() for _ in range(nSamples) ]
    aMP = []

    # MP 2.5 um
    Get_MP_B = lambda: RA.randint(1, 79)
    Get_MP_A = lambda: RA.randint(80, 109)
    Get_MP_P = lambda: RA.randint(110, 169)
    Get_MP_E = lambda: RA.randint(170, 999)

    aRangRndo = [
        Get_MP_B,
        Get_MP_A,
        Get_MP_P,
        Get_MP_E
    ]

    def Get_MP(cAQI, aSamples) -> list:
        aMP.append(dICA[cAQI])
        for e in aSamples:
            aMP.append(aRangRndo[e]())
        return aMP

    def Next_State(t, e) -> int:
        if aU[t] >= 0.0 and aU[t] <= aT[cAQI][e][0]:
            return nSTATE_0
        if aU[t] > aT[cAQI][e][0] and aU[t] <= aT[cAQI][e][0] + aT[cAQI][e][1]:
            return nSTATE_1
        if aU[t] > aT[cAQI][e][0] + aT[cAQI][e][1] and aU[t] <= aT[cAQI][e][0] + aT[cAQI][e][1] + aT[cAQI][e][2]:
            return nSTATE_2               
        if aU[t] > aT[cAQI][e][0] + aT[cAQI][e][1] + aT[cAQI][e][2] and aU[t] <= 1.0:
            return nSTATE_3

    for t in range(0, nSamples):
        aX[t] = Next_State(t, nE)
        nE = aX[t]

    return Get_MP(cAQI, aX)

def Get_Trama() -> list:
    return Markov_Dist(RA.choice(['B', 'A', 'P', 'E']))

def Get_GPS(sAux) -> str:
    if sAux == 'La':
        if RA.random() >= 0.50:
            return ('Lat:' + str( +1 * RA.randint(0, 90)) + ' N ')
        return ('Lat:' + str( RA.randint(-90, 0)) + ' S ')

    if sAux == 'Lo':
        if RA.random() >= 0.50:
            return ('Long:' + str( +1 * RA.randint(0, 180)) + ' E ')
        return ('Long:' + str( RA.randint(-180, 0)) + ' W ')

dDATA_SA = {
           'SA0': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA1': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA2': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA3': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA4': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA5': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA6': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA7': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA8': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ],
           'SA9': [ [Get_GPS('La'), Get_GPS('Lo')], Get_Trama() ]
          }

for sKey in dDATA_SA:
    print(dDATA_SA[sKey])