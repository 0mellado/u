import wave
import numpy as np


def freq_note(n): return 440 * ((2 ** (1/12)) ** (n - 10))


def f(x, freq): return np.sin(2 * np.pi * freq * x)


def make_scale(rate):
    notes = np.array([1, 3, 5, 6, 8, 10, 12])
    x = np.linspace(0, 7, rate * 7)

    y = np.zeros(rate * 7)

    note = 0
    for i in range(rate * 7):
        y[i] = f(x[i], freq_note(notes[note]))
        if ((i % rate) == 0) and (i != 0):
            note += 1

    return (y * ((1 << 15) - 1)).astype("<h")


def make_scale_stereo(rate):
    notes = np.array([1, 3, 5, 6, 8, 10, 12])
    x = np.linspace(0, 7, rate * 7)

    y = np.zeros(rate * 7)

    note = 0
    for i in range(rate * 7):
        y[i] = f(x[i], freq_note(notes[note]))
        if ((i % rate) == 0) and (i != 0):
            note += 1

    return (np.array([y, y]).T * ((1 << 15) - 1)).astype("<h")


def uno():
    rate = 44100
    wv = wave.open('escala44100.wav', 'w')
    wv.setparams((1, 2, rate, rate * 7, 'NONE', 'no compressed'))

    wv.writeframes(make_scale(rate).tobytes())

    wv.close()


def dos():
    rate = 22050
    wv = wave.open('escala22050.wav', 'w')
    wv.setparams((2, 2, rate, rate * 7*2, 'NONE', 'no compressed'))

    wv.writeframes(make_scale_stereo(rate).tobytes())

    wv.close()


def tres():
    rate = 44100

    x = np.arange(0, rate * 3)

    y = np.zeros(rate * 3)

    for i in x:
        y[i] = 8000 * np.sin(2 * np.pi * 500.0 / rate * i) + 8000 * \
            np.sin(2 * np.pi * 250.0 / rate * i)

    y = (np.array([y, y]).T * ((1 << 15) - 1)).astype("<h")

    wv = wave.open('stereo.wav', 'w')
    wv.setparams((2, 2, rate, rate * 3, 'NONE', 'no compressed'))

    wv.writeframes(y.tobytes())

    wv.close()


def main():
    uno()
    dos()
    tres()


if __name__ == '__main__':
    main()
