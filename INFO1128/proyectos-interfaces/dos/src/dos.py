from numpy import sin, cos, e, exp, pi
import numpy as np
import matplotlib.pyplot as plt


def f1(t):
    return sin(t) * cos(1 / t)


def f2(t):
    return exp(-t) * cos(2 * pi * t)


def f3(t):
    return sin(t) * cos(1 / (t + 0.1))


def f4(t):
    return -2 * pi * exp(-t) * sin(2 * pi * t) - e**(-t) * cos(2 * pi * t)


def plot_functions():

    x_f1 = np.arange(-5.0, 1, 0.10)
    x_f2 = np.arange(-5.0, 1, 0.10)
    x_f3 = np.arange(-3.0, 2, 0.02)
    x_f4 = np.arange(-0.2, 0.2, 0.001)

    _, axes = plt.subplots(2, 2)

    axes[0][0].plot(x_f1, f2(x_f1))
    axes[0][1].plot(x_f2, f4(x_f2))
    axes[1][0].plot(x_f3, f1(x_f3))
    axes[1][1].plot(x_f4, f1(x_f4))

    axes[0][0].set_title('La función 1')
    axes[0][1].set_title('La función 2')
    axes[1][0].set_title('La función 3')
    axes[1][1].set_title('La función 4')

    axes[0][0].set(ylim=(-100, 151), xlim=(-5, 1.1),
                   xticks=np.arange(-5, 1.1), yticks=np.arange(-100, 151, 50))
    axes[0][1].set(ylim=(-800, 601), xlim=(-5, 1.1),
                   xticks=np.arange(-5, 1.1, 1), yticks=np.arange(-800, 601, 200))
    axes[1][0].set(ylim=(-1, 1.1), xlim=(-3, 2.1),
                   xticks=np.arange(-3, 2.1), yticks=np.arange(-1.0, 1.1, 0.5))
    axes[1][1].set(ylim=(-0.16, 0.16), xlim=(-0.2, 0.21),
                   xticks=np.arange(-0.2, 0.21, 0.1), yticks=np.array([-0.15, -0.10, 0.00, 0.10, 0.15]))

    plt.show()


def main():
    plot_functions()


if __name__ == '__main__':
    main()
