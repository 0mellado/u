import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks


def read_csv() -> tuple:
    data = pd.read_csv(
        '/home/oscar/Documentos/u/INFO1128/proyectos-interfaces/dos/datos/datos.csv')
    return (data['PM1.0'], data['PM2.5'], data['PM10'])


def plot_data(data: tuple) -> None:

    plt.style.use('ggplot')

    fig, axes = plt.subplots(3)

    x = np.arange(0, 2000)

    print(data[0].describe())
    print(data[1].describe())
    print(data[2].describe())

    peaks01, _ = find_peaks(data[0], height=135, distance=50)
    peaks25, _ = find_peaks(data[1], height=165, distance=50)
    peaks10, _ = find_peaks(data[2], height=185, distance=50)

    axes[0].plot(x, data[0], color='r', label='MP 1.0 um')
    axes[0].scatter(peaks01, data[0][peaks01])
    axes[1].plot(x, data[1], color='g', label='MP 2.5 um')
    axes[1].scatter(peaks25, data[1][peaks25])
    axes[2].plot(x, data[2], color='b', label='MP 10 um')
    axes[2].scatter(peaks10, data[2][peaks10])

    axes[0].set_ylabel('ug/m3')
    axes[0].legend()
    axes[1].set_ylabel('ug/m3')
    axes[1].legend()
    axes[2].set_ylabel('ug/m3')
    axes[2].legend()

    fig.suptitle('Niveles de contaminación MP 1.0, 2.5, 10 um')

    plt.show()


def main():
    plot_data(read_csv())


if __name__ == '__main__':
    main()
