import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt


def runge(x):
    return 1 / (1 + 25 * x ** 2)

def runge_noise(x):
    return runge(x) + 0.05 * np.random.randn(11)

def main():

    x = np.linspace(-1, 1, 11)
    
    y = runge_noise(x)

    f = interpolate.interp1d(x, y, kind=3)

    xx = np.linspace(-1, 1, 250)

    fig, ax = plt.subplots(figsize=(8, 4))

    ax.plot(xx, f(xx), 'k', lw=2, label='prueba de interpolación')
    ax.plot(xx, runge(xx), 'r--', lw=2, label='función original')

    ax.scatter(x, y, label='puntos de la función')


    ax.legend()

    plt.show()




if __name__ == '__main__':
    main()