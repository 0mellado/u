#include <avr/interrupt.h>
#include <avr/io.h>
#include <xc.h>

void my_setup() {
    cli();
    DDRB = 0x03;
    DDRD = 0x00;
    PORTB = 0x01;  
    TCCR0A = 0x00; 
    TCCR0B = 0x06; 
    TIMSK0 = 0x01; 
    TCNT0 = 0xfb;  
    sei();         
}

ISR(TIMER0_OVF_vect) {
    PORTB |= 0x02;     
    TCNT0 = 0xfb;       
    PORTB ^= 1;        
    PORTB ^= (1 << 1); 
}

int main(void) {
    my_setup();
    while (1);
}


