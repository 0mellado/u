from flask import Flask, jsonify
from flask_cors import CORS
import numpy as np, random as RA

nSTATE_0 = 0 ; nSTATE_1 = 1 ; nSTATE_2 = 2 ; nSTATE_3 = 3

aT = {
      'B': [ # B    A    P    E
 	  [0.80,0.10,0.05,0.05],
	  [0.70,0.20,0.05,0.05],
	  [0.60,0.30,0.05,0.05],
	  [0.50,0.10,0.20,0.20]
           ],

      'A': [ # B    A    P    E
	  [0.20,0.70,0.05,0.05],
	  [0.10,0.80,0.05,0.05],
	  [0.10,0.60,0.20,0.10],
	  [0.10,0.50,0.10,0.30]
           ],

      'P': [ # B    A    P    E
	  [0.05,0.15,0.60,0.20],
	  [0.05,0.10,0.70,0.15],
	  [0.05,0.10,0.70,0.15],
	  [0.10,0.10,0.60,0.20]
           ],

      'E': [ # B    A    P    E
	  [0.05,0.05,0.20,0.70],
	  [0.10,0.20,0.10,0.60],
	  [0.05,0.15,0.20,0.60],
	  [0.05,0.10,0.15,0.70]
           ]
     }

def Markov_Dist(cAQI):
    nSamples = 50 ; nE = nSTATE_0 ; aX = [ 0 for i in range(nSamples) ]   
	aU = [ RA.random() for i in range(nSamples) ]
    aMP = []

    def Get_MP_B():
        return RA.randint(1,79)

    def Get_MP_A():
        return RA.randint(80,109)

    def Get_MP_P():
        return RA.randint(110,169)

    def Get_MP_E():
        return RA.randint(170,999)

    def Get_MP(cAQI,aSamples):
        aMP.append(cAQI)
        for e in aSamples:
            if e == nSTATE_0: aMP.append(Get_MP_B())
            if e == nSTATE_1: aMP.append(Get_MP_A())
            if e == nSTATE_2: aMP.append(Get_MP_P())
            if e == nSTATE_3: aMP.append(Get_MP_E())
        return aMP

    def Next_State(t,e):
        if cAQI == 'B':
           if e == 0:
              if aU[t] >= 0.0 and aU[t] <= aT['B'][0][0]: return nSTATE_0
              if aU[t] > aT['B'][0][0] and aU[t] <= aT['B'][0][0] + aT['B'][0][1]: return nSTATE_1
              if aU[t] > aT['B'][0][0] + aT['B'][0][1] and aU[t] <= aT['B'][0][0] + aT['B'][0][1] + aT['B'][0][2]: return nSTATE_2
              if aU[t] > aT['B'][0][0] + aT['B'][0][1] + aT['B'][0][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 1:
              if aU[t] >= 0.0 and aU[t] <= aT['B'][1][0]: return nSTATE_0
              if aU[t] > aT['B'][1][0] and aU[t] <= aT['B'][1][0] + aT['B'][1][1]: return nSTATE_1
              if aU[t] > aT['B'][1][0] + aT['B'][1][1] and aU[t] <= aT['B'][1][0] + aT['B'][1][1] + aT['B'][1][2]: return nSTATE_2
              if aU[t] > aT['B'][1][0] + aT['B'][1][1] + aT['B'][1][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 2:
              if aU[t] >= 0.0 and aU[t] <= aT['B'][2][0]: return nSTATE_0
              if aU[t] > aT['B'][2][0] and aU[t] <= aT['B'][2][0] + aT['B'][2][1]: return nSTATE_1
              if aU[t] > aT['B'][2][0] + aT['B'][2][1] and aU[t] <= aT['B'][2][0] + aT['B'][2][1] + aT['B'][2][2]: return nSTATE_2
              if aU[t] > aT['B'][2][0] + aT['B'][2][1] + aT['B'][2][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 3:
              if aU[t] >= 0.0 and aU[t] <= aT['B'][3][0]: return nSTATE_0
              if aU[t] > aT['B'][3][0] and aU[t] <= aT['B'][3][0] + aT['B'][3][2]: return nSTATE_2
              if aU[t] > aT['B'][3][0] + aT['B'][3][2] and aU[t] <= aT['B'][3][0] + aT['B'][3][2] + aT['B'][3][3]: return nSTATE_3
              if aU[t] > aT['B'][3][0] + aT['B'][3][2] + aT['B'][3][3] and aU[t] <= 1.0: return nSTATE_1

        if cAQI == 'A':
           if e == 0:
              if aU[t] >= 0.0 and aU[t] <= aT['A'][0][1]: return nSTATE_1
              if aU[t] > aT['A'][0][1] and aU[t] <= aT['A'][0][0] + aT['A'][0][1]: return nSTATE_0
              if aU[t] > aT['A'][0][0] + aT['A'][0][1] and aU[t] <= aT['A'][0][0] + aT['A'][0][1] + aT['A'][0][2]: return nSTATE_2
              if aU[t] > aT['A'][0][0] + aT['A'][0][1] + aT['A'][0][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 1:
              if aU[t] >= 0.0 and aU[t] <= aT['A'][1][1]: return nSTATE_1
              if aU[t] > aT['A'][1][1] and aU[t] <= aT['A'][1][0] + aT['A'][1][1]: return nSTATE_0
              if aU[t] > aT['A'][1][0] + aT['A'][1][1] and aU[t] <= aT['A'][1][0] + aT['A'][1][1] + aT['A'][1][2]: return nSTATE_2
              if aU[t] > aT['A'][1][0] + aT['A'][1][1] + aT['A'][1][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 2:
              if aU[t] >= 0.0 and aU[t] <= aT['A'][2][1]: return nSTATE_1
              if aU[t] > aT['A'][2][1] and aU[t] <= aT['A'][2][1] + aT['A'][2][2]: return nSTATE_2
              if aU[t] > aT['A'][2][1] + aT['A'][2][2] and aU[t] <= aT['A'][2][0] + aT['A'][2][1] + aT['A'][2][2]: return nSTATE_0
              if aU[t] > aT['A'][2][0] + aT['A'][2][1] + aT['A'][2][2] and aU[t] <= 1.0: return nSTATE_3

           if e == 3:
              if aU[t] >= 0.0 and aU[t] <= aT['A'][3][1]: return nSTATE_1
              if aU[t] > aT['A'][3][1] and aU[t] <= aT['A'][3][1] + aT['A'][3][3]: return nSTATE_3
              if aU[t] > aT['A'][3][1] + aT['A'][3][3] and aU[t] <= aT['A'][3][1] + aT['A'][3][3] + aT['A'][3][0]: return nSTATE_0
              if aU[t] > aT['A'][3][1] + aT['A'][3][3] + aT['A'][3][0] and aU[t] <= 1.0: return nSTATE_2


        if cAQI == 'P':
           if e == 0:
              if aU[t] >= 0.0 and aU[t] <= aT['P'][0][2]: return nSTATE_2
              if aU[t] > aT['P'][0][2] and aU[t] <= aT['P'][0][2] + aT['P'][0][3]: return nSTATE_3
              if aU[t] > aT['P'][0][2] + aT['P'][0][3] and aU[t] <= aT['P'][0][1] + aT['P'][0][2] + aT['P'][0][3]: return nSTATE_1
              if aU[t] > aT['P'][0][1] + aT['P'][0][2] + aT['P'][0][3] and aU[t] <= 1.0: return nSTATE_0

           if e == 1:
              if aU[t] >= 0.0 and aU[t] <= aT['P'][1][2]: return nSTATE_2
              if aU[t] > aT['P'][1][2] and aU[t] <= aT['P'][1][2] + aT['P'][1][3]: return nSTATE_3
              if aU[t] > aT['P'][1][2] + aT['P'][1][3] and aU[t] <= aT['P'][1][1] + aT['P'][1][2] + aT['P'][1][3]: return nSTATE_1
              if aU[t] > aT['P'][1][1] + aT['P'][1][2] + aT['P'][1][3] and aU[t] <= 1.0: return nSTATE_0

           if e == 2:
              if aU[t] >= 0.0 and aU[t] <= aT['P'][2][2]: return nSTATE_2
              if aU[t] > aT['P'][2][2] and aU[t] <= aT['P'][2][2] + aT['P'][2][3]: return nSTATE_3
              if aU[t] > aT['P'][2][2] + aT['P'][2][3] and aU[t] <= aT['P'][2][1] + aT['P'][2][2] + aT['P'][2][3]: return nSTATE_1
              if aU[t] > aT['P'][2][1] + aT['P'][2][2] + aT['P'][2][3] and aU[t] <= 1.0: return nSTATE_0

           if e == 3:
              if aU[t] >= 0.0 and aU[t] <= aT['P'][3][2]: return nSTATE_2
              if aU[t] > aT['P'][3][2] and aU[t] <= aT['P'][3][2] + aT['P'][3][3]: return nSTATE_3
              if aU[t] > aT['P'][3][2] + aT['P'][3][3] and aU[t] <= aT['P'][3][1] + aT['P'][3][2] + aT['P'][3][3]: return nSTATE_1
              if aU[t] > aT['P'][3][1] + aT['P'][3][2] + aT['P'][3][3] and aU[t] <= 1.0: return nSTATE_0


        if cAQI == 'E':
           if e == 0:
              if aU[t] >= 0.0 and aU[t] <= aT['E'][0][3]: return nSTATE_3
              if aU[t] > aT['E'][0][3] and aU[t] <= aT['E'][0][3] + aT['E'][0][2]: return nSTATE_2
              if aU[t] > aT['E'][0][3] + aT['E'][0][2] and aU[t] <= aT['E'][0][3] + aT['E'][0][2] + aT['E'][0][1]: return nSTATE_1
              if aU[t] > aT['E'][0][3] + aT['E'][0][2] + aT['E'][0][1] and aU[t] <= 1.0: return nSTATE_0

           if e == 1:
              if aU[t] >= 0.0 and aU[t] <= aT['E'][1][3]: return nSTATE_3
              if aU[t] > aT['E'][1][3] and aU[t] <= aT['E'][1][3] + aT['E'][1][1]: return nSTATE_1
              if aU[t] > aT['E'][1][3] + aT['E'][1][1] and aU[t] <= aT['E'][1][3] + aT['E'][1][1] + aT['E'][1][2]: return nSTATE_1
              if aU[t] > aT['E'][1][3] + aT['E'][1][1] + aT['E'][1][2] and aU[t] <= 1.0: return nSTATE_0

           if e == 2:
              if aU[t] >= 0.0 and aU[t] <= aT['E'][2][3]: return nSTATE_3
              if aU[t] > aT['E'][2][3] and aU[t] <= aT['E'][2][3] + aT['E'][2][2]: return nSTATE_2
              if aU[t] > aT['E'][2][3] + aT['E'][2][2] and aU[t] <= aT['E'][2][3] + aT['E'][2][2] + aT['E'][2][1]: return nSTATE_1
              if aU[t] > aT['E'][2][3] + aT['E'][2][2] + aT['E'][2][1] and aU[t] <= 1.0: return nSTATE_0

           if e == 3:
              if aU[t] >= 0.0 and aU[t] <= aT['E'][3][3]: return nSTATE_3
              if aU[t] > aT['E'][3][3] and aU[t] <= aT['E'][3][3] + aT['E'][3][2]: return nSTATE_2
              if aU[t] > aT['E'][3][3] + aT['E'][3][2] and aU[t] <= aT['E'][3][3] + aT['E'][3][2] + aT['E'][3][1]: return nSTATE_1
              if aU[t] > aT['E'][3][3] + aT['E'][3][2] + aT['E'][3][1] and aU[t] <= 1.0: return nSTATE_0

        return nSTATE_0

    for t in range(0,nSamples):
        aX[t] = Next_State(t,nE)
        nE = aX[t]

    return Get_MP(cAQI,aX)

def Get_Trama():
    cAIQ = RA.choice(['B','A','P','E'])
    if cAIQ == 'B': return Markov_Dist('B')
    if cAIQ == 'A': return Markov_Dist('A')
    if cAIQ == 'P': return Markov_Dist('P')
    if cAIQ == 'E': return Markov_Dist('E')

dCFG_SA = {
           'SA0' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA1' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA2' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA3' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA4' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA5' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA6' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA7' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA8' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ],
           'SA9' : [ [ +32.00000000,-32.00000000 ], Get_Trama() ]
          }

print(dCFG_SA)