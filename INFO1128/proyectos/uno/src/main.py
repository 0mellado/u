from PIL import Image
import numpy as np
from numpy import asarray


imgs_path = '/home/oscar/Documentos/u/INFO1128/proyectos/uno/imgs/'


def prueba(Img: Image):
    src = asarray(Img)
    new_src = np.zeros((len(src), len(src[0])), int)

    for u in range(len(src)):
        for v in range(len(src[u])):
            if src[u][v][0] == src[u][v][1] == src[u][v][2]:
                continue
            new_src[u][v] = 1

    print(' ' * 4, end='')
    for u in range(len(new_src[0])):
        print('%2d' % u, end='')
    print()
    print()
    for v in range(len(new_src)):
        print('%2d' % v, end='   ')
        for u in range(len(new_src[v])):
            print(new_src[v][u], end=' ')
        print()


def to_binary_region(I: list) -> list:
    new_src = np.zeros((len(I), len(I[0])), int)

    for v in range(len(I)):
        for u in range(len(I[v])):
            if I[v][u][0] == I[v][u][1] == I[v][u][2]:
                continue
            new_src[v][u] = 1

    return new_src


def m(I: list, p: int, q: int) -> int:
    mpq = 0
    for v in range(len(I)):
        for u in range(len(I[v])):
            if (I[v][u] == 0):
                continue
            mpq += (u**p) * (v**q)

    return mpq


def centroide(I: list) -> tuple:
    m00 = m(I, 0, 0)
    return (np.round(m(I, 1, 0) / m00), np.round(m(I, 0, 1) / m00))


def u(I: list, p: int, q: int) -> float:
    x = centroide(I)
    upq = 0
    for v in range(len(I)):
        for u in range(len(I[v])):
            if (I[v][u] == 0):
                continue
            upq += ((u - x[0])**p) * ((v - x[1])**q)

    return upq


def u_n(I: list, p: int, q: int) -> float:
    return u(I, p, q) * ((1 / u(I, 0, 0)) ** ((p + q + 2) / 2))


def Hu1(I: list):
    return u_n(I, 2, 0)


def marca_centroide(I, centroide: tuple):
    u = int(centroide[0] - 4)
    v = int(centroide[1] - 4)

    while u < centroide[0] + 5:
        I[int(centroide[1])][u] = [0, 0, 0]
        u += 1

    while v < centroide[1] + 5:
        I[v][int(centroide[0])] = [0, 0, 0]
        v += 1


def uno_a(I: Image):
    print('Calculos de la figura 1a: ')
    src = asarray(I)
    new_src = to_binary_region(src)

    x = centroide(new_src)

    marca_centroide(src, x)
    new = Image.fromarray(src)
    new.save(imgs_path + 'new_a.png')

    print('  Área: ', m(new_src, 0, 0))
    print('  El centroide de la imagen esta en las coordenadas: ', x)
    print('  El centroide fue marcado y guardado en la imagen :',
          imgs_path + 'new_a.png')


def uno_b(I: Image):
    print('Calculos de la figura 1b: ')
    src = asarray(I)
    new_src = to_binary_region(src)

    m23 = m(new_src, 2, 3)
    u23 = u(new_src, 2, 3)
    u_n23 = u_n(new_src, 2, 3)

    print('  El momento de orden p = 2 y q = 3:', m23)
    print('  El momento central de orden p = 2 y q = 3:', u23)
    print('  El momento central normalizado de orden p = 2 y q = 3:', u_n23)


def uno_c(I: Image):
    pass


def main():
    a = Image.open(imgs_path + 'a.png')
    b = Image.open(imgs_path + 'b.png')
    c = Image.open(imgs_path + 'c.png')
    uno_a(a)
    uno_b(b)
    uno_c(c)


if __name__ == '__main__':
    main()
