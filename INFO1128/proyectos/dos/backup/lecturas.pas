unit lecturas;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazSerial, Forms, Controls, Graphics, Dialogs, Buttons,
  StdCtrls, ExtCtrls, TAGraph, TASeries;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel: TPanel;
    Chart1: TChart;
    datos: TLineSeries;
    Label1: TLabel;
    Serial: TLazSerial;

    procedure gridOnOff(Sender: TObject);
    procedure saveGraph(Sender: TObject);
    procedure createForm(Sender: TObject);
    procedure destroyForm(Sender: TObject);
    procedure SerialRxData(Sender: TObject);
    procedure beforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
              const ARect: TRect; var ADoDefaultDrawing: Boolean);
    procedure changeBackImage(Sender: TObject);
  private
    backImages: array[1..6] of TPicture;
  public

  end;

var
  Form1: TForm1;
  grid_on: Boolean;
  count_data: integer;
  count_img: integer;
  front: integer;
  a_data: array[0..29] of integer;


implementation

{$R *.lfm}



{ TForm1 }

procedure TForm1.createForm(Sender: TObject);
var i: integer;
begin
  Serial.Active := True;
  count_data := 0;
  count_img := 1;
  for i := 1 to 6 do
    begin
      backImages[i] := TPicture.Create;
      backImages[i].LoadFromFile('./imgs/img'+IntToStr(i)+'.png');
    end;
end;

procedure TForm1.destroyForm(Sender: TObject);
var
  i: integer;
begin
  Serial.Free;
  for i := 1 to 6 do
    begin
      backImages[i].Free;
    end;
end;
                         

procedure TForm1.SerialRxData(Sender: TObject);
var
  str: string;
  data, i : integer;
begin
  if (count_data < 30) then
    begin
      str :=  Serial.ReadData();
      data := StrToInt(str);
      datos.AddXY(count_data, data, str);
      a_data[count_data] := data;
      count_data := count_data + 1;

    end
  else
    begin
      datos.clear;
      data := StrToInt(Serial.ReadData());
      a_data[front] := data;
      front := front + 1;
      front := front mod 30;
      i := front;
      while (i < (front + 30)) do
        begin
          datos.AddXY(i - front, a_data[i mod 30], IntToStr(a_data[i mod 30]));
          i := i + 1;
        end;
    end;
end;

procedure TForm1.gridOnOff(Sender: TObject);
begin
  if grid_on then
    begin
      Chart1.LeftAxis.Grid.Visible := false;
      Chart1.BottomAxis.Grid.Visible := false;
      grid_on := false;
    end
  else
    begin
      Chart1.LeftAxis.Grid.Visible := true;
      Chart1.BottomAxis.Grid.Visible := true;
      grid_on := true;
    end;
end;

procedure TForm1.saveGraph(Sender: TObject);
begin
  Chart1.SaveToBitmapFile('./saves/captura.png');
end;

procedure TForm1.beforeDrawBackWall(ASender: TChart; ACanvas: TCanvas;
              const ARect: TRect; var ADoDefaultDrawing: Boolean);
begin
  ACanvas.StretchDraw(ARect, backImages[count_img].Graphic);
  ADoDefaultDrawing := false;
end;

procedure TForm1.changeBackImage(Sender: TObject);
var
  i: integer;
begin
  count_img := count_img + 1;
  if (count_img > 6) then
    begin
      count_img := 1;
    end;
  if (count_data < 30) then
    begin
      i := 0;
      datos.Clear;
      while (i < count_data) do
        begin
          datos.AddXY(i, a_data[i], IntToStr(a_data[i]));
          i := i + 1;
        end;
    end
  else
    begin
      i := front;
      datos.Clear;
      while (i < (front + 30)) do
        begin
          datos.AddXY(i - front, a_data[i mod 30], IntToStr(a_data[i mod 30]));
          i := i + 1;
        end;
    end;


end;

end.

