import serial
from time import sleep
from random import randint


def main():
    ser = serial.Serial('/dev/pts/2')

    try:
        while 1:
            val = randint(1, 200)
            string = "%3d" % val
            print('[*] Enviando:', string)
            ser.write(string.encode())
            sleep(0.5)

    except KeyboardInterrupt:
        print('\n[*] Saliendo ...')
        ser.close()


if __name__ == '__main__':
    main()
