from serial import Serial


def main():
    ser = Serial('/dev/pts/1')

    ser.write(b'GET')
    val = ser.read(3)
    print(int(val.decode()))

    ser.close()


if __name__ == '__main__':
    main()
