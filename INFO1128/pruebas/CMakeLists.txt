cmake_minimum_required(VERSION 3.27.0)
project(images VERSION 0.1.0 LANGUAGES C CXX)

include(CTest)

find_package(PNG REQUIRED)

set (LIBVISO2_SRC_DIR src)

include_directories(${PNG_INCLUDE_DIRS})
include_directories("${LIBVISO2_SRC_DIR}")

link_directories(${PNG_LIBRARY_DIRS})
add_definitions(${PNG_DEFINITIONS})

set(CMAKE_C_COMPILER "gcc")
set(CMAKE_CXX_COMPILER "g++")
include_directories(
        ${PROJECT_SOURCE_DIR}/include
)

enable_testing()

file(GLOB all_SRCS
    "${PROJECT_SOURCE_DIR}/src/*.cpp"
)


add_executable(images ${all_SRCS})
target_link_libraries (images ${PNG_LIBRARY})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

include(CPack)
  