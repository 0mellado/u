#ifndef IMAGELOAD_HPP
#define IMAGELOAD_HPP

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <png.h>

class ImageLoad
{
private:
    std::size_t width_;
    std::size_t height_;
    u_int16_t color_type_;
    u_int16_t bit_depth_;
    png_bytep *rows_pointers;
    unsigned short k;
    char *file_name_;

private:
    void write_new_image();
    void write_new_image(const char *src_name);
    void write_new_image(png_bytep *rows);
    void write_new_image(const char *src_name, png_bytep *rows);

    u_int8_t px_size();

public:
    ImageLoad();
    ImageLoad(const char *src_image);
    ~ImageLoad();

    u_int16_t bit_depth() { return bit_depth_; }
    unsigned short get_k() { return k; }
    u_int16_t color_type() { return color_type_; }
    std::size_t width() { return width_; }
    std::size_t height() { return height_; }

    static void write_image(const char *src_name,
                            png_bytep *rows,
                            u_int16_t bit_depth,
                            std::size_t width,
                            std::size_t height);

    void inv();

    void split();

    png_bytep *copy();

    unsigned char **get_r();
    unsigned char **get_g();
    unsigned char **get_b();

    unsigned int *get_hist_r();
    unsigned int *get_hist_g();
    unsigned int *get_hist_b();

    unsigned int *get_hist_r(unsigned int b);
    unsigned int *get_hist_g(unsigned int b);
    unsigned int *get_hist_b(unsigned int b);
};

static unsigned char **get_plane(ImageLoad &self, const char plane)
{
    u_int8_t index_plane = plane == 'r' ? 0 : plane == 'g' ? 1
                                          : plane == 'b'   ? 2
                                                           : 0;

    png_bytep *rows_pointers = self.copy();
    unsigned char **rows = (unsigned char **)malloc(sizeof(unsigned char *) * self.height());

    for (std::size_t u{}; u < self.height(); u++)
    {
        rows[u] = (unsigned char *)malloc(self.width());
        for (std::size_t v{}; v < self.width(); v++)
        {
            rows[u][v] = (&(rows_pointers[u][v * 4]))[index_plane];
        }
    }

    return rows;
}

static unsigned int *get_hist(ImageLoad &self, const char plane)
{
    std::size_t height = self.height();
    std::size_t width = self.width();

    unsigned char **I = get_plane(self, plane);

    unsigned short k = self.get_k();

    unsigned int *hist = (unsigned int *)malloc(sizeof(unsigned int) * k);

    memset(hist, 0, sizeof(unsigned int) * k);

    for (std::size_t u{}; u < height; u++)
    {
        for (std::size_t v{}; v < width; v++)
        {
            unsigned char i = I[u][v];
            hist[i]++;
        }
    }

    return hist;
}

static unsigned int *get_hist(ImageLoad &self, const char plane, unsigned int b)
{
    std::size_t height = self.height();
    std::size_t width = self.width();

    unsigned short k = self.get_k();

    unsigned short kb = k / b;

    unsigned int *hist_binning = (unsigned int *)malloc(sizeof(unsigned int) * b);

    memset(hist_binning, 0, sizeof(unsigned int) * b);

    unsigned char **I = self.get_r();

    for (std::size_t u{}; u < height; u++)
    {
        for (std::size_t v{}; v < width; v++)
        {
            unsigned char i = I[u][v];
            hist_binning[i / kb]++;
        }
    }

    return hist_binning;
}

#endif