#include <image.hpp>
#include <chrono>

ImageLoad::ImageLoad(const char *src_image)
{
    file_name_ = (char *)malloc(sizeof(src_image));

    memcpy(file_name_, src_image, sizeof(src_image));

    FILE *png_file = fopen(src_image, "rb");

    if (!png_file)
    {
        std::cerr << "Error: Archivo o directorio no encontrado\n";
        abort();
    }

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png)
        abort();

    png_infop info = png_create_info_struct(png);
    if (!info)
        abort();

    if (setjmp(png_jmpbuf(png)))
        abort();

    png_init_io(png, png_file);

    png_read_info(png, info);

    width_ = png_get_image_width(png, info);
    height_ = png_get_image_height(png, info);
    color_type_ = png_get_color_type(png, info);
    bit_depth_ = png_get_bit_depth(png, info);
    k = 1 << bit_depth_;

    if (bit_depth_ == 16)
        png_set_strip_16(png);
    // /*
    if (color_type_ == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png);

    if (color_type_ == PNG_COLOR_TYPE_GRAY && bit_depth_ < 8)
        png_set_expand_gray_1_2_4_to_8(png);

    if (png_get_valid(png, info, PNG_INFO_tRNS))
        png_set_tRNS_to_alpha(png);

    if (color_type_ == PNG_COLOR_TYPE_RGB || color_type_ == PNG_COLOR_TYPE_GRAY || color_type_ == PNG_COLOR_TYPE_PALETTE)
        png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

    if (color_type_ == PNG_COLOR_TYPE_GRAY || color_type_ == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png);
    // */
    png_read_update_info(png, info);

    rows_pointers = (png_bytep *)malloc(sizeof(png_bytep) * height_);

    for (std::size_t y{}; y < height_; y++)
        rows_pointers[y] = (png_byte *)malloc(png_get_rowbytes(png, info));

    png_read_image(png, rows_pointers);

    fclose(png_file);
}

ImageLoad::ImageLoad() {}

ImageLoad::~ImageLoad()
{
    free(rows_pointers);
    free(file_name_);
}

void ImageLoad::write_image(const char *src_name,
                            png_bytep *rows,
                            u_int16_t bit_depth,
                            std::size_t width,
                            std::size_t height)
{
    FILE *png_file = fopen(src_name, "wb");

    if (!png_file)
        abort();

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png)
        abort();

    png_infop info = png_create_info_struct(png);

    if (!info)
        abort();

    if (setjmp(png_jmpbuf(png)))
        abort();

    png_init_io(png, png_file);
    png_set_IHDR(png, info, width, height, bit_depth, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png, info);

    png_write_image(png, rows);
    png_write_end(png, NULL);

    fclose(png_file);
}

void ImageLoad::write_new_image(const char *src_name, png_bytep *rows)
{
    write_image(src_name, rows, bit_depth_, width_, height_);
}

void ImageLoad::write_new_image(png_bytep *rows)
{
    write_image(file_name_, rows, bit_depth_, width_, height_);
}

void ImageLoad::write_new_image(const char *src_name)
{
    write_image(src_name, rows_pointers, bit_depth_, width_, height_);
}

void ImageLoad::write_new_image()
{
    write_image(file_name_, rows_pointers, bit_depth_, width_, height_);
}

u_int8_t ImageLoad::px_size()
{
    if (color_type_ == 0)
        return 1;
    if (color_type_ == 2)
        return 3;
    if (color_type_ == 3)
        return 1;
    if (color_type_ == 4)
        return 2;
    if (color_type_ == 6)
        return 4;
    return 4;
}

void ImageLoad::inv()
{
    u_int8_t value_color_type = px_size();

    for (std::size_t y{}; y < height_; y++)
    {
        png_bytep row = rows_pointers[y];
        for (std::size_t x{}; x < width_; x++)
        {
            png_bytep px = &(row[x * value_color_type]);

            px[0] = ~px[0];
            px[1] = ~px[1];
            px[2] = ~px[2];
        }
    }

    write_new_image("../imgs/image_inv.png");
}

void ImageLoad::split()
{
    png_bytep *rows_pointers_r = (png_bytep *)malloc(sizeof(png_bytep) * height_);
    png_bytep *rows_pointers_g = (png_bytep *)malloc(sizeof(png_bytep) * height_);
    png_bytep *rows_pointers_b = (png_bytep *)malloc(sizeof(png_bytep) * height_);

    for (std::size_t y{}; y < height_; y++)
    {
        rows_pointers_r[y] = (png_byte *)malloc(width_ * 4);
        rows_pointers_g[y] = (png_byte *)malloc(width_ * 4);
        rows_pointers_b[y] = (png_byte *)malloc(width_ * 4);

        for (std::size_t x{}; x < width_; x++)
        {
            png_bytep pixel = &(rows_pointers[y][x * 4]);
            png_bytep pixel_r = &(rows_pointers_r[y][x * 4]);
            png_bytep pixel_g = &(rows_pointers_g[y][x * 4]);
            png_bytep pixel_b = &(rows_pointers_b[y][x * 4]);

            memcpy(pixel_r, pixel, 4);
            memcpy(pixel_g, pixel, 4);
            memcpy(pixel_b, pixel, 4);

            pixel_r[1] = 0;
            pixel_r[2] = 0;

            pixel_g[0] = 0;
            pixel_g[2] = 0;

            pixel_b[0] = 0;
            pixel_b[1] = 0;
        }
    }

    write_new_image("../imgs/r.png", rows_pointers_r);
    write_new_image("../imgs/g.png", rows_pointers_g);
    write_new_image("../imgs/b.png", rows_pointers_b);

    free(rows_pointers_r);
    free(rows_pointers_g);
    free(rows_pointers_b);
}

png_bytep *ImageLoad::copy()
{
    png_bytep *rows_dest = (png_bytep *)malloc(sizeof(png_bytep) * height_);

    for (std::size_t u{}; u < height_; u++)
    {
        rows_dest[u] = (png_byte *)malloc(sizeof(png_byte) * width_ * 4);
        for (std::size_t v{}; v < width_; v++)
        {
            png_bytep pixel = &(rows_pointers[u][v * 4]);
            png_bytep pixel_dest = &(rows_dest[u][v * 4]);

            memcpy(pixel_dest, pixel, 4);
        }
    }
    return rows_dest;
}

unsigned char **ImageLoad::get_r() { return get_plane(*this, 'r'); }
unsigned char **ImageLoad::get_g() { return get_plane(*this, 'g'); }
unsigned char **ImageLoad::get_b() { return get_plane(*this, 'b'); }

unsigned int *ImageLoad::get_hist_r() { return get_hist(*this, 'r'); }
unsigned int *ImageLoad::get_hist_g() { return get_hist(*this, 'g'); }
unsigned int *ImageLoad::get_hist_b() { return get_hist(*this, 'b'); }

unsigned int *ImageLoad::get_hist_r(unsigned int b) { return get_hist(*this, 'r', b); }
unsigned int *ImageLoad::get_hist_g(unsigned int b) { return get_hist(*this, 'g', b); }
unsigned int *ImageLoad::get_hist_b(unsigned int b) { return get_hist(*this, 'b', b); }