#include <Avr/interrupt.h>
#include <avr/io.h>
#include <xc.h>

void my_setup() {
    cli();         
    DDRB |= 0x01;  
    TCCR0B |= 0x01;
    TIMSK0 |= 0x01;
    TCNT0 = 0;     
    sei();         
}

ISR(TIMER0_OVF_vect) {
    PORTB ^= (1 << PB0);
}

int main(void) {

    my_setup();

    while (1);
}
