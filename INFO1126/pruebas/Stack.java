
public class Stack <E> {
    private E[] stack;
    private int count;

    public Stack(int capacity) {
        stack = (E[]) new Object[capacity];
        count = 0;
    }

    public int size() {
        return count;
    }

    public boolean empty() {
        return count == 0;
    }

    public boolean push(E e) {
        if (!(count < stack.length)) { return false; }
        stack[count] = e;
        count++;
        return true;
    }

    public E peek() {
        if (empty()) { return null; }
        return stack[count - 1];
    }

    public E pop() {
        if (empty()) { return null; }
        count--;
        return stack[count];
    }
}
