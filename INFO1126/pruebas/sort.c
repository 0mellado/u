#include <stdio.h>

void sort(int*, int);

struct Node {
    Node left, right;
};

int main() {

    int numbers[] = {3, 5, 8, 32, 43, 2, 23, 98};

    sort(numbers, 8);

    return 0;
}
