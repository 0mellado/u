#include <iostream>

unsigned long powerR(unsigned long int b, unsigned long exp) {
    if (exp == 0)
        return 1;
    /* if (exp == 1) */
    /*     return b; */

    return b * powerR(b, exp - 1);
}

unsigned long power(unsigned long b, unsigned long exp) {
    unsigned long power = 1;
    while (exp > 0) {
        power = power * b;
        exp--;
    }
    return power;
}


int main() {

    /* std::printf("%lu\n", powerR(2, 63)); */
    std::printf("%lu\n", power(2, 63));

    return 0;
}
