#include <iostream>


int multR(int n, int c) {
    if (c == 0)
        return 0;
    if (c == 1)
        return n;

    return n + multR(n, c - 1);
}


int main() {
    std::printf("%d\n", multR(4, 2));

    return 0;
}
