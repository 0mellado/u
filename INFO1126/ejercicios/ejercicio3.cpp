#include <iostream>

int mcdR(int n1, int n2) {
    if (n2 == 0)
        return n1;

    return mcdR(n2, n1 % n2);
}

int main() {

    std::printf("%d\n", mcdR(432, 342890));

    return 0;
}
