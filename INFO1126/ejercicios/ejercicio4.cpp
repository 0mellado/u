#include <iostream>

int palR(int *arr, int begin, int end) {

    if (begin >= end) 
        return true;
    if (arr[begin] == arr[end])
        return palR(arr, begin + 1, end - 1);

    return false;
}


int main() {

    int arr[] = {4, 4, 3, 4};

    std::printf("%d\n", palR(arr, 0, 3));

    return 0;
}
