#include <iostream>


struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    TreeNode* searchBST(TreeNode* root, int val) {
        if (root == nullptr || root->val == val)
            return root;
        if (val < root->val)
            return searchBST(root->left, val);
        else
            return searchBST(root->right, val);
    }
};


int main() {

    TreeNode* right3 = new TreeNode(84);
    TreeNode* right2 = new TreeNode(63, nullptr, right3);

    TreeNode* right1 = new TreeNode(22, nullptr, right2);
    TreeNode* left1 = new TreeNode(2);

    TreeNode* root = new TreeNode(18, left1, right1);

    Solution* ejercicio = new Solution();

    TreeNode* result = ejercicio->searchBST(root, 63);

    std::cout << result->val << "\n";
    std::cout << (result->left != nullptr ? result->left->val : -1) << "\n";
    std::cout << (result->right != nullptr ? result->right->val : -1) << "\n";

    delete right3;
    delete right2;

    delete left1;
    delete right1;

    delete root;

    delete ejercicio;

    return 0;
}
