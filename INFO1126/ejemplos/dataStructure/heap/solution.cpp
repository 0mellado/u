#include <iostream>
#include <vector>
#include <queue>

class KthLargest {
    private:
        int k;
        std::priority_queue<int> heap;
    public:
        KthLargest(int _k, std::vector<int>& nums) {
            k = _k;
            for (int i : nums) add(i);
        }
    
        int add(int val) {
            if (heap.size() < k)
                heap.push(-val);
            else if (-heap.top() < val) {
                heap.pop();
                heap.push(-val);
            }

            return -heap.top();
        }
};

int main() {

    std::vector<int> nums {1, 3, 4, 7, 2};

    KthLargest *obj = new KthLargest(5, nums);

    std::cout << obj->add(-1) << "\n";

    return 0;
}
