#include <iostream>


template <typename T>
class Queue_array {
    private:
        T *queues;
        int length, count, first;

    public:
        Queue_array(int capacity) {
            if (capacity <= 1)
                throw "No se pueden ingresar valores menores a 1\n";
            
            queues = (T*)malloc(sizeof(T) * capacity);
            count = 0;
            length = capacity;
            first = 0;
        }

        bool inqueue(T element) {
            if (count == length)
                return false;

            queues[(first + count) % length] = element;
            count++;
            return true;
        }

        bool dequeue() {
            if (empty()) return false;

            first = (first + 1) % length;
            count--;
            return true;
        }

        bool empty() { return count == 0; }

        int size() { return count; }

        int search(T element) { 
            bool find = false;
            int i = first; 
            while (i < count && !find) {
                if (queues[i % length] == element)
                    find = true;
                else i++;
            }
            return i - first;
        }

        bool isFull() {
            return count == length;
        }

        void printQueue() {
            std::printf("[ ");
            for (int i = first; i < (count + first); i++)
                std::printf("%d ", queues[i % length]);
            std::printf("]\n");
        }

        T Rear() {
            if (empty()) return NULL;
            return queues[(first + count - 1) % length];
        }

};


int main() {

    Queue_array<int> *myCircularQueue = new Queue_array<int>(3);
    std::cout << myCircularQueue->inqueue(1) << "\n";// return True
    std::cout << myCircularQueue->inqueue(2) << "\n";// return True
    std::cout << myCircularQueue->inqueue(3) << "\n";// return True
    std::cout << myCircularQueue->inqueue(4) << "\n";// return False
    std::cout << myCircularQueue->Rear()     << "\n";// return 3
    std::cout << myCircularQueue->isFull()   << "\n";// return True
    std::cout << myCircularQueue->dequeue()  << "\n";// return True
    std::cout << myCircularQueue->inqueue(4) << "\n";// return True
    std::cout << myCircularQueue->Rear()     << "\n";// return 4

    return 0;
}
