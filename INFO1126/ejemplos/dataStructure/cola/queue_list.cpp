#include <iostream>

template <typename T>
struct Node {
    T data;
    Node *next;
};

template <typename T>
class Linked_list {
    private:
        Node<T> *head;

    public:
        Linked_list() {
            head = NULL;
        }

        int length() {
            int len = 0;
            Node<T> *node = head;
            while (node != NULL) {
                node = node->next;
                len++;
            }
            return len;
        }

        T shift() {
            if (head == NULL) throw "La lista esta vacía\n";
            T data = head->data;

            Node<T> *del_node = head;

            head = del_node->next;

            free(del_node);

            return data;
        }

        T pop() {
            if (head == NULL) throw "La lista esta vacía\n";

            Node<T> *prev_del_node = head;

            while (prev_del_node->next->next != NULL)
                prev_del_node = prev_del_node->next;

            T data = prev_del_node->next->data;

            free(prev_del_node->next->next);

            prev_del_node->next = NULL;

            return data;
        }

        T del(int index) {

            if (head == NULL) throw "La lista esta vacía\n";
            if (index > length()) throw "Índice fuera de rango\n";
            if (index == 0) return shift();
            if (index == (length() - 1)) return pop();
            std::printf("%d\n", length());

            Node<T> *prev_del_node = head;

            int i = 0;
            while ((i-1) < index && prev_del_node->next != NULL) {
                prev_del_node = prev_del_node->next;
                i++;
            }

            return prev_del_node->data;
        }

        void push(T data) {
            Node<T> *new_node = (Node<T>*)malloc(sizeof(Node<T>*));

            new_node->data = data;

            new_node->next = head;
            head = new_node;
            return;
        }

        void append(T data) {
            Node<T> *new_node = (Node<T>*)malloc(sizeof(Node<T>*));

            new_node->data = data;
            new_node->next = NULL;

            if (head == NULL) {
                head = new_node;
                return;
            }

            Node<T> *last = head;

            while (last->next != NULL)
                last = last->next;

            last->next = new_node;
            return;
        }

        void printList() {
            Node<T>* node = head;
            std::printf("[ ");
            while (node != NULL) {
                std::cout << node->data << " ";
                node = node->next;
            }
            std::printf("]\n");
        }
};

template <typename T>
class Queue_list {
    private:
        T *queues;
        int length, count, first;

    public:
        Queue_list(int capacity) {
            if (capacity <= 1)
                throw "No se pueden ingresar valores menores a 1\n";
            
            queues = (T*)malloc(sizeof(T) * capacity);
            count = 0;
            length = capacity;
            first = 0;
        }

        bool inqueue(T *element) {
            if (count == length)
                throw "Cola llena\n";

            queues[(first + count) % length] = *element;
            count++;
            return true;
        }

        T dequeue() {
            if (empty()) throw "Cola vacia\n";

            T data = queues[first];
            first = (first + 1) % length;
            count--;
            return data;
        }

        bool empty() { return count == 0; }

        int size() { return count; }

        int search(T element) { 
            bool find = false;
            int i = first; 
            while (i < count && !find) {
                if (queues[i % length] == element)
                    find = true;
                else i++;
            }
            return i - first;
        }

        void printQueue() {
            std::printf("[ ");
            for (int i = first; i < (count + first); i++)
                std::printf("%d ", queues[i % length]);
            std::printf("]\n");
        }
};

int main() {

    return 0;
}
