#include <iostream>

class MyCircularQueue {
private:
    int *queues;
    int length, count, first;

public:
    MyCircularQueue(int k) {
        if (k < 1)
            throw "No se pueden ingresar valores menores a 1\n";
            
        queues = (int*)malloc(sizeof(int) * k);
        count = 0;
        length = k;
        first = 0;
    }
    
    bool enQueue(int data) {
        if (isFull()) return false;

        queues[(first + count) % length] = data;
        count++;
        return true;
    }
    
    bool deQueue() {
        if (isEmpty()) return false;

        first = (first + 1) % length;
        count--;
        return true;    
    }
    
    int Front() {
        if (isEmpty()) return -1;
        return queues[first];
    }
    
    int Rear() {
        if (isEmpty()) return -1;
        return queues[(first + count - 1) % length];
    }
    
    bool isEmpty() {
        return count == 0;
    }
    
    bool isFull() {
        return count == length;
    }
};

int main() {

    MyCircularQueue myCircularQueue = MyCircularQueue(1);
    std::cout << myCircularQueue.enQueue(1) << "\n";// return True
    std::cout << myCircularQueue.enQueue(2) << "\n";// return True
    /* std::cout << myCircularQueue.enQueue(3) << "\n";// return True */
    /* std::cout << myCircularQueue.enQueue(4) << "\n";// return False */
    /* std::cout << myCircularQueue.Rear()     << "\n";// return 3 */
    /* std::cout << myCircularQueue.isFull()   << "\n";// return True */
    /* std::cout << myCircularQueue.deQueue()  << "\n";// return True */
    /* std::cout << myCircularQueue.enQueue(4) << "\n";// return True */
    /* std::cout << myCircularQueue.Rear()     << "\n";// return 4 */

    return 0;
}
