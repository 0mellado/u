#include <vector>

class HashTable {
    public:
        ~HashTable() {}
        HashTable();
        HashTable(std::size_t m);

        void append(int data);
        void remove(int data);
        bool search(int data);

        double alpha();
        std::size_t hash_code(int data);

        std::size_t max_size() { return max_s; }
        std::size_t size() { return count; }

        bool resize_hash(bool more);


    private:
        std::vector<int>* hash_arr;
        std::size_t max_s;
        std::size_t count;

};
