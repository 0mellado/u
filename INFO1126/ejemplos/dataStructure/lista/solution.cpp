#include <iostream>


class List {
private:
    struct Node {
        int data;
        Node *next;
        Node *prev;
    };
    Node *head, *tail;
    int length;

public:
    List() {
        head = tail = NULL;
        length = 0;
    }
    
    int get(int index) {
        if (index > length - 1) return -1;
        if (length == 0) return -1;

        Node *node = head;

        int i = 0;
        while (i != index) {
            node = node->next;
            i++;
        }

        return node->data;    
    }
    
    void addAtHead(int val) {
        Node *new_node = new Node;
        new_node->data = val;

        if (length == 0) {
            head = new_node;
            tail = new_node;
            length++;
            return;
        } 

        head->prev = new_node;
        new_node->next = head;
        head = new_node;
        length++;

        return;
    }
    
    void addAtTail(int data) {
        Node *new_node = new Node;
        new_node->data = data;

        if (length == 0) {
            head = new_node;
            tail = new_node;
            length++;
            return;
        } 

        tail->next = new_node;
        new_node->prev = tail;
        tail = new_node;
        length++;

        return; 
    }
    
    void addAtIndex(int index, int data) {
        Node *new_node = new Node;
        new_node->data = data;

        if (index > length) return;

        if (index == length) {
            addAtTail(data);
            return;
        }

        if (index == 0) {
            addAtHead(data);
            return;
        }

        if (length == 0) {
            addAtTail(data);
            return;
        } 

        Node *prev_node = head;
        Node *next_node;

        int i = 0;
        while (i < index - 1) {
            std::cout << prev_node << "\n";
            prev_node = prev_node->next;
            i++;
        }
        
        next_node = prev_node->next;

        prev_node->next = new_node;
        new_node->prev = prev_node;

        new_node->next = next_node;
        next_node->prev = new_node;
        length++;

        return;
    }
    
    void deleteAtIndex(int index) {
        if (index >= length) return;

        if (length == 0) return;

        Node *del_node, *next_node;

        if (index == 0 && length == 1) {
            head = tail = NULL;
            length--;
            return;
        }

        if (index == 0) {
            del_node = head;
            head = head->next;
            head->prev = NULL;
            length--;
            delete del_node;

            return;
        }

        if (index == length - 1) {
            del_node = tail;
            tail = tail->prev;
            tail->next = NULL;
            length--;
            delete del_node;

            return;
        }

        Node *prev_node = head;

        int i = 0;
        while (i != index - 1) {
            prev_node = prev_node->next;
            i++;
        }
        
        del_node = prev_node->next;
        next_node = del_node->next;

        prev_node->next = next_node;
        next_node->prev = prev_node;
        length--;

        delete del_node;

        return;   
    }

    void printList() {
        Node *node = head;
        std::printf("[ ");
        while (node != NULL) {
            std::cout << node->data << " ";
            node = node->next;
        }
        std::printf("]\n");
    }
};

int main() {

    List *lista = new List();

    lista->addAtHead(1);
    lista->printList();
    lista->addAtTail(3);
    lista->printList();
    lista->addAtIndex(1, 2);
    lista->printList();
    lista->get(1);
    lista->printList();
    lista->deleteAtIndex(1);
    lista->printList();
    lista->get(1);
    lista->printList();
    lista->get(3);
    lista->printList();
    lista->deleteAtIndex(3);
    lista->printList();
    lista->deleteAtIndex(0);
    lista->printList();
    lista->get(0);
    lista->printList();
    lista->deleteAtIndex(0);
    lista->printList();
    lista->get(0);

    lista->printList();

    return 0;
}
