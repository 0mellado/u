#pragma once
#ifndef LIST
#define LIST
#include <iostream>

template <class T>
struct Node {
    T data;
    Node<T> *next;
    Node<T> *prev;
};

 // A.mult(B)
 // A * B

template <class T>
class List {
    private:
        Node<T> *head, *tail;
        size_t length;

    public:
        List();
        void add(T data);
        void add(T data, int index);
        T get(int index);
        int lengthList();
        void printList();
        T operator[](std::size_t const index) const; 
};

#include "list.tpp"

#endif

