template <class T>
List<T>::List() {
    head = tail = NULL;
    length = 0;
}

template <class T>
void List<T>::add(T data) {
    Node<T> *new_node = (Node<T>*)malloc(sizeof(Node<T>*));
    new_node->data = data;

    if (length == 0) {
        head = new_node;
        tail = new_node;
        length++;
        return;
    } 

    tail->next = new_node;
    new_node->prev = tail;
    tail = new_node;
    length++;

    return;
}

template <class T>
void List<T>::printList() {
    Node<T> *node = head;
    std::printf("[ ");
    while (node != NULL) {
        std::cout << node->data << " ";
        node = node->next;
    }
    std::printf("]\n");
}

template <class T>
T List<T>::operator[](std::size_t const index) const {
    if (index > length - 1) {
        std::cerr << "Índice fuera de rango\n";
        exit(-1);
    }

    Node<T> *node = head;

    std::size_t i = 0;
    while (i != index) {
        node = node->next;
        i++;
    }

    return node->data;
}
