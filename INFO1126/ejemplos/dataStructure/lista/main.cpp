#include <iostream>

#include "list.hpp"

int main() {

    List<int> lista = List<int>();

    lista.add(3);
    lista.add(1);
    lista.add(4);
    lista.add(2);

    std::cout << lista[1] << "\n";

    return 0;
}
