#include <graph.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <map>

/*
Dado un grafo G y K colores asignar un color a cada nodo de forma
tal que los nodos adyacentes tengan distintos colores
*/

Graph::Graph()
{
    count = 0;
}

Graph::~Graph()
{
}

bool Graph::is_there(size_t id)
{
    bool find = false;
    for (size_t i{}; i < count; i++)
    {
        if (vertexes[i]->id == id)
            find = true;
    }

    return find;
}

Vertex *Graph::get_vertex(size_t id)
{

    if (!is_there(id))
    {
        std::cerr << "[Error]: Esa id no está en el grafo\n";
        return nullptr;
    }

    Vertex *find_vertex;

    for (size_t i{}; i < count; i++)
    {
        if (vertexes[i]->id == id)
            find_vertex = vertexes[i];
    }

    return find_vertex;
}

bool Graph::add_vertex(int data, size_t id)
{
    if (id == 0)
    {
        std::cerr << "[Error]: La id no puede ser igual a 0\n";
        return false;
    }

    if (is_there(id))
    {
        std::cerr << "[Error]: No pueden haber dos id iguales\n";
        return false;
    }

    Vertex *new_vertex = (Vertex *)malloc(sizeof(Vertex));

    new_vertex->data = data;
    new_vertex->id = id;

    vertexes.append(new_vertex);
    count++;
    return true;
}

bool Graph::add_edge(size_t id_src, size_t id_dest)
{
    if (count == 0)
    {
        std::cerr << "[Error]: No hay vertices para unir\n";
        return false;
    }

    if (!is_there(id_src) || !is_there(id_dest))
    {
        std::cerr << "[Error]: El origen o el destino no existen\n";
        return false;
    }

    Vertex *src_vertex = get_vertex(id_src);
    Vertex *dest_vertex = get_vertex(id_dest);

    src_vertex->adjs.append(dest_vertex);

    return true;
}

bool Graph::are_connected(size_t id_src, size_t id_dest)
{
    bool connected = false;

    for (size_t i{}; i < count; i++)
    {
        if (vertexes[i]->id != id_src)
            continue;
        for (size_t j{}; j < vertexes[i]->adjs.size(); j++)
        {
            if (vertexes[i]->adjs[j]->id != id_dest)
                continue;
            connected = true;
            j = vertexes[i]->adjs.size();
        }
        i = count;
    }

    return connected;
}

void Graph::del_edge(size_t id_src, size_t id_dest)
{
    if (!are_connected(id_src, id_dest))
    {
        std::cerr << "[Warn]: No estan conectados\n";
        return;
    }

    for (size_t i{}; i < count; i++)
    {
        if (vertexes[i]->id != id_src)
            continue;
        for (size_t j{}; j < vertexes[i]->adjs.size(); j++)
        {
            if (vertexes[i]->adjs[j]->id != id_dest)
                continue;
            vertexes[i]->adjs.remove(j);
            j = vertexes[i]->adjs.size();
        }
        i = count;
    }
}

int Graph::del_vertex(size_t id)
{
    if (!is_there(id))
    {
        std::cerr << "[Error]: Esa id no esta en el grafo\n";
        return 0;
    }
}

void Graph::print()
{
    Vertex *(*array) = vertexes.get_array();

    for (size_t i{}; i < count; i++)
    {
        std::cout << array[i]->id << ": [";
        for (size_t j{}; j < array[i]->adjs.size(); j++)
        {
            std::cout << " " << array[i]->adjs[j]->id;
        }
        std::cout << " ]\n";
    }
}

/*
void Graph::bfs(size_t id_start)
{
    std::map<Vertex, size_t> level;
    std::map<Vertex, size_t> dads;
    level.emplace(*(vertexes[id_start]), 0);
    dads.emplace(*(vertexes[id_start]), NULL);

    size_t i = 1;

    LinkedList<Vertex> frontera;

    frontera.append(*(vertexes[id_start]));

    while (!frontera.empty())
    {
        LinkedList<Vertex> proximafrontera;
        /*for (size_t u{}; u < frontera.size(); u++)
        {
            for (size_t v{}; v <)
        }
    }
}
*/

LinkedList<Vertex *> Graph::vecinos(size_t id)
{
    return vertexes[id]->adjs;
}