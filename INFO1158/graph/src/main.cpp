#include <graph.hpp>

/*
EVALUACIONES

prueba teórica -> 35%
prueba practica -> 35%

proyecto final (grupo) -> 30%

Definición del problema:
- Se tiene un grupo de grafos donde las aristas representan la compatibilidad entre los vertices.
  La meta consiste en encontrar la mayor cantidad de parejas compatibles.

- Emparejamiento perfecto: Es cuando la cantidad de parejas posibles es igual a la mitad de la cantidad de vertices.
    cantidad de parejas = |v| / 2



traditional marriage algorithm
*/

int main()
{
  Graph *grafo = new Graph();

  grafo->add_vertex(13, 1);
  grafo->add_vertex(3, 2);
  grafo->add_vertex(33, 3);
  grafo->add_vertex(122, 4);
  grafo->add_vertex(23, 5);

  grafo->add_edge(1, 2);
  grafo->add_edge(1, 5);
  grafo->add_edge(3, 1);
  grafo->add_edge(2, 3);
  grafo->add_edge(5, 3);
  grafo->add_edge(4, 3);
  grafo->add_edge(5, 4);

  grafo->add_edge(2, 5);
  grafo->add_edge(5, 2);

  grafo->print();

  return 0;
}
