#pragma once
#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <iostream>
#include <stdlib.h>

template <typename T>
struct List_node
{
    T data;
    List_node *next;
    List_node *prev;
};

template <class T>
class LinkedList
{
private:
    List_node<T> *head, *tail;
    std::size_t count;

public:
    LinkedList()
    {
        head = nullptr;
        tail = nullptr;
        count = 0;
    }

    std::size_t size() { return count; }

    bool empty() { return count == 0; }

    T operator[](std::size_t index)
    {
        if (index > count - 1)
        {
            std::cerr << "[Error]: Indice fuera de rango\n";
            exit(-1);
        }

        if (index > count / 2)
        {
            List_node<T> *node = head;
            std::size_t i = 0;
            while (i++ != index)
                node = node->next;

            return node->data;
        }

        List_node<T> *node = tail;
        std::size_t i = count - 1;
        while (i-- != index)
            node = node->prev;

        return node->data;
    }

    void append(T data)
    {
        List_node<T> *new_node = (List_node<T> *)malloc(sizeof(List_node<T>));

        new_node->data = data;

        if (empty())
        {
            head = new_node;
            tail = new_node;
            count++;
            return;
        }

        tail->next = new_node;
        new_node->prev = tail;
        tail = new_node;
        count++;

        return;
    }

    T pop()
    {
        if (empty())
        {
            std::cerr << "[Error]: La lista esta vacía\n";
            exit(-1);
        }

        List_node<T> *del_node = tail;
        tail = del_node->prev;
        tail->next = nullptr;

        T data = del_node->data;

        free(del_node);

        count--;

        return data;
    }

    T shift()
    {
        if (empty())
        {
            std::cerr << "[Error]: La lista esta vacía\n";
            exit(-1);
        }

        List_node<T> *del_node = head;
        head = del_node->next;
        head->prev = nullptr;
        T data = del_node->data;
        free(del_node);

        count--;

        return data;
    }

    void remove(size_t index)
    {
        if (empty())
        {
            std::cerr << "[Error]: La lista esta vacía\n";
            exit(-1);
        }

        if (index > count - 1)
        {
            std::cerr << "[Error]: Indice fuera de rango\n";
            return;
        }

        if (index == 0)
            shift();
        else if (index == count - 1)
            pop();

        List_node<T> *del_node = head;

        std::size_t i = 0;
        while (index != i++)
            del_node = del_node->next;

        List_node<T> *prev_node = del_node->prev;
        List_node<T> *next_node = del_node->next;

        del_node->prev = nullptr;
        del_node->next = nullptr;

        prev_node->next = next_node;
        next_node->prev = prev_node;

        count--;

        free(del_node);
    }

    T *get_array()
    {
        T *array = (T *)malloc(sizeof(T) * count);

        size_t i{};
        List_node<T> *node = head;
        while (node != nullptr)
        {
            array[i] = node->data;
            node = node->next;
            i++;
        }

        return array;
    }
};

#endif