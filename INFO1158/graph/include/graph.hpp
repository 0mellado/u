#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <linkedList.hpp>

using std::size_t;

struct Vertex
{
    int data;
    size_t id;
    LinkedList<Vertex *> adjs;
};

class Graph
{
public:
    Graph();
    ~Graph();
    bool add_vertex(int data, size_t id);
    bool add_edge(size_t id_src, size_t id_dest);
    bool is_there(size_t id);
    bool are_connected(size_t id_src, size_t id_dest);
    int del_vertex(size_t id);
    void del_edge(size_t id_src, size_t id_dest);
    void bfs(size_t id_start);

    size_t size() { return count; }
    bool empty() { return count == 0; }

    void print();

private:
    LinkedList<Vertex *> vecinos(size_t id);
    Vertex *get_vertex(size_t id);
    size_t count;
    LinkedList<Vertex *> vertexes;
};

#endif