const delete_cookie = name => {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
const getCookie = cname => {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const token = getCookie('tokenUser')
const username = getCookie('username')

const auxFetch = async (method, body) => {
    let settings = {
        method: method,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': token
        },
        body: JSON.stringify(body)
    }
    if (!body) settings = {
        method: method,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': token
        }
    }

    try {
        const response = await fetch('http://localhost:8000/foro', settings);
        const data = await response.json()
        return data
    } catch (err) {
        console.error(err)
    }
}

const sendData = message => {
    return auxFetch('POST', {
        message: message,
        username: username
    })
}

const readData = () => {
    return auxFetch('GET')
}

const logoutFunc = async () => {
    await fetch('src/logout.php', {method: 'POST'});
};

let lastIdComment;
const loadComments = async () => {
    const response = await readData();
    if (!response) window.location.replace('login.html')

    let commentsDiv = "";
    for (const comment of response) {

        const time = new Date(comment.createAt).toLocaleTimeString()
        const datetime = `${comment.createAt.slice(0, 10)} ${time}`
        if (username != comment.author) {
            commentsDiv +=
                `<div class="d-flex flex-row p-3">
                    <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                    <div class="w-100">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex flex-row align-items-center">
                                <span class="mr-2">${comment.author}</span>
                            </div>
                            <small>${datetime}</small>
                        </div>
                        <p id="msg-content" class="text-justify comment-text mb-0">${comment.message}</p>
                    </div>
                </div>
                `;
            continue;
        }
        commentsDiv +=
            `<div class="d-flex flex-row p-3">
                <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                <div id=${comment.id_comments} class="w-100">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span class="mr-2">${comment.author}</span>
                        </div>
                        <small>${datetime}</small>
                    </div>
                    <p id="msg-content" class="text-justify comment-text mb-0">${comment.message}</p>
                    <div>
                        <button class="btn btn-danger btn-sm btn-delete">borrar</button>
                        <button class="btn btn-info btn-sm btn-update">editar</button>
                    </div>
                </div>
            </div>
            `;
    }
    return commentsDiv;
}

const addCommentElement = (comment) => {
    const time = new Date(comment.createAt).toLocaleTimeString()
    const datetime = `${comment.createAt.slice(0, 10)} ${time}`
    div.innerHTML =
        `<div class="d-flex flex-row p-3">
            <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
            <div id=${comment.id} class="w-100">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-row align-items-center">
                        <span class="mr-2">${comment.author}</span>
                    </div>
                    <small>${datetime}</small>
                </div>
                <p id="msg-content" class="text-justify comment-text mb-0">${comment.message}</p>
                <div>
                    <button class="btn btn-danger btn-sm btn-delete">borrar</button>
                    <button class="btn btn-info btn-sm btn-update">editar</button>
                </div>
            </div>
        </div>
        ` + div.innerHTML;
};

