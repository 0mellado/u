const div = document.getElementById('comments-box');
const inputComment = document.getElementById('input-comment');
const logoutBtn = document.getElementById('btn-logout');
const title = document.getElementById('title')
const socket = io('ws://localhost:4100', {
    auth: {
        token: token
    }
})
let btnUpdate;
let btnDelete;

socket.on("connect_error", err => {
    console.log(err instanceof Error)
    console.log(err.message)
    console.log(err.data)
});

window.addEventListener('load', async () => {
    title.textContent = `${username} Foro`

    socket.emit('add username', username);

    const comments = await loadComments();
    div.innerHTML = comments;

    socket.emit('loading');

});


logoutBtn.addEventListener('click', async () => {
    delete_cookie('tokenUser')
    delete_cookie('username')
    window.location.replace('login.html');
});


inputComment.addEventListener('change', async event => {
    event.preventDefault();

    const response = await sendData(inputComment.value);
    if (!response) window.location.replace('login.html')

    if (!response.status) {
        alert('No se a podido mandar el comentario');
        return;
    }

    socket.emit('comment', inputComment.value);

    inputComment.value = '';

    addCommentElement(response.data);

    socket.emit('loading');
});

socket.on('new comment', data => {
    const commentsDiv =
        `<div class="d-flex flex-row p-3">
                <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                <div class="w-100">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span class="mr-2">${data.username}</span>
                        </div>
                        <small>${data.datetime}</small>
                    </div>
                    <p id="msg-content" class="text-justify comment-text mb-0">${data.content}</p>
                </div>
            </div>
            `;
    div.innerHTML = commentsDiv + div.innerHTML;
});

socket.on('loading', () => {
    btnUpdate = document.getElementsByClassName('btn-update');
    btnDelete = document.getElementsByClassName('btn-delete');

    for (let i = 0; i < btnDelete.length; i++) {
        btnDelete[i].addEventListener('click', async () => {
            const parent = btnDelete[i].parentNode.parentNode;
            const idComment = parseInt(parent.id)

            const contentHtml = parent.getElementsByTagName('p');
            const content = contentHtml[0].innerHTML;

            const dateHtml = parent.getElementsByTagName('small');
            const date = dateHtml[0].innerHTML;

            const status = auxFetch('DELETE', {
                id: idComment,
                content: content,
                createAt: date,
                author: username,
            })

            if (!status) {
                alert("no se pudo eliminar el comentario");
                return;
            };

            const comments = await loadComments();
            div.innerHTML = comments;

            socket.emit('loading');
        });

        btnUpdate[i].addEventListener('click', () => {
            const parent = btnDelete[i].parentNode.parentNode;
            const idComment = parseInt(parent.id)

            const inputForm = document.createElement('input');
            const dateHtml = parent.getElementsByTagName('small')[0];
            const contentHtml = parent.getElementsByTagName('p')[0];

            inputForm.className = 'form-control-sm';
            inputForm.type = 'text';
            inputForm.value = contentHtml.innerHTML;

            parent.replaceChild(inputForm, contentHtml);
            inputForm.addEventListener('change', async () => {
                const newContent = inputForm.value;
                const newElementP = document.createElement('p');

                const status = auxFetch('PUT', {
                    id: idComment,
                    date: dateHtml.innerHTML,
                    oldContent: contentHtml.innerHTML,
                    newContent: newContent,
                    username: username,
                })

                if (!status) {
                    alert("no se pudo editar el comentario");
                    return;
                }

                newElementP.className = "text-justify comment-text mb-0";
                newElementP.id = "msg-content";
                newElementP.textContent = newContent;

                parent.replaceChild(newElementP, inputForm);

                const comments = await loadComments();
                div.innerHTML = comments;
                socket.emit('loading');
            });
        });
    }
});
