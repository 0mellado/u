// btn guarda el elemento del boton para enviar los datos del form
const form = document.getElementById("form");
const btn = document.getElementById("btn-list-contacts");
const inputs = document.getElementsByClassName("in");
const inrut = document.getElementById("in-rut");
const statusForm = document.getElementById("status-msg");
const navItems = document.getElementsByClassName("nav-item");
const navRef = document.getElementsByClassName("nav-ref");
const tableContacts = document.getElementById("table-contacts");

let lastNav = 0;

for (let i = 0; i < 5; i++) {

    navItems[i].addEventListener("click", () => {
        navItems[lastNav].style.color = "";
        window.scrollTo({
            top: getPosSection()[i],
            left: 100,
            behavior: 'smooth'
        });
        navItems[i].style.color = "#fc7703";
        lastNav = i;
    });
}


inrut.addEventListener("keydown", () => {
    if (fn.validaRut(inrut.value)) {
        btn.style.display = "";
        statusForm.textContent = "";
    } else {
        btn.style.display = "none";
        statusForm.textContent = "El rut ingresado no es valido";
    }
});


btn.addEventListener("click", event => {
    event.preventDefault();
    getContacts();
});


// cuando el boton de enviar los datos es accionado se le
// envia una alerta al usuario
form.addEventListener("submit", async event => {
    event.preventDefault();
    const formData = new FormData(form);
    let data = {}
    formData.forEach((value, key) => {
        data[key] = value
    })

    const httpCode = await sendForm(data);

    const name = form[0].value;
    const lastname = form[1].value;
    const empname = form[2].value;
    const rut = form[3].value;
    const email = form[4].value;

    if (httpCode == 200)
        alert("Los datos fueron ingresado correctamente\n" +
            `Nombre: ${name}\n` +
            `Apellido: ${lastname}\n` +
            `Nombre de la empresa: ${empname}\n` +
            `Rut: ${rut}\n` +
            `Correo electronico: ${email}\n`
        );
});

