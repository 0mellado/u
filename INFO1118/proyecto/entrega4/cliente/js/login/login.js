const form = document.getElementById('form-login')

form.addEventListener("submit", async event => {
    event.preventDefault();
    const loginData = new FormData(form);

    let userData = {}
    loginData.forEach((value, key) => {
        userData[key] = value
    })

    userData['token'] = getCookie('tokenUser') !== '' ? getCookie('tokenUser') : undefined

    const response = await login(userData);

    alert(response.msg);
    if (response.code) window.location.replace("index.html")
    if (!response.status || !response.token) return

    setCookie('tokenUser', response.token, 30)
    setCookie('username', response.username, 30)
    window.location.replace("index.html")
})



