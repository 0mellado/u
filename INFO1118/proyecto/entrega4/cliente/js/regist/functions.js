const regist = async registData => {

    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(registData)
    }

    try {
        const response = await fetch('http://localhost:8000/register', settings)
        const data = await response.json()
        return data;
    } catch (err) {
        console.log(err)
    }
};
