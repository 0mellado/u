const formRegist = document.getElementById('form-regist');

formRegist.addEventListener("submit", async event => {
    event.preventDefault();

    const registData = new FormData(formRegist);
    let userData = {}
    registData.forEach((value, key) => {
        userData[key] = value
    })

    if (userData.pwd !== userData.pwdverify) {
        alert('Las contraseñas no coinciden >:(')
        return
    }

    const response = await regist(userData);

    alert(response.msg);
    if (response.status) {
        window.location.replace("login.html");
    }
});
