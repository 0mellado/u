import morgan from 'morgan'
import cors from 'cors'
import cookieParse from 'cookie-parser'
import express from 'express'
import {config} from 'dotenv'
config()

import loginRoute from './routes/login.routes.js'
import foroRoute from './routes/foro.routes.js'
import registerRoute from './routes/register.routes.js'
import contactsRoute from './routes/contacts.routes.js'

const app = express()

app.use(express.urlencoded({
    extended: false
}))

app.use(cookieParse())
const corsConfig = {
    origin: true
}

app.use(cors(corsConfig));

app.use(express.json())
app.use(morgan('dev'))

app.use(contactsRoute)
app.use(loginRoute)
app.use(foroRoute)
app.use(registerRoute)

export default app
