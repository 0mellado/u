import {Router} from "express";

const router = Router()

import * as loginRoute from "../controllers/login.controller.js";

router.post('/login', loginRoute.auth)

export default router
