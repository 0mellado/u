import {Router} from "express";
const router = Router()

import * as foroController from '../controllers/foro.controller.js'
import {verifyToken} from '../middlewares/index.js'

router.post('/foro', verifyToken, foroController.createComment)
router.get('/foro', verifyToken, foroController.getComment)
router.put('/foro', verifyToken, foroController.updateComment)
router.delete('/foro', verifyToken, foroController.deleteComment)

export default router
