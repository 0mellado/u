import {Router} from "express";
const router = Router()

import {
    getContacts,
    insertContacts
} from '../controllers/contacts.controller.js'

router.get('/contacts', getContacts)
router.post('/contacts', insertContacts)

export default router
