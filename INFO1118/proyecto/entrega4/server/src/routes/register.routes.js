import {Router} from "express";

const router = Router()

import * as registerRoute from "../controllers/register.controller.js";

router.post('/register', registerRoute.createRegist)

export default router
