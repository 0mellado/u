import pkg from 'jsonwebtoken'
const {verify} = pkg
import {createServer} from "http"
import {Server} from "socket.io"
import {config} from 'dotenv'
config()

const socketListen = (port, callback) => {
    const httpServer = createServer()
    const io = new Server(httpServer, {
        cors: {
            origin: "http://localhost"
        }
    })

    io.use((socket, next) => {
        const {token} = socket.handshake.auth
        try {
            verify(token, process.env.SECRET)
            next()
        } catch (error) {
            console.log(error)
            const err = new Error("No autorizado")
            err.data = {content: "Intente más tarde"}
            next(err)
        }
    })

    io.on('connection', socket => {
        socket.on('loading', () => {
            socket.emit('loading')
        })

        socket.on('add username', username => {
            console.log(`se conectado un weon de nombre ${username}`)
            socket.data.username = username
        })

        socket.on('comment', message => {
            const now = Date.now()
            const today = new Date(now)

            socket.broadcast.emit('new comment', {
                username: socket.data.username,
                datetime: today.toDateString(),
                content: message
            })
        })
    })

    httpServer.listen(port)
    callback()
}

export default socketListen 
