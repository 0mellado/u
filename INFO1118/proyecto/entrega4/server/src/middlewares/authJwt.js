import pkg from 'jsonwebtoken'
const {verify} = pkg
import {config} from 'dotenv'
config()

export const verifyToken = (req, res, next) => {
    const token = req.headers['x-access-token']
    try {
        verify(token, process.env.SECRET)
        next()
    } catch (err) {
        return res.status(403).json(false)
    }
}
