import app from './app.js'
import socketListen from './socketServer.js'

socketListen(4100, () => {
    console.log('En escucha en el puerto 4100')
})

app.listen(8000, () => {
    console.log(`En escucha en el puerto 8000`)
})
