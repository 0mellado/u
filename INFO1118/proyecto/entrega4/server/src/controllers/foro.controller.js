import connection from '../db.js'

export const createComment = (req, res) => {
    const {username, message} = req.body

    connection.query(
        'INSERT INTO comments (author, message, createAt) VALUES (?, ? , NOW());',
        [username, message],
        (err, result) => {
            if (err) throw err
            const timeElapsed = Date.now();
            const today = new Date(timeElapsed);
            res.json({
                status: true,
                data: {
                    id: result.insertId,
                    author: username,
                    createAt: today.toISOString(),
                    message: message
                }
            })
        })
}

export const getComment = (req, res) => {
    connection.query(
        'SELECT * FROM comments ORDER BY id_comments DESC;',
        (err, result) => {
            if (err) throw err
            res.json(result)
        })
}

export const deleteComment = (req, res) => {
    const {id, content, author, createAt} = req.body
    const time = new Date(createAt).toLocaleTimeString()
    const datetime = `${createAt.slice(0, 10)} ${time}`
    connection.query(
        'DELETE FROM comments WHERE id_comments = ? AND message = ? AND createAt = ? AND author = ?;',
        [id, content, datetime, author],
        err => {
            if (err) throw err
            res.status(201).json(true)
        })
}

export const updateComment = (req, res) => {
    const {id, date, oldContent, newContent, username} = req.body

    const sql = `UPDATE comments SET message = ?
        WHERE id_comments = ?
        AND author = ?
        AND message = ?
        AND createAt = ?;`

    connection.query(sql,
        [newContent, id, username, oldContent, date],
        err => {
            if (err) return res.json(false)

            res.json(true)
        })

}
