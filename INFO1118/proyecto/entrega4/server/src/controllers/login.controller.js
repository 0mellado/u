import connection from '../db.js'
import pkg from 'jsonwebtoken'
const {sign, verify} = pkg
import bcrypt from 'bcrypt'
import {config} from 'dotenv'
config()

const sendExit = (msg, res, code, status) => {
    return res.status(code).json({
        status: status,
        msg: msg,
    })
}

const selectUser = (res, userData, pwd, callback) => {
    connection.query(
        'SELECT * FROM users WHERE email = ? OR username = ?;',
        [userData, userData],
        async (err, result) => {
            if (err) throw err
            if (result.length == 0) {
                sendExit(process.env.MSG_NOT_REGISTERED, res, 301, false)
                return
            }
            const match = await bcrypt.compare(pwd, result[0].passwd)

            if (!match) {
                sendExit(process.env.MSG_INVALID_PWD, res, 401, false)
                return
            }

            const token = sign({
                exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
                id: result[0].id_user,
                username: result[0].username,
                email: result[0].email,
            }, process.env.SECRET)

            callback(res, token, result[0].username)
        })

}


export const auth = (req, res) => {
    const {useroremail, pwd, token} = req.body

    try {
        verify(token, process.env.SECRET)
        return res.status(301).json({
            status: true,
            code: 300,
            msg: process.env.YOU_ARE_LOGIN
        })
    } catch (err) {
        selectUser(res, useroremail, pwd, (res, token, username) => {
            res.status(201).json({
                status: true,
                token: token,
                username: username,
                msg: process.env.LOGIN_SUCCESSFULLY
            })
        })
    }
}

