import connection from "../db.js"

export const getContacts = (req, res) => {
    connection.query(
        'SELECT name, lastname, empname FROM contacts;',
        (err, result) => {
            if (err) throw err
            res.status(200).json({
                httpCode: 200,
                contacts: result
            })
        })
}

export const insertContacts = (req, res) => {
    console.log(req.body)
    const {name, lastname, empresa, rut, email, msg} = req.body

    const [rutComple, digv] = rut.split('-')

    const sql =
        `INSERT INTO contacts (name, lastname, empname, rut, digv, email, message)
    VALUES (?, ?, ?, ?, ?, ?, ?);`

    connection.query(sql, [name, lastname, empresa, rutComple, digv, email, msg],
        err => {
            if (err) return res.status(404).json({
                status: 'No se puedo enviar el mensaje',
                httpCode: 404
            })
            res.status(200).json({
                status: 'Mensaje enviado',
                httpCode: 200
            })
        })
}
