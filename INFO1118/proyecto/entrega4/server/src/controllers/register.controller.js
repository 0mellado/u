import connection from "../db.js"
import bcrypt from 'bcrypt'
import {config} from "dotenv"
config()

const sendExit = (msg, res, status, code) => {
    return res.status(code).json({
        status: status,
        msg: msg
    })
}

const selectUser = (req, res, callback) => {
    connection.query(
        'SELECT username,email FROM users WHERE email = ? OR username = ?;',
        [req.body.email, req.body.username],
        async (err, result) => {
            if (err) throw err
            if (result.length != 0) {
                sendExit(process.env.MSG_REGIST_DUPLI, res, false, 400)
                return
            }
            callback(req, res)
        })
}

export const createRegist = async (req, res) => {
    selectUser(req, res, async (req, res) => {
        const pwd = await bcrypt.hash(req.body.pwd, 10)

        connection.query(
            'INSERT INTO users (username, email, passwd) VALUES (?, ?, ?)',
            [req.body.username, req.body.email, pwd],
            (err) => {
                if (err) sendExit(process.env.MSG_REGIST_ERR, res, false, 400)
                sendExit(process.env.MSG_REGIST_OK, res, true, 201)
                return
            })
    })
}

