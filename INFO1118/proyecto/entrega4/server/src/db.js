import mysql from 'mysql2'
import {config} from 'dotenv'
config()

const connection = mysql.createConnection({
    host: process.env.CONN_HOST,
    user: process.env.CONN_USER,
    database: process.env.CONN_DB,
    password: process.env.CONN_PWD
})

export default connection


