let navItems = [];
// btn guarda el elemento del boton para enviar los datos del form
const btn = document.getElementById("btn-contact");
const posSection = [0, 930, 1860, 4000]

for (let i = 0; i < 4; i++) {
    const nav = document.getElementById(`nav-${i}`);
    navItems[i] = nav;
}


let currentNav = 0;
navItems.map((nav, i) => {
    nav.addEventListener("click", () => {
        navItems[currentNav].style.color = "";
        window.scrollTo({
            top: posSection[i],
            left: 100,
            behavior: 'smooth'
        });
        nav.style.color = "#fc7703";
        currentNav = i;
    });
});


// cuando el boton de enviar los datos es accionado se le
// envia una alerta al usuario
btn.addEventListener("click", () => {
    alert("Los datos fueron ingresados correctamente");
});
