<?php

include_once "config.php";

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$name = $_POST['name'];
$lastname = $_POST['lastname'];
$empname = $_POST['empresa'];
$rutempCmplt = $_POST['rut'];
$email = $_POST['email'];
$message = $_POST['msg'];

list($rut, $digv) = explode("-", $rutempCmplt);

$createTable = 
"CREATE TABLE IF NOT EXISTS contacts (
    id_contact INT(5) AUTO_INCREMENT ,
    name VARCHAR(32) NOT NULL,
    lastname VARCHAR(32) NOT NULL,
    empname VARCHAR(32) NOT NULL,
    rut INT(10) NOT NULL,
    digv VARCHAR(2) NOT NULL,
    email VARCHAR(32) NOT NULL,
    message VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_contact)
);";

$insertContact = 
"INSERT INTO contacts (name, lastname, empname, rut, digv, email, message)
VALUES ('$name', '$lastname', '$empname', $rut, '$digv', '$email', '$message');
";

mysqli_query($db, $createTable);

$result = mysqli_query($db, $insertContact);

$httpCode = http_response_code();

$response = array();

if ($result) {
    $response['status'] = "Mensage enviado";
    $response['httpCode'] = $httpCode;
    exit(json_encode($response));
} else {
    $response['status'] = "No se puedo enviar el mensaje";
    $response['httpCode'] = $httpCode;
    exit(json_encode($response));
}

mysqli_close($db);

?>
