// btn guarda el elemento del boton para enviar los datos del form
const form = document.getElementById("form");
const btn = document.getElementById("btn-list-contacts");
const inputs = document.getElementsByClassName("in");
const inrut = document.getElementById("in-rut");
const statusForm = document.getElementById("status-msg");
const navItems = document.getElementsByClassName("nav-item");
const navRef = document.getElementsByClassName("nav-ref");
const tableContacts = document.getElementById("table-contacts");


const getPosSection = () => {
    let posSection = [0, 0, 0, 0, 4000];
    let firstPos = 0;

    for (let i = 0; i < navRef.length; i++) {
        const coords = navRef[i].getBoundingClientRect();

        if (i == 0) firstPos = coords.top;

        if (i > 0 && i < 4) {
            const pos = coords.top - firstPos;
            posSection[i] = pos;
        }
    }
    return posSection;
};


let fn = {
    validaRut: rutCompleto => {
        rutCompleto = rutCompleto.replace("‐", "-");
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
            return false;
        let tmp = rutCompleto.split('-');
        let digv = tmp[1];
        let rut = tmp[0];
        if (digv == 'K') digv = 'k';

        return (fn.dv(rut) == digv);
    },

    dv: T => {
        let M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
}


const getContacts = async () => {
    const response = await fetch('src/select.php', {
        method: 'POST'
    });

    const data = await response.json();
    let insertText =
        `<table class="table-contacts">
            <tr>
                <th><b>Nombres</b></th>
                <th><b>Apellidos</b></th>
                <th><b>Nombres de las empresas</b></th>
            </tr>
        `;

    data.contacts.forEach(ele => {
        const contact =
            `<tr>
                <td>${ele.name}</td>
                <td>${ele.lastname}</td>
                <td>${ele.empname}</td>
            </tr>
            `;
        insertText = insertText + contact;
    });
    insertText = insertText + "</table>";
    tableContacts.innerHTML = insertText;

    return;
};


const sendForm = async formData => {
    const response = await fetch('src/insert.php', {
        method: 'POST',
        body: formData,
    });

    const respuesta = await response.text();
    const obj = JSON.parse(respuesta);

    statusForm.innerHTML = obj.status;
    return obj.httpCode;
};


let lastNav = 0;

for (let i = 0; i < 5; i++) {

    navItems[i].addEventListener("click", () => {
        navItems[lastNav].style.color = "";
        window.scrollTo({
            top: getPosSection()[i],
            left: 100,
            behavior: 'smooth'
        });
        navItems[i].style.color = "#fc7703";
        lastNav = i;
    });
}


inrut.addEventListener("keydown", () => {
    if (fn.validaRut(inrut.value)) {
        btn.style.display = "";
        statusForm.textContent = "";
    } else {
        btn.style.display = "none";
        statusForm.textContent = "El rut ingresado no es valido";
    }
});


btn.addEventListener("click", event => {
    event.preventDefault();
    getContacts();
});


// cuando el boton de enviar los datos es accionado se le
// envia una alerta al usuario
form.addEventListener("submit", async event => {
    event.preventDefault();
    const formData = new FormData(form);
    const httpCode = await sendForm(formData);

    const name = form[0].value;
    const lastname = form[1].value;
    const empname = form[2].value;
    const rut = form[3].value;
    const email = form[4].value;

    if (httpCode == 200)
        alert("Los datos fueron ingresado correctamente\n" +
            `Nombre: ${name}\n` +
            `Apellido: ${lastname}\n` +
            `Nombre de la empresa: ${empname}\n` +
            `Rut: ${rut}\n` +
            `Correo electronico: ${email}\n`
        );
});

