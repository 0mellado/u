<?php

include_once "config.php";

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$query = "SELECT name, lastname, empname FROM contacts;";

$result = mysqli_query($db, $query);

$httpCode = http_response_code();


if (mysqli_num_rows($result) > 0) {

    $response = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $response["contacts"][] = $row;
    }
    exit(json_encode($response));

} else {
    $response = array("httpCode" => $httpCode);
    exit(json_encode($response));
}


mysqli_close($db);

?>
