const form = document.getElementById('form-login')

form.addEventListener("submit", async event => {
    event.preventDefault();
    const loginData = new FormData(form);

    const response = await login(loginData);

    alert(response.msg);
    if (response.status) window.location.replace("index.php");
});



