const login = async loginData => {
    const response = await fetch('src/login.php', {
        method: "POST",
        body: loginData
    });

    return response.json();
};

