const sendData = async data => {
    const response = await fetch('src/insertComment.php', {
        method: 'POST',
        body: data
    });

    return response.json();
}

const readData = async () => {
    const response = await fetch('src/readComments.php', {
        method: 'POST',
    })

    return response.json();
}

const logoutFunc = async () => {
    await fetch('src/logout.php', {method: 'POST'});
};

let lastIdComment;
const loadComments = async () => {
    const response = await readData();

    let commentsDiv = "";
    for (const comment of response.data) {
        if (response.sessionUser != comment[1]) {
            commentsDiv +=
                `<div class="d-flex flex-row p-3">
                    <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                    <div class="w-100">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex flex-row align-items-center">
                                <span class="mr-2">${comment[1]}</span>
                            </div>
                            <small>${comment[3]}</small>
                        </div>
                        <p id="msg-content" class="text-justify comment-text mb-0">${comment[2]}</p>
                    </div>
                </div>
                `;
            continue;
        }
        commentsDiv +=
            `<div class="d-flex flex-row p-3">
                <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                <div class="w-100">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span class="mr-2">${comment[1]}</span>
                        </div>
                        <small>${comment[3]}</small>
                    </div>
                    <p id="msg-content" class="text-justify comment-text mb-0">${comment[2]}</p>
                    <div>
                        <button class="btn btn-danger btn-sm btn-delete">borrar</button>
                        <button class="btn btn-info btn-sm btn-update">editar</button>
                    </div>
                </div>
            </div>
            `;
    }
    return commentsDiv;
}

const addCommentElement = (comment) => {
    div.innerHTML =
        `<div class="d-flex flex-row p-3">
            <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
            <div class="w-100">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex flex-row align-items-center">
                        <span class="mr-2">${comment.author}</span>
                    </div>
                    <small>${comment.createat}</small>
                </div>
                <p id="msg-content" class="text-justify comment-text mb-0">${comment.message}</p>
                <div>
                    <button class="btn btn-danger btn-sm btn-delete">borrar</button>
                    <button class="btn btn-info btn-sm btn-update">editar</button>
                </div>
            </div>
        </div>
        ` + div.innerHTML;
};

