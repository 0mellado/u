const div = document.getElementById('comments-box');
const inputComment = document.getElementById('input-comment');
const logoutBtn = document.getElementById('btn-logout');
const socket = io.connect('ws://127.0.0.1:3000');
let btnUpdate;
let btnDelete;


window.addEventListener('load', async () => {
    const username = await fetch('src/getUsername.php')
        .then(response => response.json());

    socket.emit('add username', username);

    const comments = await loadComments();
    div.innerHTML = comments;

    socket.emit('loading');

});


logoutBtn.addEventListener('click', async () => {
    await logoutFunc();
    window.location.replace('login.html');
});


inputComment.addEventListener('change', async event => {
    event.preventDefault();

    const dataComment = new FormData();
    dataComment.append("message", inputComment.value);

    const response = await sendData(dataComment);

    if (!response.status) {
        alert('No se a podido mandar el comentario');
        return;
    }

    socket.emit('comment', inputComment.value);

    inputComment.value = '';

    addCommentElement(response.data);

    socket.emit('loading');
});

socket.on('new comment', data => {
    const commentsDiv =
        `<div class="d-flex flex-row p-3">
                <img src="img/usuario.png" width="40" height="40" class="rounded-circle mr-3">
                <div class="w-100">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-row align-items-center">
                            <span class="mr-2">${data.username}</span>
                        </div>
                        <small>${data.datetime}</small>
                    </div>
                    <p id="msg-content" class="text-justify comment-text mb-0">${data.content}</p>
                </div>
            </div>
            `;
    div.innerHTML = commentsDiv + div.innerHTML;
});

socket.on('loading', () => {
    btnUpdate = document.getElementsByClassName('btn-update');
    btnDelete = document.getElementsByClassName('btn-delete');

    for (let i = 0; i < btnDelete.length; i++) {
        btnDelete[i].addEventListener('click', async () => {
            const parent = btnDelete[i].parentNode.parentNode;

            const dateHtml = parent.getElementsByTagName('small');
            const date = dateHtml[0].innerHTML;

            const contentHtml = parent.getElementsByTagName('p');
            const content = contentHtml[0].innerHTML;

            const dataDel = new FormData();

            dataDel.append("date", date);
            dataDel.append("content", content);

            const status = await fetch('src/deleteComment.php', {
                method: 'POST',
                body: dataDel
            }).then(response => response.json());

            if (!status) {
                alert("no se pudo eliminar el comentario");
                return;
            };

            const comments = await loadComments();
            div.innerHTML = comments;

            socket.emit('loading');
        });

        btnUpdate[i].addEventListener('click', () => {
            const parent = btnDelete[i].parentNode.parentNode;

            const inputForm = document.createElement('input');
            const dateHtml = parent.getElementsByTagName('small')[0];
            const contentHtml = parent.getElementsByTagName('p')[0];

            const dataUpdate = new FormData();
            dataUpdate.append("date", dateHtml.innerHTML);
            dataUpdate.append("oldContent", contentHtml.innerHTML);

            inputForm.className = 'form-control-sm';
            inputForm.type = 'text';
            inputForm.value = contentHtml.innerHTML;

            parent.replaceChild(inputForm, contentHtml);
            inputForm.addEventListener('change', async () => {
                const newContent = inputForm.value;
                const newElementP = document.createElement('p');

                dataUpdate.append("newContent", newContent);

                const status = await fetch('src/updateComment.php', {
                    method: 'POST',
                    body: dataUpdate
                }).then(response => response.json());

                if (!status) {
                    alert("no se pudo editar el comentario");
                    return;
                }

                newElementP.className = "text-justify comment-text mb-0";
                newElementP.id = "msg-content";
                newElementP.textContent = newContent;

                parent.replaceChild(newElementP, inputForm);

                const comments = await loadComments();
                div.innerHTML = comments;
                socket.emit('loading');
            });
        });
    }
});
