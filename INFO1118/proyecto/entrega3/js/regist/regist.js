const formRegist = document.getElementById('form-regist');

formRegist.addEventListener("submit", async event => {
    event.preventDefault();

    const registData = new FormData(formRegist);
    const response = await regist(registData);

    alert(response.msg);
    if (response.status) {
        window.location.replace("login.html");
    }
});
