const regist = async registData => {
    const response = await fetch('src/regist.php', {
        method: "POST",
        body: registData
    });

    return response.json();
};
