const getPosSection = () => {
    let posSection = [0, 0, 0, 0, 4000];
    let firstPos = 0;

    for (let i = 0; i < navRef.length; i++) {
        const coords = navRef[i].getBoundingClientRect();

        if (i == 0) firstPos = coords.top;

        if (i > 0 && i < 4) {
            const pos = coords.top - firstPos;
            posSection[i] = pos;
        }
    }
    return posSection;
};


let fn = {
    validaRut: rutCompleto => {
        rutCompleto = rutCompleto.replace("‐", "-");
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
            return false;
        let tmp = rutCompleto.split('-');
        let digv = tmp[1];
        let rut = tmp[0];
        if (digv == 'K') digv = 'k';

        return (fn.dv(rut) == digv);
    },

    dv: T => {
        let M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
}


const getContacts = async () => {
    const response = await fetch('src/select.php', {
        method: 'POST'
    });

    const data = await response.json();
    let insertText =
        `<table class="table-contacts">
            <tr>
                <th><b>Nombres</b></th>
                <th><b>Apellidos</b></th>
                <th><b>Nombres de las empresas</b></th>
            </tr>
        `;

    data.contacts.forEach(ele => {
        const contact =
            `<tr>
                <td>${ele.name}</td>
                <td>${ele.lastname}</td>
                <td>${ele.empname}</td>
            </tr>
            `;
        insertText = insertText + contact;
    });
    insertText = insertText + "</table>";
    tableContacts.innerHTML = insertText;

    return;
};


const sendForm = async formData => {
    const response = await fetch('src/insert.php', {
        method: 'POST',
        body: formData,
    });

    const respuesta = await response.text();
    const obj = JSON.parse(respuesta);

    statusForm.innerHTML = obj.status;
    return obj.httpCode;
};
