<?php
    session_start();
    session_regenerate_id();
    if(!isset($_SESSION['username'])) {
        header("Location: login.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
        <title>Comentarios</title>
        <link rel="stylesheet" href="css/style-comments.css">
        <link rel="stylesheet" href="css/style.bootstrap.css">
        <link rel="stylesheet" href="css/style.cloudflare.css">
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/comments/function.js"></script>
    </head>

    <body>
        <div class="container mt-5 mb-5">
            <div class="row height d-flex justify-content-center align-items-center">
                <div class="col-md-7">
                    <div class="card">
                        <script>
                            function volver() {
                                window.location.replace('/~oscar/entrega3/index.php')
                            }
                        </script>
                        <div class="p-3 header">
                            <h6><?php echo $_SESSION['username']; ?> Foro</h6>
                            <button id="btn-regret" class="btn btn-light" onclick="volver()">Volver</button>
                            <button id="btn-logout" class="btn btn-light">Cerrar sesión</button>
                        </div>
                        <div class="mt-3 d-flex flex-row align-items-center p-3 form-color">
                            <img src="img/usuario.png" width="50" class="rounded-circle mr-2">
                            <input id="input-comment" type="text" class="form-control" name="message" placeholder="Mandar comentario...">
                        </div>
                        <div class="mt-2" id="comments-box"></div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/socket.io.js"></script>
        <script src="js/comments/insert.js"></script>
    </body>

</html>
