<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'a103556910edaf7c51a890f5d5645aa9eeba298b',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'a103556910edaf7c51a890f5d5645aa9eeba298b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/channel' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'reference' => '3df772d0d20d4cebfcfd621c33d1a1ab732db523',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/channel',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/phpsocket.io' => array(
            'pretty_version' => 'v1.1.14',
            'version' => '1.1.14.0',
            'reference' => 'a5758da4d55b4744a4cc9c956816d88ce385601e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/phpsocket.io',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'workerman/workerman' => array(
            'pretty_version' => 'v4.1.4',
            'version' => '4.1.4.0',
            'reference' => '83e007acf936e2233ac92d7368b87716f2bae338',
            'type' => 'library',
            'install_path' => __DIR__ . '/../workerman/workerman',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
