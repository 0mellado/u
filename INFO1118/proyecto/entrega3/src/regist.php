<?php

include_once "config.php";

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$createTableQuery =
"CREATE TABLE IF NOT EXISTS users (
    id_user INT(5) AUTO_INCREMENT ,
    username VARCHAR(32) NOT NULL,
    email VARCHAR(32) NOT NULL,
    passwd VARCHAR(512) NOT NULL,
    PRIMARY KEY (id_user)
);";

mysqli_query($db, $createTableQuery);

/* $createTableQuery = */
/* "CREATE TABLE IF NOT EXISTS users ( */
/*     id_user SERIAL, */
/*     username VARCHAR(32) NOT NULL, */
/*     email VARCHAR(32) NOT NULL, */
/*     passwd VARCHAR(64) NOT NULL, */
/*     PRIMARY KEY (id_user) */
/* );"; */

/* pg_query($db, $createTableQuery); */

$response = array();

if ($_POST['pwd'] != $_POST['pwdverify']) {
    $response['status'] = false;
    $response['msg'] = "Las contraseñas no son iguales";
    exit(json_encode($response));
}

if ($_POST['username'] === "") {
    $response['status'] = false;
    $response['msg'] = "El nombre de usuario es necesario";
    exit(json_encode($response));
}

if ($_POST['email'] === "") {
    $response['status'] = false;
    $response['msg'] = "El correo electronico es necesario";
    exit(json_encode($response));
}

$name = stripslashes($_POST['username']);
$email = stripslashes($_POST['email']);
$pwd = stripslashes($_POST['pwd']);

/* $name = pg_escape_string($db, $name); */
/* $email = pg_escape_string($db, $email); */
/* $pwd = pg_escape_string($db, $pwd); */

$name = mysqli_real_escape_string($db, $name);
$email = mysqli_real_escape_string($db, $email);
$pwd = mysqli_real_escape_string($db, $pwd);

$userRepeat = "SELECT * FROM users WHERE username = '$name' OR email = '$email';";

/* $resultUser = pg_query($db, $userRepeat); */
/* $numRows = pg_num_rows($resultUser); */

$resultUser = mysqli_query($db, $userRepeat);
$numRows = mysqli_num_rows($resultUser);

if ($numRows != 0) {
    $response['status'] = false;
    $response['msg'] = "El nombre de usuario o el correo electronico ya estan registrados";
    exit(json_encode($response));
}

$passwd = password_hash($pwd, PASSWORD_BCRYPT);

$queryInsertContact = 
"INSERT INTO users (username, email, passwd)
VALUES ('$name', '$email', '$passwd');";

$result = mysqli_query($db, $queryInsertContact);
/* $result = pg_query($db, $queryInsertContact); */

if (!$result) {
    $response['status'] = false;
    $response['msg'] = "No se pudieron registrar los datos";
    exit(json_encode($response));
} 

$response['status'] = true;
$response['msg'] = "Se ha registrado correctamente";
exit(json_encode($response));

?>
