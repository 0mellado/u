<?php

include_once "config.php";

session_start();

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$date = stripslashes($_POST['date']);
$oldContent = stripslashes($_POST['oldContent']);
$newContent = stripslashes($_POST['newContent']);

$user = $_SESSION['username'];
/* $date = pg_escape_string($db, $date); */
/* $oldContent = pg_escape_string($db, $oldContent); */
/* $newContent = pg_escape_string($db, $newContent); */

$date = mysqli_real_escape_string($db, $date);
$oldContent = mysqli_real_escape_string($db, $oldContent);
$newContent = mysqli_real_escape_string($db, $newContent);

$query = 
    "UPDATE comments 
    SET message = '$newContent'
    WHERE author = '$user' 
    AND message = '$oldContent' 
    AND createat = '$date';";


/* $result = pg_query($db, $query); */
$result = mysqli_query($db, $query);

$response = false;

if (!$result) {
    exit(json_encode($response));
}

$response = true;
exit(json_encode($response));

?>
