<?php

include_once "config.php";

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$name = stripslashes($_POST['name']);
$lastname = stripslashes($_POST['lastname']);
$empname = stripslashes($_POST['empresa']);
$rutempCmplt = stripslashes($_POST['rut']);
$email = stripslashes($_POST['email']);
$message = stripslashes($_POST['msg']);

/* $name = pg_escape_string($db, $name); */
/* $lastname = pg_escape_string($db, $lastname); */
/* $empname = pg_escape_string($db, $empname); */
/* $rutempCmplt = pg_escape_string($db, $rutempCmplt); */
/* $email = pg_escape_string($db, $email); */
/* $message = pg_escape_string($db, $message); */

$name = mysqli_real_escape_string($db, $name);
$lastname = mysqli_real_escape_string($db, $lastname);
$empname = mysqli_real_escape_string($db, $empname);
$rutempCmplt = mysqli_real_escape_string($db, $rutempCmplt);
$email = mysqli_real_escape_string($db, $email);
$message = mysqli_real_escape_string($db, $message);

list($rut, $digv) = explode("-", $rutempCmplt);

$createTable = 
"CREATE TABLE IF NOT EXISTS contacts (
    id_contact INT(5) AUTO_INCREMENT ,
    name VARCHAR(32) NOT NULL,
    lastname VARCHAR(32) NOT NULL,
    empname VARCHAR(32) NOT NULL,
    rut INT(10) NOT NULL,
    digv VARCHAR(2) NOT NULL,
    email VARCHAR(32) NOT NULL,
    message VARCHAR(250) NOT NULL,
    PRIMARY KEY (id_contact)
);";

/* $createTable = */ 
/* "CREATE TABLE IF NOT EXISTS contacts ( */
/*     id_contact SERIAL, */
/*     name VARCHAR(32) NOT NULL, */
/*     lastname VARCHAR(32) NOT NULL, */
/*     empname VARCHAR(32) NOT NULL, */
/*     rut INT(10) NOT NULL, */
/*     digv VARCHAR(2) NOT NULL, */
/*     email VARCHAR(32) NOT NULL, */
/*     message VARCHAR(250) NOT NULL, */
/*     PRIMARY KEY (id_contact) */
/* );"; */

/* pg_query($db, $createTable); */
mysqli_query($db, $createTable);

$insertContact = 
"INSERT INTO contacts (name, lastname, empname, rut, digv, email, message)
VALUES ('$name', '$lastname', '$empname', $rut, '$digv', '$email', '$message');
";

$result = mysqli_query($db, $insertContact);
/* $result = pg_query($db, $insertContact); */

$httpCode = http_response_code();

$response = array();

if ($result) {
    $response['status'] = "Mensage enviado";
    $response['httpCode'] = $httpCode;
    exit(json_encode($response));
} else {
    $response['status'] = "No se puedo enviar el mensaje";
    $response['httpCode'] = $httpCode;
    exit(json_encode($response));
}

mysqli_close($db);
/* pg_close($db) */

?>
