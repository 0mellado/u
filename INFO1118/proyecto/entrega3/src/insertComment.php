<?php

session_start();
include_once "config.php";

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$user = $_SESSION['username'];
$msg = stripslashes($_POST['message']);
/* $msg = pg_escape_string($db, $msg); */
$msg = mysqli_real_escape_string($db, $msg);

$dateNow = date('Y-m-d H:i:s', time());

$createTable = 
"CREATE TABLE IF NOT EXISTS comments (
    id_comments INT(5) AUTO_INCREMENT ,
    author VARCHAR(32) NOT NULL,
    message VARCHAR(1024) NOT NULL,
    createat DATETIME NOT NULL,
    PRIMARY KEY (id_comments)
);";

/* $createTable = */ 
/* "CREATE TABLE IF NOT EXISTS comments ( */
/*     id_comments SERIAL, */
/*     author VARCHAR(32) NOT NULL, */
/*     message VARCHAR(1024) NOT NULL, */
/*     createat TIMESTAMP NOT NULL, */
/*     PRIMARY KEY (id_comments) */
/* );"; */

mysqli_query($db, $createTable);
/* pg_query($db, $createTable); */

$query =
"INSERT INTO comments (author, message, createAt)
VALUES ('$user', '$msg', '$dateNow');";

$result = mysqli_query($db, $query);
/* $result = pg_query($db, $query); */

$comment = array(
    "author" => $user,
    "createat" => $dateNow,
    "message" => $msg
);

if (!$result) {
    exit(json_encode(array("status" => false)));
}

exit(json_encode(array("status" => true, "data" => $comment)));

?>
