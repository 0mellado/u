<?php

include_once "config.php";

session_start();

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}


$query = "SELECT * FROM comments ORDER BY id_comments DESC;";

$result = mysqli_query($db, $query);
/* $result = pg_query($db, $query); */

$response = array(
    /* "data" => pg_fetch_all($result), */
    "data" => mysqli_fetch_all($result),
    "sessionUser" => $_SESSION['username']
);

exit(json_encode($response));

?>
