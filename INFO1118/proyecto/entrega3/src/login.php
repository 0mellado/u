<?php

include_once "config.php";

session_start();

$response = array();

if (isset($_SESSION['username'])) {
    $response['status'] = true;
    $response['msg'] = "Ya estas logeado";
    exit(json_encode($response));
}

/* $db = pg_connect($psql['host']." ".$psql['db']." ".$psql['user']." ".$psql['pwd']) */
/*     or die('No se pudo conectar a la base de datos'. pg_last_error()); */

$db = mysqli_connect($mysql['host'], $mysql['user'], $mysql['pwd'], $mysql['db']);

if (!$db) {
    die("Conexión fallida: ". mysqli_connect_error());
}

$useroremail = stripslashes($_POST['useroremail']);

/* $useroremail = pg_escape_string($db, $useroremail); */

$useroremail = mysqli_real_escape_string($db, $useroremail);

$query = "SELECT username, passwd FROM users WHERE username = '$useroremail' OR email = '$useroremail';";

$result = mysqli_query($db, $query);
/* $result = pg_query($db, $query); */

/* $numRows = pg_num_rows($result); */
$numRows = mysqli_num_rows($result);

if ($numRows == 0) {
    $response['status'] = false;
    $response['msg'] = "El usuario o email no estan registrados";
    exit(json_encode($response));
}

/* $user = pg_fetch_assoc($result); */
$user = mysqli_fetch_assoc($result);

$match = password_verify($_POST['pwd'], $user['passwd']);

if (!$match) {
    $response['status'] = false;
    $response['msg'] = "La contraseña es incorrecta";
    exit(json_encode($response));
}


$_SESSION['username'] = $user['username'];

$response['status'] = true;
$response['msg'] = "Se inicio sesión correctamente";
exit(json_encode($response));

?>
