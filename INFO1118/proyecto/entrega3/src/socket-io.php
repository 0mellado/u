<?php

include_once '../vendor/autoload.php';


use Workerman\Worker;
use PHPSocketIO\SocketIO;

$io = new SocketIO(3000);

$io->on('connection', function($socket) {

    $socket->on('loading', function() use ($socket) {
        $socket->emit('loading');
    });

    $socket->on('add username', function($username) use ($socket) {
        echo "se ha conectado $username\n";
        $socket->username = $username;
    });

    $socket->on('comment', function($msg) use ($socket) {
        $dateNow = date('Y-m-d H:i:s', time());
        $socket->broadcast->emit('new comment', array(
            "username" => $socket->username,
            "datetime" => $dateNow,
            "content" => $msg
        ));
    });
});

Worker::runAll();

?>
