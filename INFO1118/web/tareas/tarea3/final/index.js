const div = document.getElementById("content"); // obtengo el div
const form = document.getElementById("inputs-form"); // obtengo el formulario
const btn1 = document.getElementById("btn-1"); // obtengo el primer boton
const btn2 = document.getElementById("btn-2"); // obtengo el segundo boton


let count = 1; // contador de botones
// parte en 1 porque ya hay un input en el html 
btn1.onclick = () => { // en el evento del click del boton 1
    const input = document.createElement("input"); // se crea un elemento input
    const nl = document.createElement("br"); // se crea un elemento br
    input.type = "text"; // al input se le agrega el typo
    input.id = `form-${count}`; // al input se le asigna una id
    input.placeholder = `input N°${count}`; // al input se le agrega un placeholder
    form.appendChild(nl); // se le añade al formulario el elemento br que es un salto de linea
    form.appendChild(input); // se le añade al formulario el input
    count++; // se le suma 1 al contador de los inputs
}

// se define una función que retorna true si un arreglo tiene algún elemento que no sea entero
const typeOfArray = arr => {
    for (let i = 0; i < arr.length; i++) { // se recorre el arreglo
        let num = parseInt(arr[i].value); // se transforma el valor del input a entero
        if (`${num}` == "NaN") return [true, i]; // se compara si el valor es o no un número 
    }
    return [false, 0];
};

// se define un función que retorna la suma de un arreglo
const sumar = arr => {
    let suma = 0; // se define una variable suma igual a 0
    for (let i = 0; i < arr.length; i++) // se recorre el arreglo
        suma = suma + parseInt(arr[i].value); // se le va sumando a la variable suma el valor del input

    return suma; // se retorna la suma del arreglo
};

btn2.onclick = () => { // en el evento del click del boton 2
    let forms = []; // se defien un arreglo vacío
    for (let i = 0; i < count; i++) { // con un bucle se va a llenar el arreglo forms
        const form = document.getElementById(`form-${i}`); // se obtiene cada input con su id
        forms[i] = form; // se añade al arreglo forms los inputs
    }

    const state = typeOfArray(forms)[0]; // se define una constante que guarda el booleano que retorna la función typeOfArray
    const index = typeOfArray(forms)[1]; // se define una constante que guarda el índice de el elemento que no es un número
    if (state) { // se comprueba si es el estado es true
        div.innerHTML = `En el input ${index} no hay un número`; // se inserta en el div en que input no hay un número
    } else { // si el estado no es true
        const suma = sumar(forms); // suma los inputs y los guarda en una constante suma
        div.innerHTML = `La suma es de: ${suma}`; // inserta en el div la suma de los inputs
    }
}

