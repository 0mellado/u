import styles from '../styles/formProd.module.css'

const FormProd = ({handle, product}) => (
    <form className={styles.form} onSubmit={handle.submit}>
        <label htmlFor="name">Nombre del producto</label>
        <input
            type="text"
            name="name"
            value={product.name}
            onChange={handle.change}
            required
        />
        <label htmlFor="stock">Stock del producto</label>
        <input
            type="number"
            name="stock"
            value={product.stock}
            onChange={handle.change}
            required
        />
        <label htmlFor="price">Precio del producto</label>
        <input
            type="number"
            name="price"
            value={product.price}
            onChange={handle.change}
            required
        />
        <label htmlFor="weight">Masa del producto</label>
        <input
            type="number"
            name="weight"
            value={product.weight}
            onChange={handle.change}
            required
        />
        <button type="submit">agregar</button>
    </form>
)

export default FormProd
