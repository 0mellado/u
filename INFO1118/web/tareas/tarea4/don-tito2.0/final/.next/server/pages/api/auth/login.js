"use strict";
(() => {
var exports = {};
exports.id = 908;
exports.ids = [908];
exports.modules = {

/***/ 7096:
/***/ ((module) => {

module.exports = require("bcrypt");

/***/ }),

/***/ 4802:
/***/ ((module) => {

module.exports = require("cookie");

/***/ }),

/***/ 9344:
/***/ ((module) => {

module.exports = require("jsonwebtoken");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 6205:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _lib_db__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2759);
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(404);
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7096);
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bcrypt__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9344);
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4802);
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(cookie__WEBPACK_IMPORTED_MODULE_4__);





const loginHandler = async (req, res)=>{
    const { tokenName  } = req.cookies;
    try {
        (0,jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__.verify)(tokenName, process.env.SECRET);
        res.status(300).json({
            success: true,
            data: process.env.YOU_ARE_LOGIN
        });
    } catch (err1) {
        const { method  } = req;
        if (method != "POST") return res.status(403).json({
            success: false,
            data: process.env.FORBIDDEN
        });
        const userJson = JSON.parse(req.body);
        try {
            await (0,_lib_db__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z)();
            const user = await _models_user__WEBPACK_IMPORTED_MODULE_1__/* ["default"].findOne */ .Z.findOne({
                $or: [
                    {
                        username: userJson.user
                    },
                    {
                        email: userJson.user
                    }
                ]
            });
            if (!user || !bcrypt__WEBPACK_IMPORTED_MODULE_2___default().compareSync(userJson.pwd, user.pwd)) return res.status(401).json({
                success: false,
                data: process.env.LOGIN_INVALID
            });
            const token = (0,jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__.sign)({
                exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
                email: user.email,
                username: user.username,
                admin: user.admin
            }, process.env.SECRET);
            const serialized = (0,cookie__WEBPACK_IMPORTED_MODULE_4__.serialize)("tokenName", token, {
                httpOnly: true,
                secure: "production" === "production",
                sameSite: "lax",
                maxAge: 1000 * 60 * 60 * 24 * 30,
                path: "/"
            });
            res.setHeader("Set-Cookie", serialized);
            res.status(201).json({
                success: true,
                data: process.env.LOGIN_SUCCESSFULLY
            });
        } catch (err) {
            res.status(400).json({
                success: false,
                data: process.env.MONGODB_CONNECT_ERROR
            });
        }
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (loginHandler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [46], () => (__webpack_exec__(6205)));
module.exports = __webpack_exports__;

})();