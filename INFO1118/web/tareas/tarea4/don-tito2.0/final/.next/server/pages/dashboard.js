(() => {
var exports = {};
exports.id = 26;
exports.ids = [26];
exports.modules = {

/***/ 3553:
/***/ ((module) => {

// Exports
module.exports = {
	"all": "dashboard_all__oWFmf",
	"addproduct": "dashboard_addproduct__giZ3e",
	"noproduct": "dashboard_noproduct__N3osQ",
	"body": "dashboard_body__MHLHV",
	"thead": "dashboard_thead__T_1Zc",
	"tbuttons": "dashboard_tbuttons__Ps2G1",
	"hitem": "dashboard_hitem__Q6Bca",
	"subnav": "dashboard_subnav__T3MRl",
	"button": "dashboard_button__27hai"
};


/***/ }),

/***/ 7410:
/***/ ((module) => {

// Exports
module.exports = {
	"form": "formProd_form__mAlHF"
};


/***/ }),

/***/ 8613:
/***/ ((module) => {

// Exports
module.exports = {
	"tbuttons": "products_tbuttons__aZush",
	"updbtn": "products_updbtn__kdH42",
	"delbtn": "products_delbtn__4h3Qy",
	"tbody": "products_tbody__RgZma",
	"item": "products_item__sBues"
};


/***/ }),

/***/ 8807:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ dashboard),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/Nav.js
var Nav = __webpack_require__(8731);
// EXTERNAL MODULE: ./styles/dashboard.module.css
var dashboard_module = __webpack_require__(3553);
var dashboard_module_default = /*#__PURE__*/__webpack_require__.n(dashboard_module);
// EXTERNAL MODULE: ./styles/products.module.css
var products_module = __webpack_require__(8613);
var products_module_default = /*#__PURE__*/__webpack_require__.n(products_module);
// EXTERNAL MODULE: ./styles/formProd.module.css
var formProd_module = __webpack_require__(7410);
var formProd_module_default = /*#__PURE__*/__webpack_require__.n(formProd_module);
;// CONCATENATED MODULE: external "mongoose"
const external_mongoose_namespaceObject = require("mongoose");
var external_mongoose_default = /*#__PURE__*/__webpack_require__.n(external_mongoose_namespaceObject);
;// CONCATENATED MODULE: ./lib/db.js

const MONGODB_URI = process.env.MONGODB_URI;
if (!MONGODB_URI) throw new Error("Por favor defina la variable de entorno MONGODB_URI dentro del archivo .env.local");
let cached = global.mongoose;
if (!cached) {
    cached = global.mongoose = {
        conn: null,
        promise: null
    };
}
const connect = async ()=>{
    if (cached.conn) {
        return cached.conn;
    }
    if (!cached.promise) {
        const opts = {
            bufferCommands: false
        };
        cached.promise = external_mongoose_default().connect(MONGODB_URI, opts).then((mongoose)=>{
            return mongoose;
        });
    }
    try {
        cached.conn = await cached.promise;
    } catch (err) {
        cached.promise = null;
        throw err;
    }
    return cached.conn;
};
/* harmony default export */ const db = (connect);

;// CONCATENATED MODULE: ./models/product.js

const ProductSchema = new (external_mongoose_default()).Schema({
    name: {
        type: String,
        required: [
            true,
            "el nombre del prodcuto es necesario"
        ]
    },
    price: {
        type: Number,
        required: [
            true,
            "El precio es necesario para el producto"
        ]
    },
    stock: {
        type: Number,
        required: [
            true,
            "El stock es necesario para el producto"
        ]
    },
    weight: {
        type: Number,
        required: [
            true,
            "La masa es necesaria para el producto"
        ]
    }
});
/* harmony default export */ const product = ((external_mongoose_default()).models.Product || external_mongoose_default().model("Product", ProductSchema));

;// CONCATENATED MODULE: ./pages/dashboard.js










const getStaticProps = async ()=>{
    await db();
    const data = await product.find();
    const productsText = JSON.stringify(data);
    return {
        props: {
            productsText
        }
    };
};
const DashboardPage = ({ productsText  })=>{
    const router = (0,router_.useRouter)();
    const [showForm, setShowForm] = (0,external_react_.useState)(false);
    const [loadProd, setLoadProds] = (0,external_react_.useState)(true);
    const [onEdit, setOnEdit] = (0,external_react_.useState)(false);
    const [idEdit, setIdEdit] = (0,external_react_.useState)("");
    const [list, setList] = (0,external_react_.useState)([]);
    if (loadProd) {
        const products = JSON.parse(productsText);
        products.forEach((prod)=>list.push(prod));
        setLoadProds(false);
    }
    const postLogout = async ()=>{
        const response = await fetch("/api/auth/logout");
        return response.json();
    };
    const logout = async ()=>{
        await postLogout();
        router.push("/login");
    };
    const displayForm = ()=>{
        setShowForm(!showForm);
    };
    const [product, setProduct] = (0,external_react_.useState)({
        name: "",
        stock: "",
        price: "",
        weight: ""
    });
    const resetValues = ()=>{
        setProduct({
            name: "",
            stock: "",
            price: "",
            weight: ""
        });
    };
    const sendProduct = async (prod)=>{
        const settings = {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application"
            },
            body: JSON.stringify(prod)
        };
        const response = await fetch("/api/products", settings);
        const data = await response.json();
        return data;
    };
    const handleChange = (element)=>{
        const target = element.target;
        const { name , value  } = target;
        setProduct({
            ...product,
            [name]: value
        });
    };
    const toInt = (data)=>{
        data.price = parseInt(data.price);
        data.stock = parseInt(data.stock);
        data.weight = parseFloat(data.weight);
    };
    const editHandler = async (id, body)=>{
        const settings = {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application"
            },
            body: JSON.stringify(body)
        };
        const response = await fetch(`/api/products/${id}`, settings);
        const data = await response.json();
        return data;
    };
    const handleSubmit = async (event)=>{
        event.preventDefault();
        toInt(product);
        if (onEdit) {
            const data = await editHandler(idEdit, product);
            if (!data.success) return;
            const { prod  } = data;
            setList(list.map((ele)=>{
                if (ele._id === prod._id) return prod;
                return ele;
            }));
            setOnEdit(false);
            displayForm();
            resetValues();
            return;
        }
        const data1 = await sendProduct(product);
        if (!data1.success) return;
        displayForm();
        setList([
            ...list,
            data1.prod
        ]);
        resetValues();
    };
    const deleteHandler = async (id)=>{
        const settings = {
            method: "DELETE",
            headers: {
                Accept: "application/json",
                "Content-Type": "application"
            }
        };
        const response = await fetch(`/api/products/${id}`, settings);
        const data = await response.json();
        if (!data.success) return;
        setList(list.filter(({ _id  })=>id !== _id));
    };
    return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: (dashboard_module_default()).all,
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (dashboard_module_default()).subnav,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Nav/* default */.Z, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                        className: (dashboard_module_default()).button,
                        onClick: ()=>logout(),
                        children: "Cerrar sesi\xf3n"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                children: "Inventario"
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: (dashboard_module_default()).body,
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: (dashboard_module_default()).tbuttons,
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: (dashboard_module_default()).thead,
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                        className: (dashboard_module_default()).hitem,
                                        children: "Producto"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                        className: (dashboard_module_default()).hitem,
                                        children: "Stock"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                        className: (dashboard_module_default()).hitem,
                                        children: "Precio unitario"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                        className: (dashboard_module_default()).hitem,
                                        children: "Masa"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                        className: (dashboard_module_default()).hitem,
                                        children: "Precio total"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                onClick: displayForm,
                                children: [
                                    "Agregar",
                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                    "producto"
                                ]
                            })
                        ]
                    }),
                    list.map(({ _id , name , stock , price , weight  })=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: (products_module_default()).tbuttons,
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: (products_module_default()).tbody,
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            className: (products_module_default()).item,
                                            children: name
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            className: (products_module_default()).item,
                                            children: stock
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                            className: (products_module_default()).item,
                                            children: [
                                                "$ ",
                                                price
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                            className: (products_module_default()).item,
                                            children: [
                                                weight,
                                                " kg"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                            className: (products_module_default()).item,
                                            children: [
                                                "$ ",
                                                stock * price
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    onClick: ()=>{
                                        setIdEdit(_id);
                                        displayForm();
                                        setOnEdit(true);
                                    },
                                    className: (products_module_default()).updbtn,
                                    children: "Editar"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    onClick: ()=>deleteHandler(_id),
                                    className: (products_module_default()).delbtn,
                                    children: "Eliminar"
                                })
                            ]
                        }, _id))
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: (dashboard_module_default()).addproduct,
                style: {
                    display: showForm ? "block" : "none"
                },
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                    className: (formProd_module_default()).form,
                    onSubmit: handleSubmit,
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                            htmlFor: "name",
                            children: "Nombre del producto"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            type: "text",
                            name: "name",
                            value: product.name,
                            onChange: handleChange,
                            required: true
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                            htmlFor: "stock",
                            children: "Stock del producto"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            type: "number",
                            name: "stock",
                            value: product.stock,
                            onChange: handleChange,
                            required: true
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                            htmlFor: "price",
                            children: "Precio del producto"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            type: "number",
                            name: "price",
                            value: product.price,
                            onChange: handleChange,
                            required: true
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("label", {
                            htmlFor: "weight",
                            children: "Masa del producto"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                            type: "number",
                            name: "weight",
                            value: product.weight,
                            onChange: handleChange,
                            required: true
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                            type: "submit",
                            children: "agregar"
                        })
                    ]
                })
            })
        ]
    });
};
/* harmony default export */ const dashboard = (DashboardPage);


/***/ }),

/***/ 3280:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/app-router-context.js");

/***/ }),

/***/ 2796:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4406:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/page-path/denormalize-page-path.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 1751:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/add-path-prefix.js");

/***/ }),

/***/ 6220:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/compare-states.js");

/***/ }),

/***/ 299:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/format-next-pathname-info.js");

/***/ }),

/***/ 3938:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 5789:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-next-pathname-info.js");

/***/ }),

/***/ 1897:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-bot.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 8854:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-path.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 4567:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/path-has-prefix.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 3297:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/remove-trailing-slash.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 6405:
/***/ ((module) => {

"use strict";
module.exports = require("react-dom");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [676,664,731], () => (__webpack_exec__(8807)));
module.exports = __webpack_exports__;

})();