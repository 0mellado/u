"use strict";
(() => {
var exports = {};
exports.id = 221;
exports.ids = [221];
exports.modules = {

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 2759:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

const MONGODB_URI = process.env.MONGODB_URI;
if (!MONGODB_URI) throw new Error("Por favor defina la variable de entorno MONGODB_URI dentro del archivo .env.local");
let cached = global.mongoose;
if (!cached) {
    cached = global.mongoose = {
        conn: null,
        promise: null
    };
}
const connect = async ()=>{
    if (cached.conn) {
        return cached.conn;
    }
    if (!cached.promise) {
        const opts = {
            bufferCommands: false
        };
        cached.promise = mongoose__WEBPACK_IMPORTED_MODULE_0___default().connect(MONGODB_URI, opts).then((mongoose)=>{
            return mongoose;
        });
    }
    try {
        cached.conn = await cached.promise;
    } catch (err) {
        cached.promise = null;
        throw err;
    }
    return cached.conn;
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (connect);


/***/ }),

/***/ 9931:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

const ProductSchema = new (mongoose__WEBPACK_IMPORTED_MODULE_0___default().Schema)({
    name: {
        type: String,
        required: [
            true,
            "el nombre del prodcuto es necesario"
        ]
    },
    price: {
        type: Number,
        required: [
            true,
            "El precio es necesario para el producto"
        ]
    },
    stock: {
        type: Number,
        required: [
            true,
            "El stock es necesario para el producto"
        ]
    },
    weight: {
        type: Number,
        required: [
            true,
            "La masa es necesaria para el producto"
        ]
    }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((mongoose__WEBPACK_IMPORTED_MODULE_0___default().models.Product) || mongoose__WEBPACK_IMPORTED_MODULE_0___default().model("Product", ProductSchema));


/***/ }),

/***/ 5547:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ products)
});

// EXTERNAL MODULE: ./lib/db.js
var db = __webpack_require__(2759);
// EXTERNAL MODULE: ./models/product.js
var models_product = __webpack_require__(9931);
;// CONCATENATED MODULE: ./controllers/products.controller.js


const createProduct = async (data)=>{
    const product = JSON.parse(data);
    try {
        await (0,db/* default */.Z)();
        const result = await models_product/* default.create */.Z.create(product);
        return result;
    } catch (err) {
        console.error(err);
    }
};
const readProduct = async ()=>{
    try {
        await (0,db/* default */.Z)();
        const products = await models_product/* default.find */.Z.find();
        return products;
    } catch (err) {
        console.error(err);
    }
};
const updateProduct = (req, res)=>{};
const deleteProduct = (req, res)=>{};

;// CONCATENATED MODULE: ./pages/api/products/index.js

const productsHandler = async (req, res)=>{
    const { body , method  } = req;
    switch(method){
        case "GET":
            const products = await readProduct();
            res.status(200).json(products);
            break;
        case "POST":
            const response = await createProduct(body);
            res.status(200).json({
                success: true,
                prod: response
            });
            break;
    }
};
/* harmony default export */ const products = (productsHandler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(5547));
module.exports = __webpack_exports__;

})();