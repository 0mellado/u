exports.id = 731;
exports.ids = [731];
exports.modules = {

/***/ 456:
/***/ ((module) => {

// Exports
module.exports = {
	"header": "Nav_header__ICGJp",
	"navigation": "Nav_navigation__KhKCU"
};


/***/ }),

/***/ 8731:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_Nav_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(456);
/* harmony import */ var _styles_Nav_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_Nav_module_css__WEBPACK_IMPORTED_MODULE_2__);



const links = [
    {
        label: "Inicio",
        route: "/"
    },
    {
        label: "Iniciar sesi\xf3n",
        route: "/login"
    },
    {
        label: "Registrarse",
        route: "/register"
    },
    {
        label: "Inventario",
        route: "/dashboard"
    }
];
const nav = links.map(({ label , route  })=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_link__WEBPACK_IMPORTED_MODULE_1___default()), {
            href: route,
            children: label
        })
    }, route));
const NavigationPage = ()=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("header", {
        className: (_styles_Nav_module_css__WEBPACK_IMPORTED_MODULE_2___default().header),
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                className: (_styles_Nav_module_css__WEBPACK_IMPORTED_MODULE_2___default().navigation),
                children: nav
            })
        })
    });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NavigationPage);


/***/ })

};
;