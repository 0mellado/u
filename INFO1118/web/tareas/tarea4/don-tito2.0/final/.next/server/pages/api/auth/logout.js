"use strict";
(() => {
var exports = {};
exports.id = 845;
exports.ids = [845];
exports.modules = {

/***/ 4802:
/***/ ((module) => {

module.exports = require("cookie");

/***/ }),

/***/ 9344:
/***/ ((module) => {

module.exports = require("jsonwebtoken");

/***/ }),

/***/ 9242:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9344);
/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jsonwebtoken__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4802);
/* harmony import */ var cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cookie__WEBPACK_IMPORTED_MODULE_1__);


const logoutHandler = (req, res)=>{
    const { tokenName  } = req.cookies;
    if (!tokenName) {
        return res.status(401).json({
            error: "no existe el token"
        });
    }
    try {
        (0,jsonwebtoken__WEBPACK_IMPORTED_MODULE_0__.verify)(tokenName, process.env.SECRET);
        const serialized = (0,cookie__WEBPACK_IMPORTED_MODULE_1__.serialize)("tokenName", null, {
            httpOnly: true,
            secure: "production" === "production",
            sameSite: "lax",
            maxAge: 0,
            path: "/"
        });
        res.setHeader("Set-Cookie", serialized);
        res.status(200).json("sesi\xf3n cerrada correctamente");
    } catch (err) {
        return res.status(401).json({
            error: "token invalido"
        });
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (logoutHandler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(9242));
module.exports = __webpack_exports__;

})();