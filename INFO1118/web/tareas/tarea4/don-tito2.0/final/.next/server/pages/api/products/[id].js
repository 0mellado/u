"use strict";
(() => {
var exports = {};
exports.id = 480;
exports.ids = [480];
exports.modules = {

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 9931:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

const ProductSchema = new (mongoose__WEBPACK_IMPORTED_MODULE_0___default().Schema)({
    name: {
        type: String,
        required: [
            true,
            "el nombre del prodcuto es necesario"
        ]
    },
    price: {
        type: Number,
        required: [
            true,
            "El precio es necesario para el producto"
        ]
    },
    stock: {
        type: Number,
        required: [
            true,
            "El stock es necesario para el producto"
        ]
    },
    weight: {
        type: Number,
        required: [
            true,
            "La masa es necesaria para el producto"
        ]
    }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((mongoose__WEBPACK_IMPORTED_MODULE_0___default().models.Product) || mongoose__WEBPACK_IMPORTED_MODULE_0___default().model("Product", ProductSchema));


/***/ }),

/***/ 2110:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _models_product__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9931);

const productHandler = async (req, res)=>{
    const { query: { id  } , method  } = req;
    switch(method){
        case "PUT":
            try {
                const updateProd = JSON.parse(req.body);
                const product = await _models_product__WEBPACK_IMPORTED_MODULE_0__/* ["default"].findByIdAndUpdate */ .Z.findByIdAndUpdate(id, updateProd, {
                    new: true,
                    runValidators: true
                });
                if (!product) {
                    return res.status(400).json({
                        success: false
                    });
                }
                res.status(200).json({
                    success: true,
                    prod: product
                });
            } catch (error) {
                res.status(400).json({
                    success: false
                });
            }
            break;
        case "DELETE":
            try {
                const deletedProd = await _models_product__WEBPACK_IMPORTED_MODULE_0__/* ["default"].deleteOne */ .Z.deleteOne({
                    _id: id
                });
                if (!deletedProd) {
                    return res.status(400).json({
                        success: false
                    });
                }
                res.status(200).json({
                    success: true
                });
            } catch (error1) {
                res.status(400).json({
                    success: false
                });
            }
            break;
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (productHandler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(2110));
module.exports = __webpack_exports__;

})();