"use strict";
(() => {
var exports = {};
exports.id = 553;
exports.ids = [553];
exports.modules = {

/***/ 7096:
/***/ ((module) => {

module.exports = require("bcrypt");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 3348:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7096);
/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(bcrypt__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lib_db__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2759);
/* harmony import */ var _models_user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(404);



const registerHandler = async (req, res)=>{
    const { method  } = req;
    const userJson = JSON.parse(req.body);
    const newUser = {
        username: userJson.username,
        admin: false,
        email: userJson.email,
        pwd: bcrypt__WEBPACK_IMPORTED_MODULE_0___default().hashSync(userJson.pwd, 10)
    };
    if (method != "POST") return res.status(403).json({
        success: false,
        data: process.env.FORBIDDEN
    });
    try {
        await (0,_lib_db__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z)();
        const userQuery = await _models_user__WEBPACK_IMPORTED_MODULE_2__/* ["default"].findOne */ .Z.findOne({
            "$or": [
                {
                    username: userJson.username
                },
                {
                    email: userJson.email
                }
            ]
        });
        if (userQuery) return res.status(418).json({
            success: false,
            data: process.env.USER_EXIST
        });
        const user = await _models_user__WEBPACK_IMPORTED_MODULE_2__/* ["default"].create */ .Z.create(newUser);
        if (!user) return res.status(400).json({
            success: true,
            data: process.env.USER_REGISTERED_ERROR
        });
        res.status(201).json({
            success: true,
            data: process.env.USER_REGISTERED
        });
    } catch (err) {
        res.status(400).json({
            success: false,
            data: process.env.MONGODB_CONNECT_ERROR
        });
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (registerHandler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [46], () => (__webpack_exec__(3348)));
module.exports = __webpack_exports__;

})();