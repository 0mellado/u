import {verify} from "jsonwebtoken"
import {serialize} from "cookie"

const logoutHandler = (req, res) => {

    const {tokenName} = req.cookies

    if (!tokenName) {
        return res.status(401).json({error: 'no existe el token'})
    }

    try {
        verify(tokenName, process.env.SECRET)
        const serialized = serialize('tokenName', null, {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            sameSite: 'lax',
            maxAge: 0,
            path: '/'
        })
        res.setHeader('Set-Cookie', serialized)
        res.status(200).json('sesión cerrada correctamente')
    } catch (err) {
        return res.status(401).json({error: 'token invalido'})
    }
}

export default logoutHandler
