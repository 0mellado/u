import {useRouter} from "next/router"
import {useState} from "react"
import styles from '../styles/login.module.css'
import NavigationPage from "../components/Nav"

const RegisterPage = () => {

    const router = useRouter()
    const [message, setMessage] = useState()
    const [user, setUser] = useState({
        email: '',
        username: '',
        pwd: '',
        pwdconfirm: ''
    })

    const validatePasswd = () => {
        return user.pwd !== user.pwdconfirm
    }

    const sendNewUser = async data => {
        const response = await fetch('/api/register', {
            method: 'POST',
            body: JSON.stringify(data)
        })

        return response.json()
    }

    const handleChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async e => {
        e.preventDefault()

        if (validatePasswd()) {
            setMessage('Las contraseñas no coinciden')
            return
        }

        const response = await sendNewUser(user)
        alert(response.data)

        if (response.success) {
            router.push('/login')
        }
    }

    return (
        <div>
            <NavigationPage />
            <div className={styles.container}>
                <div>
                    <h2>Registrarse</h2>
                    <form className={styles.form} onSubmit={handleSubmit}>
                        <label htmlFor='email'>Correo electronico</label>
                        <input
                            name="email"
                            type="email"
                            placeholder="email"
                            onChange={handleChange}
                            required
                        />
                        <label htmlFor='username'>Nombre de usuario</label>
                        <input
                            name="username"
                            type="text"
                            placeholder="Nombre de usuario"
                            onChange={handleChange}
                            required
                        />
                        <label htmlFor='pwd'>Contraseña</label>
                        <input
                            name="pwd"
                            type="password"
                            placeholder="Contraseña"
                            onChange={handleChange}
                            required
                        />
                        <label htmlFor='pwdconfirm'>Confirmar contraseña</label>
                        <input
                            name="pwdconfirm"
                            type="password"
                            placeholder="Confirmar contraseña"
                            onChange={handleChange}
                            required
                        />
                        <button>
                            Registrarse
                        </button>
                    </form>
                    <p>{message}</p>
                </div>
            </div>
        </div>
    )
}

export default RegisterPage 
