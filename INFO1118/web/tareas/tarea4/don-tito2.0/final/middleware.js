import {NextResponse} from "next/server";
import {jwtVerify} from "jose";

export async function middleware(request) {

    const token = request.cookies.get('tokenName')

    if (!token)
        return NextResponse.redirect(new URL('/login', request.url))

    try {
        const {payload: {admin}} = await jwtVerify(
            token.value,
            new TextEncoder().encode(process.env.SECRET)
        )
        console.log(admin)
        if (!admin) return NextResponse.redirect(new URL('/', request.url))
        return NextResponse.next()
    } catch (err) {
        return NextResponse.redirect(new URL('/login', request.url))
    }
}

export const config = {
    matcher: ['/dashboard/:path*']
}
