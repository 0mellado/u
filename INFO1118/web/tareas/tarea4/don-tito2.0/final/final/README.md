## Configuración del archivo .env.local

Dentro del archivo .env.local se encontraran las credenciales de acceso a la base de datos
y los diferentes mensajes que recibe el usuario mientras usa la aplicación.

En la constante MONGODB_URI se escribe la url con la conexión a la base de datos.

```env
MONGODB_URI='aquí/va/la/url'
```

En el archivo con la variables de entorno también se encuentra un secret, para el manejo de las sesiones con
jsonwebtoken.

```env
SECRET=''
```

## Instalar dependencias

```bash
npm install
```

## Ejecutar el proyecto

Para ejecutar el servidor en modo de desarrollo se :

```bash
npm run dev
# or
yarn dev
```

Copia y pega el siguiente link en tu navegador: [http://localhost:3000](http://localhost:3000).

