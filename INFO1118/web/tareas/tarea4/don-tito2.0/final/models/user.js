import mongoose from 'mongoose'

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'el nombre de usuario es necesario']
    },
    admin: {
        type: Boolean
    },
    email: {
        type: String,
        required: [true, 'El email es necesario para el usuario']
    },
    pwd: {
        type: String,
        required: [true, 'La contraseña es necesaria para el usuario']
    },
})

export default mongoose.models.User || mongoose.model('User', UserSchema)
