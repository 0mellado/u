import mongoose from 'mongoose'

const ProductSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'el nombre del prodcuto es necesario']
    },
    price: {
        type: Number,
        required: [true, 'El precio es necesario para el producto']
    },
    stock: {
        type: Number,
        required: [true, 'El stock es necesario para el producto']
    },
    weight: {
        type: Number,
        required: [true, 'La masa es necesaria para el producto']
    },
})

export default mongoose.models.Product || mongoose.model('Product', ProductSchema)
