import dbConnect from '../lib/db'
import Product from '../models/product'

export const createProduct = async (data) => {
    const product = JSON.parse(data)
    try {
        await dbConnect()
        const result = await Product.create(product)
        return result
    } catch (err) {
        console.error(err)
    }

}

export const readProduct = async () => {
    try {
        await dbConnect()
        const products = await Product.find()
        return products
    } catch (err) {
        console.error(err)
    }
}

export const updateProduct = (req, res) => {

}

export const deleteProduct = (req, res) => {

}

