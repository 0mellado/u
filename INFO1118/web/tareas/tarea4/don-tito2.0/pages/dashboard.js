import {useRouter} from 'next/router'
import Link from 'next/link'
import {useState} from 'react'
import NavigationPage from '../components/Nav'
import styles from '../styles/dashboard.module.css'
import stylesProds from '../styles/products.module.css'
import stylesForm from '../styles/formProd.module.css'
import dbConnect from '../lib/db'
import Product from '../models/product'

export const getStaticProps = async () => {
    await dbConnect()
    const data = await Product.find()
    const productsText = JSON.stringify(data)
    return {props: {productsText}}
}

const DashboardPage = ({productsText}) => {

    const router = useRouter()
    const [showForm, setShowForm] = useState(false)
    const [loadProd, setLoadProds] = useState(true)
    const [onEdit, setOnEdit] = useState(false)
    const [idEdit, setIdEdit] = useState('')
    const [list, setList] = useState([])

    if (loadProd) {
        const products = JSON.parse(productsText)
        products.forEach(prod => list.push(prod))
        setLoadProds(false)
    }

    const postLogout = async () => {
        const response = await fetch('/api/auth/logout')
        return response.json()
    }


    const logout = async () => {
        await postLogout()
        router.push('/login')
    }

    const displayForm = () => {
        setShowForm(!showForm)
    }

    const [product, setProduct] = useState({
        name: '',
        stock: '',
        price: '',
        weight: ''
    })

    const resetValues = () => {
        setProduct({
            name: '',
            stock: '',
            price: '',
            weight: ''
        })
    }

    const sendProduct = async (prod) => {
        const settings = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application',
            },
            body: JSON.stringify(prod)
        }
        const response = await fetch('/api/products', settings)
        const data = await response.json()
        return data
    }

    const handleChange = element => {
        const target = element.target
        const {name, value} = target

        setProduct({
            ...product,
            [name]: value,
        })
    }

    const toInt = data => {
        data.price = parseInt(data.price)
        data.stock = parseInt(data.stock)
        data.weight = parseFloat(data.weight)
    }

    const editHandler = async (id, body) => {
        const settings = {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application',
            },
            body: JSON.stringify(body)
        }

        const response = await fetch(`/api/products/${id}`, settings)
        const data = await response.json()
        return data
    }

    const handleSubmit = async event => {
        event.preventDefault()
        toInt(product)
        if (onEdit) {
            const data = await editHandler(idEdit, product)
            if (!data.success) return
            const {prod} = data
            setList(list.map(ele => {
                if (ele._id === prod._id) return prod
                return ele
            }))
            setOnEdit(false)
            displayForm()
            resetValues()
            return
        }
        const data = await sendProduct(product)
        if (!data.success) return
        displayForm()
        setList([...list, data.prod])
        resetValues()
    }


    const deleteHandler = async id => {
        const settings = {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application',
            }
        }
        const response = await fetch(`/api/products/${id}`, settings)
        const data = await response.json()
        if (!data.success) return
        setList(list.filter(({_id}) => id !== _id))
    }

    return (
        <div className={styles.all}>
            <div className={styles.subnav}>
                <NavigationPage />
                <button className={styles.button} onClick={() => logout()}>
                    Cerrar sesión
                </button>
            </div>
            <h1>Inventario</h1>
            <div className={styles.body}>
                <div className={styles.tbuttons}>
                    <div className={styles.thead}>
                        <h3 className={styles.hitem}>Producto</h3>
                        <h3 className={styles.hitem}>Stock</h3>
                        <h3 className={styles.hitem}>Precio unitario</h3>
                        <h3 className={styles.hitem}>Masa</h3>
                        <h3 className={styles.hitem}>Precio total</h3>
                    </div>
                    <button onClick={displayForm}>Agregar<br />producto</button>
                </div>
                {list.map(({_id, name, stock, price, weight}) =>
                    <div key={_id} className={stylesProds.tbuttons}>
                        <div className={stylesProds.tbody}>
                            <p className={stylesProds.item}>{name}</p>
                            <p className={stylesProds.item}>{stock}</p>
                            <p className={stylesProds.item}>$ {price}</p>
                            <p className={stylesProds.item}>{weight} kg</p>
                            <p className={stylesProds.item}>$ {stock * price}</p>
                        </div>
                        <button onClick={() => {
                            setIdEdit(_id);
                            displayForm();
                            setOnEdit(true);
                        }} className={stylesProds.updbtn}>Editar</button>
                        <button onClick={() => deleteHandler(_id)} className={stylesProds.delbtn}>Eliminar</button>
                    </div>
                )}
            </div>
            <div className={styles.addproduct} style={{
                display: showForm ? "block" : "none"
            }}>
                <form className={stylesForm.form} onSubmit={handleSubmit}>
                    <label htmlFor="name">Nombre del producto</label>
                    <input
                        type="text"
                        name="name"
                        value={product.name}
                        onChange={handleChange}
                        required
                    />
                    <label htmlFor="stock">Stock del producto</label>
                    <input
                        type="number"
                        name="stock"
                        value={product.stock}
                        onChange={handleChange}
                        required
                    />
                    <label htmlFor="price">Precio del producto</label>
                    <input
                        type="number"
                        name="price"
                        value={product.price}
                        onChange={handleChange}
                        required
                    />
                    <label htmlFor="weight">Masa del producto</label>
                    <input
                        type="number"
                        name="weight"
                        value={product.weight}
                        onChange={handleChange}
                        required
                    />
                    <button type="submit">agregar</button>
                </form>
            </div>
        </div>
    )
}

export default DashboardPage
