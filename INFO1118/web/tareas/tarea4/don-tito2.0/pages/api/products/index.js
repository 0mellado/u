import {
    createProduct,
    readProduct,
} from '../../../controllers/products.controller'

const productsHandler = async (req, res) => {

    const {body, method} = req

    switch (method) {
        case 'GET':
            const products = await readProduct()
            res.status(200).json(products)
            break
        case 'POST':
            const response = await createProduct(body)
            res.status(200).json({success: true, prod: response})
            break
    }
}

export default productsHandler
