const idsList = ["l1", "l2", "l3", "l4"];
const idsImg = ["v16-1", "v16-2", "v16-3", "v16-4"];
const btn1 = document.getElementById("btn1");
const btn2 = document.getElementById("btn2");
const ol = document.getElementById("list");
const divDate = document.getElementById("dates");

const v16s = [];

for (let i = 0; i < 4; i++) {
    v16s[i] = {
        obj: document.getElementById(idsList[i]),
        objImg: document.getElementById(idsImg[i]),
        flag: false
    };
}

let currentLi;
let last = 0;

for (let i = 0; i < v16s.length; i++) {
    if (!v16s[i].flag) {
        v16s[i].objImg.style.display = "none";
    }

    v16s[i].obj.addEventListener("click", event => {

        v16s[last].obj.style.color = "";
        v16s[last].objImg.style.display = "none";

        v16s[i].obj.style.color = "red";
        v16s[i].objImg.style.display = "";

        currentLi = i;

        last = currentLi;
    });
}

let btnStatus = 1;

btn1.onclick = () => {
    if (btnStatus) {
        ol.style.fontSize = "20px";
        btnStatus = 0;
    } else {
        ol.style.fontSize = "";
        btnStatus = 1;
    }
}

btn2.onclick = () => {
    let hoy = new Date();
    let ahora = hoy.toLocaleString();
    const paraf = document.createElement("p");
    paraf.textContent = `La fecha y hora en es: ${ahora}`;

    divDate.appendChild(paraf);
}
