const body = document.getElementById("body"); // se obtiene el body para poder manejarlo
const sleep = ms => new Promise(r => setTimeout(r, ms)); // se define una función para que
// el programa se detenga un tiempo determinado

let sections = []; // se inicializan arreglos vacíos que van 
let navs = [];     // a tener los elementos de las secciones y los elementos del nav
for (let i = 0; i < 5; i++) { // se usa un bucle para obtener y añadir los elementos
    const sec = document.getElementById(`section-${i + 1}`); // se obtienen los elementos de las secciones
    const nav = document.getElementById(`nav-${i + 1}`);  // se obtienen los elementos del nav
    navs[i] = nav; // se añaden los elementos del nav al arreglo del nav
    sections[i] = sec; // se añaden los elementos de las secciones al arreglo de secciones
}

body.style.backgroundImage = "url(\"img/yo.jpg\")"// se pone un fondo por defecto
let imgs = ["yo.jpg", "estudios.jpg", "experiencia.png", "hobbies.jpg", "proyectos.jpg"]; // arreglo con los nombres de las imagenes

navs.map((nav, i) => { // con la función map se recorren los arreglos
    let status = 1 // se inicializa un variable status para saber en que estado esta la sección
    nav.addEventListener("click", async () => { // se añade un evento que escucha a los click sobre el nav
        if (status) { // si el estado es 1 hace lo siguiente
            sections[i].style.display = ""; // muestra la sección
            body.style.backgroundImage = `url(\"img/${imgs[i]}\")`; // cambia el fondo de la página
            await sections[i].animate([ // y anima como entra la sección 
                {transform: `translateX(-200%)`},
                {transform: `translateX(${20 * i}%)`}
            ], {
                duration: 500 // la animación dura 500 milisegundos
            });
            status = 0; // se cambia el estado de la sección
        } else { // si el estado es 0 hace lo siguiente
            await sections[i].animate([ // anima la salida de la sección
                {transform: 'translateX(0px)'},
                {transform: `translateX(-${600 * (i + 1)}px)`}
            ], {
                duration: (500 * (i + 1)) // la salida de la sección dura 500 ms por su índice más 1
            });
            await sleep(500); // el programa se detiene por 500 milisegundos
            sections[i].style.display = "none"; // se oculta la sección
            status = 1; // se cambia el estado de la sección
        }
    });
});

