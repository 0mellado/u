# By Alberto Caro S.
# Ing. Civil en Informatica
# Doctor(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
# Programacion de Robot -> INFO1139
#---------------------------------------------------------------------
#__________      ___.           __  .__               
#\______   \ ____\_ |__   _____/  |_|__| ____ _____   
# |       _//  _ \| __ \ /  _ \   __\  |/ ___\\__  \  
# |    |   (  <_> ) \_\ (  <_> )  | |  \  \___ / __ \_
# |____|_  /\____/|___  /\____/|__| |__|\___  >____  /
#        \/           \/                    \/     \/ 
#---------------------------------------------------------------------
import random as ra, ctypes as CT, matplotlib.pyplot as plt, serial as sr


class eCelda(CT.Structure):
    _fields_ = [
        ('nF',  CT.c_ubyte), # fila
        ('nC',  CT.c_ubyte), # columna
        ('Au',  CT.c_ubyte), # oro
        ('FeC', CT.c_ubyte), # acero profe el acero no sale de la tierra xd
        ('Li',  CT.c_ubyte), # litio
        ('Ni',  CT.c_ubyte), # niquel
        ('Cu',  CT.c_ubyte), # cobre
    ]


def readMap():
    aData= []
    datMap = open('mapa.dat', 'rb')
    for i in range(0, 9):
        aData.append([])
        for _ in range(0, 12):
            celda = eCelda()
            datMap.readinto(celda)
            aData[i].append(celda)
    datMap.close()

    return aData


def fShow_Data(wea):
    line = 'F:%02d-C:%02d-Oro:%03d-Acero:%03d-Litio:%03d-Niquel:%03d-Cobre:%03d\n'
    for i in range(9):
        for j in range(12):
            nF = wea[i][j].nF
            nC = wea[i][j].nC
            Au = wea[i][j].Au
            FeC= wea[i][j].FeC
            Li = wea[i][j].Li
            Ni = wea[i][j].Ni
            Cu = wea[i][j].Cu
            print(line % (nF, nC, Au, FeC, Li, Ni, Cu))
    return

def fMyG1(wea):
    aTE = [] ; aMP25 = [] ; aMP10 = [] ; aPH = [] ; aPA = []
    for i in range(9):
        for j in range(12):
            aTE.append(wea[i][j].Au)
            aMP25.append(wea[i][j].FeC)
            aMP10.append(wea[i][j].Li)
            aPH.append(wea[i][j].Ni)
            aPA.append(wea[i][j].Cu)

    fig, axs = plt.subplots(5,sharex=True)
    fig.suptitle('Recursos por baldosa')

    axs[0].plot(aTE,'r')
    axs[0].grid(1)
    axs[0].set_title('Oro')
    axs[0].set(ylabel='Grados')

    axs[1].plot(aMP25,'g')
    axs[1].grid(1)
    axs[1].set_title('Acero')
    axs[1].set(ylabel='ug/m3')

    axs[2].plot(aMP10,'b')
    axs[2].grid(1)
    axs[2].set_title('Litio')
    axs[2].set(ylabel='ug/m3')

    axs[3].plot(aPH,'k')
    axs[3].grid(1)
    axs[3].set_title('Niquel')
    axs[3].set(ylabel='PH')

    axs[4].plot(aPA,'y')
    axs[4].grid(1)
    axs[4].set_title('Cobre')
    axs[4].set(ylabel='Pa')
    axs[4].set(xlabel='Muestras')

    fig.savefig('estaciones.png')
    plt.show()
    return


aData = [[eCelda() for _ in range(9)] \
                    for _ in range(12)]

wea = readMap()
fShow_Data(wea) ; fMyG1(wea)


