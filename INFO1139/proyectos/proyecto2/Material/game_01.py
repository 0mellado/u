# By Alberto Caro S.
# Ing. Civil en Computacion
# Magister Cs. Ing.
# MAgister Cs. Computacion
# Doctor(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
#-------------------------------------------
import pygame,time, random as RA, ctypes as ct
from pygame.locals import *

#---------------------------------------------------------------------
# Definicion de Constantes.-
#---------------------------------------------------------------------
nRES = (629,860) ; nMAX_MALOS   = 9

nPOS_MAX_Y_1 = 710 ; nPOS_MAX_Y_2 = 540 ; nPOS_MAX_Y_3 = 813
nMIN_X = 001 ; nMAX_X = 550 ; nMIN_Y = 040 ; nMAX_Y = 550 #limite mov enemigos
nFALSE = 0 ; nTRUE  = 1 ; nTIME1 = 50 ; nTIME2 = 100

#nPOS_MAX_Y_1 => posicion en y del pajaro
#nPOS_MAX_Y_3 => posicion en y de la cucaracha

aPOS_MALOS = [
              [050,100],[270,100],[500,100],
              [050,200],[270,200],[500,200],
              [050,300],[270,300],[500,300],
             ]

#---------------------------------------------------------------------
# Definicion de Estructura de Datos.-
#---------------------------------------------------------------------
class eMalos(ct.Structure):
 _fields_ = [
             ('nF',ct.c_short),('nX',ct.c_short),('nY',ct.c_short),
	     ('nR',ct.c_short),('dX',ct.c_short),('dY',ct.c_short),
	     ('lV',ct.c_short)
            ]

class eBig(ct.Structure):
 _fields_ = [
             ('nX',ct.c_short),('nY',ct.c_short),('nR',ct.c_short),
             ('xD',ct.c_short),('yD',ct.c_short)
            ]

class eBird(ct.Structure):
 _fields_ = [
             ('nX',ct.c_short),('nY',ct.c_short),
	   ('nT',ct.c_short),('oK',ct.c_short),
             ('nF',ct.c_short),('Sw',ct.c_short)
            ]

class eCuca(ct.Structure):
 _fields_ = [
             ('nX',ct.c_short),('nY',ct.c_short),
             ('nT',ct.c_short),('oK',ct.c_short),
             ('nF',ct.c_short),('Sw',ct.c_short)
            ]

class eNave(ct.Structure):
 _fields_ = [
             ('nX',ct.c_short),('nY',ct.c_short),
             ('nD',ct.c_short),('nF',ct.c_short)
            ]

#---------------------------------------------------------------------
# Carga imagenes y convierte formato pygame
#---------------------------------------------------------------------
def Load_Image(sFile,transp = False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Pinta fondo a Pantalla
#---------------------------------------------------------------------
def Pinta_Fondo():
    Panta.blit(Fondo,(000,000))
    return

#---------------------------------------------------------------------
# Dibuja las Naves Malas.-
#---------------------------------------------------------------------
def Pinta_Malos():
    for i in range(0,nMAX_MALOS):
        aM[i].nR -= 1
	if aM[i].nR < 0:
	   aM[i].nR = RA.randint(100,500)
	   nDir = RA.randint(1,9)
	   if nDir == 1:
	      aM[i].dX = +0 ; aM[i].dY = -1
           elif nDir == 2:
	      aM[i].dX = +1 ; aM[i].dY = -1
           elif nDir == 3:
	      aM[i].dX = +1 ; aM[i].dY = +0
           elif nDir == 4:
	      aM[i].dX = +1 ; aM[i].dY = +1
           elif nDir == 5:
	      aM[i].dX = +0 ; aM[i].dY = +1
           elif nDir == 6:
	      aM[i].dX = -1 ; aM[i].dY = +1
           elif nDir == 7:
	      aM[i].dX = -1 ; aM[i].dY = +0
	   elif nDir == 8:
	      aM[i].dX = -1 ; aM[i].dY = -1
           else:
	      aM[i].dX = +0 ; aM[i].dY = +0

	#Actualizamos (Xs,Ys) de los Sprites.-
	#-------------------------------------
	aM[i].nX += aM[i].dX*1
	aM[i].nY += aM[i].dY*1

	if aM[i].nX < nMIN_X: aM[i].nX = nMIN_X ; aM[i].nR = 0
	if aM[i].nX > nMAX_X: aM[i].nX = nMAX_X ; aM[i].nR = 0
	if aM[i].nY < nMIN_Y: aM[i].nY = nMIN_Y ; aM[i].nR = 0
	if aM[i].nY > nMAX_Y: aM[i].nY = nMAX_Y ; aM[i].nR = 0

	if aM[i].nF ==   0: Panta.blit(Img01,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 1: Panta.blit(Img02,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 2: Panta.blit(Img03,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 3: Panta.blit(Img04,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 4: Panta.blit(Img05,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 5: Panta.blit(Img06,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 6: Panta.blit(Img07,(aM[i].nX,aM[i].nY))
        elif aM[i].nF == 7: Panta.blit(Img08,(aM[i].nX,aM[i].nY))
        else:               Panta.blit(Img09,(aM[i].nX,aM[i].nY))

    return

#---------------------------------------------------------------------
# Dibuja Pajaro-
# nT se utiliza como contador para comenzar a pintar el pajaro
# Se activa oK para hacer que el pajaro comience a moverse de izq. a der.
# Sw se ocupa para cambiar la figura del pajaro
#---------------------------------------------------------------------
def Pinta_Bird():
    if eP.nT < 0:
       eP.nT = RA.randint(100,1000)
       eP.oK = nTRUE
    else:
       eP.nT -= 1

    if eP.oK:
       eP.nX += 1
       if eP.nX > 640: eP.nX = -50 ; eP.oK = nFALSE
       if eP.nF == 1: Panta.blit(Img11,(eP.nX,nPOS_MAX_Y_1))
       if eP.nF == 2: Panta.blit(Img12,(eP.nX,nPOS_MAX_Y_1))
       eP.Sw -= 1
       if eP.Sw == 0:
          eP.Sw = 20
          eP.nF += 1
          if eP.nF > 2:
              eP.nF = 1
    return

#---------------------------------------------------------------------
# Mueve Cucaracha-
# idem al pajaro
#---------------------------------------------------------------------
def Mueve_Cuca():
    if eC.nT < 0:
       eC.nT = RA.randint(640,650)
       eC.oK = nTRUE
    else:
       eC.nT -= 1

    if eC.oK:
       eC.nX -= 1
       if eC.nX < -050: eC.nX = 630 ; eC.oK = nFALSE
       eC.Sw -= 1
       if eC.Sw == 0:
          eC.Sw = 5
          eC.nF += 1
          if eC.nF > 4:
              eC.nF = 1
    return

#---------------------------------------------------------------------
# Pinta Cucaracha-
#---------------------------------------------------------------------
def Pinta_Cuca():
    if eC.nF == 1: Panta.blit(Img13,(eC.nX,nPOS_MAX_Y_3))
    if eC.nF == 2: Panta.blit(Img14,(eC.nX,nPOS_MAX_Y_3))
    if eC.nF == 3: Panta.blit(Img15,(eC.nX,nPOS_MAX_Y_3))
    if eC.nF == 4: Panta.blit(Img16,(eC.nX,nPOS_MAX_Y_3))
    return

#---------------------------------------------------------------------
# Init PyGames
#---------------------------------------------------------------------
def Init():
    pygame.init()
    pygame.time.set_timer(USEREVENT+1,nTIME1)#fija 2 eventos que se activan cada
    pygame.time.set_timer(USEREVENT+2,nTIME2)#50 y 100 ms
    pygame.display.set_caption('Demo Games - By Alberto Caro - 2013()')
    return pygame.display.set_mode(nRES)

#---------------------------------------------------------------------
# Init Naves de los Malos
#---------------------------------------------------------------------
def Malos_Init():
    for i in range(0,nMAX_MALOS):
        aM[i].nF = i                # Primary Key Nave.-
        aM[i].nX = aPOS_MALOS[i][0] # Columnas.-
        aM[i].nY = aPOS_MALOS[i][1] # Filas.-
        aM[i].nR = 100             # Rango de Desplazamiento.-
        aM[i].dX = 000              # No se mueve en X.- Dire = 9
        aM[i].dY = 000              # No se mueve en Y.- Dire = 9
        aM[i].lV = nTRUE            # Esta Vivo.-
    return

#---------------------------------------------------------------------
# Init eBig = Nave Mala Grande.-
#---------------------------------------------------------------------
def Big_Init():
    eB.nX = 250
    eB.nY = 300
    eB.nR = 100 + RA.randint(001,200)
    eB.dX = RA.randint(-1,1)
    eB.dY = RA.randint(-1,1)
    return

#---------------------------------------------------------------------
# Init eBird = Pajaro.-
#---------------------------------------------------------------------
def Bird_Init():
    eP.nX = -050
    eP.nY = nPOS_MAX_Y_1
    eP.nT = RA.randint(100,1000)
    eP.oK = nFALSE
    eP.nF = 1
    eP.Sw = 20
    return

#---------------------------------------------------------------------
# Init eCuca = Cucaracha-
#---------------------------------------------------------------------
def Cuca_Init():
    eC.nX = 630
    eC.nY = nPOS_MAX_Y_3
    eC.nT = RA.randint(640,650)
    eC.oK = nFALSE
    eC.nF = 01
    eC.Sw = 5
    return

#---------------------------------------------------------------------
# Init eNave = My Nave.-
#---------------------------------------------------------------------
def Nave_Init():
    eN.nX = 310
    eN.nY = nPOS_MAX_Y_3 #misma posicion de la cucaracha en y
    eN.nD = 00           #en este codigo no se usa esta nave, pero usted puede
    eN.nF = 01           #implementarla
    return

#--------------------------------------------------------------
# Handle de Pause.-
#--------------------------------------------------------------
def Pausa():
    while 1:
      e = pygame.event.wait()
      if e.type in (pygame.QUIT, pygame.KEYDOWN):
         return

#---------------------------------------------------------------------
# Main.-
#---------------------------------------------------------------------
Panta = Init()
Fondo = Load_Image('fondo.png',False)  # fondo.-
Img00 = Load_Image('n1.png',True ) # Nave Buena.-
Img01 = Load_Image('m1.png',True) # Malo 1
Img02 = Load_Image('m2.png',True ) # Malo 2
Img03 = Load_Image('m3.png',True ) # Malo 3
Img04 = Load_Image('m4.png',True ) # Malo 4
Img05 = Load_Image('m5.png',True ) # Malo 5
Img06 = Load_Image('m6.png',True ) # Malo 6
Img07 = Load_Image('m7.png',True ) # Malo 7
Img08 = Load_Image('m8.png',True ) # Malo 8
Img09 = Load_Image('m9.png',True ) # Malo 9
Img10 = Load_Image('big.png',True) # Nave Gigante.-
Img11 = Load_Image('pj1.png',True) # Pajaro Sube
Img12 = Load_Image('pj2.png',True) # Pajaro Baja
Img13 = Load_Image('cu1.png',True) # Cucaracha Paso 1
Img14 = Load_Image('cu2.png',True) # Cucaracha Paso 2
Img15 = Load_Image('cu3.png',True) # Cucaracha Paso 3
Img16 = Load_Image('cu4.png',True) # Cucaracha Paso 4

aM = [ eMalos() for i in range(0,nMAX_MALOS) ]
eB = eBig() ; eP = eBird() ; eC = eCuca() ; eN = eNave()
Malos_Init(); Big_Init(); Bird_Init(); Cuca_Init(); Nave_Init()

clock = pygame.time.Clock()

lOK = 1
while lOK:
 cKey = pygame.key.get_pressed()
 ev = pygame.event.get()
 for e in ev:
     if cKey[pygame.K_ESCAPE]:
        lOK = 'a' > 'b'
     if cKey[pygame.K_p]:
        Pausa()
     if e.type == QUIT:
        lOK = 0
     if e.type == USEREVENT+1: #cuando sucede el timer...
        Mueve_Cuca()           #lo que sucede de mnera paralela al resto del codigo
     #if e.type == USEREVENT+2:

 Pinta_Fondo()
 Pinta_Malos()
 Pinta_Bird()
 Pinta_Cuca()
 pygame.display.flip()
 clock.tick(100)


