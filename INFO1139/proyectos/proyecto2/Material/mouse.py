# By Alberto Caro S.
# PhD Robotica - PUC
#-------------------------

import pygame
from pygame.locals import *

nMAX_X = 640 ; nMAX_Y = 480
LEFT   = 1   ; MIDDLE = 2 ; RIGTH  = 3

#---------------------------------------------------------------------
# Carga imagenes y convierte formato pygame
#---------------------------------------------------------------------
def Load_Image(sFile,transp=False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Pinta Cursor (x,y) en MAPA
#---------------------------------------------------------------------
def Pinta_Cursor(nX=0,nY=0):
    if nX >=0 and nX <= nMAX_X:
       if nY >=0 and nY <= nMAX_Y:
          scr.blit(Img1,(nX_M,nY_M))
    return

#---------------------------------------------------------------------
#  MAIN.-
#---------------------------------------------------------------------
pygame.init()
scr  = pygame.display.set_mode((nMAX_X,nMAX_Y))
nX_M = nY_M = cKey = 0 ; lGo  = True
Img1 = Load_Image('pj1.png',True)
while lGo:
 cKey = pygame.key.get_pressed()
 scr.fill((0,0,0))
 Pinta_Cursor(nX_M,nY_M)
 for e in pygame.event.get():
          if e.type == pygame.QUIT:
             lGo = False
          if cKey[pygame.K_ESCAPE]:
             lGo = False
          if e.type == pygame.MOUSEMOTION:
             nX_M,nY_M = e.pos
 pygame.display.flip()



