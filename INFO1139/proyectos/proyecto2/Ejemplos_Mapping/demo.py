#-----------------------------------------------------------------------------
# By Alberto Caro S.
# Ing. Civil en Computacion
# Doctor(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
# Area de Investigacio:
#       Robotica
#       MicroControladores y Sensores
#       Inteligencia Artificial
#       Aprendizaje de Maquinas
#       Procesamiento de Imagenes
#       Programacion y Simulacion
#       Modelamientos Markovianos
#
# Lenguajes preferidos:
#
#      Python
#      C
#      Delphi / Lazarus
#      Assembler
#      JavaScript
#
# Sistema Operativo:
#
#  Me da lo mismo. Con cualquiera de ellos lo paso bien.
#-----------------------------------------------------------------------------

from pygame.locals import *
import pygame as pg, random as ra, time as ti, ctypes as ct

nRES = (500,800) ; lOK = True ; nMAX_GUN = 20 ; nON = 1 ; nOFF = 0;
nBTN_RIGHT = 3 ; nBTN_LEFT = 1 ; nMIN_Y = -30


#-----------------------------------------------------------------------------
# Estructura de datos (Clase)
# En esta estructura almaceno las variables que modelan el disparo y estado
# del armamento del lado izquierdo de la nave
#-----------------------------------------------------------------------------
class eGun_I(ct.Structure): # Estructura Armamento Ala Izquierda
 _fields_ = [('nX',ct.c_short), # Pos X  Misil.-
             ('nY',ct.c_short), # Pos Y  Misil.-
             ('nF',ct.c_short), # Sprite Misil.-
             ('nV',ct.c_short), # Veloc. Misil.-
             ('nD',ct.c_short), # Direc. Misil.-
             ('lF',ct.c_short)  # Flag.  Misil.-
            ]

#-----------------------------------------------------------------------------
# Estructura de datos (Clase)
# En esta estructura almaceno las variables que modelan el disparo y estado
# del armamento del lado derecho de la nave
#-----------------------------------------------------------------------------
class eGun_D(ct.Structure): # Estructura Armamento Ala Derecha
 _fields_ = [
             ('nX',ct.c_short), # Pos X  Misil.-
             ('nY',ct.c_short), # Pos Y  Misil.-
             ('nF',ct.c_short), # Sprite Misil.-
             ('nV',ct.c_short), # Veloc. Misil.-
             ('nD',ct.c_short), # Direc. Misil.-
             ('lF',ct.c_short)  # Flag.  Misil.-
            ]

#-----------------------------------------------------------------------------
# Funcion de Carga de Archivos Graficos, con opcion de transparencia.-
# Retorna una imagen tipo surface valida en pygame.-
#-----------------------------------------------------------------------------
def Load_Image(sFile,transp = False):
    try: image = pg.image.load(sFile)
    except pg.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#-----------------------------------------------------------------------------
# Inicializa PyGames con todos sus servicios
# Retorna display principal (el que se ve en el escritorio)
#-----------------------------------------------------------------------------
def PyGame_Init():
    pg.init()
    pg.display.set_caption('PYGAME + PIL - By Alberto Caro - 2015()')
    return pg.display.set_mode(nRES)

#-----------------------------------------------------------------------------
# Pinta la Pantalla Principal de PyGames.-
# Wm es la pantalla principal donde se grafican (pintan) todos los sprites.-
#-----------------------------------------------------------------------------
def Copy_B2D():
    Wm.blit(Bm,(0,0))
    return

#-----------------------------------------------------------------------------
# Copia Final del Mapa.-
#-----------------------------------------------------------------------------
def Copy_M2B():
    Bm.blit(Bk,(0,0))
    return

#-----------------------------------------------------------------------------
# Copia un pedazo de Surface( Grafico en RAM) mediante la utilizacion de la
# funcion subsurface((x,y),(ancho_x,alto_y)).
# Ma -> Es una Surface de 500x2 RGBA que se manipula en RAM. No se ve!
# Bm -> Es una Surface de 500x800 RGBA
# Obs:
#     Lo que hace esta funcion es copiar un pedazo de graficos del bloque de
#     memoria Bm(RAM Grafico) desde la posicion (0,798) de 500 x 2 pixeles y
#     pintarlos en Ma en la posicion (0,0).
#
#     Despues repinta BM con su contenido pero desplazado en la fila 2 y en
#     Bm grafica en la posicion (0,0) la copia de datos de Ma.
#-----------------------------------------------------------------------------
def Copy_Ite():
    Ma = pg.Surface((500,2))
    Ma.blit(Bm.subsurface((0,798),(500,2)),(0,0))
    Bm.blit(Bm,(0,2))
    Bm.blit(Ma,(0,0))
    return

#-----------------------------------------------------------------------------
# Pinta / Grafica la Nave en la posicion (x,Y) de la Pantalla Wm (Surface).-
# Nv -> tiene cargado la grafica de la nave de tipo Surface(.)
#-----------------------------------------------------------------------------
def Draw_Nav(nX,nY):
    Wm.blit(Nv,(nX,nY))
    return

#-----------------------------------------------------------------------------
# Grafica las balas/bombas/misiles, etc. por lado derecho e izquierdo de la
# nave si esta activa su grafica:
#
#                 If aGun_I[i][1].lF = True -> Se pinta la Bala Izquierda
#                 If aGun_D[i][1].lF = True -> Se pinta la Bala Derecha
#-----------------------------------------------------------------------------
def Draw_Balas():
    for i in range(nMAX_GUN):
     if aGun_I[i][1].lF: Wm.blit(M1,(aGun_I[i][1].nX,aGun_I[i][1].nY))
     if aGun_D[i][1].lF: Wm.blit(M2,(aGun_D[i][1].nX,aGun_D[i][1].nY))
    return

#-----------------------------------------------------------------------------
# Inicializa la estructura de datos que almacena el estado de las balas de la
# nave (Izquierda y Derecha). La cantidad de balas depende de la constante
# nMAX_GUN.-
#-----------------------------------------------------------------------------
def Init_Gun_I():
    for i in range(nMAX_GUN):
     aGun_I[i][0] =     nON # 1: Disponible - 0: No Disponible
     aGun_I[i][1].nX = -100 # Posicion X
     aGun_I[i][1].nY = +900 # Posicion Y
     aGun_I[i][1].nF = +001 # Imagen 1.-
     aGun_I[i][1].nV = +020 # Velocidad.-
     aGun_I[i][1].nD = -001 # Subiendo.-
     aGun_I[i][1].lF =  nON # 1: Se grafica - 0: No se grafica
    return

#-----------------------------------------------------------------------------
def Init_Gun_D():
    for i in range(nMAX_GUN):
     aGun_D[i][0] =     nON # 1: Disponible - 0: No Disponible
     aGun_D[i][1].nX = +600 # Posicion X
     aGun_D[i][1].nY = +900 # Posicion Y
     aGun_D[i][1].nF = +002 # Imagen 1.-
     aGun_D[i][1].nV = +020 # Velocidad.-
     aGun_D[i][1].nD = -001 # Subiendo.-
     aGun_D[i][1].lF =  nON # 1: Se grafica - 0: No se grafica
    return

#-----------------------------------------------------------------------------
# Intenta meter una bala a la estructura de datos de las balas izquierda y
# derecha si hay disponibilidad.
#
#       if aGun_I[i][0] == nON -> Que se puede ingresar una bala para ser
#       monitorizada antes que se salga de vision (pantalla) o impacte con
#       su objetivo.-
#-----------------------------------------------------------------------------
def Mete_Balas_I(nX,nY):
    for i in range(nMAX_GUN):
     if aGun_I[i][0] == nON:
        aGun_I[i][0] = nOFF # Desactivacion temporal!
        aGun_I[i][1].nX = nX
        aGun_I[i][1].nY = nY
        aGun_I[i][1].lF = nON # Se activa su grafica (pintado)
        return
    return

#-----------------------------------------------------------------------------
# IDEM.....
#-----------------------------------------------------------------------------
def Mete_Balas_D(nX,nY):
    for i in range(nMAX_GUN):
     if aGun_D[i][0] == nON:
        aGun_D[i][0] =  nOFF
        aGun_D[i][1].nX = nX
        aGun_D[i][1].nY = nY
        aGun_D[i][1].lF = nON
        return
    return


#-----------------------------------------------------------------------------
# Intenta meter ambas balas/misiles izquierda/derecha de la Nave.-
#-----------------------------------------------------------------------------
def Mete_Balas(nX,nY):
    Mete_Balas_I(nX+10,nY)
    Mete_Balas_D(nX+60,nY)
    Show_Status() # Muestra el estado de cada uno del armanto de la Nave.-
    return

#-----------------------------------------------------------------------------
# Se deben actualizar ambas estructuras de datos de las balas izquierda y
# derecha, pues las balas/misiles tienen "vida propia", es decir, una vez que
# se disparan, ya no pertenecen a la nave y se deben monitorizar autonomamente
#-----------------------------------------------------------------------------
def UpDate_Balas():
    for i in range(nMAX_GUN):
     if aGun_I[i][1].lF:
        aGun_I[i][1].nY += aGun_I[i][1].nV * aGun_I[i][1].nD #velocidad * direc
        if aGun_I[i][1].nY <= nMIN_Y:
           aGun_I[i][0] = nON ; aGun_I[i][1].lF = nOFF

     if aGun_D[i][1].lF:
        aGun_D[i][1].nY += aGun_D[i][1].nV * aGun_D[i][1].nD
        if aGun_D[i][1].nY <= nMIN_Y:
           aGun_D[i][0] = nON ; aGun_D[i][1].lF = nOFF

    return

#-----------------------------------------------------------------------------
# Imprime por pantalla en estado de todas las balas de la Nave.-
#-----------------------------------------------------------------------------
def Show_Status():
    sON = 'ON: %04d %04d %04d %04d %04d' % (aGun_I[0][0],   aGun_I[1][0],   aGun_I[2][0],   aGun_I[3][0],   aGun_I[4][0])
    snX = 'nX: %04d %04d %04d %04d %04d' % (aGun_I[0][1].nX,aGun_I[1][1].nX,aGun_I[2][1].nX,aGun_I[3][1].nX,aGun_I[4][1].nX)
    snY = 'nY: %04d %04d %04d %04d %04d' % (aGun_I[0][1].nY,aGun_I[1][1].nY,aGun_I[2][1].nY,aGun_I[3][1].nY,aGun_I[4][1].nY)
    snF = 'nF: %04d %04d %04d %04d %04d' % (aGun_I[0][1].nF,aGun_I[1][1].nF,aGun_I[2][1].nF,aGun_I[3][1].nF,aGun_I[4][1].nF)
    snV = 'nV: %04d %04d %04d %04d %04d' % (aGun_I[0][1].nV,aGun_I[1][1].nV,aGun_I[2][1].nV,aGun_I[3][1].nV,aGun_I[4][1].nV)
    snD = 'nD: %04d %04d %04d %04d %04d' % (aGun_I[0][1].nD,aGun_I[1][1].nD,aGun_I[2][1].nD,aGun_I[3][1].nD,aGun_I[4][1].nD)
    slF = 'lF: %04d %04d %04d %04d %04d' % (aGun_I[0][1].lF,aGun_I[1][1].lF,aGun_I[2][1].lF,aGun_I[3][1].lF,aGun_I[4][1].lF)

    print ' '*30
    print '='*30
    print sON ; print snX ; print snY ; print snF ; print snV ; print snD ;  print slF
    print ' '*30

    sON = 'ON: %04d %04d %04d %04d %04d' % (aGun_D[0][0],   aGun_D[1][0],   aGun_D[2][0],   aGun_D[3][0],   aGun_D[4][0])
    snX = 'nX: %04d %04d %04d %04d %04d' % (aGun_D[0][1].nX,aGun_D[1][1].nX,aGun_D[2][1].nX,aGun_D[3][1].nX,aGun_D[4][1].nX)
    snY = 'nY: %04d %04d %04d %04d %04d' % (aGun_D[0][1].nY,aGun_D[1][1].nY,aGun_D[2][1].nY,aGun_D[3][1].nY,aGun_D[4][1].nY)
    snF = 'nF: %04d %04d %04d %04d %04d' % (aGun_D[0][1].nF,aGun_D[1][1].nF,aGun_D[2][1].nF,aGun_D[3][1].nF,aGun_D[4][1].nF)
    snV = 'nV: %04d %04d %04d %04d %04d' % (aGun_D[0][1].nV,aGun_D[1][1].nV,aGun_D[2][1].nV,aGun_D[3][1].nV,aGun_D[4][1].nV)
    snD = 'nD: %04d %04d %04d %04d %04d' % (aGun_D[0][1].nD,aGun_D[1][1].nD,aGun_D[2][1].nD,aGun_D[3][1].nD,aGun_D[4][1].nD)
    slF = 'lF: %04d %04d %04d %04d %04d' % (aGun_D[0][1].lF,aGun_D[1][1].lF,aGun_D[2][1].lF,aGun_D[3][1].lF,aGun_D[4][1].lF)

    print sON ; print snX ; print snY ; print snF ; print snV ; print snD ;  print slF
    print '='*30

    return

#-----------------------------------------------------------------------------
# Logica principal de  la simulacion
# Estudiar a fondo esta simulacion para poder realizar despues las simulaciones de robotica
#
# By Alberto Caro 2022() Temuco - Chile
#
#                               Fe en el conocimiento
#-----------------------------------------------------------------------------
Wm = PyGame_Init(); pg.mouse.set_visible(False) ; Fps = pg.time.Clock() # Timer. Se vera despues
Bk = Load_Image('bk.jpg')      ; Nv = Load_Image('nave.png',True) # Surfaces
M1 = Load_Image('b1.png',True) ; M2 = Load_Image('b2.png',True) # Surfaces
Bm = pg.Surface((500,800))     ; nMx = nMy = 0
aGun_I = [[0,eGun_I()] for i in range(nMAX_GUN)] # Array de Balas Izquierdas
aGun_D = [[0,eGun_D()] for i in range(nMAX_GUN)] # Array de Balas Derechas
Init_Gun_I();Init_Gun_D() # Inicializacion Estructuras de datos balas Izq. y Der.
Copy_M2B() # Sacamos una copia de pantalla de fondo principal.-

# Iteramos .......
while lOK:
 cKey = pg.key.get_pressed() # Que Tecla se presiono
 if cKey[pg.K_ESCAPE] : lOK = False
 ev = pg.event.get() # Manejo de eventos de Pygame
 for e in ev:
  if e.type == QUIT:
     lOK = False
  if e.type == pg.MOUSEMOTION: nMx,nMy = e.pos # Guardamos Posicion (X,Y) del Mouse
  if e.type == pg.MOUSEBUTTONDOWN and e.button == nBTN_LEFT : Mete_Balas_I(nMx+10,nMy)# Disp. Izq
  if e.type == pg.MOUSEBUTTONDOWN and e.button == nBTN_RIGHT: Mete_Balas_D(nMx+60,nMy)# Disp. Der
  if e.type == pg.KEYDOWN and e.key == K_SPACE: Mete_Balas(nMx,nMy) # Dispara ambas Balas
 Copy_Ite()          # Copiamos un Bloque Memoria Surface
 Copy_B2D()          # Copiamos Bloque a Display Main
 UpDate_Balas()      # UpDate Balas Izq. y Der.
 Draw_Balas()        # Pintamos las Balas
 Draw_Nav(nMx,nMy)   # Pintamos la Nave
 pg.display.update() # UpDate Display principal. Siempre se hace!
 Fps.tick(25)        # Timer de Syncronizacion General
pg.quit              # Si se presiona ESC nos salimos de la aplicacion y cerramis PyGame!





