# By Alberto Caro S.
# Ing. Civil Computacion
# Dr.(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
#-------------------------------------------
import pygame
from pygame.locals import *
import time, random

#---------------------------------------------------------------------
# Carga imagenes y convierte formato pygame
#---------------------------------------------------------------------
def Load_Image(sFile,transp=False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Pinta Mapa
#---------------------------------------------------------------------
def Pinta_Mapa():
    AuxF = AuxC = 0
    for f in range(0,15):
        for c in range(0,24):           # (x,y)
            Panta.blit(aTile[aMapa[f][c]],(AuxC,AuxF))
            AuxC += 32
        AuxF += 32 ; AuxC = 0
    return

#---------------------------------------------------------------------
# Main
#---------------------------------------------------------------------
lOK = 1 ; nRes = (640,480)
aFile = ['b04.png','t01.png','t02.png','b01.png','s11.png']
pygame.init()
pygame.display.set_caption('Mapa 2D Demo.-')
Panta = pygame.display.set_mode(nRes)
#aTile = [Load_Image(aFile[i],False) for i in range(5)]
aTile = [
         Load_Image(aFile[0],False),
         Load_Image(aFile[1],False),
         Load_Image(aFile[2],False),
         Load_Image(aFile[3],False),
         Load_Image(aFile[4],True ),
         Load_Image(aFile[4],False)
        ]

nTile = 0
aMapa = [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
        [0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        ]

while lOK:
 ev = pygame.event.get()
 for e in ev:
     if e.type == QUIT:
	lOK = 0
     elif e.type == pygame.KEYDOWN:
          if e.key == pygame.K_ESCAPE:
             lOK = 0
          if e.key == pygame.K_s:
             pygame.image.save(Panta,'foto.jpg')
 Pinta_Mapa()
 Panta.blit(aTile[4],(0,0))
 Panta.blit(aTile[5],(32,32))

 pygame.display.flip()

