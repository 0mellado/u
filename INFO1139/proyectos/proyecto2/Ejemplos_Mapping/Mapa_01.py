# By Alberto Caro S.
# Ing. Civil Computacion
# Dr.(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
#---------------------------------------------------------------------

# Importamos las librerias de PyGame, de Tiempo y Random.
#---------------------------------------------------------------------
import pygame
from pygame.locals import *
import time, random

#---------------------------------------------------------------------
# Carga imagenes y convierte formato pygame
# sFile -> Nombre del archivo grafico (BMP, JPEG, PNG, etc.)
# lTransparente -> Parametro logico que indica si la imagen a graficar
# tiene contornos o areas transparentes que se deben mostrar.
# Por defecto es FALSO. Es decir, se asume que la grafica es del tipo
# BLOCK sin contornos o areas trasparentes.
#---------------------------------------------------------------------
def Load_Image(sFile,lTransparente=False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if lTransparente:
       color = image.get_at((0,0)) # Aqui esta el color que se utiliza
                                   # para la transparencia.
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Pinta Mapa
# Esta funcion pinta (grafica) en la Superficie principal la Superficie
# deseada en las coordenadas (X,Y)
# Los Tiles (Baldosas) tienen un tamano de 32x32 pixeles de 24 Bits.
# El Mapa esta compuesto por 15 Filas y 24 Columnas
#---------------------------------------------------------------------
def Pinta_Mapa():
    nX = nY = 0
    for nF in range(0,15):
     for nC in range(0,24):           #(x,y)
      Panta.blit(aTile[aMapa[nF][nC]],(nX,nY))
      nX += 32
     nY += 32 ; nX = 0
    return

#---------------------------------------------------------------------
# Bloque principal de la simulacion
#---------------------------------------------------------------------
lOK = 1 ; nRes = (640,480) # Resulcion de la pantalla (Panta)
aFile = ['b04.png','t01.png','t02.png','b01.png','s11.png'] # Tiles
pygame.init() # Inicializamos Pygame
pygame.display.set_caption('Mapa 2D Demo.-') # Nombre de Pantalla
Panta = pygame.display.set_mode(nRes) # Activamos pantalla
nTile = 0

# Array de 5 Tiles sin transparencia
# aTile = [ Load_Image(aFile[i],False) for i in range(5)]
aTile = [
         Load_Image(aFile[0],False),
         Load_Image(aFile[1],False),
         Load_Image(aFile[2],False),
         Load_Image(aFile[3],False),
         Load_Image(aFile[4],True ),
         Load_Image(aFile[4],False)
        ]

aMapa = [
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
         [0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,0,0,0,0,0,0],
         [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        ]

while lOK:
 # Manejo de Eventos de Pygame
 aEv = pygame.event.get() # aEv = Array de Eventos internos de Pygame
 for e in aEv:
     if e.type == QUIT: # Click en X Esquina Pantalla
	lOK = 0
     elif e.type == pygame.KEYDOWN: # Evento de Tecla Presionada
          if e.key == pygame.K_ESCAPE: #  Fue la tecla (key) ESC ?
             lOK = 0
          if e.key == pygame.K_s: # Fue Key = S -> Salva en DISCO el Mapa.
             pygame.image.save(Panta,'foto.jpg')
 # Genera el Mapa segun los Tiles (numeros) que estan en el array aMapa[]
 Pinta_Mapa()
 # Fijate en la diferencia de Tiles Transparentes y No Transp.
 Panta.blit(aTile[4],(0,0))
 Panta.blit(aTile[5],(32,32))
 # flip -> pinta Panta en Display Main.
 pygame.display.flip()

