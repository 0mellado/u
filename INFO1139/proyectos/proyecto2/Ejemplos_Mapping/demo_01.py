# By Alberto Caro S.
# Ing. Civil Computacion
# Dr.(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
#-------------------------------------------
import pygame
from pygame.locals import *
import time, random as RA


nRES = (1920,800) ;  lOK = 1 ; nW_H = 32

aMAPA_N = [[1 for x in range(nRES[0]/nW_H)] for x in range(nRES[1]/nW_H)]

#---------------------------------------------------------------------
# Carga imagenes y convierte formato pygame
#---------------------------------------------------------------------
def Load_Image(sFile,transp=False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Random Mapa
#---------------------------------------------------------------------
def Make_Mapa():
    for nF in range(0,nRES[1]/nW_H):
     for nC in range(0,nRES[0]/nW_H):
         aMAPA_N[nF][nC] = RA.randint(1,12) # Seleccion de Tiles.-
    return

#---------------------------------------------------------------------
# Pinta Mapa
#---------------------------------------------------------------------
def Pinta_Mapa():
    nPos_X = 0 ; nPos_Y = 0
    for nF in range(0,nRES[1]/nW_H):
     for nC in range(0,nRES[0]/nW_H):
         if aMAPA_N[nF][nC] == 1:
	    Block_Mem_1.blit(nFig01,(nPos_X,nPos_Y))
            nPos_X += nW_H
         if aMAPA_N[nF][nC] == 2:
	    Block_Mem_1.blit(nFig02,(nPos_X,nPos_Y))
            nPos_X += nW_H
         if aMAPA_N[nF][nC] == 3:
	    Block_Mem_1.blit(nFig03,(nPos_X,nPos_Y))
            nPos_X += nW_H
         if aMAPA_N[nF][nC] == 4:
	    Block_Mem_1.blit(nFig04,(nPos_X,nPos_Y))
            nPos_X += nW_H
         if aMAPA_N[nF][nC] == 5:
	    Block_Mem_1.blit(nFig05,(nPos_X,nPos_Y))
	    nPos_X += nW_H
	 if aMAPA_N[nF][nC] == 6:
	    Block_Mem_1.blit(nFig06,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 7:
	    Block_Mem_1.blit(nFig07,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 8:
	    Block_Mem_1.blit(nFig08,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 9:
	    Block_Mem_1.blit(nFig09,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 10:
	    Block_Mem_1.blit(nFig10,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 11:
	    Block_Mem_1.blit(nFig11,(nPos_X,nPos_Y))
	    nPos_X += nW_H
         if aMAPA_N[nF][nC] == 12:
	    Block_Mem_1.blit(nFig12,(nPos_X,nPos_Y))
	    nPos_X += nW_H
     nPos_X = 0 ; nPos_Y += nW_H
    return

#---------------------------------------------------------------------
# Pinta Panel
#---------------------------------------------------------------------
def Pinta_Panel():
    nAux_X = 10 ; nAux_Y = 5
    scr.blit(nFig23,(nAux_X,nAux_Y))
    return

#---------------------------------------------------------------------
# Lee Bloque Memoria de Superficie Grafica
#---------------------------------------------------------------------
def Mem2Scr():
    #return Block_Mem_1.subsurface((0,0), (900-1,640-1))
    return Block_Mem_1.subsurface((nPosX,nPosY),(nPosX + 900-1,nPosY + 640-1))

#----------------------------------------------------------------
# Main
#----------------------------------------------------------------
pygame.init()
pygame.display.set_caption('Mapa 2D Demo.-')
scr = pygame.display.set_mode((900,640))
Block_Mem_1 = pygame.Surface(nRES)

#----------------------------------------------------------------
# Carga de Tiles.-
#----------------------------------------------------------------
nFig01  = Load_Image('b01.png') # Tile Roca
nFig02  = Load_Image('b02.png') # Tile Ploma
nFig03  = Load_Image('b03.png') # Tile Amarilla
nFig04  = Load_Image('b04.png') # Tile Azul
nFig05  = Load_Image('t01.png') # Tile Tierra 1
nFig06  = Load_Image('t02.png') # Tile Tierra 2
nFig07  = Load_Image('t03.png') # Tile Tierra + Piedras
nFig08  = Load_Image('t04.png') # Tile Tierra Planta 1
nFig09  = Load_Image('t05.png') # Tile Tierra Yerba 1
nFig10  = Load_Image('t06.png') # Tile Tierra Yerba 2
nFig11  = Load_Image('t07.png') # Tile Tierra Planta 2
nFig12  = Load_Image('t08.png') # Tile Tierra Minerales
#----------------------------------------------------------------

# Sprites Robot
#----------------------------------------------------------------
nFig13  = Load_Image('r1.png',True)  # Robot 1 Animacion 1
nFig14  = Load_Image('r2.png',True)  # Robot 1 Animacion 2
#----------------------------------------------------------------

#----------------------------------------------------------------
# Sprites Sondas
#----------------------------------------------------------------
nFig15  = Load_Image('s11.png',True) # Sonda 1 Animacion 1
nFig16  = Load_Image('s12.png',True) # Sonda 1 Animacion 2
nFig17  = Load_Image('s21.png',True) # Sonda 2 Animacion 1
nFig18  = Load_Image('s22.png',True) # Sonda 2 Animacion 2
nFig19  = Load_Image('s31.png',True) # Sonda 3 Animacion 1
nFig20  = Load_Image('s32.png',True) # Sonda 3 Animacion 2
nFig21  = Load_Image('s41.png',True) # Sonda 4 Animacion 1
nFig22  = Load_Image('s42.png',True) # Sonda 4 Animacion 2
#----------------------------------------------------------------
# Panel
#----------------------------------------------------------------
nFig23  = Load_Image('panel2.png',True) # Panel Control Sondas
#----------------------------------------------------------------

nC = 0 ; nPosX = nPosY = 0
Make_Mapa()
while lOK:
 nC += 1
 ev = pygame.event.get()
 for e in ev:
     if e.type == QUIT:
	lOK = 0
     elif e.type == pygame.KEYDOWN:
          if e.key == pygame.K_ESCAPE:
             lOK = 0
          if e.key == pygame.K_s:
             pygame.image.save(scr,'foto.jpg')
 Mx, My = pygame.mouse.get_pos()

 if Mx <= 100:
    nPosX-=1
    if nPosX <= 0:
       nPosX = 0

 if Mx >= 790:
    nPosX+=1
    if nPosX >= 1279:
       nPosX = 1279

 if My <= 100:
    nPosY-=1
    if nPosY <= 0:
       nPosY = 0

 if My >= 530:
    nPosY+=1
    if nPosY >= 639:
       nPosY = 639

 Pinta_Mapa()
 Mem2Scr().blit(nFig13,(nC,150))
 scr.blit(Mem2Scr(),(0,0))
 Pinta_Panel()
 pygame.display.flip()

