# By Alberto Caro S.
# Ing. Civil en Computacion
# Doctor(c) Cs. de la Computacion
# Pontificia Universidad Catolica de Chile
#-------------------------------------------
import pygame,ctypes as ct
from pygame.locals import *

#---------------------------------------------------------------------
# Definicion de Constantes.-
#---------------------------------------------------------------------
nRES = (1190,400) ; nW_X = 33 ; nH_Y = 33 ; lOK = True
nBTN_LEFT = 1 ; tFONDO = (0,366) ; tNEGRO = (0,0,0)

#---------------------------------------------------------------------
# Carga imagenes y convierte formato PyGame
# Segun lo explicado en el codigo anterior
#---------------------------------------------------------------------
def Load_Image(sFile,transp = False):
    try: image = pygame.image.load(sFile)
    except pygame.error,message:
           raise SystemExit,message
    image = image.convert()
    if transp:
       color = image.get_at((0,0))
       image.set_colorkey(color,RLEACCEL)
    return image

#---------------------------------------------------------------------
# Inicializa PyGames.-
# retorna un display del tipo Pygame con todos sus atributos.
# Es una buena idea!
#---------------------------------------------------------------------
def PyGame_Init():
    pygame.init()
    pygame.display.set_caption('Exporta Tiles - By Alberto Caro')
    return pygame.display.set_mode(nRES)

#---------------------------------------------------------------------
# Inicializa Array de Figuras.-
# Array Global donde se van almacenando los tiles seleccionados y la
# Libreria de Tiles Principal.
# Se podria haber separado la libreria Principal.03d
# Trata de mejorar tu esta idea.
#---------------------------------------------------------------------
def Fig_Init():
    aImg = []
    aImg.append(Load_Image('Libreria.png',False))
    return aImg # Retorna copia en RAM de array aIMG[]

#---------------------------------------------------------------------
# Pinta Libreria Principal de los Tiles
#---------------------------------------------------------------------
def Pinta_Panta():
    Panta.fill(tNEGRO) # Rellena fondo (BackGround) con color negro
    Panta.blit(aTile[0],(0,0)) # Pinta la Libreria en (0,0) Coord.
    return

#---------------------------------------------------------------------
# Pinta Regla Guia y Cursor sobre Tile de interes.
#
# Cada Tile mide 32 x 32 pixeles. nMx y nMy coordenadas del mouse dentro
# de la ventana principal (Panta).
#
# (nMx/nW_X)        -> Division entera (Columna) donde me encuentro - eje X
# (nMx/nW_X) * nW_X -> Posicion real dentro de la libreria (Mapa)   - eje X
# lo mismo par el eje Y
#  nW_x => tamano del tile (ancho) + 1
#  nW_y => tamano del tile (altura) + 1 #esto es por las lineas entre tiles
#
# Luego pintamos lineas Horiz. (Constante nY),ancho 1,color(255,0,0)   -> Rojo
# Luego pintamos lineas Verti. (Constante nX),ancho 1,color(255,255,0) -> Verde

#---------------------------------------------------------------------
def Pinta_Regla():
    nX = (nMx/nW_X)*nW_X ; nY = (nMy/nH_Y)*nH_Y
    pygame.draw.line(Panta,(255,0,0),(0,nY),(1188,nY),1)
    pygame.draw.line(Panta,(0,255,0),(nX,0),(nX,363),1)
    pygame.draw.rect(Panta,(0,0,255),(nX,nY,34,34),1) # Pinta Rectangulo Azul
    return

#---------------------------------------------------------------------
# Agrega Baldosas (Tiles) en Array aTile
#---------------------------------------------------------------------
def Get_Tile():
    sAux = pygame.Surface((nW_X-1,nH_Y-1)) # Crea Surface (.)
    nX = (nMx/nW_X)*nW_X ; nY = (nMy/nH_Y)*nH_Y # Obtiene posicion dentro mapa
    # Copia a sCopy (Surface) el area de memoria RAM de la Superficie
    # referenciada por nX y nY de 32 x 32 pixeles
    # notar que suma y resta un pixel en x,y ya que en la libreria los
    # tiles estan separados por lineas de 1px
    sCopy.blit(Panta.subsurface((nX+1,nY+1,nW_X-1,nH_Y-1)),(0,0))
    for x in range(nW_X-1): # Recorrimos el tamano en eje x del tile 0..31
     for y in range(nH_Y-1): # Recorrimos el tamano en eje y del tile 0..31
      tRGB = sCopy.get_at((x,y)) # Sacamos el Color en (x,y)
      sAux.set_at((x,y),tRGB)    # lo copiamos en sAux...
    aTiles.append(sAux) # Agregamos el Tile a aTiles...
    return

#---------------------------------------------------------------------
# Pinta Tile de interes
# Pintamos el tile seleccionado al final de barra estado de Panta
#---------------------------------------------------------------------
def Pinta_Tile():
    Panta.blit(sCopy,tFONDO)
    return

#---------------------------------------------------------------------
# Exporta los Tiles seleccionados.-
# Exportamos a archivos secuenciales *.png todos los tiles de aTiles
#---------------------------------------------------------------------
def Exporta():
    for i in range(len(aTiles)):
     sL = 'expo_%03d.png' %(i)
     pygame.image.save(aTiles[i],sL)
    return

#---------------------------------------------------------------------
# Exporta todos los Tiles de Libreria
# Esta se pude mejorar.03d
# Trata de hacerlo tu
#---------------------------------------------------------------------
def Expo_All():
    sAux = pygame.Surface((nW_X-1,nH_Y-1)) ; sL = '' ; nPos_X = 0; nPos_Y = 0
    for nY in range(12):
     for nX in range(37):
      nPos_X = nX*(nW_X)
      #sCopy.blit(Panta.subsurface((nPos_X+1,nPos_Y+1,nW_X-1,nH_Y-1)),(0,0))
      pygame.draw.rect(Panta,(0,0,255),(nPos_X,nPos_Y,34,34),1)
      for x in range(nW_X-1):
       for y in range(nH_Y-1):
        rgb = sCopy.get_at((x,y))
        sAux.set_at((x,y),rgb)
      aTiles.append(sAux)
     nPos_Y = nY*(nH_Y) ; nPos_X = 0

    pygame.image.save(Panta,'out.png')
    for i in range(len(aTiles)):
     sL = 'tile_%03d.png' %(i)
     #pygame.image.save(aTiles[i],sL)

    return

#---------------------------------------------------------------------
# While Principal del Demo.-
#---------------------------------------------------------------------
Panta = PyGame_Init() ; aTile = Fig_Init() ; sCopy = pygame.Surface((32,32))
nMx = 0 ; nMy = 0 ; aTiles = []
while lOK:
 ev = pygame.event.get()
 for e in ev:
  if e.type == QUIT: lOK = False
  if e.type == pygame.KEYDOWN:
     if e.key == pygame.K_e: Exporta()
     if e.key == pygame.K_a: Expo_All()
     if e.key == pygame.K_ESCAPE: lOK = False
  if e.type == pygame.MOUSEMOTION : nMx,nMy = e.pos
  if e.type == pygame.MOUSEBUTTONDOWN and e.button == nBTN_LEFT: Get_Tile()

 Pinta_Panta()
 Pinta_Regla()
 Pinta_Tile()

 pygame.display.flip()

pygame.quit
