#!/usr/bin/python3

import serial as sr, matplotlib.pyplot as plt, ctypes as ct


class eCelda(ct.Structure):
    _fields_ = [
        ('nF',  ct.c_ubyte), # fila
        ('nC',  ct.c_ubyte), # columna
        ('Au',  ct.c_ubyte), # oro
        ('FeC', ct.c_ubyte), # acero profe el acero no sale de la tierra xd
        ('Li',  ct.c_ubyte), # litio
        ('Ni',  ct.c_ubyte), # niquel
        ('Cu',  ct.c_ubyte), # cobre
    ]


def ear():
    ser = sr.Serial('/dev/ttyVirtualS1')
    print("En escucha por el puerto /dev/ttyVirtualS1")
    arrTile = []
    i = 0
    while i < 58:
        tile = eCelda()
        response = ser.readline()
        strRes = response.decode('utf-8')[:-2].split(" ")
        tile.nF = int(strRes[0])
        tile.nC = int(strRes[1])
        tile.Au = int(strRes[2])
        tile.FeC = int(strRes[3])
        tile.Li = int(strRes[4])
        tile.Ni = int(strRes[5])
        tile.Cu = int(strRes[6])
        print(tile.nF, tile.nC, tile.Au, tile.FeC, tile.Li, tile.Ni, tile.Cu)
        arrTile.append(tile)
        i += 1
    return arrTile


def grafic(arrTiles):
    Au = [] ; FeC = [] ; Li = [] ; Ni = [] ; Cu = []
    for i in range(len(arrTiles)):
        Au.append(arrTiles[i].Au)
        FeC.append(arrTiles[i].FeC)
        Li.append(arrTiles[i].Li)
        Ni.append(arrTiles[i].Ni)
        Cu.append(arrTiles[i].Cu)

    fig, axs = plt.subplots(5,sharex=True)
    fig.suptitle('Recursos por baldosa')

    axs[0].plot(Au, 'r')
    axs[0].grid(1)
    axs[0].set_title('Oro')

    axs[1].plot(FeC, 'g')
    axs[1].grid(1)
    axs[1].set_title('Acero')

    axs[2].plot(Li, 'b')
    axs[2].grid(1)
    axs[2].set_title('Litio')

    axs[3].plot(Ni, 'k')
    axs[3].grid(1)
    axs[3].set_title('Niquel')

    axs[4].plot(Cu, 'y')
    axs[4].grid(1)
    axs[4].set_title('Cobre')

    axs[4].set(xlabel='Muestras')
    fig.savefig('estaciones.png')
    plt.show()
    return


def main():
    arrTile = ear()
    grafic(arrTile)

if __name__ == '__main__':
    main()
