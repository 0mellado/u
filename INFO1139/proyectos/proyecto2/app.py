#!/usr/bin/python3
import pygame as pg, ctypes as ct, serial as sr
from pygame.locals import *

sizeTile = 44
sizeRes = (sizeTile * 12, sizeTile * 9)
rangos = [(sizeRes[1] - 43), (44 * 2),
          (sizeRes[1] - 44), (44 * 2),
          (sizeRes[1] - 44), (44 * 4),
          (44 * 2), (44 * 2), (44 * 2),
          (44 * 2), (44 * 2), (44 * 2),
          (44 * 2), (44 * 4), (sizeRes[1] - 44)]


mapa = [[8, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
        [0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
        [0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
        [0, 1, 0, 1, 0 ,1, 0, 1, 1, 1 ,0, 1],
        [0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1],
        [0, 1, 0, 1, 0 ,1, 1, 1, 0, 1, 0, 1],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 7, 1]]


class eRobot(ct.Structure):
    _fields_ = [
        ('tipo', ct.c_ubyte),
        ('x', ct.c_ushort),
        ('y', ct.c_ushort),
        ('rang', ct.c_ushort),
        ('mov', ct.c_ubyte),
        ('dirX', ct.c_byte),
        ('dirY', ct.c_byte),
        ('vel', ct.c_byte),
    ]


class eCelda(ct.Structure):
    _fields_ = [
        ('nF',  ct.c_ubyte), # fila
        ('nC',  ct.c_ubyte), # columna
        ('Au',  ct.c_ubyte), # oro
        ('FeC', ct.c_ubyte), # acero
        ('Li',  ct.c_ubyte), # litio
        ('Ni',  ct.c_ubyte), # niquel
        ('Cu',  ct.c_ubyte), # cobre
    ]

def loadImage(img, transp=False):
    try:
        image = pg.image.load(img)
        if transp:
            image = image.convert_alpha()
            color = image.get_at((0, 0))
            image.set_colorkey(color)
        return image
    except:
        print("Error: No se pudo cargar las imagenes, no encuentra el archivo o directorio")
        exit(1)


def Init_Fig():
    aImg = []
    aImg.append(loadImage('./bg.png', False )) # baldosa con recursos 0
    aImg.append(loadImage('./mu.png', False )) # muro                 1
    aImg.append(loadImage('./re.png', True  )) # robot este           2
    aImg.append(loadImage('./rn.png', True  )) # Robot norte          3
    aImg.append(loadImage('./ro.png', True  )) # Robot oeste          4
    aImg.append(loadImage('./rs.png', True  )) # Robot sur            5
    aImg.append(loadImage('./rt.png', True  )) # mouse                6
    aImg.append(loadImage('./tf.png', False )) # meta                 7
    aImg.append(loadImage('./ti.png', False )) # inicio               8
    return aImg


def initPg():
    pg.init()
    pg.mouse.set_visible(False)
    pg.display.set_caption('Proyecto 2 de PR')
    return pg.display.set_mode(sizeRes)


def initRobot():
    robot = eRobot()
    robot.tipo = 5
    robot.x = 0
    robot.y = 0
    robot.mov = 0
    robot.rang = rangos[0]
    robot.dirX = 0
    robot.dirY = 1
    robot.vel = 1
    return robot


def readMap():
    aMap = []
    datMap = open('mapa.dat', 'rb')
    for i in range(0, sizeRes[1] // sizeTile):
        aMap.append([])
        for _ in range(0, sizeRes[0] // sizeTile):
            celda = eCelda()
            datMap.readinto(celda)
            aMap[i].append(celda)
    datMap.close()

    return aMap


def drawRobot(screen, robot: eRobot, sprt: list):
    screen.blit(sprt[robot.tipo], (robot.x, robot.y))
    return


def drawMap(screen, sprt: list, mapa: list):
    for fil in range(0, sizeRes[1] // sizeTile):
        for col in range(0, sizeRes[0] // sizeTile):
            screen.blit(sprt[mapa[fil][col]], (col * 44, fil * 44))
    return


def drawMouse(screen, sprt: list, posM: tuple):
    screen.blit(sprt, posM)
    return


def auxMovRobot(x: int, y: int, z: int):
    if x == -1:
        robot.tipo = 4
    if y == -1:
        robot.tipo = 3
    if x == 1:
        robot.tipo = 2
    if y == 1:
        robot.tipo = 5
    robot.rang = rangos[z]
    robot.dirX = x
    robot.dirY = y
    return z


def getInfoTile(robot: eRobot, aMap: list):
    celda = eCelda()
    ser = sr.Serial('/dev/ttyVirtualS0')
    strLine = ""
    if robot.x % 44 == 0 and robot.y % 44 == 0:
        celda = aMap[robot.y // 44][robot.x // 44]
        line = '%d %d %d %d %d %d %d\n'
        strLine = line % (celda.nF, celda.nC, celda.Au, celda.FeC, celda.Li, celda.Ni, celda.Cu)
        bLine = strLine.encode('utf-8')
        ser.write(bLine)
        ser.close()

        return strLine

def saveTileText(dat):
    celdaDat = dat[:-2].split(' ')
    line = 'F:%02d-C:%02d-Oro:%03d-Acero:%03d-Litio:%03d-Niquel:%03d-Cobre:%03d\n'
    strLine = line % (int(celdaDat[0]), int(celdaDat[1]), int(celdaDat[2]), int(celdaDat[3]), int(celdaDat[4]), int(celdaDat[5]), int(celdaDat[6]))
    sondeoFile = open('recursos.txt', 'a')
    sondeoFile.write(strLine)
    sondeoFile.close()
    return

def saveTileBin(arrSondeo: list, celdaDat):
    tile = eCelda()
    celdaDat = celdaDat[:-2].split(' ')
    tile.nF = int(celdaDat[0])
    tile.nC = int(celdaDat[1])
    tile.Au = int(celdaDat[2])
    tile.FeC = int(celdaDat[3])
    tile.Li = int(celdaDat[4])
    tile.Ni = int(celdaDat[5])
    tile.Cu = int(celdaDat[6])
    arrSondeo.append(tile)
    sondeoFile = open('sondeo.dat', 'wb')
    for datos in arrSondeo:
        celda = eCelda()
        celda = datos
        sondeoFile.write(celda)
    sondeoFile.close()
    return


def moveRobot(robot: eRobot, sondeo):
    robot.rang -= 1
    if robot.rang == 0:
        if robot.mov == 0: robot.mov = auxMovRobot(1, 0, 1)
        elif robot.mov == 1: robot.mov = auxMovRobot(0, -1, 2)
        elif robot.mov == 2: robot.mov = auxMovRobot(1, 0, 3)
        elif robot.mov == 3: robot.mov = auxMovRobot(0, 1, 4)
        elif robot.mov == 4: robot.mov = auxMovRobot(1, 0, 5)
        elif robot.mov == 5: robot.mov = auxMovRobot(0, -1, 6)
        elif robot.mov == 6: robot.mov = auxMovRobot(-1, 0, 7)
        elif robot.mov == 7: robot.mov = auxMovRobot(0, -1, 8)
        elif robot.mov == 8: robot.mov = auxMovRobot(1, 0, 9)
        elif robot.mov == 9: robot.mov = auxMovRobot(0, -1, 10)
        elif robot.mov == 10: robot.mov = auxMovRobot(-1, 0, 11)
        elif robot.mov == 11: robot.mov = auxMovRobot(0, -1, 12)
        elif robot.mov == 12: robot.mov = auxMovRobot(1, 0, 13)
        elif robot.mov == 13: robot.mov = auxMovRobot(0, 1, 14)
        elif robot.mov == 14: robot.mov = auxMovRobot(0, 0, 14)

    if robot.dirX != 0 or robot.dirY != 0:
        tileTxt = getInfoTile(robot, aMap)
        if tileTxt:
            saveTileText(tileTxt)
            saveTileBin(sondeo, tileTxt)

    robot.x += robot.dirX * robot.vel
    robot.y += robot.dirY * robot.vel

    return


def main():
    cleanFile = open('recursos.txt', 'w')
    cleanFile.write('')
    cleanFile.close()
    screen = initPg()
    nMx, nMy = 0,0
    a = [1, 1]
    global aMap
    global robot

    sondeo = []
    clock = pg.time.Clock()

    robot = initRobot()

    aMap = readMap()
    aFig = Init_Fig()
    while a[0] is a[1]:
        ev = pg.event.get()

        for event in ev:
            if event.type == QUIT:
                a[1] = 256
            if event.type == pg.MOUSEMOTION:
                nMx, nMy = event.pos


        moveRobot(robot, sondeo)
        drawMap(screen, aFig, mapa)
        drawRobot(screen, robot, aFig)
        drawMouse(screen, aFig[6], (nMx, nMy))
        pg.display.flip()

        clock.tick(120)


if __name__ == '__main__':
    main()

