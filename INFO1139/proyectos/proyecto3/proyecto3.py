#!/usr/bin/python3

import pygame as pg, time, random as ra, ctypes as ct
import telepot as te

sizeRes = (1184, 576)
sizeTile = 32
running = True
nMx = nMy = 0
rango0 = 1184 - sizeTile
rango1 = sizeTile


class eRobot(ct.Structure):
    _fields_ = [
        ('tipo', ct.c_short),
        ('x', ct.c_short),
        ('y', ct.c_short),
        ('ran', ct.c_short),
        ('mov', ct.c_short),
        ('dirX', ct.c_short),
        ('dirY', ct.c_short),
        ('vel', ct.c_short)
    ]


class eCelda(ct.Structure):
    _fields_ = [
        ('tipo', ct.c_ubyte),
        ('fil', ct.c_ubyte),
        ('col', ct.c_ubyte),
        ('img', ct.c_ubyte)
    ]


#---------------------------------------------------------------------
# Carga imagenes y convierte formato PyGame
#---------------------------------------------------------------------
def loadImage(img, transp=False):
    try:
        image = pg.image.load(img)
        if transp:
            image = image.convert_alpha()
            color = image.get_at((0, 0))
            image.set_colorkey(color)
        return image
    except:
        print("Error: No se pudo cargar las imagenes, no encuentra el archivo o directorio")
        exit(1)
#---------------------------------------------------------------------
# Inicializa PGs.-
#---------------------------------------------------------------------
def Init_PyGame():
    pg.init()
    pg.mouse.set_visible(False)
    pg.display.set_caption('hola wena kbrxz de pygame')
    return pg.display.set_mode(sizeRes)

#---------------------------------------------------------------------
# Inicilaiza parametros de los Robots
#---------------------------------------------------------------------
def Init_Robot():
    robot = eRobot()
    robot.nF = 1
    robot.nX = 0
    robot.nY = 0
    robot.nR = rango0
    robot.nS = 1
    robot.dX = 1
    robot.dY = 0
    robot.nV = 1
    robot.nC = 1
    return robot


#---------------------------------------------------------------------
# Inicializa Array de Sprites.-
#---------------------------------------------------------------------
def Init_Fig():
    aImg = []
    aImg.append(loadImage('T00.png', False)) # Tile Tierra, id = 0
    aImg.append(loadImage('Boe.png', True )) # Tile Roca,   id = 1
    aImg.append(loadImage('Rat.png', True )) # Tile Marmol, id = 2
    return aImg

#------------------------------------------------------------------------------
# Armando el Mapa con sus recursos y todos sus parametros para ser utilizado 
# por los sensores de los Robots.-
#------------------------------------------------------------------------------
def Init_Mapa():
    for fil in range(0,sizeRes[1] // sizeTile):
        for col in range(0,sizeRes[0] // sizeTile):
            aMap[fil][col].tipo = 0
            aMap[fil][col].fil = fil
            aMap[fil][col].col = col
            aMap[fil][col].img = ra.randint(0, 11)
    return

#---------------------------------------------------------------------
# Pinta Mapa 
#---------------------------------------------------------------------
def Pinta_Mapa():
    for fil in range(0,sizeRes[1] // sizeTile):
        for col in range(0,sizeRes[0] // sizeTile):
            sWin.blit(aFig[0], (aMap[fil][col].col* sizeTile, aMap[fil][col].fil* sizeTile))
    return

#---------------------------------------------------------------------
# Pinta los Robots en el Super Extra Mega Mapa.-
# Se pintan los Robots en Surface -> sMapa (6400 x 480)
#---------------------------------------------------------------------
def Pinta_Robot(robot: eRobot):
    if robot.nF == 8: sWin.blit(aFig[10],(robot.x, robot.y))
    sWin.blit(aFig[1], (robot.x, robot.y))
    return

#---------------------------------------------------------------------
# Actualiza la estructura de datos de cada uno de los robots dentro del
# Mapa sMapa.
#---------------------------------------------------------------------
def Mueve_Robot(robot: eRobot):
    robot.ran -= 1
    if robot.ran <= 0:
        if robot.mov == 1:
            robot.mov = 2
            robot.ran = rango1
            robot.dirX = 0
            robot.dirY = 1
        elif robot.mov == 2:
            robot.mov = 3
            robot.nR = rango0
            robot.dX = -1
            robot.dY = 0
        elif robot.mov == 3:
            robot.mov = 4
            robot.ran = rango1
            robot.dirX = 0
            robot.dirY = 1
        else:
            robot.mov = 1
            robot.ran = rango0
            robot.dirX = 1
            robot.dirY = 0

    robot.x += robot.dirX * robot.vel
    robot.y += robot.dirY * robot.vel

    print(robot.x, robot.y)

    return robot


def Pinta_Mouse():
    sWin.blit(aFig[2],(nMx,nMy))
    return


aMap = [[eCelda() for _ in range(sizeRes[0] // sizeTile)] for _ in range(sizeRes[1] // sizeTile)]

sWin = Init_PyGame() ; aFig = Init_Fig()
robot = Init_Robot()
Init_Mapa()
aClk = [pg.time.Clock(), pg.time.Clock()]
eReg = eCelda()

while running:
    cKey = pg.key.get_pressed()

    ev = pg.event.get()
    for e in ev:
        if e.type == pg.QUIT:
            running = False
        if e.type == pg.MOUSEMOTION:
            nMx,nMy = e.pos

    Pinta_Mapa()
    robot = Mueve_Robot(robot)
    Pinta_Robot(robot)
    Pinta_Mouse()
    pg.display.flip()

pg.quit
