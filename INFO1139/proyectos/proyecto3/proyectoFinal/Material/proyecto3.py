#!/usr/bin/python3
import pygame as pg
import random as ra
import ctypes as ct
from pygame.locals import *
import telepot as te


nRES = (1184, 576)
nT_WX = nT_HY = 32
nMAX_ROBOTS = 1
lGo = True
nMx = nMy = 0
nR_1 = 1154
nR_2 = 32


class eRobot(ct.Structure):
    _fields_ = [
        ('nF', ct.c_short),
        ('nX', ct.c_short),
        ('nY', ct.c_short),
        ('nR', ct.c_short),
        ('nS', ct.c_short),
        ('dX', ct.c_short),
        ('dY', ct.c_short),
        ('nV', ct.c_short)
    ]


class eCelda(ct.Structure):
    _fields_ = [
        ('nT', ct.c_ubyte),
        ('nF', ct.c_ubyte),
        ('nC', ct.c_ubyte),
        ('nI', ct.c_ubyte),
    ]

def loadImage(img, transp=False):
    try:
        image = pg.image.load(img)
        if transp:
            image = image.convert_alpha()
            color = image.get_at((0, 0))
            image.set_colorkey(color)
        return image
    except:
        print("Error: No se pudo cargar las imagenes, no encuentra el archivo o directorio")
        exit(1)


def Init_PyGame():
    pg.init()
    pg.mouse.set_visible(False)
    pg.display.set_caption(' Mapa 2D + Telegram - By Oscar melleado and Benjamin carrasco')
    return pg.display.set_mode(nRES)


def Init_Robot():
    for i in range(0, nMAX_ROBOTS):
        aBoe[i].nF = 1
        aBoe[i].nX = 0
        aBoe[i].nY = 0
        aBoe[i].nR = nR_1
        aBoe[i].nS = 1
        aBoe[i].dX = 1
        aBoe[i].dY = 0
        aBoe[i].nV = 1
    return


def Init_Fig():
    aImg = []
    aImg.append(loadImage('T00.png', False))
    aImg.append(loadImage('Boe.png', True))
    aImg.append(loadImage('Rat.png', True))
    return aImg


def Init_Mapa():
    for nF in range(0, nRES[1] // nT_HY):
        for nC in range(0, nRES[0] // nT_WX):
            aMap[nF][nC].nT = 0
            aMap[nF][nC].nF = nF
            aMap[nF][nC].nC = nC
            aMap[nF][nC].nI = ra.randint(0, 11)
    return


def Pinta_Mapa():
    for nF in range(0, nRES[1] // nT_HY):
        for nC in range(0, nRES[0] // nT_WX):
            if aMap[nF][nC].nT == 0:
                sWin.blit(aFig[0], (aMap[nF][nC].nC *
                          nT_HY, aMap[nF][nC].nF*nT_WX))
    return


def Pinta_Robot():
    for i in range(0, nMAX_ROBOTS):
        if aBoe[i].nF == 1:
            sWin.blit(aFig[1], (aBoe[i].nX, aBoe[i].nY))
    return


def Mueve_Robot():
    for i in range(0, nMAX_ROBOTS):
        aBoe[i].nR -= 1
        if aBoe[i].nR <= 0:
            if aBoe[i].nS == 1:
                aBoe[i].nS = 2
                aBoe[i].nR = nR_2
                aBoe[i].dX = 0
                aBoe[i].dY = 1
            elif aBoe[i].nS == 2:
                aBoe[i].nS = 3
                aBoe[i].nR = nR_1
                aBoe[i].dX = -1
                aBoe[i].dY = 0
            elif aBoe[i].nS == 3:
                aBoe[i].nS = 4
                aBoe[i].nR = nR_2
                aBoe[i].dX = 0
                aBoe[i].dY = 1
            else:
                aBoe[i].nS = 1
                aBoe[i].nR = nR_1
                aBoe[i].dX = 1
                aBoe[i].dY = 0

        aBoe[i].nX += aBoe[i].dX*aBoe[i].nV
        aBoe[i].nY += aBoe[i].dY*aBoe[i].nV

        if aBoe[i].nX < 1 and aBoe[i].nY > nRES[1] - 32:
            Init_Robot()
    return


def Pinta_Mouse():
    sWin.blit(aFig[2], (nMx, nMy))
    return


def SendImg(a):
    oscar = 418840064
    benja = 5231352674
    bot.sendPhoto(chat_id=oscar, photo=open(aImg[a], 'rb'))


aMap = [
    [eCelda() for _ in range(nRES[0]//nT_WX)] for _ in range(nRES[1]//nT_HY)
]

sWin = Init_PyGame()

aFig = Init_Fig()
aBoe = [eRobot() for _ in range(0, nMAX_ROBOTS)]

Init_Robot()
Init_Mapa()

aImg = ['F01.png', 'F02.png', 'F03.png', 'F04.png', 'F05.png', 'F06.png',
        'F07.png', 'F08.png', 'F09.png', 'F10.png', 'F11.png', 'F12.png']

aClk = [pg.time.Clock(), pg.time.Clock()]
eReg = eCelda()

tokenBenja = '5388183808:AAHIJd6-vnpUXCoC7vTmhyZc8_A3X81h9xo'
tokenOscar = '5550734107:AAGwUzL-0dVwkz8CNyHew0Pd0fR4-9DXDRo'

bot = te.Bot(tokenOscar)

while lGo:
    cKey = pg.key.get_pressed()
    if cKey[pg.K_ESCAPE]:
        lGo = ('A' > 'B')

    if cKey[pg.K_s]:
        pg.image.save(sWin, 'mapinte.png')
    if cKey[pg.K_F2]:
        renx = ((aBoe[0].nX)//32)
        reny = ((aBoe[0].nY)//32)
        nuimg = aMap[reny][renx].nI
        SendImg(nuimg)

    ev = pg.event.get()
    for e in ev:
        if e.type == QUIT:
            lGo = (2 > 3)
        if e.type == pg.MOUSEMOTION:
            nMx, nMy = e.pos

    Pinta_Mapa()
    Pinta_Robot()
    Mueve_Robot()
    Pinta_Mouse()
    pg.display.flip()
    aClk[0].tick(100)

pg.quit

