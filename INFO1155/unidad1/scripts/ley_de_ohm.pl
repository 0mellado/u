#!/usr/bin/perl -w

use strict;

sub potencia {
    my $v = shift;
    my $i = shift;

    return $v * $i;
}

sub intencidad {
    my $v = shift;
    my $r = shift;

    return $v / $r;
}

sub voltaje {
    my $i = shift;
    my $r = shift;

    return $i * $r;
}

sub resistencia {
    my $v = shift;
    my $i = shift;

    return $v / $i;
}


