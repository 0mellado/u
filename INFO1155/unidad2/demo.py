import random as ra, ctypes as ct

nMAX = 10

class eData(ct.Structure):
    __fields__ = [
        ('nBITS',ct.c_ushort)
    ]


def Make_Trama():
    nID_K = ra.randint(0x01,0x0f)
    nID_Q = ra.randint(0x01,0xff)
    nID_P = ra.randint(0x01,0x0f)
    return (nID_K << 12 | nID_Q << 4 | nID_P)

eInfo = eData() ; aVentas = []

for i in range(nMAX):
    eInfo.nBITS = Make_Trama()
    aVentas.append((i,eInfo.nBITS))
    print('NUM: ' + str(aVentas[i][1] >> 0  & 0xffff))
    print('BIT: ' + bin(aVentas[i][1] >> 0  & 0xffff))
    print('IDK: ' + str(aVentas[i][1] >> 12 & 0x000f))
    print('QTY: ' + str(aVentas[i][1] >> 4  & 0x00ff))
    print('$$$: ' + str(aVentas[i][1] >> 0  & 0x000f))
    print('')

'''
Salida -> Como los datos son generados random -> pueden variar

NUM: 36923
BIT: 0b1001000000111011
IDK: 9
QTY: 3
$$$: 11

NUM: 27223
BIT: 0b110101001010111
IDK: 6
QTY: 165
$$$: 7

NUM: 36806
BIT: 0b1000111111000110
IDK: 8
QTY: 252
$$$: 6

NUM: 14297
BIT: 0b11011111011001
IDK: 3
QTY: 125
$$$: 9

NUM: 27268
BIT: 0b110101010000100
IDK: 6
QTY: 168
$$$: 4

NUM: 35916
BIT: 0b1000110001001100
IDK: 8
QTY: 196
$$$: 12

NUM: 40309
BIT: 0b1001110101110101
IDK: 9
QTY: 215
$$$: 5

NUM: 64776
BIT: 0b1111110100001000
IDK: 15
QTY: 208
$$$: 8

NUM: 21661
BIT: 0b101010010011101
IDK: 5
QTY: 73
$$$: 13

NUM: 8020
BIT: 0b1111101010100
IDK: 1
QTY: 245
$$$: 4

'''
