def encode(cKey, nKey) -> int:
    nXOR = ord(cKey) ^ nKey
    nLSB = nXOR & 0x0f
    nMSB = nXOR >> 0x04
    return (nLSB << 0x04) | nMSB

def decode(cKey, nKey) -> int:
    nLSB = cKey & 0x0f
    nMSB = cKey >> 0x04
    aux = (nLSB << 4) | nMSB
    xor = aux ^ nKey
    return xor




aMsg = [116, 103, 215, 71, 199, 51, 7, 39, 22, 39, 51, 119, 103, 51, 54, 103, 54, 167, 215, 199]
aKey = [7, 13, 19, 21]

decodeMsgs = []

wea = 0
for key in aKey:
    decodeMsgs.append([])
    for char in aMsg:
        decodeMsgs[-1].append(decode(char, key))
    wea += 1


for i in range(len(decodeMsgs)):
    for j in range(len(decodeMsgs[i])):
        print(chr(decodeMsgs[i][j]), end="")

    print()
# char = 'A'
# encoded = encode(char, aKey[0])
# decoded = decode(encoded, aKey[0])
# print(ord(char))
# print()
# print(encoded)
# print()
# print(decoded)
