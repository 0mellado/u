def code(text, key) -> str:
    msgEncoded = ''

    for char in text:
        xor = ord(char) ^ key
        lsb = xor & 0x0f
        msb = xor >> 0x04
        aux = lsb << 4 | msb
        msgEncoded += chr(aux)

    return msgEncoded


def decode(text, key) -> str:
    msgDecoded = ''

    for char in text:
        lsb = ord(char) & 0x0f
        msb = ord(char) >> 0x04
        aux = lsb << 4 | msb
        xor = aux ^ key
        msgDecoded += chr(xor)


    return msgDecoded

string = 'HOLA'
encoded = code(string, 19)
decoded = decode(encoded, 19)

print(string)
print(encoded)
print(decoded)
