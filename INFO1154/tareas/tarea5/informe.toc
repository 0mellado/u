\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Resumen}{2}{section.1}%
\contentsline {section}{\numberline {2}Introducción}{3}{section.2}%
\contentsline {section}{\numberline {3}Marco teórico}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}ADSL}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}HFC}{4}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Cisco IOS}{4}{subsection.3.3}%
\contentsline {section}{\numberline {4}Implementación}{6}{section.4}%
\contentsline {subsection}{\numberline {4.1}Asignación de segmentos públicos}{6}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Equipos utilizados}{6}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}ISP ADSL}{6}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}ISP HFC}{7}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Servicio de Google}{7}{subsubsection.4.2.3}%
\contentsline {subsection}{\numberline {4.3}Configuraciones en común}{7}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Configuración del servidor DHCP}{7}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Configuración del servidor de DNS}{8}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Configuración del servidor del HTTP}{8}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Configuracion de VLAN}{9}{subsubsection.4.3.4}%
\contentsline {subsection}{\numberline {4.4}Configuración de los bridge en los cloud}{11}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Nodo de distribución ADSL}{11}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Nodo de distribución HFC}{12}{subsubsection.4.4.2}%
\contentsline {subsection}{\numberline {4.5}Conexión con los suscriptores}{12}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Interconexión de los ISP}{13}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Interconexión de los ISP con el servicio de Google}{14}{subsection.4.7}%
\contentsline {section}{\numberline {5}Esquema completo}{15}{section.5}%
\contentsline {section}{\numberline {6}Conclusiones}{16}{section.6}%
