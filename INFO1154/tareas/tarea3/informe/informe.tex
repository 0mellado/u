\documentclass[a4paper, 12pt]{article}
\usepackage{config}
\begin{document}
\fontfamily{phv}\selectfont
\begin{titlepage}
    \centering
    \begin{flushleft}
     \includegraphics[scale=0.2]{img/Logo_uct_inf.pdf}
    \end{flushleft}
    \vfill
    {\Huge Protocolos de comunicación}
    \vfill
    \begin{flushright}
        {\large  Oscar Mellado}\\
        \vspace{0.5cm}
        {\large 25 de Mayo 2023}        
    \end{flushright}
\end{titlepage}


\section{Resumen}
Los protocolos de comunicación son conjuntos de reglas y normas
que permiten a los dispositivos de una red comunicarse entre sí
de manera efectiva y confiable. El modelo TCP/IP, compuesto por
una serie de protocolos interrelacionados, es fundamental en el
desarrollo de la red global. Este modelo divide la comunicación en capas, proporcionando un enfoque modular y
organizado. Las capas del modelo TCP/IP incluyen la capa de aplicación, la capa de transporte, la capa de
Internet y la capa de acceso a la red. La segmentación de redes es una práctica común en la que se divide una red
en subredes más pequeñas. Esto permite una gestión más eficiente de la red y un mejor rendimiento. La segmentación
se realiza mediante enrutadores que dirigen el tráfico entre las subredes.


\section{Introdución}
En el mundo interconectado en el que vivimos, los protocolos de
comunicación desempeñan un papel fundamental en el desarrollo y
funcionamiento de la red global. Estos protocolos son conjuntos
de reglas y normas que permiten a los dispositivos de una red
comunicarse entre sí de manera efectiva y confiable. Sin ellos,
la transmisión de información y el intercambio de datos a través
de la red serían caóticos e incoherentes.

Los protocolos de comunicación también garantizan la
interoperabilidad, lo que significa que dispositivos y sistemas
de diferentes fabricantes pueden comunicarse y trabajar juntos
de manera fluida. Esto permite que la red global crezca y se
expanda, ya que no está limitada a dispositivos y tecnologías
específicas. Además, los protocolos de comunicación proporcionan
flexibilidad y escalabilidad, permitiendo que la red se adapte a
medida que aumenta el número de dispositivos y la demanda de
servicios.

El modelo TCP/IP ha desempeñado un papel fundamental en el
desarrollo de la red global que tenemos hoy en día. Este modelo
se compone de una serie de protocolos interrelacionados que
permiten la comunicación de datos a través de Internet. Fue
desarrollado en las décadas de 1970 y 1980, y se convirtió en el
estándar de facto para las comunicaciones en red.

\newpage
\section{Marco teórico}
\subsection{Protocolo IPSec}
Internet Protocol Security (IPSec) es un conjunto de protocolos para asegurar las comunicaciones del
Protocolo de Internet (IP) mediante la autenticación y el cifrado de cada paquete IP de una transmisión
de datos. IPSec también incluye protocolos para establecer una autenticación mutua entre los agentes
al comienzo de la sesión y la negociación de claves criptográficas que se utilizarán durante la sesión.
IPSec es un esquema de seguridad de doble modo y de extremo a extremo que opera en la Capa de Internet
del conjunto de protocolos de Internet o en la Capa 3 del modelo OSI (\cite{dhall2012implementation}).

IPsec es un estándar abierto que forma parte de la suite IPv4 y utiliza los siguientes protocolos para
realizar diversas funciones (\cite{thayer1998ip}):

\begin{itemize}
    \item Las cabeceras de autenticación (AH) proporcionan integridad de los datos sin conexión y
        autenticación del origen de los datos para datagramas IP y proporcionan protección contra ataques
        de repetición (\cite{kent2005rfc}).
    \item Encapsulating Security Payloads (ESP) proporciona confidencialidad, integridad de los datos sin
        conexión, autenticación del origen de los datos, un servicio anti-reproducción (una forma de
        integridad parcial de la secuencia) y confidencialidad limitada del flujo de tráfico
        (\cite{kent2005ip}).
    \item Internet Security Association and Key Management Protocol (ISAKMP) proporciona un marco para la
        autenticación y el intercambio de claves, con material de claves autenticado real
        proporcionado ya sea por configuración manual con claves precompartidas, Internet Key Exchange
        (IKE e IKEv2), Kerberized Internet Negotiation of Keys (KINK), o registros DNS IPSECKEY
        (\cite{harkins1998internet}).
\end{itemize}
\newpage
\subsection{Protocolo IPX}
Internetwork Packet Exchange (IPX) es el protocolo de capa de red del conjunto de protocolos IPX/SPX.
IPX se deriva de IDP de Xerox Network Systems. También tiene la capacidad de actuar como un protocolo de
capa de transporte.

Una gran ventaja de IPX era el pequeño espacio de memoria que ocupaba el controlador IPX, lo que era vital
para DOS y Windows hasta Windows 95 debido al tamaño limitado que tenía entonces la memoria convencional.
Otra ventaja de IPX es la fácil configuración de sus ordenadores cliente. Sin embargo, IPX no se adapta
bien a grandes redes como Internet (\cite{garfinkel2003practical}), por lo que su uso disminuyó cuando el
auge de Internet hizo casi universal el protocolo TCP/IP.

Una gran ventaja del protocolo IPX es su escasa o nula necesidad de configuración. En la época en que no
existían protocolos para la configuración dinámica de host y el protocolo BOOTP para la asignación
centralizada de direcciones no era habitual, la red IPX podía configurarse casi automáticamente. Un
ordenador cliente utiliza la dirección MAC de su tarjeta de red como dirección de nodo y aprende lo que
necesita saber sobre la topología de la red de los servidores o enrutadores - las rutas se propagan
mediante el Protocolo de Información de Enrutamiento, los servicios mediante el Protocolo de Publicidad de
Servicios.

Un pequeño administrador de red IPX sólo tenía que preocuparse de:
\begin{itemize}
    \item Asignar a todos los servidores de la misma red el mismo número de red.
    \item Asignar diferentes números de red a diferentes formatos de trama en la misma red.
    \item Asignar diferentes números de red a diferentes interfaces de servidores con múltiples tarjetas de red (el servidor Novell NetWare con múltiples tarjetas de red funcionaba automáticamente como un router).
    \item Asignar diferentes números de red a servidores de diferentes redes interconectadas.
    \item Iniciar el proceso de enrutamiento en nodos con múltiples tarjetas de red en redes más complejas.
\end{itemize}

\subsection{Protocolo ICMP}
El Protocolo de Mensajes de Control de Internet (ICMP) es un protocolo de soporte del conjunto de
protocolos de Internet. Lo utilizan los dispositivos de red, incluidos los routers, para enviar mensajes
de error e información operativa que indican el éxito o el fracaso de la comunicación con otra dirección
IP; por ejemplo, se indica un error cuando un servicio solicitado no está disponible o que no se ha podido
acceder a un host o router (\cite{forouzan2007data}). ICMP se diferencia de protocolos de transporte como
TCP y UDP en que no se utiliza normalmente para intercambiar datos entre sistemas, ni es empleado
regularmente por aplicaciones de red de usuario final (con la excepción de algunas herramientas de
diagnóstico como ping y traceroute).

\subsubsection{Detalles técnicos}
ICMP forma parte del conjunto de protocolos de Internet, tal y como se define en el RFC 792. Los mensajes
ICMP se utilizan normalmente con fines de diagnóstico o control o se generan en respuesta a errores en
operaciones IP (como se especifica en RFC 1122). Los errores ICMP se dirigen a la dirección IP de origen
del paquete (\cite{forouzan2007data}).

Por ejemplo, cada dispositivo (como un router intermedio) que reenvía un datagrama IP primero disminuye en
uno el campo de tiempo de vida (TTL) de la cabecera IP. Si el TTL resultante es 0, el paquete se descarta
y se envía un mensaje ICMP de tiempo excedido en tránsito a la dirección de origen del datagrama.

En muchos casos, es necesario inspeccionar el contenido del mensaje ICMP y entregar el mensaje de error
apropiado a la aplicación responsable de transmitir el paquete IP que provocó el envío del mensaje ICMP.

ICMP es un protocolo de capa de red, lo que lo convierte en un protocolo de capa 3 según el modelo OSI
de 7 capas. Basado en el modelo TCP/IP de 4 capas, ICMP es un protocolo de capa de Internet, lo que lo
convierte en un protocolo de capa 2 (modelo TCP/IP estándar de Internet RFC 1122 con 4 capas) o un
protocolo de capa 3 basado en las definiciones modernas de protocolo TCP/IP de 5 capas
(\cite{tanenbaum2003redes}).

\subsection{Protocolo IPv4}
El Protocolo de Internet versión 4 (IPv4) es la cuarta versión del Protocolo de Internet (IP). Es uno de
los protocolos centrales de los métodos de interconexión basados en estándares de Internet y otras redes
de conmutación de paquetes. IPv4 fue la primera versión desplegada para producción en SATNET en 1982 y
en ARPANET en enero de 1983. Todavía se utiliza para enrutar la mayor parte del tráfico de Internet,
incluso con el despliegue en curso del Protocolo de Internet versión 6 (IPv6), su sucesor. IPv4 utiliza
un espacio de direcciones de 32 bits que proporciona 4.294.967.296 ($2^{32}$) direcciones únicas, pero
se reservan grandes bloques para fines de red especiales (\cite{ipv4}).

\subsubsection{Representación de las direcciones}
Las direcciones IPv4 pueden representarse en cualquier notación que exprese un valor entero de 32 bits.
Lo más habitual es escribirlas en notación punto-decimal, que consiste en cuatro octetos de la dirección
expresados individualmente en números decimales y separados por puntos.

Por ejemplo, la dirección IP 192.0.2.235 con cuatro puntos representa el número decimal de 32 bits
3221226219, que en formato hexadecimal es 0xC00002EB.

La notación CIDR combina la dirección con su prefijo de enrutamiento en un formato compacto, en el que
la dirección va seguida de un carácter de barra oblicua (/) y el recuento de bits de 1 consecutivos a
la izquierda en el prefijo de enrutamiento (máscara de subred).

Otras representaciones de direcciones eran de uso común cuando se practicaban\hfill las\hfill redes\hfill de\hfill clase\hfill A.\hfill Por \hfill
ejemplo,\hfill la\hfill dirección \hfill loopback \\ 127.0.0.1 se escribe comúnmente como 127.1, dado que pertenece a una red
de clase A con ocho bits para la máscara de red y 24 bits para el número de host. Cuando en la dirección
se especifican menos de cuatro números en notación con puntos, el último valor se trata como un número
entero de tantos bytes como sean necesarios para completar la dirección hasta cuatro octetos. Así, la
dirección 127.65530 equivale a 127.0.255.250.

\subsubsection{Asignación}
En el diseño original de IPv4, una dirección IP se dividía en dos partes: el identificador de red era
el octeto más significativo de la dirección, y el identificador de host era el resto de la dirección.
Este último también se denominaba campo de resto. Esta estructura permitía un máximo de 256
identificadores de red, lo que pronto se consideró insuficiente.

Para superar este límite, en 1981 se redefinió el octeto más significativo de la dirección para crear
clases de red, en un sistema que más tarde se conocería como classful networking. El sistema revisado
definía cinco clases. Las clases A, B y C tenían diferentes longitudes de bits para la identificación
de la red. El resto de la dirección se utilizaba como antes para identificar un host dentro de una red.
Debido a los diferentes tamaños de los campos en las distintas clases, cada clase de red tenía una
capacidad diferente para direccionar hosts. Además de las tres clases para el direccionamiento de hosts,
la Clase D se definió para el direccionamiento multicast y la Clase E se reservó para futuras
aplicaciones (\cite{clasesip}) (\cite{gonzalez2007sistemas}).

La división de las redes de clase existentes en subredes comenzó en 1985 con la publicación del RFC 950.
Esta división se hizo más flexible con la introducción de las máscaras de subred de longitud variable
(VLSM) en el RFC 1109 de 1987. En 1993, basándose en este trabajo, el RFC 1517 introdujo el enrutamiento
entre dominios sin clases (CIDR), (\cite{class1996understanding}) que expresaba el número de bits (a
partir del más significativo) como, por ejemplo, /24, y el esquema basado en clases se denominó classful,
por contraste. CIDR se diseñó para permitir la repartición de cualquier espacio de direcciones de forma
que se pudieran asignar bloques de direcciones más pequeños o más grandes a los usuarios. La estructura
jerárquica creada por CIDR es gestionada por la Autoridad de Asignación de Números de Internet (IANA) y
los registros regionales de Internet (RIR). Cada RIR mantiene una base de datos WHOIS de consulta pública
que proporciona información sobre las asignaciones de direcciones IP.

\subsection{Protocolo IPv6}
El Protocolo de Internet versión 6 (IPv6) es la versión más reciente del Protocolo de Internet (IP), el
protocolo de comunicaciones que proporciona un sistema de identificación y localización de ordenadores en
redes y encamina el tráfico a través de Internet. IPv6 fue desarrollado por el Grupo de Trabajo de
Ingeniería de Internet (IETF) para hacer frente al problema previsto desde hace tiempo del agotamiento de
las direcciones IPv4, y está destinado a sustituir a IPv4. En diciembre de 1998, IPv6 se convirtió en
un Proyecto de Norma para el IETF, que posteriormente lo ratificó como Norma de Internet el 14 de julio
de 2017 (\cite{deering2017internet}).

A los dispositivos en Internet se les asigna una dirección IP única para su identificación y definición
de ubicación. Con el rápido crecimiento de Internet tras su comercialización en los años 90, se hizo
evidente que se necesitarían muchas más direcciones para conectar dispositivos de las que disponía el
espacio de direcciones IPv4. En 1998, el IETF había formalizado el protocolo sucesor. IPv6 utiliza
direcciones de 128 bits, lo que teóricamente permite $2^{128}$, o aproximadamente $3,4\cdot10^{38}$
direcciones totales. El número real es ligeramente inferior, ya que varios rangos se reservan para
usos especiales o se excluyen completamente de su uso. Los dos protocolos no están diseñados para ser
interoperables, por lo que es imposible la comunicación directa entre ellos, lo que complica el paso
a IPv6. Sin embargo, se han ideado varios mecanismos de transición para subsanar este problema.

Además de un mayor espacio de direccionamiento, IPv6 ofrece otras ventajas técnicas. En concreto, permite
métodos de asignación jerárquica de direcciones que facilitan la agregación de rutas en Internet y limitan
así la expansión de las tablas de enrutamiento. El uso del direccionamiento multidifusión se amplía y
simplifica, y proporciona una optimización adicional para la prestación de servicios. En el diseño del
protocolo se han tenido en cuenta los aspectos de movilidad, seguridad y configuración de los dispositivos.

Las direcciones IPv6 se representan como ocho grupos de cuatro dígitos hexadecimales \hfill cada \hfill uno,\hfill  separados\hfill 
por \hfill dos \hfill puntos. \hfill La\\\hfill  representación \hfill completa \hfill puede \hfill acortarse; \hfill por\hfill ejemplo,
\\ 2001:0db8:0000:0000:0000:8a2e:0370:7334 \hfill se \hfill convierte \hfill en \\ 2001:db8::8a2e:370:7334.

\subsubsection{Principales características}
IPv6 es un protocolo de la capa de Internet para la interconexión de redes por conmutación de paquetes y
proporciona transmisión de datagramas de extremo a extremo a través de múltiples redes IP, ciñéndose
estrechamente a los principios de diseño desarrollados en la versión anterior del protocolo, el Protocolo
de Internet versión 4 (IPv4).

Además de ofrecer más direcciones, IPv6 también implementa funciones no presentes en IPv4. Simplifica
aspectos de la configuración de direcciones, la renumeración de redes y los anuncios de los routers al
cambiar de proveedor de conectividad de red. Asimismo, simplifica el procesamiento de paquetes en los
routers, al situar la responsabilidad de la fragmentación de paquetes en los puntos finales. El tamaño
de la subred IPv6 se estandariza fijando el tamaño de la parte del identificador de host de una dirección
en 64 bits.

La arquitectura de direccionamiento de IPv6 se define en el RFC 4291 y permite tres tipos diferentes de
transmisión: unicast, anycast y multicast (\cite{rosen2014linux}).

\subsubsection{Representación de las direcciones}
Los 128 bits de una dirección IPv6 se representan en 8 grupos de 16 bits cada uno. Cada grupo se escribe
como cuatro dígitos hexadecimales (a veces llamados hextetos (\cite{graziani2013ipv6}) o más formalmente
hexadectetos e informalmente un quibble o quad-nibble) y los grupos están separados\hfill por\hfill dos\hfill
puntos\hfill (:).\hfill Un\hfill ejemplo\hfill de\hfill esta\hfill representación\hfill es\\ 2001:0db8:0000:0000:0000:ff00:0042:8329.

Por comodidad y claridad, la representación de una dirección IPv6 puede abreviarse con las siguientes
reglas:

\begin{itemize}
    \item Se eliminan uno o más ceros a la izquierda de cualquier grupo de dígitos hexadecimales, lo
        que normalmente se hace con todos los ceros a la izquierda. Por ejemplo, el grupo 0042 se
        convierte en 42. El grupo 0000 se convierte en 0.
    \item Las secciones consecutivas de ceros se sustituyen por dos dos puntos (::). Esto sólo puede
        usarse una vez en una dirección, ya que su uso múltiple haría que la dirección fuera indeterminada.
        La RFC 5952 exige que no se utilicen dos puntos dobles para denotar una única sección de ceros
        omitida (\cite{kawamura2010recommendation}).
\end{itemize}
\newpage
Un ejemplo de aplicación de estas normas:


\hspace{1cm} Dirección inicial: 2001:0db8:0000:0000:0000:ff00:0042:8329.

\hspace{1cm} Tras\hfill eliminar\hfill todos\hfill los\hfill ceros\hfill a\hfill la\hfill izquierda\hfill de\hfill cada\hfill grupo:
\vspace{0.3mm}
\hspace{1cm} 2001:db8:0:0:0:ff00:42:8329.

\hspace{1cm} Tras omitir secciones consecutivas de ceros: 2001:db8::ff00:42:8329.

La dirección loopback 0000:0000:0000:0000:0000:0000:0001 está definida en el RFC 5156 y se abrevia a ::1
utilizando ambas reglas.

Dado que una dirección IPv6 puede tener más de una representación, el IETF ha publicado una propuesta de
norma para representarlas en texto.

\subsubsection{Dirección de link-local}
Todas las interfaces de los hosts IPv6 requieren una dirección link-local, que tiene el prefijo fe80::/10.
A este prefijo le siguen 54 bits que pueden utilizarse para la creación de subredes, aunque normalmente
se establecen en ceros, y un identificador de interfaz de 64 bits. El host puede calcular y asignar el
identificador de interfaz por sí mismo sin la presencia o cooperación de un componente de red externo
como un servidor DHCP, en un proceso denominado autoconfiguración de direcciones link-local.

Los 64 bits inferiores de la dirección link-local (el sufijo) se derivaban originalmente de la dirección
MAC de la tarjeta de interfaz de red subyacente. Como este método de asignación de direcciones provocaba
cambios de dirección no deseados cuando se sustituían tarjetas de red defectuosas, y como también
adolecía de una serie de problemas de seguridad y privacidad, el RFC 8064 ha sustituido el método
original basado en MAC por el método basado en hash especificado en el RFC 7217 (\cite{gont2014rfc}).

\subsubsection{Direccionamiento global}
El procedimiento de asignación de direcciones globales es similar al de la construcción de direcciones
locales. El prefijo se suministra a partir de anuncios de router en la red. Múltiples anuncios de prefijo
hacen que se configuren múltiples direcciones (\cite{narten1999neighbor}).

La autoconfiguración de direcciones sin estado (SLAAC) requiere un bloque de 64 direcciones, tal y como
se define en el RFC 4291. A los registros locales de Internet se les asignan al menos 32 bloques, que
dividen entre redes subordinadas. La recomendación inicial establecía la asignación de una subred de
48 a los sitios de consumidores finales (RFC 3177). Esto fue reemplazado por el RFC 6177, que ''recomienda
dar a los sitios domésticos bastante más que un único 64, pero tampoco recomienda que cada sitio
doméstico reciba un 48''. Se consideran específicamente los 56. Queda por ver si los ISP cumplirán esta
recomendación. Por ejemplo, durante las pruebas iniciales, los clientes de Comcast recibieron una sola
red de 64.


\subsection{Protocolo IPX/SPX}
IPX/SPX son las siglas de Internetwork Packet Exchange/Sequenced Packet Exchange. IPX y SPX son protocolos
de red utilizados inicialmente en redes que utilizaban los sistemas operativos Novell NetWare (ya
descatalogados). También se utilizaron ampliamente en redes que desplegaban LANS Microsoft Windows, ya que
sustituyeron a las LANS NetWare, pero ya no se utilizan ampliamente. IPX/SPX también se utilizó
ampliamente antes y hasta Windows XP, que soportaba los protocolos, mientras que las versiones posteriores
de Windows no,(\cite{ipxspx}) y TCP/IP tomó el relevo para la creación de redes.

IPX y SPX derivan de los protocolos IDP y SPP de Xerox Network Systems, respectivamente. IPX es un
protocolo de capa de red (capa 3 del modelo OSI), mientras que SPX es un protocolo de capa de transporte
(capa 4 del modelo OSI). La capa SPX se sitúa sobre la capa IPX y proporciona servicios orientados a la
conexión entre dos nodos de la red. SPX se utiliza principalmente en aplicaciones cliente-servidor.

Tanto IPX como SPX proporcionan servicios de conexión similares a TCP/IP, teniendo el protocolo IPX
similitudes con el Protocolo de Internet, y SPX similitudes con TCP. IPX/SPX se diseñó principalmente
para redes de área local (LAN) y es un protocolo muy eficiente para este fin (normalmente el rendimiento
de SPX supera al de TCP en una LAN pequeña, ya que en lugar de ventanas de congestión y acuses de recibo
confirmatorios, SPX utiliza simples NAK). Sin embargo, TCP/IP se ha convertido en el protocolo estándar
de facto. Esto se debe en parte a su rendimiento superior en redes de área extensa e Internet (que utiliza
exclusivamente IP), y también a que TCP/IP es un protocolo más maduro, diseñado específicamente con este
propósito.

A pesar de la asociación de los protocolos con NetWare, no son necesarios para la comunicación NetWare (a
partir de NetWare 5.x), ni se utilizan exclusivamente en redes NetWare. La comunicación NetWare requiere
una implementación NCP, que puede utilizar IPX/SPX, TCP/IP, o ambos, como transporte.



\subsection{Protocolo TCP}
El Protocolo de Control de Transmisión (TCP) es uno de los principales protocolos del conjunto de
protocolos de Internet. Se originó en la implementación inicial de la red en la que complementaba al
Protocolo de Internet (IP). Por ello, el conjunto se denomina comúnmente TCP/IP. TCP proporciona una
entrega fiable, ordenada y con comprobación de errores de un flujo de octetos (bytes) entre aplicaciones
que se ejecutan en hosts que se comunican a través de una red IP. Las principales aplicaciones de
Internet, como la World Wide Web, el correo electrónico, la administración remota y la transferencia de
archivos, dependen de TCP, que forma parte de la capa de transporte del conjunto TCP/IP. SSL/TLS suele
ejecutarse sobre TCP.

El protocolo TCP está orientado a la conexión, por lo que se establece una conexión entre el cliente y el
servidor antes de poder enviar datos. El servidor debe estar a la escucha (apertura pasiva) de las
solicitudes de conexión de los clientes antes de que se establezca una conexión. La conexión de tres
vías (apertura activa), la retransmisión y la detección de errores aumentan la fiabilidad, pero alargan
la latencia. Las aplicaciones que no requieren un servicio de flujo de datos fiable pueden utilizar en
su lugar el Protocolo de Datagramas de Usuario (UDP), que proporciona un servicio de datagramas sin
conexión que prioriza el tiempo sobre la fiabilidad. TCP evita la congestión de la red. Sin embargo,
existen vulnerabilidades en TCP, como la denegación de servicio, el secuestro de conexiones, el veto
TCP y el ataque de reinicio.

\subsubsection{Funcionamiento del protocolo}
Las operaciones del protocolo TCP pueden dividirse en tres fases. El establecimiento de la conexión es
un proceso handshake de varios pasos que establece una conexión antes de entrar en la fase
de transferencia de datos. Una vez completada la transferencia de datos, la terminación de la conexión
la cierra y libera todos los recursos asignados.

\subsubsection{Establecimiento de la conexión}
Antes de que un cliente intente conectarse a un servidor, el servidor debe primero enlazar y escuchar
en un puerto para abrirlo a conexiones: esto se denomina apertura pasiva. Una vez establecida la apertura
pasiva, un cliente puede establecer una conexión iniciando una apertura activa mediante el protocolo de
tres pasos:

\begin{enumerate}
    \item \textbf{SYN:} La apertura activa la realiza el cliente enviando un SYN al servidor. El cliente
        establece el número de secuencia del segmento en un valor aleatorio A.
    \item \textbf{SYN-ACK:} En respuesta, el servidor responde con un SYN-ACK. El número de acuse de
        recibo se establece en uno más que el número de secuencia recibido, es decir, A+1, y el número
        de secuencia que el servidor elige para el paquete es otro número aleatorio, B.
    \item \textbf{ACK:} Por último, el cliente envía un ACK al servidor. El número de secuencia se
        establece en el valor de acuse de recibo recibido, es decir, A+1, y el número de acuse de
        recibo se establece en uno más que el número de secuencia recibido, es decir, B+1.
\end{enumerate}
Los pasos 1 y 2 establecen y confirman el número de secuencia para una dirección. Los pasos 2 y 3
establecen y confirman el número de secuencia para la otra dirección. Una vez completados estos pasos,
tanto el cliente como el servidor han recibido acuses de recibo y se establece una comunicación
full-duplex.

\subsubsection{Terminación de la conexión}
La fase de terminación de la conexión utiliza un handshake de cuatro vías, en el que cada lado de la
conexión termina de forma independiente. Cuando un extremo desea detener su mitad de la conexión,
transmite un paquete FIN, que el otro extremo reconoce con un ACK. Por lo tanto, una interrupción
típica requiere un par de segmentos FIN y ACK de cada extremo TCP. Después de que el lado que envió
el primer FIN haya respondido con el ACK final, espera un tiempo de espera antes de cerrar finalmente
la conexión, durante el cual el puerto local no está disponible para nuevas conexiones; este estado
permite al cliente TCP reenviar el acuse de recibo final al servidor en caso de que el ACK se pierda
en tránsito. La duración del tiempo depende de la implementación, pero algunos valores comunes son 30
segundos, 1 minuto y 2 minutos. Una vez transcurrido el tiempo de espera, el cliente entra en estado
CLOSED y el puerto local pasa a estar disponible para nuevas conexiones (\cite{kurose2017computer}).

También es posible terminar la conexión mediante un handshake de 3 vías, cuando el host A envía un FIN
y el host B responde con un FIN \& ACK (combinando dos pasos en uno) y el host A responde con un ACK
(\cite{tanenbaum2003redes}).

\subsubsection{Transferencia de datos}
\begin{itemize}
    \item Transferencia de datos ordenada: el host de destino reordena los segmentos según un número de
        secuencia.
    \item Retransmisión de paquetes perdidos: cualquier flujo acumulado no reconocido se retransmite.
    \item Transferencia de datos sin errores: los paquetes dañados se consideran perdidos y se retransmiten.
    \item Control de flujo: limita la velocidad a la que un emisor transfiere datos para garantizar una
        entrega fiable. El receptor indica continuamente al emisor la cantidad de datos que puede
        recibir. Cuando el búfer del host receptor se llena, el siguiente acuse de recibo suspende
        la transferencia y permite procesar los datos del búfer.
    \item Control de la congestión: la pérdida de paquetes (supuestamente debida a la congestión)
        provoca una reducción de la velocidad de entrega de datos.
\end{itemize}
\begin{flushright}
    (\cite{comer2006tcpip})
\end{flushright}

\subsection{Protocolo NetBIOS}
NetBIOS proporciona servicios relacionados con la capa de sesión del modelo OSI que permiten a las
aplicaciones de ordenadores independientes comunicarse a través de una red de área local. Al ser
estrictamente una API, NetBIOS no es un protocolo de red. Los sistemas operativos de los años 80 (DOS y
Novell Netware principalmente) ejecutaban NetBIOS sobre IEEE 802.2 e IPX/SPX utilizando los protocolos
NetBIOS Frames (NBF) y NetBIOS sobre IPX/SPX (NBX), respectivamente. En las redes modernas, NetBIOS se
ejecuta normalmente sobre TCP/IP a través del protocolo NetBIOS sobre TCP/IP (NBT). Esto hace que cada
ordenador de la red tenga tanto una dirección IP como un nombre NetBIOS correspondiente a un nombre de
host (posiblemente diferente). NetBIOS también se utiliza para identificar nombres de sistema en TCP/IP
(Windows). En pocas palabras, es un protocolo que permite la comunicación de datos para archivos e
impresoras a través de la Capa de Sesión del Modelo OSI en una LAN.

\subsubsection{Servicios}
NetBIOS ofrece tres servicios distintos:
\begin{itemize}
    \item Servicio de nombres (NetBIOS-NS) para el registro y la resolución de nombres.
    \item Servicio de distribución de datagramas (NetBIOS-DGM) para la comunicación sin conexión.
    \item Servicio de sesión (NetBIOS-SSN) para la comunicación orientada a la conexión.
\end{itemize}

(Nota: SMB, una capa superior, es un servicio que se ejecuta sobre el Servicio de Sesión y el Servicio de
Datagramas, y no debe confundirse como una parte necesaria e integral del propio NetBIOS. Ahora puede
ejecutarse sobre TCP con una pequeña capa de adaptación que añade un campo de longitud a cada mensaje SMB; esto
es necesario porque TCP sólo proporciona un servicio de flujo de bytes sin noción de límites de mensaje).

\newpage
\section{Desarrollo}

\subsection{¿Qué tiene TCP/IP frente a otros protocolos que consolida la internet como red global?}

TCP/IP es uno de los modelos de protocolos más importantes para la red global debido a que, es altamente
interoperable, pudiendo funcionar en una gran variedad de dispositivos, permitiendo la comunicación entre
ellos. Esta bajo estándares abiertos, que lo hace recibir un amplio apoyo de la comunidad de
desarrolladores a nivel global, fomentando la colaboración, y la innovación. Se fue adaptando a las 
necesidades que ha tenido la internet mientras ha evolucionado, adaptandose también a diferentes
tecnologias, contribuyendo a su longevidad.

\subsection{¿Cuál es el propósito de la máscara de subred?}
La máscara de una subred tiene como propósito indicar la candidad de 1 para hacer una operación AND lógico
con la IP de la interfaz que van a estar definiendo la dirección IP de la subred o segmento. Por ejemplo
(en IPv4) una interfaz tiene la dirección 192.168.56.1/24, la mascara tiene 24 bits en 1, osea:
11111111.11111111.11111111.0, por lo que realizando la operación AND entre la máscara y la IP resulta en
la dirección IP de la red que sería la 192.168.56.0. También tiene el propósito de determinar la candidad
de direcciones disponibles para las interfaces de red que se conecten.

\subsection{¿Cómo se realiza el proceso de segmentación de redes?}
El proceso de segmentación de una red se realiza sub dividiendo en redes de menor tamaño asignando
a cada subred una dirección IP que va a ser el identificador de ese segmento. A cada segmento, también se
le asigna una máscara, para determinar la cantidad de direcciones que se pueden asignar dentro de la subred.

Segmentar las redes contribuye a la eficiencía en la comunicación entre los equipos de una misma
subred, ya que estos se comunican atravez de un switch en común, evitando congestionar otros nodos externos
a la subred.

\subsection{¿Cuáles son la mejoras estructurales del protocolo IPv6?}
\begin{itemize}
    \item Debido a la escacez de direcciones IPv4, IPv6 viene a resolver ese problema de manera muy basta,
        ya que las direcciones IPv6 se componen de 128 bits, o 8 grupos de 16 bits, siendo $2^{128}$ la
        cantidad de combinaciones posibles.
    \item IPv6 incluye la capacidad de autoconfiguración de direcciones, lo que permite que los
        dispositivos generen y configuren automáticamente sus direcciones IPv6 sin necesidad de
        intervención manual o de un servidor DHCP. Esto simplifica la configuración de redes y facilita
        la conectividad plug-and-play.
    \item El formato de encabezado en IPv6 se ha simplificado en comparación con IPv4. Esto ayuda a
        mejorar la eficiencia en el procesamiento de paquetes y reduce la sobrecarga de los routers y
        dispositivos de red.
    \item IPv6 tiene soporte nativo para la seguridad a través de la incorporación de IPSec (Protocolo
        de Seguridad de Internet). IPSec proporciona autenticación y cifrado de paquetes IP, lo que
        mejora la seguridad de las comunicaciones en comparación con IPv4, donde IPSec es una opción
        adicional.
\end{itemize}

\subsection{¿Por qué no puede existir control del tráfico que pasa por Internet?}
No se puede por la naturaleza misma de la internet. Descentralizada, es la interconexión de redes
individuales, no existe una entidad central que controle la internet. Autogestionada con enrrutamientos
dinámicos, los paquetes buscan las mejores rutas para poder llegar a su destino, osea que no es una única
ruta para cada conexión lo que dificulta el control del tráfico. Privacidad y criptografía, muchos de los 
servicios y aplicaciones que están en la internet utilizan técnicas de cifrado y protección de la
privacidad para garantizar la privacidad de los datos.

\subsection{Comunicación por socket TCP/IP}
Realizar un programa en C o C++ que realice la transferencia de un archivo de texto entre un cliente y un
servidor con protocolo TCP.

\subsubsection{Cliente}
\begin{center}
    \includegraphics[scale=0.21]{img/cliente.png}
    Cliente con la dirección IP 192.168.1.14
\end{center}

\subsubsection{Servidor}
\begin{center}
    \includegraphics[scale=0.21]{img/server.png}
    Servidor con la dirección IP 192.168.56.30
\end{center}

\subsubsection{Archivo a enviar}
\begin{center}
    \includegraphics[scale=0.21]{img/archivoaenviar.png}
\end{center}
\newpage
En el servidor el directorio donde se va a copiar el archivo 
desde el cliente tiene estos archivos por el momento:
\begin{center}
    \includegraphics[scale=0.155]{img/server-ll.png}
\end{center}
Se ejecuta el código que va a recibir los archivos que se quieran
copiar.
\begin{center}
    \includegraphics[scale=0.26]{img/server-listen.png}
\end{center}
El cliente envia el archivo a la dirección IP del servidor y al
puerto en donde el servidor espera archivos.
\begin{center}
    \includegraphics[scale=0.19]{img/cliente-send.png}
\end{center}
Matando el proceso en el servidor se puede ver como llegó el
archivo al servidor.
\begin{center}
    \includegraphics[scale=0.154]{img/server-result.png}
\end{center}

\section{Concluciones}
Los protocolos de comunicación son fundamentales para el desarrollo y funcionamiento de las redes. Establecen
reglas y normas que permiten la transmisión de datos de manera efectiva y confiable, facilitando la comunicación
entre dispositivos y aplicaciones.

El modelo TCP/IP ha sido clave en el desarrollo de la red global. Dividido en capas, proporciona una estructura
organizada y modular para la transmisión de datos. Este modelo ha permitido el crecimiento y la expansión de
Internet, así como la interoperabilidad entre dispositivos y sistemas de diferentes fabricantes.

El protocolo IPv6 representa un cambio estructural importante en comparación con su predecesor, IPv4. Con su
espacio de direcciones más amplio, IPv6 aborda la escasez de direcciones IP en IPv4 y ofrece mejoras en la
seguridad, la calidad de servicio y la eficiencia de la red. Su adopción gradual es necesaria para garantizar
la continuidad y el crecimiento de Internet.

La comunicación por sockets TCP/IP permite establecer conexiones entre aplicaciones en diferentes dispositivos a
través de la red. Utilizando el protocolo TCP, se garantiza la entrega confiable de los datos, mientras que el
protocolo IP se encarga del enrutamiento y la entrega de los paquetes de datos. Los sockets TCP/IP son fundamentales
para la transferencia de datos y el intercambio de información en Internet.
\newpage
\printbibliography

\end{document}
