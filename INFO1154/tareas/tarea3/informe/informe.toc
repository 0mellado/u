\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Resumen}{3}{section.1}%
\contentsline {section}{\numberline {2}Introdución}{3}{section.2}%
\contentsline {section}{\numberline {3}Marco teórico}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Protocolo IPSec}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Protocolo IPX}{5}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Protocolo ICMP}{6}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Detalles técnicos}{6}{subsubsection.3.3.1}%
\contentsline {subsection}{\numberline {3.4}Protocolo IPv4}{7}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Representación de las direcciones}{7}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Asignación}{8}{subsubsection.3.4.2}%
\contentsline {subsection}{\numberline {3.5}Protocolo IPv6}{8}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Principales características}{9}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Representación de las direcciones}{10}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Dirección de link-local}{11}{subsubsection.3.5.3}%
\contentsline {subsubsection}{\numberline {3.5.4}Direccionamiento global}{11}{subsubsection.3.5.4}%
\contentsline {subsection}{\numberline {3.6}Protocolo IPX/SPX}{12}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Protocolo TCP}{13}{subsection.3.7}%
\contentsline {subsubsection}{\numberline {3.7.1}Funcionamiento del protocolo}{13}{subsubsection.3.7.1}%
\contentsline {subsubsection}{\numberline {3.7.2}Establecimiento de la conexión}{14}{subsubsection.3.7.2}%
\contentsline {subsubsection}{\numberline {3.7.3}Terminación de la conexión}{14}{subsubsection.3.7.3}%
\contentsline {subsubsection}{\numberline {3.7.4}Transferencia de datos}{15}{subsubsection.3.7.4}%
\contentsline {subsection}{\numberline {3.8}Protocolo NetBIOS}{15}{subsection.3.8}%
\contentsline {subsubsection}{\numberline {3.8.1}Servicios}{16}{subsubsection.3.8.1}%
\contentsline {section}{\numberline {4}Desarrollo}{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}¿Qué tiene TCP/IP frente a otros protocolos que consolida la internet como red global?}{17}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}¿Cuál es el propósito de la máscara de subred?}{17}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}¿Cómo se realiza el proceso de segmentación de redes?}{17}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}¿Cuáles son la mejoras estructurales del protocolo IPv6?}{18}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}¿Por qué no puede existir control del tráfico que pasa por Internet?}{18}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Comunicación por socket TCP/IP}{19}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}Cliente}{19}{subsubsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.2}Servidor}{19}{subsubsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.3}Archivo a enviar}{19}{subsubsection.4.6.3}%
\contentsline {section}{\numberline {5}Concluciones}{21}{section.5}%
