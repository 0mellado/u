#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>


void error(std::string s, int status) {
    std::cerr << s << ": " << strerror(errno) << "\n";
    exit(status);
}


void read_file(char *name_file, char **buffer, char **buffer_size) {

    char *file_content;
    char *name_file_tmp;
    size_t file_size;
    size_t name_size = strlen(name_file) + 1;

    name_file_tmp = new char[name_size];

    strncpy(name_file_tmp, name_file, name_size-1);

    name_file_tmp[name_size-1] = '\n';

    std::ifstream file(name_file, std::ifstream::binary);

    if (file.fail()) 
        error("Error: archivo o directorio no encontrado", -1);

    file.seekg(0, file.end);
    file_size = file.tellg();
    file.seekg(0, file.beg);

    *buffer = new char[file_size + name_size];

    file_content = new char[file_size];
    file.read(file_content, file_size);

    strncpy(*buffer, name_file_tmp, name_size);
    delete [] name_file_tmp;
    strncpy(*buffer + name_size, file_content, file_size);
    delete [] file_content;

    std::size_t full_size = file_size + name_size;

    std::string tmp = std::to_string(full_size);
    /* long unsigned int b = tmp.length()-1; */
    /* *buffer_size = (char *) malloc(sizeof(char)*b); */
    *buffer_size = new char[tmp.length()-1];
    strncpy(*buffer_size, tmp.c_str(), tmp.length()+1);

    return;
}


int main(int argc, char *argv[]) {
    if (argc != 4)
        error("Uso: ./client <archivo> <host> <puerto>", -2);

    char *file_data;
    /* char *file_size_str = nullptr; */
    char *file_size_str;

    read_file(argv[1], &file_data, &file_size_str);

    const char *host = argv[2];
    const uint16_t port = (short unsigned int)atoi(argv[3]);

    struct sockaddr_in server_addr;
    int socketfd;

    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd == -1) error("No se pudo crear el socket", -3);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_pton(AF_INET, host, &server_addr.sin_addr);

    if (connect(socketfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
        error("La wea ta mala", -4);

    send(socketfd, file_size_str, 64, 0);

    int const file_size = atoi(file_size_str);

    send(socketfd, file_data, file_size, 0);

    close(socketfd);

    delete [] file_data;
    delete [] file_size_str;

    return 0;
}
