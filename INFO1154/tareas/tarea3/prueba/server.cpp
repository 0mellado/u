#include <iostream>
#include <fstream>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void error(std::string s, int status) {
    std::cerr << s << ": " << strerror(errno) << "\n";
    exit(status);
}

int main() {
    char const host[] = "127.0.0.1";
    uint16_t port = 8080;

    struct sockaddr_in server_addr;
    int socketfd, clientfd;
    socklen_t client_size;

    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd == -1) error("No se pudo crear el socket", -1);

    int opt = 1;
    if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
        error("No se pudo reutilizar la dirección IP", -2);

    if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt)) < 0)
        error("No se pudo reutilizar el puerto", -3);

    memset(&server_addr, 0, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_pton(AF_INET, host, &server_addr.sin_addr);

    if (bind(socketfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
        error("Error al asignar la IP y el puerto al socket", -4);

    if (listen(socketfd, SOMAXCONN) == 1) 
        error("Error al dejar en escucha el socket", -5);

    std::cout << "[Server] Escuchando peticiones...\n";

    while (true) {
        int length_file;
        char length_file_str[64];
        char *file_data;
        char *file_name;
        char *file_content;

        client_size = sizeof(clientfd);
        clientfd = accept(socketfd, (struct sockaddr *)&server_addr, &client_size);
        if (clientfd == -1) error("Error al conectarse con el cliente", -6);

        recv(clientfd, length_file_str, 64, 0);

        length_file = std::atoi(length_file_str);

        file_data = new char[length_file];

        recv(clientfd, file_data, length_file, 0);

        std::size_t name_size = 0;

        char c;
        while ((c = file_data[name_size]) != '\n') name_size++;

        file_name = new char[name_size];

        strncpy(file_name, file_data, name_size);

        file_content = new char[length_file - (name_size+1)];

        strncpy(file_content, file_data + name_size+1, length_file-(name_size+1));

        close(clientfd);

        std::ofstream new_file;
        new_file.open(file_name);

        new_file << file_content;
        new_file.close();

        delete [] file_content;
        delete [] file_name;
        delete [] file_data;
    }

    close(socketfd);

    return 0;
}
