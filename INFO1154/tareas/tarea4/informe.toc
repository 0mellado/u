\babel@toc {spanish}{}\relax 
\contentsline {section}{\numberline {1}Resumen}{3}{section.1}%
\contentsline {section}{\numberline {2}Introducción}{3}{section.2}%
\contentsline {section}{\numberline {3}Marco teórico}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Servicios residenciales}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Redes 4G}{4}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Redes 5G}{4}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Redes MPLS}{5}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Redes VLAN}{5}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Redes VPN}{5}{subsection.3.6}%
\contentsline {section}{\numberline {4}Desarrollo}{6}{section.4}%
\contentsline {subsection}{\numberline {4.1}Servicio residencial de StarLink}{6}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Medios físicos utilizados}{6}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Principales características del nivel de enlace}{7}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Equipamiento utilizado}{7}{subsubsection.4.1.3}%
\contentsline {subsubsection}{\numberline {4.1.4}Estructura de la red}{7}{subsubsection.4.1.4}%
\contentsline {subsubsection}{\numberline {4.1.5}Asignación de direcciones IP}{7}{subsubsection.4.1.5}%
\contentsline {subsubsection}{\numberline {4.1.6}Capacidades y tipos de servicios}{8}{subsubsection.4.1.6}%
\contentsline {subsection}{\numberline {4.2}Redes 4G}{9}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Medios físicos utilizados}{9}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Principales características del nivel de enlace}{9}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Equipamiento utilizado}{9}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}Estructura de la red}{10}{subsubsection.4.2.4}%
\contentsline {subsubsection}{\numberline {4.2.5}Asignación de direcciones IP}{11}{subsubsection.4.2.5}%
\contentsline {subsubsection}{\numberline {4.2.6}Capacidades y tipos de servicios}{11}{subsubsection.4.2.6}%
\contentsline {subsection}{\numberline {4.3}Redes 5G}{12}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Medios físicos utilizados}{12}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Principales características del nivel de enlace}{12}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Equipamiento utilizado}{12}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Estructura de la red}{14}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Asignación de direcciones IP}{14}{subsubsection.4.3.5}%
\contentsline {subsubsection}{\numberline {4.3.6}Capacidades y tipos de servicios}{15}{subsubsection.4.3.6}%
\contentsline {subsection}{\numberline {4.4}Redes MPLS}{16}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Medios físicos utilizados}{16}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Principales características del nivel de enlace}{17}{subsubsection.4.4.2}%
\contentsline {subsubsection}{\numberline {4.4.3}Equipamiento utilizado}{17}{subsubsection.4.4.3}%
\contentsline {subsubsection}{\numberline {4.4.4}Estructura de la red}{19}{subsubsection.4.4.4}%
\contentsline {subsubsection}{\numberline {4.4.5}Asignación de direcciones IP y etiquetas}{19}{subsubsection.4.4.5}%
\contentsline {subsubsection}{\numberline {4.4.6}Capacidades y tipos de servicios}{20}{subsubsection.4.4.6}%
\contentsline {subsection}{\numberline {4.5}Redes VLAN}{21}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}Medios físicos utilizados}{21}{subsubsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.2}Principales características del nivel de enlace}{21}{subsubsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.3}Equipamiento utilizado}{22}{subsubsection.4.5.3}%
\contentsline {subsubsection}{\numberline {4.5.4}Estructura de la red}{23}{subsubsection.4.5.4}%
\contentsline {subsubsection}{\numberline {4.5.5}Asignación de direcciones IP}{23}{subsubsection.4.5.5}%
\contentsline {subsubsection}{\numberline {4.5.6}Capacidades y tipos de servicios}{24}{subsubsection.4.5.6}%
\contentsline {subsection}{\numberline {4.6}Redes VPN}{25}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}Medios físicos utilizados}{25}{subsubsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.2}Principales características del nivel de enlace}{26}{subsubsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.3}Equipamiento utilizado}{27}{subsubsection.4.6.3}%
\contentsline {subsubsection}{\numberline {4.6.4}Estructura de la red}{28}{subsubsection.4.6.4}%
\contentsline {subsubsection}{\numberline {4.6.5}Asignación de direcciones IP}{28}{subsubsection.4.6.5}%
\contentsline {subsubsection}{\numberline {4.6.6}Capacidades y tipos de servicios}{29}{subsubsection.4.6.6}%
\contentsline {section}{\numberline {5}Conclusiones}{31}{section.5}%
