#include <iostream>

class PaymentMethod {
    public:
        virtual ~PaymentMethod() {}
        virtual std::string Pay() const = 0;
};


class CreditCard: public PaymentMethod {
    public:
        std::string Pay() const override {
            return "{Pagando con tarjeta de credito}";
        }
};


class DebitCard: public PaymentMethod {
    public:
        std::string Pay() const override {
            return "{Pagando con tarjeta de debito}";
        }
};


class Creator {
    public:
        virtual ~Creator() {};
        virtual PaymentMethod* FactoryMethod() const = 0;

        std::string SomePay() const {
            PaymentMethod* payment_method = FactoryMethod();

            std::string result = "Creator: El mismo código de la fabrica que se ejecutó con: "
                + payment_method->Pay();

            delete payment_method;
            return result;
        }
};


class CreatorMethodPay0 : public Creator {
    public:
        PaymentMethod* FactoryMethod() const override {
            return new CreditCard();
        }
};

class CreatorMethodPay1: public Creator {
    public:
        PaymentMethod* FactoryMethod() const override {
            return new DebitCard();
        }
};


void ClientCode(const Creator& creator) {
    std::cout << "Cliente: No se de la clase creator, pero de todas formas funciona.\n"
            << creator.SomePay() << std::endl;
    return;
}


int main() {

    std::cout << "Aplicación: Lanzada para pagar con tarjeta de credito.\n";
    Creator *credito = new CreatorMethodPay0();
    ClientCode(*credito);

    std::cout << std::endl;

    std::cout << "Aplicación: Lanzada para pagar con tarjeta de debito.\n";
    Creator *debito = new CreatorMethodPay1();
    ClientCode(*debito);

    delete credito;
    delete debito;

    return 0;
}
