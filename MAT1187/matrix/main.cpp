#include <cctype>
#include <iostream>
#include <cstdio>
#include <cstdlib>
/* #include <time.h> */

#include "matrix.hpp"

/* void print_menu(); */
/* void menu(); */
/* std::size_t select_option(); */


int main() {

    /* srand((unsigned int)time(NULL)); */

    /* menu(); */

    /* select_option(); */

    Matrix A{
        { 32, 8, 11, 17 },
        { 8, 20, 17, 23 },
        { 11, 17, 14, 26 },
        { 17, 23, 26, 21 }
    };


    std::cout << A.inv() * A << "\n";


    return 0;
}

/* void menu() { */
/*     system("clear"); */

/*     std::cout << "\nCalculadora de matrices\n\n"; */

/*     print_menu(); */
/* } */

/* void print_menu() { */
/*     std::cout << "Opciones de uso:\n"; */
/*     std::cout << "\n1. Sumar matrices\n"; */
/*     std::cout << "2. Restar matrices\n"; */
/*     std::cout << "3. Multiplicar matrices\n"; */
/*     std::cout << "4. Determinate de la matriz\n"; */
/*     std::cout << "5. Matriz de cofactores\n"; */
/*     std::cout << "6. Matriz inversa\n"; */
/*     std::cout << "7. Salir\n"; */
/* } */

/* std::size_t select_option() { */

/*     std::size_t option = 0; */

/*     while (!std::isdigit((int)option) || option == 0 || option > 8 || option < 1) { */
/*         menu(); */
/*         std::cout << "\n-> "; */
/*         std::cin >> option; */
/*     } */

/*     return option; */
/* } */

