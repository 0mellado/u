#include "matrix.hpp"
#include <iomanip>
#include <valarray>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <assert.h>
#include <cstddef>
#include <stdexcept>
#include <limits>
#include <algorithm>


double const ZERO = std::numeric_limits<double>().epsilon();


Matrix::Matrix(std::size_t const rows, std::size_t const cols)
    : rows_{rows} ,cols_{cols}
{
    if (rows_ == 0 || cols_ == 0)
        throw std::out_of_range("[Matrix::Matrix] No se puede crear una matriz de dimensión 0\n");
}

Matrix::Matrix(initL<initL<double>> const& l)
    : Matrix(l.size(), (l.size() > 0) ? l.begin()->size() : 0)
{
    std::size_t i{};

    for (auto& row : l) {
        if (row.size() != cols_)
            throw std::out_of_range("[Matrix::Matrix] no se de que es este error\n");
        for (auto& item : row) {
            data_[i] = item;
            ++i;
        }
    }
}


double& Matrix::operator()(const std::size_t row, const std::size_t col) {
    assert(row < rows_);
    assert(col < cols_);
    return data_[row*cols_ + col];
}

double Matrix::operator()(const std::size_t row, const std::size_t col) const {
    assert(row < rows_);
    assert(col < cols_);
    return data_[row*cols_ + col];
}


bool Matrix::is_row_null(std::size_t const row) const noexcept {
    bool row_null = true;
    std::size_t i = 0;

    while (row_null && i < cols())
        if ((*this)(row, i++) != 0) row_null = false;

    return row_null;
}

std::size_t Matrix::rows_not_null() const noexcept {
    std::size_t count =  0;
    for (std::size_t r = 0; r < rows(); r++)
        if (!is_row_null(r)) count++;

    return count;
}

Matrix Matrix::operator*(Matrix const &right_matrix) const {
    if (cols() != right_matrix.rows())
        throw std::out_of_range("[Matrix::*] No se pueden multiplicar estas matrices, tamaños incompatibles\n");
    Matrix result(rows(), right_matrix.cols());

    for (std::size_t r = 0; r < rows(); r++) {
        for (std::size_t c = 0; c < right_matrix.cols(); c++) {
            result(r, c) = 0;
            for (std::size_t i = 0; i < cols(); i++) {
                result(r, c) += (*this)(r, i) * right_matrix(i, c);
            }
        }
    }

    return result;
}


Matrix Matrix::operator*(double const &scalar) const {
    Matrix result(rows(), cols());

    for (std::size_t r = 0; r < rows(); r++)
        for (std::size_t c = 0; c < cols(); c++)
            result(r, c) = (*this)(r, c) * scalar;

    return result;
}



Matrix Matrix::operator+(Matrix const &matrix) const {
    if (rows() != matrix.rows() || cols() != matrix.cols())
        throw std::out_of_range("[Matrix::+] No se pueden sumar matrices de diferentes dimenciones\n");

    Matrix result(rows(), cols());

    for (std::size_t r = 0; r < rows(); r++)
        for (std::size_t c = 0; c < cols(); c++)
            result(r, c) = (*this)(r, c) + matrix(r, c);

    return result;
}
 

Matrix Matrix::operator-(const Matrix &matrix) const {
    return (*this) + (matrix * -1);
}


Matrix Matrix::T() const {
    Matrix result(cols(), rows());

    for (std::size_t r = 0; r < rows(); r++) {
        for (std::size_t c = 0; c < cols(); c++) {
            result(c, r) = (*this)(r, c);
        }
    }

    return result;
}


Matrix Matrix::minor(std::size_t i, std::size_t j) const {
    Matrix result(rows() -1, cols() -1);

    std::size_t i_new = 0;
    for (std::size_t r = 0; r < rows(); r++) {
        if (r + 1 == i) continue;
        std::size_t j_new = 0;
        for (std::size_t c = 0; c < cols(); c++) {
            if (c + 1 == j) continue;
            result(i_new, j_new) = (*this)(r, c);
            j_new++;
        }
        i_new++;
    }

    return result;
}


double Matrix::cof(std::size_t i, std::size_t j) const {
    Matrix minor = (*this).minor(i, j);
    return std::pow(-1, (i + j)) * minor.det();
}


Matrix Matrix::cof() const {

    if (((*this).det() == ZERO) || ((*this).det() == 0))
        throw std::out_of_range("[Matrix::cof] La matriz no es inversible\n");

    Matrix cofactors(rows(), cols());

    for (std::size_t r = 0; r < rows(); r++)
        for (std::size_t c = 0; c < cols(); c++)
            cofactors(r, c) = (*this).cof(r+1, c+1);
        
    return cofactors;
}


Matrix Matrix::inv() const {

    if (cols() == 1) {
        Matrix result{ { 1 / (*this)(0, 0) } };
        return result;
    }


    if ((std::sqrt(std::pow((*this).det(), 2)) < ZERO) || ((*this).det() == 0))
        throw std::out_of_range("[Matrix::inv] La matriz no es inversible\n");
    return (*this).cof().T() * (1 / (*this).det());
}


double Matrix::det() const {
    if (!is_sqrt())
        throw std::out_of_range("[Matrix::det] La matriz debe ser cuadrada\n");

    if (cols() == 1) return (*this)(0, 0);

    if (cols() == 2)
        return ((*this)(0, 0) * (*this)(1, 1)) - ((*this)(0, 1) * (*this)(1, 0));

    if (cols() == 3)
        return ((*this)(0, 0) * (*this)(1, 1) * (*this)(2, 2)) + 
               ((*this)(0, 1) * (*this)(1, 2) * (*this)(2, 0)) + 
               ((*this)(0, 2) * (*this)(1, 0) * (*this)(2, 1)) -
              (((*this)(0, 2) * (*this)(1, 1) * (*this)(2, 0)) +
               ((*this)(0, 0) * (*this)(1, 2) * (*this)(2, 1)) +
               ((*this)(0, 1) * (*this)(1, 0) * (*this)(2, 2)));

    double det = 0.0;

    for (std::size_t r = 0; r < rows(); r++)
        det += (*this)(r, 0) * (*this).cof(r+1, 1);

    if (std::sqrt(std::pow(det, 2)) < ZERO)
        det = 0.0;

    return det;
}


Matrix Matrix::be_sqrt() const {

    bool rows_bigger = cols() < rows();
    std::size_t max_size = rows_bigger ? rows() : cols();

    Matrix result(max_size, max_size);

    for (std::size_t r = 0; r < rows(); r++) 
        for (std::size_t c = 0; c < cols(); c++) 
            result(r, c) = (*this)(r, c);

    return result;
}


Matrix Matrix::copy() const {
    Matrix result(rows(), cols());

    for (std::size_t r = 0; r < rows(); r++)
        for (std::size_t c = 0; c < cols(); c++)
            result(r, c) = (*this)(r, c);

    return result;
}

void Matrix::F_mult_row(const std::size_t row, const double scalar) {

    for (std::size_t c = 0; c < cols(); c++)
        (*this)(row, c) = (*this)(row, c) * scalar;

    return;
}


void Matrix::F_swap_rows(const std::size_t row0, const std::size_t row1) {
    if (row0 == row1) return;

    std::unique_ptr<double[]> swap_row{std::make_unique<double[]>(cols())};

    for (std::size_t c = 0; c < cols(); c++) {
        swap_row[c] = (*this)(row0, c);
        (*this)(row0, c) = (*this)(row1, c);
        (*this)(row1, c) = swap_row[c];
    }

    return;
}


void Matrix::F_sum_rows(const std::size_t row0, const std::size_t row1, const double scalar) {

    if (row0 == row1) {
        (*this).F_mult_row(row1, scalar);
        return;
    }

    std::unique_ptr<double[]> swap_row{std::make_unique<double[]>(cols())};

    for (std::size_t c = 0; c < cols(); c++) {
        swap_row[c] = (*this)(row0, c);
        (*this)(row0, c) = (*this)(row1, c);
        (*this)(row1, c) = (*this)(row1, c) + swap_row[c] * scalar;
    }

    return;
}


/* std::size_t Matrix::rang() const { */
/*     Matrix matrix = (*this).be_sqrt(); */

/*     std::size_t rang = cols() < rows() ? cols() : rows(); */

/*     double dets = matrix.det(); */

/*     if (dets != 0) return rang; */

/*     Matrix tmp = matrix.be_sqrt(); */

/*     double last_pivot = 1; */
/*     double a_pivot; */
/*     for (std::size_t pivot = 0; pivot < matrix.rows()-2; pivot++) { */
/*         std::cout << matrix(pivot, pivot) << "\n"; */
/*          a_pivot = matrix(pivot, pivot); */

/*         /1* if (pivot == matrix.rows()-1 && matrix(pivot, pivot) == 0) continue; *1/ */
/*         if (a_pivot == 0.0) { */
/*             std::cout << matrix; */
/*             matrix.F_swap_rows(pivot, pivot+1); */
/*             std::cout << matrix; */
/*             std::cout << matrix(pivot, pivot) << "\n"; */
/*             a_pivot = matrix(pivot, pivot); */
/*         } */

/*         for (std::size_t r = 0; r < matrix.rows(); r++) { */
/*             if (r == pivot) continue; */
/*             for (std::size_t c = 0; c < matrix.cols(); c++) { */
/*                 tmp(r, c) = (a_pivot * matrix(r, c) - matrix(pivot, c) * matrix(r, pivot)) / last_pivot; */
/*             } */
/*         } */

/*         last_pivot = a_pivot; */

/*         for (std::size_t r = 0; r < matrix.rows(); r++) */
/*             for (std::size_t c = 0; c < matrix.cols(); c++) */
/*                 matrix(r, c) = tmp(r, c); */
/*         std::cout << matrix; */
/*     } */

/*     return matrix.rows_not_null(); */
/* } */


std::string to_fraction(double number, int cycles = 10, double precision = 5e-4) {

    if (std::fmod(number, 1) == 0) return std::to_string((int)number);

    int sign  = number > 0 ? 1 : -1;
    number = number * sign;
    double new_number, whole_part;
    double decimal_part = number - (int)number;
    int counter = 0;
    
    std::valarray<double> vec_1{double((int) number), 1}, vec_2{1,0}, temporary;
    
    while ((decimal_part > precision) & (counter < cycles)) {
        new_number = 1 / decimal_part;
        whole_part = (int) new_number;
        
        temporary = vec_1;
        vec_1 = whole_part * vec_1 + vec_2;
        vec_2 = temporary;
        
        decimal_part = new_number - whole_part;
        counter += 1;
    }

    if (vec_1[1] == 1) return std::to_string((int)(sign * vec_1[0]));

    return std::to_string((int)(sign * vec_1[0]))+'/'+std::to_string((int)vec_1[1]);
}


std::ostream& operator<<(std::ostream& out, Matrix const& matrix) {
    out << "[";
    for (std::size_t r = 0; r < matrix.rows(); r++) {
        if (r == 0) out << "[ ";
        else out << " [ ";
        for (std::size_t c = 0; c < matrix.cols(); c++) {
            out << to_fraction(matrix(r, c)) << " ";
            /* out << matrix(r, c) << " "; */
        }
        if (r != matrix.rows() -1) out << "]\n";
    }
    out << "]]\n";
    return out;
}

