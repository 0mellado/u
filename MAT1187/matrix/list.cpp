#include <iostream>

class MyLinkedList {
private:
    class ListNode {
        public:
            int val;
            ListNode* next;
            ListNode* prev;

            ListNode(int _val) {
                val = _val;
                next = NULL;
                prev = NULL;
            }
    };
    int Gindex = -1;
    ListNode* head = NULL;
    ListNode* tail = NULL;
    
public:
    MyLinkedList() {
        head = new ListNode(-1);
        tail = new ListNode(-1);
        head->next = tail;
        tail->prev = head;
    }

    void printList() {
        ListNode* curr = head;
        while (curr) {
            printf("%d ", curr->val);
            curr = curr->next;
        }
        printf("\n");

    }
    
    int get(int index) {
        if (index > Gindex)
            return -1;
        int currIndex = 0;
        ListNode* curr = head->next;
        while (curr && curr->next && currIndex != index) {
            curr = curr->next;
            currIndex++;
        }
        
        return (curr->val);
    }

    void addAtHead(int val) {

        ListNode* temp = new ListNode(val);
        
        temp->next = head->next;
        temp->prev = head;
        temp->next->prev = temp;
        head->next = temp;
        
        Gindex++;
        // printf("addAtHead Stop\n");
        // printList();
    }
    
    void addAtTail(int val) {

        // printf("addAtTail Start\n");
        // printList();
        ListNode* temp = new ListNode(val);
        
        temp->prev = tail->prev;
        temp->next = tail;
        tail->prev->next = temp;
        tail->prev = temp;
        
        Gindex++;

        // printf("addAtTail Stop\n");
        // printList();
    }
    
    void addAtIndex(int index, int val) {

        // printf("addAtIndex Start\n");
        // printList();
        int len = Gindex+1;
        if (index > len)
            return;
        if (index == len)
            addAtTail(val);
        else if(index == 0)
            addAtHead(val);
        else {
            int currIndex = 0;
            ListNode* curr = head->next;
            while (curr && curr->next && currIndex != index) {
                curr = curr->next;
                currIndex++;
            }
            ListNode* temp = new ListNode(val);
            temp->next = curr;
            temp->prev = curr->prev;
            temp->prev->next = temp;
            curr->prev = temp;
            Gindex++;
        }
    }
    
    void deleteAtIndex(int index) {

        if (index > Gindex)
            return;
        
        int currIndex = 0;
        ListNode* curr = head->next;
        while(curr && curr->next && currIndex != index) {
            curr = curr->next;
            currIndex++;
        }
        
        curr->prev->next = curr->next;
        curr->next->prev = curr->prev;
        delete (curr);
        Gindex--;
    }
};


