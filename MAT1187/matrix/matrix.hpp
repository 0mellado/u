#pragma once
#include <memory>
#include <vector>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <string>

struct Matrix {

    template <typename TL>
    using initL = std::initializer_list<TL>;

    explicit Matrix(std::size_t const rows, std::size_t const cols);
    explicit Matrix(initL<initL<double>> const& l);

    [[nodiscard]] double& operator()(std::size_t const row, std::size_t const col);
    [[nodiscard]] double  operator()(std::size_t const row, std::size_t const col) const;

    [[nodiscard]] Matrix  operator*(Matrix const &right_matrix) const;
    [[nodiscard]] Matrix  operator*(double const &scalar) const;
    [[nodiscard]] Matrix  operator+(Matrix const &matrix) const;
    [[nodiscard]] Matrix  operator-(Matrix const &matrix) const;


    [[nodiscard]] Matrix T() const;
    [[nodiscard]] double det() const;
    [[nodiscard]] Matrix minor(std::size_t i, std::size_t j) const;
    [[nodiscard]] Matrix cof() const;
    [[nodiscard]] double cof(std::size_t i, std::size_t j) const;
    [[nodiscard]] Matrix inv() const;
    /* [[nodiscard]] std::size_t rang() const; */

    [[nodiscard]] Matrix copy() const;

    void F_mult_row(std::size_t const row, double const scalar);
    void F_swap_rows(std::size_t const row0, std::size_t const row1);
    void F_sum_rows(std::size_t const row0, std::size_t const row1, double const scalar);

    [[nodiscard]] Matrix be_sqrt() const;

    friend std::ostream& operator<<(std::ostream& out, Matrix const& matrix);
    friend std::string to_fraction(double number, int cycles, double precision);


    [[nodiscard]] auto cols() const noexcept { return cols_; }
    [[nodiscard]] auto rows() const noexcept { return rows_; }

    bool is_sqrt() const noexcept { return cols_ == rows_; }
    std::size_t rows_not_null() const noexcept;
    bool is_row_null(std::size_t const row) const noexcept;

    private:
        std::size_t const rows_, cols_;
        std::unique_ptr<double[]> data_{std::make_unique<double[]>(rows_*cols_)};
};

