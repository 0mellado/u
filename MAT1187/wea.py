#!/usr/bin/python3

import numpy as np

def matrix_cofactor(matrix):
    try:
        determinant = np.linalg.det(matrix)
        if(determinant!=0):
            cofactor = None
            cofactor = np.linalg.inv(matrix).T * determinant
            return cofactor
        else:
            raise Exception("singular matrix")
    except Exception as e:
        print("could not find cofactor matrix due to",e)


a = np.array([[-1, 3, 7],
              [-4, 11, 2],
              [-3, 1, 6]])

b = np.array([[1, 2, 3],
              [2, 5, 7],
              [-2, -4, -5]])

# x = np.array([[-96, 29, 39],
#               [190, -82, -58],
#               [-108, 51, 33]])

c = np.array([[3, -2, 3],
              [1, 0, 2],
              [-3, 1, 3]])


sus = np.subtract(a, c.T)
print("A - C.T:")
print(sus)

actc = np.matmul(sus, c)
print("\n(A - C.T) * C:")
print(actc)

x = np.matmul(np.linalg.inv(b), np.matmul(np.subtract(a, c.T), c))
print("\nB(-1) * ((A - C.t) * C):")
print(x)
# print(np.linalg.inv(b))

print("\nme deberia dar:")
print(a)
ap = (np.matmul(b, np.matmul(x, np.linalg.inv(c))) + c.T)

print("\nme da:")
print(ap)


# a = np.array([[1, 1, 1],
#               [1, 0, -0.5],
#               [-0.5, 1, 0]])

# print(np.linalg.det(a))
# x = np.array([[35000, 1, 1],
#               [0, 0, -0.5],
#               [0, 1, 0]])

# x = np.array([[35000, 1, 1],
#               [0, 0, -0.5],
#               [0, 1, 0]])

# y = np.array([[1, 35000, 1],
#               [1, 0, -0.5],
#               [-0.5, 0, 0]])

# z = np.array([[1, 1, 35000],
#               [1, 0, 0],
#               [-0.5, 1, 0]])

# print(np.round(np.linalg.det(x) / np.linalg.det(a)))
# print()
# print(np.round(np.linalg.det(y) / np.linalg.det(a)))
# print()
# print(np.round(np.linalg.det(z) / np.linalg.det(a)))

# print(np.round(np.linalg.det(x)))
# print()
# print(np.round(np.linalg.det(y)))
# print()
# print(np.round(np.linalg.det(z)))

# wea = np.array([[1, 0, -0.5],
#                 [-0.5, 1, 0],
#                 [0, -4, 1]])

# print(np.linalg.det(wea))


