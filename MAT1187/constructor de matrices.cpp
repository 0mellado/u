#include <iostream>
#include <stdio.h>

using namespace std;

const int f1 = 4;
const int c1 = 4;

const int f2 = 4;
const int c2 = 4;

void printMatrix(double arr[f1][c1]) {
    for (int i = 0; i < f1; i++) {
        for (int j = 0; j < c1; j++) {
            printf("%4f", arr[i][j]);
        }
        cout << endl;
    }
    cout << endl;
    return;
}

int makeMatrix1(int i, int j) {
    return i*3 - j*2;
}

int makeMatrix2(int i, int j) {
    return i - j*2;
}

void multMatrix(double arr1[f1][c1], double arr2[f2][c2]) {

    printMatrix(arr1);
    printMatrix(arr2);

    for (int i = 0; i < f1; i++) {
        for (int j = 0; j < c2; j++) {
            double product = 0;
            for (int z = 0; z < c1; z++) {
                product += arr1[i][z] * arr2[z][i];
            }
            printf("%16lf", product);
        }
        cout << endl;
    }

    return;
}

int main(int argc, char **argv) {

    double matrix1[f1][c1] = {
        {0, 1, 2, -1},
        {1, 0, -1, 3},
        {2, -1, 0, 1},
        {1, 0, -1, 2}
    };
    double matrix2[f2][c2] = {
        {(1/4), (-7/4), (1/4) , (9/4)},
        {(1/2), 2     , (-1/2), 0},
        {(1/4), (1/4) , (1/4) , (-3/4)},
        {0    , 1     , 0     , -1}
    };

    /* for (int i = 0; i < f1; i++) */
    /*     for (int j = 0; j < c1; j++) */
    /*         matrix1[i][j] = makeMatrix1(i + 1, j + 1); */

    /* for (int i = 0; i < f2; i++) */
    /*     for (int j = 0; j < c2; j++) */
    /*         matrix2[i][j] = makeMatrix2(i + 1, j + 1); */

    multMatrix(matrix1, matrix2);


    return 0;
}
