import matplotlib.pyplot as plt
import numpy as np
import math
par1 = (1986, 11.7827)
par2 = (1997, 12.9708)
m = (par2[1] - par1[1]) / (par2[0] - par1[0])
a = par1[1] - m * par1[0]
def func(x) -> list:
    y = []
    for i in x:
        y.append(m*i + a)

    return y

def func1(x) -> list:
    y = []
    for i in x:
        y.append((math.e ** (a)) * (math.e ** (m * i)))

    return y


nlineas = [
    128179, 130971, 132718,
    138222, 158840, 190218,
    202209, 214409, 231090,
    245094, 286352, 358035,
    429712, 533408
]

lnlines = []
for i in nlineas:
    lnlines.append(np.log(i))

agnos = [
    1985, 1986, 1987, 1988,
    1989, 1990, 1991, 1992,
    1993, 1994, 1995, 1996,
    1997, 1998, 1999, 2000,
    2001, 2002, 2003, 2005,
    2005
]

wea = func1(agnos)

for i in range(len(agnos)):
    print(agnos[i], wea[i])

plt.style.use('_mpl-gallery')

# plt.plot(agnos, nlineas)
# plt.plot(agnos, lnlines)
# plt.plot(agnos, func(agnos))

plt.plot(agnos, func1(agnos))

# plt.show()
