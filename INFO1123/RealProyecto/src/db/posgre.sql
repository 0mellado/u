CREATE TABLE IF NOT EXISTS products (
    id_prod INTEGER NOT NULL,
    name_prod VARCHAR(32) NOT NULL,
    type_prod VARCHAR(12) NOT NULL,
    type_weight VARCHAR(12) NOT NULL,
    weight BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (id_prod)
);


CREATE TABLE IF NOT EXISTS vehicles (
    id_vehicle INTEGER NOT NULL,
    name_vehicle VARCHAR(10) NOT NULL,
    price_vehicle INTEGER NOT NULL,
    capacity_vehicle INTEGER NOT NULL,
    PRIMARY KEY (id_vehicle)
);


CREATE TABLE IF NOT EXISTS containers (
    id_cont INTEGER NOT NULL,
    size_cont VARCHAR(10) NOT NULL,
    weight_cont VARCHAR(10) NOT NULL,
    type_cont VARCHAR(12) NOT NULL,
    capacity_cont INTEGER NOT NULL,
    total_weight INTEGER NOT NULL,
    id_vehicle INTEGER NOT NULL,
    FOREIGN KEY (id_vehicle) REFERENCES vehicles (id_vehicle),
    PRIMARY KEY (id_cont)
);

CREATE TABLE IF NOT EXISTS conts_prodcts (
    id_merge INTEGER NOT NULL AUTO_INCREMENT,
    id_cont INTEGER NOT NULL,
    id_prod INTEGER NOT NULL,
    prod_weight INTEGER NOT NULL,
    FOREIGN KEY (id_cont) REFERENCES containers (id_cont),
    FOREIGN KEY (id_prod) REFERENCES products (id_prod),
    PRIMARY KEY (id_merge)
);

