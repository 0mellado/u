#!/usr/bin/perl

use strict;
use threads;
use threads::shared;

use Class::Struct;
use DBI;
use FindBin qw($Bin);
use lib "$Bin/lib";
use Solid;
use Liquid;
use Vehicle;
use Env;
use Dotenv;

Dotenv->load;

sub dbConnect {
    return DBI->connect(
        "DBI:mysql:dbname=$ENV{CONN_DB};host=$ENV{CONN_HOST}",
        $ENV{CONN_USER},
        $ENV{CONN_PASS}
    ) or die "Cannot connect to MySQL server\n";
}


struct Product => {
    id => '$',
    name => '$',
    type => '$',
    weightType => '$',
    weight => '$'
};

struct ProdCont => {
    id => '$',
    weight => '$'
};

sub readData {
    my $conn = shift;
    my $typeProd = shift;
    my $typeWeight = shift;

    my $result;
    if ($typeWeight eq "liquida") {
        $result = $conn->prepare(
            "SELECT * FROM products
            WHERE type_prod = '$typeProd' AND (type_weight = '$typeWeight' OR type_weight = 'gas');"
        ) or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
    } else {
        $result = $conn->prepare(
            "SELECT * FROM products
            WHERE type_prod = '$typeProd' AND type_weight = '$typeWeight';"
        ) or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
    }

    $result->execute() or die "fallo la consulta\n";

    my @products;

    while (my $row = $result->fetchrow_hashref()) {
        my $tmpProd = Product->new();
        $tmpProd->id($row->{'id_prod'});
        $tmpProd->name($row->{'name_prod'});
        $tmpProd->type($row->{'type_prod'});
        $tmpProd->weightType($row->{'type_weight'});
        $tmpProd->weight($row->{'weight'});

        push @products, $tmpProd;
    }
    $result->finish;
    return \@products;
}


sub getProd {
    my @prods = @{$_[0]};
    my $type = $_[1];
    my $weightType = $_[2];
    my $weightType2 = $_[3];
    my @contProds;

    for my $prod (@prods) {
        if ($prod->type eq $type && 
            ($prod->weightType eq $weightType ||
            $prod->weightType eq $weightType2)) {
            push @contProds, $prod;
        }
    }
    return @contProds;
}


sub weightsConts {
    my @conts = @{$_[0]};
    my $totalW = 0;
    for my $cont (@conts) {
        for my $prod (@{$cont->{products}}) {
            $totalW = $totalW + $prod->weight;
        }
    }
    return $totalW;
}


sub cantsConts {
    my @conts = @{$_[0]};
    my $cantTotal = 0;
    for my $cont (@conts) {
        if ($cont->{size} eq "grande") {
            $cantTotal++;
        } else {
            $cantTotal = $cantTotal + 0.5;
        }
    }
    if ($cantTotal % 1 == 0) {
        return int $cantTotal;
    }
    return ((int $cantTotal) + 1);
}

my $conn = dbConnect;


my @solidN = @{readData($conn, "normal", "solida")};
my @liquiN = @{readData($conn, "normal", "liquida")};
my @solidR = @{readData($conn, "refrigerado", "solida")};
my @liquiR = @{readData($conn, "refrigerado", "liquida")};
my @solidI = @{readData($conn, "inflamable", "solida")};
my @liquiI = @{readData($conn, "inflamable", "liquida")};

my @prodConts = (\@solidN, \@liquiN, \@solidR, \@liquiR, \@solidI, \@liquiI);
my $idCont :shared = 0;

sub createClassConts {
    my $id = shift;
    my $class = shift;
    my $capacity = shift;

    if ($class == 0) { 
        my $tmp = new Solid($id, $capacity);
        $tmp->solid();
        return $tmp;
    } else { 
        my $tmp = new Liquid($id, $capacity);
        $tmp->liquid(shift);
        return $tmp;
    }
}


sub auxAddProd {
    my $tmp = shift;
    my $prod = shift;
    my $totalWeight = shift;
    my $tmpProd = ProdCont->new();
    $tmpProd->id($prod->id);
    $tmpProd->weight($totalWeight);
    $tmp->addProd($tmpProd);
    return $tmp;
}


sub addProducts {
    my $capacity = shift;
    my $totalWeight = shift;
    my $prod = shift;
    my $class = shift;
    my $flag = shift;
    my $tmp2 = shift;
    {
        lock($idCont);
        $idCont++;

        if ($flag) {
            return auxAddProd($tmp2, $prod, $totalWeight);
        }
        my $tmp = createClassConts($idCont, $class, $capacity, $tmp2);
        return auxAddProd($tmp, $prod, $totalWeight);
    }
}

sub divProdsNormal {
    my $capacity = shift;
    my @prods = @{$_[0]};
    my @prodsCont;
    my $prodRest;

    my $lastProdId = $prods[-1]->id;

    my @prodsMinors;

    my $diffW = 0;
    for my $prod (@prods) {
        if ($prod->weight < $capacity) {
            push @prodsMinors, $prod;
            next;
        } 
        if ($prodRest) {
            my $tmp = addProducts($capacity, $prodRest->weight, $prodRest, 0);
            $diffW = $capacity - $prodRest->weight;
            $tmp = addProducts($capacity, $diffW, $prod, 0, 1, $tmp);
            push @prodsCont, $tmp;
        }

        my $cantContProd = int(($prod->weight - $diffW) / $capacity);
        my $modContProd = ($prod->weight - $diffW) % $capacity;

        for (my $i = 0; $i < $cantContProd; $i++) {
            my $tmp  = addProducts($capacity, $capacity, $prod, 0);
            push @prodsCont, $tmp;
        }

        if ($lastProdId == $prod->id) {
            if ($modContProd < $capacity/2 && @prodsMinors == 0) {
                my $tmp = addProducts($capacity/2, $modContProd, $prod, 0);
                push @prodsCont, $tmp;
            } elsif ($modContProd < $capacity) {
                my $tmp = addProducts($capacity, $modContProd, $prod, 0);
                push @prodsCont, $tmp;
                $diffW = $capacity - $modContProd;
            }
        }

        if ($modContProd != 0) { 
            $prodRest = $prod; 
            $prodRest->weight($modContProd);
        }
    }


    if (!$prodsMinors[-1]) { return \@prodsCont; }

    $prodRest = 0;

    $lastProdId = $prodsMinors[-1]->id;

    for my $prodMinor (@prodsMinors) {
        if ($prodRest != 0) {
            my $tmp = addProducts($capacity, $prodRest->weight, $prodRest, 0);
            $diffW = $capacity - $prodRest->weight;
            if ($diffW < $prodMinor->weight) {
                $prodMinor->weight($prodMinor->weight - $diffW);
                $tmp = addProducts($capacity, $diffW, $prodMinor, 0, 1, $tmp);
                $diffW = 0;
                push @prodsCont, $tmp;
            } else {
                $tmp = addProducts($capacity, $prodMinor->weight, $prodMinor, 0, 1, $tmp);
                $diffW = $capacity - ($prodMinor->weight + $prodRest->weight);
                push @prodsCont, $tmp;
                $prodRest = 0;
                next;
            }
        }


        if ($lastProdId == $prodMinor->id) {
            if ($prodMinor->weight < $capacity/2) {
                my $tmp = addProducts($capacity/2, $prodMinor->weight, $prodMinor, 0);
                push @prodsCont, $tmp;
                next;
            } elsif ($prodMinor->weight < $capacity) {
                my $tmp = addProducts($capacity, $prodMinor->weight, $prodMinor, 0);
                push @prodsCont, $tmp;
                next;
            }
        }

        if ($diffW != 0) {
            my $tmp = pop @prodsCont;
            $tmp = addProducts($capacity, $diffW, $prodMinor, 0, 1, $tmp);
            push @prodsCont, $tmp;
            $prodRest = $prodMinor;
            $prodRest->weight($prodMinor->weight - $diffW);
        } else {
            my $tmp = addProducts($capacity, $prodMinor->weight, $prodMinor);
            push @prodsCont, $tmp;
            $diffW = $capacity - $prodMinor->weight;
        }
    }
    return \@prodsCont;
}

sub divProdsTank {
    my $capacity = shift;
    my @prods = @{$_[0]};
    my @prodsCont;

    for my $prod (@prods) {
        if ($prod->weight < $capacity/2) {
            my $tmp  = addProducts($capacity/2, $prod->weight, $prod, 1, 0, $prod->weightType);
            push @prodsCont, $tmp;
            next;
        } elsif ($prod->weight < $capacity) {
            my $tmp  = addProducts($capacity, $prod->weight, $prod, 1, 0, $prod->weightType);
            push @prodsCont, $tmp;
            next;
        }

        my $cantContProd = int($prod->weight / $capacity);
        my $modContProd = $prod->weight % $capacity;

        for (my $i = 0; $i < $cantContProd; $i++) {
            my $tmp  = addProducts($capacity, $capacity, $prod, 1, 0, $prod->weightType);
            push @prodsCont, $tmp;
        }

        if ($modContProd < $capacity/2) {
            my $tmp  = addProducts($capacity/2, $modContProd, $prod, 1, 0, $prod->weightType);
            push @prodsCont, $tmp;
        } else {
            my $tmp  = addProducts($capacity, $modContProd, $prod, 1, 0, $prod->weightType);
            push @prodsCont, $tmp;
        }
    }
    return \@prodsCont;
}

my $thrSolidN = threads->create(\&divProdsNormal, 24000, \@solidN);
my $thrSolidR = threads->create(\&divProdsNormal, 20000, \@solidR);
my $thrSolidI = threads->create(\&divProdsNormal, 22000, \@solidI);
my $thrLiquiN = threads->create(\&divProdsTank, 24000, \@liquiN);
my $thrLiquiR = threads->create(\&divProdsTank, 20000, \@liquiR);
my $thrLiquiI = threads->create(\&divProdsTank, 22000, \@liquiI);

my $contsSolidN = $thrSolidN->join();
my $contsSolidR = $thrSolidR->join();
my $contsSolidI = $thrSolidI->join();
my $contsLiquiN = $thrLiquiN->join();
my $contsLiquiR = $thrLiquiR->join();
my $contsLiquiI = $thrLiquiI->join();

# my $contsSolidN = divProdsNormal(24000, \@solidN);
# my $contsSolidR = divProdsNormal(20000, \@solidR);
# my $contsSolidI = divProdsNormal(22000, \@solidI);
# my $contsLiquiN = divProdsTank(24000, \@liquiN);
# my $contsLiquiR = divProdsTank(20000, \@liquiR);
# my $contsLiquiI = divProdsTank(22000, \@liquiI);

my @conts = ($contsSolidN, $contsSolidR, $contsSolidI,
             $contsLiquiN, $contsLiquiR, $contsLiquiI);


sub cantConts {
    my @arr = @{$_[0]};
    my $cantG = 0;
    my $cantP = 0;
    for my $typeCont (@arr) {
        for my $cont (@{$typeCont}) {
            if ($cont->{size} eq "grande") {
                $cantG++;
                next;
            }
            $cantP++;
        }
    }

    if ($cantP % 2 == 0) {
        return ($cantG + ($cantP / 2));
    }

    return ($cantG + (int($cantP / 2) + 1));
}

my $totalCantConts = cantConts(\@conts);
# print $totalCantConts."\n";

sub sumArr {
    my @arr = @{$_[0]};
    my $sum = 0;
    for my $i (@arr) {
        $sum = $sum + $i;
    }
    return $sum;
}

my @weights;
my @cantsConts;
my @totalConts;
for my $typeCont (@conts) {
    for my $cont (@{$typeCont}) {
        $cont->sumWeight();
        push @totalConts, $cont;
    }
    my $cantCont = cantsConts($typeCont);
    push @cantsConts, $cantCont;
    my $weight = weightsConts($typeCont);
    push @weights, $weight;
}

my $totalWeight = sumArr(\@weights);

my $truck = $ARGV[0];
my $train = $ARGV[1];
my $plane = $ARGV[2];
my $freighter = $ARGV[3];

my @vehicles = (
    [1, $truck],
    [250, $train],
    [10, $plane],
    [24000, $freighter]
);

my @rents;
for my $vehi (@vehicles) {
    my @vehi = @{$vehi};
    my $rent = $vehi[1] / $vehi[0];
    push @rents, $rent;
}


for (my $i = 0; $i < 4; $i++) {
    for (my $j = 1; $j < 4 - $i; $j++) {
        if ($rents[$j] < $rents[$j-1]) {
            my $vehi = $vehicles[$j];
            $vehicles[$j] = $vehicles[$j-1];
            $vehicles[$j-1] = $vehi;
            my $rent = $rents[$j];
            $rents[$j] = $rents[$j-1];
            $rents[$j-1] = $rent;
        }
    }
}


my @capacityV;
# print "rentabilidad de los vehiculos\n";
for (my $i = 0; $i < 4; $i++) {
    # print ${$vehicles[$i]}[0]." ".$rents[$i]."\n";
    push @capacityV, ${$vehicles[$i]}[0];
}


sub cantVehicles {
    my $cantCont = shift;
    my @vehicl = @{$_[0]};
    my @cantsV;

    my $vehicle = shift @vehicl;
    my $nextVehicle = shift @{$vehicle};
    my $priceVehicle = shift @{$vehicle};
    my $vehiCont = int $cantCont / $nextVehicle;
    push @cantsV, [$vehiCont, $nextVehicle, $priceVehicle];

    while (@vehicl > 0) {
        $cantCont = $cantCont % $nextVehicle;
        $vehicle = shift @vehicl;
        $nextVehicle = shift @{$vehicle};
        $priceVehicle = shift @{$vehicle};

        while ($cantCont < $nextVehicle && @vehicl > 0) {
            $vehicle = shift @vehicl;
            $nextVehicle = shift @{$vehicle};
            $priceVehicle = shift @{$vehicle};
        }

        if ($vehiCont == 0) {
            return \@cantsV;
        }

        $vehiCont = int $cantCont / $nextVehicle;
        push @cantsV, [$vehiCont, $nextVehicle, $priceVehicle];
    }

    return \@cantsV;
}

my @cantsVehicles = @{cantVehicles($totalCantConts, \@vehicles)};

my @totalVechicles;
my $idVechicle = 0;

for my $cant (@cantsVehicles) {
    while (0 < ${$cant}[0]) {
        my $tmpVechi = new Vehicle($idVechicle, ${$cant}[2], ${$cant}[1]);
        push @totalVechicles, $tmpVechi;
        $idVechicle++;
        ${$cant}[0]--;
    }
}


my @tmpContSmall;
sub addContsVehicle {
    my $capacity = shift;
    my $vehicle = shift;
    my $cant = 0;
    while ($cant != $capacity) {
        my $tmpCont = shift @totalConts;

        if ($tmpCont->{size} eq "grande") {
            $vehicle->addCont($tmpCont);
            $cant++;
        } else {
            push @tmpContSmall, $tmpCont;
        }

        if (@tmpContSmall == 2) {
            $vehicle->addCont(shift @tmpContSmall);
            $vehicle->addCont(shift @tmpContSmall);
            $cant++;
        } 
    }
}


for my $vehicle (@totalVechicles) {
    addContsVehicle($vehicle->{capacity}, $vehicle);
    if (!${$vehicle->{conts}}[-1]->{size}) {
        pop @{$vehicle->{conts}};
    }
}


my $totalCantVehicles = scalar @totalVechicles;

my @names = (
    [1, "camiones"],
    [10, "aviones"],
    [250, "trenes"],
    [24000, "barcos"]
);

my @cantsTypeVehicle;

for my $vehiCants (@cantsVehicles) {
    for my $name (@names) {
        if (${$name}[0] == ${$vehiCants}[1]) {
            push @cantsTypeVehicle, [${$name}[1], ${$vehiCants}[0], ${$vehiCants}[0] * ${$vehiCants}[2]];
        }
    }
}

my $queryVehicle = 
"INSERT INTO vehicles (id_vehicle,name_vehicle,price_vehicle,capacity_vehicle)VALUES";

my $queryConts = 
"INSERT INTO containers (id_cont,size_cont,weight_cont,type_cont,capacity_cont,total_weight,id_vehicle)VALUES";

my $queryContsProds =
"INSERT INTO conts_prodcts (id_cont,id_prod,prod_weight)VALUES";

for my $vehicle (@totalVechicles) {
    $queryVehicle .= "\n($vehicle->{id},'$vehicle->{name}',$vehicle->{price},$vehicle->{capacity}),";
    for my $cont (@{$vehicle->{conts}}) {
        $queryConts .=
        "\n($cont->{id},'$cont->{size}','$cont->{weightType}','$cont->{type}',$cont->{capacity},$cont->{totalW},$vehicle->{id}),";
        for my $prod (@{$cont->{products}}) {
            $queryContsProds .= "\n($cont->{id},".$prod->id.",".$prod->weight."),";
        }
    }
}

chop $queryConts;
chop $queryVehicle;
chop $queryContsProds;

$queryConts .= ";";
$queryVehicle .= ";";
$queryContsProds .= ";";

# print $queryContsProds."\n";
# print $queryVehicle."\n";
# print $queryConts."\n";


my $resultVehicle = $conn->prepare("DELETE FROM conts_prodcts;") or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Eliminando datos anteriores relaciones contenedores/productos...\n";
$resultVehicle->execute() or die "fallo la consulta\n";
$resultVehicle->finish;

my $resultVehicle = $conn->prepare("DELETE FROM containers;") or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Eliminando datos anteriores contenedores...\n";
$resultVehicle->execute() or die "fallo la consulta\n";
$resultVehicle->finish;

my $resultVehicle = $conn->prepare("DELETE FROM vehicles;") or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Eliminando datos anteriores vehiculos...\n";
$resultVehicle->execute() or die "fallo la consulta\n";
$resultVehicle->finish;

my $resultVehicle = $conn->prepare($queryVehicle) or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Insertando los vehiculos...\n";
$resultVehicle->execute() or die "fallo la consulta\n";
$resultVehicle->finish;

my $resultConts = $conn->prepare($queryConts) or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Insertando los contenedores...\n";
$resultConts->execute() or die "fallo la consulta\n";
$resultConts->finish;

my $resultConts = $conn->prepare($queryContsProds) or die 'fallo la preparación de la consulta '. $conn->errstr()."\n";
print "Insertando los la relación entre los contenedores y los productos...\n";
$resultConts->execute() or die "fallo la consulta\n";
$resultConts->finish;
