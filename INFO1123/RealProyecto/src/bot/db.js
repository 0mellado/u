const mysql = require('mysql')

const conectDb = () => {
    const connection = mysql.createConnection({
        host: process.env.CONN_HOST,
        user: process.env.CONN_USER,
        password: process.env.CONN_PASS,
        database: process.env.CONN_DB
    })

    connection.connect(err => {
        if (err) throw err
        console.log('conectado ...')
    })
    return connection
}


module.exports = conectDb
