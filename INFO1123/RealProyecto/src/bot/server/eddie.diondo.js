const {Server} = require('socket.io')
const {parse} = require('csv-parse')
const fs = require('node:fs')
const conectDb = require('../db')
require('dotenv').config()

const io = new Server(4000)

const readCsv = async (conection, socket) => {
    let products = []

    fs.createReadStream(`${__dirname}/../../../data/data.csv`)
        .pipe(parse({delimiter: ",", from_line: 2}))
        .on("data", (row) => {
            products.push(row)
        })
        .on('end', () => {
            conection.query('DELETE FROM conts_prodcts;', async (err) => {
                if (err) throw err
            })

            conection.query('DELETE FROM products;', async (err) => {
                if (err) throw err
            })

            let insertData =
                'INSERT INTO products (id_prod, name_prod, type_prod, type_weight, weight) VALUES '

            products.forEach(row => {
                insertData += `(${row[0]},'${row[1]}','${row[2]}','${row[3]}',${row[4]}),`
            })

            insertData = insertData.slice(0, -1) + ';'

            conection.query(insertData, async (err, values) => {
                if (err) {
                    // message.reply('No se pudieron ingresar los datos')
                    console.log(err)
                    throw err
                }
                console.log(values)
                socket.emit('finish', values.affectedRows)
            })
        })
}


io.on('connection', socket => {
    socket.on('data', async () => {
        const conn = conectDb()
        console.log("alo")
        await readCsv(conn, socket)
    });
});

