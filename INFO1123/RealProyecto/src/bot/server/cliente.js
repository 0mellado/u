const {io} = require('socket.io-client')

const socket = io('ws://localhost:4000')

socket.on('hello', arg => {
    console.log(arg)
})

socket.emit('howdy', 'stranger')
