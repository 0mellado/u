const {Server} = require('socket.io')

const io = new Server(4000);

io.on('connection', socket => {
    socket.emit('hello', 'world')

    socket.on('howdy', arg => {
        console.log(arg)
    })
})
