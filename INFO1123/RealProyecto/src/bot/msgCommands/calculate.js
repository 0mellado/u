const {spawn} = require('node:child_process')
const conectDb = require('../db')


module.exports = {
    name: 'calculate',
    async execute(message, args) {
        await message.reply("\`\`\`haciendo los calculos...\`\`\`")

        const truck = args[1] > 0 ? args[1] : '500000'
        const train = args[2] > 0 ? args[2] : '10000000'
        const plane = args[3] > 0 ? args[3] : '1000000'
        const freighter = args[4] > 0 ? args[4] : '1000000000'

        const calcProcess = spawn('perl', ['../main.pl', truck, train, plane, freighter])


        calcProcess.stdout.on('data', async () => {
            const query = "SELECT name_vehicle, SUM(price_vehicle) AS price FROM vehicles GROUP BY name_vehicle;"
            const connection = conectDb()
            connection.query(query, async (err, result) => {
                if (err) throw err
                let data = ""
                let totalP = 0

                result.map(value => {
                    totalP += value.price
                    data += `vehiculo: ${value.name_vehicle}, precio total por vehiculo: $ ${value.price}\n`
                })
                data += `Precio total: $ ${totalP}`
                await message.reply(`\`\`\`${data}\`\`\``)
            })
        })
        calcProcess.stderr.on('data', async () => {
            await message.reply(`\`\`\`Ocurrio algun error en la ejecución del programa\`\`\``)
        })
    }
}
