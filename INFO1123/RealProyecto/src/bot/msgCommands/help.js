
module.exports = {
    name: 'help',
    async execute(message) {
        await message.reply('```Uso de los comandos:\n-upload <archivo> : Sube el archivo .csv para poder calcular el.\n\n-calculate <precio-camión> <precio-tren> <precio-avión> <precio-barco> : Las opciones de precio son para asignar unos distintos de los por defecto.\n\n-getdata <id-del-vehiculo> : Mostrará todos los productos dentro de un vehiculo, junto con su infomación respectiva.```')
    }
}
