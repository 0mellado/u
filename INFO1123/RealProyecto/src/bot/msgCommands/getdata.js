const conectDb = require('../db')
require('dotenv').config()

const getQueryNames = id => {
    return (`SELECT id_prod,name_prod,type_prod,type_weight FROM products ` +
        `WHERE id_prod = ANY (` +
        `SELECT DISTINCT id_prod FROM containers ` +
        `INNER JOIN conts_prodcts ` +
        `ON containers.id_cont = conts_prodcts.id_cont ` +
        `WHERE containers.id_vehicle = ${id}` +
        `) LIMIT 10;`)
}

const getQueryWeight = (id) => {
    return (`SELECT id_prod, SUM(prod_weight) AS weight FROM containers ` +
        `INNER JOIN conts_prodcts ` +
        `ON containers.id_cont = conts_prodcts.id_cont ` +
        `WHERE id_vehicle = ${id} GROUP BY id_prod LIMIT 10;`
    )
}


const sendQuerys = (conn, query, fn = null) => {
    conn.query(query, async (err, result) => {
        if (err) throw err
        if (!fn) return
        fn(result)
    })
}


module.exports = {
    name: 'getdata',
    async execute(message, args) {
        if (args.length > 2) {
            await message.reply('```Uso: -getdata <id-del-vehiculo>```')
            return
        }

        await message.reply('```obteniendo datos... (masa en toneladas)```')

        const connection = conectDb();
        const namesQuery = getQueryNames(args[1] > 0 ? args[1] : 0)
        const weightsQuery = getQueryWeight(args[1] > 0 ? args[1] : 0)

        sendQuerys(connection, namesQuery, async (result) => {
            let prod = ""
            result.map(value => {
                prod += `id: ${value.id_prod}, nombre: ${value.name_prod}, tipo de producto: ${value.type_prod}, tipo de masa: ${value.type_weight}\n`
            })
            await message.reply(`\`\`\`${prod}\`\`\``)
        })

        sendQuerys(connection, weightsQuery, async (result) => {
            let prodW = ""
            let totalW = 0
            result.map(value => {
                const weight = value.weight / 1000
                totalW += weight
                prodW += `id: ${value.id_prod}, masa total del producto: ${weight}\n`
            })

            await message.reply(`\`\`\`${prodW}\`\`\``)
        })

    }
}
