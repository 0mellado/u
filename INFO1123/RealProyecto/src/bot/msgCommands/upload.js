const fs = require('node:fs')
const https = require('node:https')
const {io} = require('socket.io-client')
require('dotenv').config()

const socket = io('ws://localhost:4000')

module.exports = {
    name: 'upload',
    async execute(message) {
        let url;
        if (message.attachments.size == 0) {
            message.reply('El comando debe tener adjuntado un archivo .csv');
            return;
        }

        if (message.attachments.size > 1) {
            message.reply('Solo puedes subir un archivo');
            return;
        }

        let status = true;

        await message.attachments.map(async file => {
            if (file.contentType !== 'text/csv; charset=utf-8') {
                status = false;
                return;
            }
            url = file.attachment;
        });

        if (!status) {
            await message.reply('El archivo debe ser un .csv');
            return;
        }

        https.get(url, res => {
            const path = `${__dirname}/../../../data/data.csv`;
            const filePath = fs.createWriteStream(path);

            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
                socket.emit('data')
                socket.on('finish', async data => {
                    await message.reply(
                        `\`\`\`Se recibió correctamente el archivo .csv.\nSe editaron ${data} lineas\`\`\``);
                })
            });
        });
    }
}
