package Vehicle;

use strict;
use warnings;
use threads;
use threads::shared;

sub new {
    my $class = shift;
    my $self = {
        id => shift,
        name => "",
        price => shift,
        capacity => shift,
        conts => [ @_ ],
        tonProd => 0,
        tonTypeW => 0,
        tonW => 0
    };

    my @names = ("camion", "tren", "avion", "barco");
    my @capacitys = (1, 250, 10, 24000);

    my $i = 0;
    while ($i < 4) {
        if ($self->{capacity} == $capacitys[$i]) {
            $self->{name} = $names[$i];
        }
        $i++;
    }

    bless $self, $class;
    return $self;
}


sub addCont {
    my $self = shift;

    my $cont = shift;
    my @conts = @{$self->{conts}};
    push @conts, $cont;
    $self->{conts} = \@conts;
    return $self->{conts};
}


sub listConts {
    my $self = shift;
    my @conts = @{$self->{conts}};

    for my $cont (@conts) {
        print "Contenedor: ".$cont->{id}."\n";
    }
    return;
}


1;
