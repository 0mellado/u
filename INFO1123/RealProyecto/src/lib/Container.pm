package Container;

use strict;
use warnings;


sub new {
    my $class = shift;
    my $self = {
        id => shift,
        size => "grande",
        type => "",
        weightType => "",
        capacity => shift,
        products => [ @_ ],
        totalW => 0,
    };

    my @weights = (24000, 22000, 20000);
    my @types = ("normal", "inflamable", "refrigerado");
    my $i = 0;

    while ($i < 3) {
        if ($self->{capacity} == $weights[$i] || 
            $self->{capacity} == $weights[$i]/2) {
            $self->{type} = $types[$i];
        }

        if ($self->{capacity} == $weights[$i]/2) {
            $self->{size} = "pequeno";
        }

        $i++;
    }

    bless $self, $class;
    return $self;
}


sub sumWeight {
    my $self = shift;
    my @prods = @{$self->{products}};
    for my $prod (@prods) {
        $self->{totalW} = $self->{totalW} + $prod->weight;
    }
    return $self->{totalW};
}


sub addProd {
    my $self = shift;
    my $prod = shift;
    my @prods = @{$self->{products}};
    push @prods, $prod;
    $self->{products} = \@prods;
    return $self->{products};
}


1;
