package Solid;

use parent 'Container';

use strict;
use warnings;


sub solid {
    my $self = shift;
    $self->{weightType} = "solida";
    return $self->{weightType};
}


1;
