#include <iostream>

#ifndef FRUTAS_H
#define FRUTAS_H

using namespace std;







class Alimentos {
    protected:
        string nombre;
        string tipo;
        int masa;
        int calorias;

    public:
        Alimentos(string n, int m, int ca) {
            if (m < 50 && m > 0)
                tipo = "pequeño";
            else if (m >= 50 && m < 500)
                tipo = "mediano";
            else 
                tipo = "grande";

            nombre = n;
            masa = m;
            calorias = ca;
        }

        virtual void getAlimento() {
            cout << "Nombre del alimento: " << nombre << endl;
            cout << "Tipo de alimento: " << tipo << endl;
            cout << "masa: " << masa << " gr"<< endl;
            cout << "calorias: " << calorias << endl;
        }
};





#endif
