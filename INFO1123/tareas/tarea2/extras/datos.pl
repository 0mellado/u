#!/usr/bin/perl -w

use strict;

sub readData {
    my $file = shift;
    open FILE, "./$file"
        or die "No se encontro el archivo o directorio\n";

    my @data;
    while (<FILE>) {
        chomp $_;
        push @data, $_;
    }

    close FILE;

    return @data;
}

sub randNums {
    my @nums;
    my $min = shift;
    my $range = pop;
    my $i = 0;
    while ($i < 100) {
        push @nums, $min + int rand $range;
        $i++;
    }
    return @nums;
}

my @nombres = readData("nombres.txt");
my @paises = readData("paises.txt");
my @apellidos = readData("apellidos.txt");
my @ruts = randNums(13000000, 10000000);
my @edad = randNums(18, 10);
my @estatura = randNums(150, 50);

print "
INSERT INTO personas(ruts, nombres, apellidos, pais, estatura, edad)
VALUES
";

my $i = 0;
while ($i < 100) {
    if ($i != 99) {
        print "($ruts[$i], ";
        print "\'$nombres[int rand 100]\', ";
        print "\'$apellidos[int rand 100]\', ";
        print "\'$paises[int rand 100]\', ";
        print "$estatura[$i], ";
        print "$edad[$i]),\n";
    } else {
        print "($ruts[$i], ";
        print "\'$nombres[int rand 100]\', ";
        print "\'$apellidos[int rand 100]\', ";
        print "\'$paises[int rand 100]\', ";
        print "$estatura[$i], ";
        print "$edad[$i]);\n";
    }
    $i++;
}

