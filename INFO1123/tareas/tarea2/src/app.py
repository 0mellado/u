#!/usr/bin/python3
import psycopg2
import matplotlib.pyplot as plt
from random import randint as rand
from dataclasses import dataclass


@dataclass
class persona:
    rut: int
    nombre: str
    apellido: str
    pais: str
    estatura: int
    edad: int


def bdConnect():
    try:
        print("[*] Conectando con la base de datos ...")
        connection = psycopg2.connect(user = "oscar",
            password = "123455", host = "localhost",
            port = "5432", database = "distribuidor")
        return connection.cursor(), connection

    except (Exception, psycopg2.Error) as error:
        print(f"[*] Error mientras se connectaba a la base de datos: {error}")


def importData():
    bd, connection = bdConnect()
    bd.execute("SELECT * FROM personas;")
    response = bd.fetchall()

    if connection:
        bd.close()
        connection.close()
        print("[*] Conección con la base de datos cerrada")

    return response


def procesData(data: list):
    people = []
    for _ in data:
        person = persona(
            data[rand(0, 99)][0],
            data[rand(0, 99)][1],
            data[rand(0, 99)][2],
            data[rand(0, 99)][3],
            data[rand(0, 99)][4],
            data[rand(0, 99)][5]
        )

        people.append(person)

    return people


def subplotAux(i: int, axs, data: list, label: str, vert=False):
    axs[i].hist(data, bins='auto')
    axs[i].set_title(label)
    if vert:
        axs[i].tick_params(labelrotation=90)

    return


def viewData(data: list):
    nombres = []; apellidos = []; paises = []
    estaturas = []; edades = []

    for i in data:
        nombres.append(i.nombre)
        apellidos.append(i.apellido)
        paises.append(i.pais)
        estaturas.append(i.estatura)
        edades.append(i.edad)

    fig, ax = plt.subplots(5, constrained_layout=True)
    fig.suptitle('Datos de personas')

    subplotAux(0, ax, nombres, 'Nombres', True)
    subplotAux(1, ax, apellidos, 'Apellidos', True)
    subplotAux(2, ax, edades, 'Edades')
    subplotAux(3, ax, estaturas, 'Estaturas')
    subplotAux(4, ax, paises, 'Paises', True)

    plt.show()
    return


def saveData(data: list):
    handle = open("data.txt", "w")
    string = "%10d, %3d, %4d, %16s, %16s, %32s"
    for i in data:
        print(string % (i.rut,i.edad,i.estatura,i.nombre,i.apellido,i.pais))
        handle.write(string % (i.rut,i.edad,i.estatura,i.nombre,i.apellido,i.pais)+ "\n")
    handle.close()
    return


def main():
    data = importData()
    classesPerson = procesData(data)
    saveData(classesPerson)
    viewData(classesPerson)


if __name__ == '__main__':
    main()

