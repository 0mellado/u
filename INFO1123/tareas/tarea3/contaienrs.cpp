#include <iostream>

using namespace std;

/* clase padre que representa la */ 
/* forma general de los containers */
class Container {
    /* atributos protegidos */
    protected:
        unsigned short capaciCont;
        unsigned short cantConts;
        string type;
        string weightType;
        long weight;

    /* métodos publicos de la clase para la manipulación de los atributos */
    public:
        /* contructor de la clase */
        Container(string t, string wT, long w) {
            type = t;
            weightType = wT;
            weight = w;
            capaciCont = 20000;
        } 

        /* método para calcular la cantidad de contenedores */
        virtual void divCont() {
            cantConts = (weight / capaciCont) + 1;
        }

        /* método para obtener el peso total */
        long getWeight() { return weight; }

        /* método para obtener la capacidad del contenedor */
        unsigned short getCapacity() { return capaciCont; }
        /* método para obtener la cantidad de contenedores */
        unsigned short getCantCont() { return cantConts; }
};

/* las clases que son hijas de la clase padre */
/* heredan todos los atributos siempre que no */
/* tengan el operador de acceso private */

/* clase hija de los contenedores normales */
class Normal: public Container {
    /* métodos protegidos de la clase hija */
    protected:
        unsigned short capaciContS;
        unsigned short cantContsS;

    /* métodos publicos para la manipulación de los atributos */
    public:
        /* constructor de la clase hija */
        Normal(string t, string wT, long w):Container(t, wT, w) { 
            capaciCont = 24000;
            capaciContS = 12000;
        }

        /* método para obtener la capacidad del contenedor */
        unsigned short getCapacityS() { return capaciContS; }
        /* método para obtener la cantidad de los contenedores pequeños */
        unsigned short getCantContS() { return cantContsS; }

        /* override de el método de la clase padre */
        void divCont() override {
            cantConts = (weight / capaciCont) + 1;
            cantContsS = (weight / capaciContS) + 1;
        }
};


/* clase hija de la clase normal */
class Refri: public Normal {
    /* atributos protegidos */
    protected:
        unsigned short capaciContS;
        unsigned short cantContsS;

    public:
        /* constructor de la clase refri */
        Refri(string t, string wT, long w):Normal(t, wT, w) { 
            capaciCont = 20000;
            capaciContS = 10000;
        }
};

/* la siguientes 3 clases hijas son identicas a la clase padre */

class Enorml: public Container {
    public:
        /* constructor de la clase Enorml */
        Enorml(string t, string wT, long w):Container(t, wT, w) { 
            capaciCont = 24000;
        };
};

class Erefri: public Container {
    public:
        /* constructor de la clase Erefri */
        Erefri(string t, string wT, long w):Container(t, wT, w) { 
            capaciCont = 20000;
        };
};

class Einfla: public Container {
    public:
        /* constructor de la clase Einfla */
        Einfla(string t, string wT, long w):Container(t, wT, w) { 
            capaciCont = 20000;
        };
};

int main() {

    Container container = Container("general", "todo", 200000);

    /* se imprimen los atributos de cada clase */
    cout << "Contenedor refrigerado solida\n";
    cout << "Capacidad del contenedor: " << container.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    container.divCont();
    cout << "Cantidad de contenedores: " << container.getCantCont() << endl;

    cout << endl;
    
    /* se instacia cada clase en un objeto */
    Normal normal = Normal("normal", "solida", 100000);
    Refri refri = Refri("refrigerado", "solida", 100000);
    Enorml enorml = Enorml("normal", "liquida", 100000);
    Erefri erefri = Erefri("refrigerado", "liquida", 100000);
    Einfla einfla = Einfla("inflamable", "liquida", 100000);


    /* se imprimen los atributos de cada clase */
    cout << "Contenedor normal solida\n";
    cout << "Capacidad del contenedor pequeño: " << normal.getCapacityS() << endl;
    cout << "Capacidad del contenedor grande: " << normal.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    normal.divCont();
    cout << "Cantidad de contenedores pequeños: " << normal.getCantContS() << endl;
    cout << "Cantidad de contenedores grandes: " << normal.getCantCont() << endl;

    cout << endl;

    /* se imprimen los atributos de cada clase */
    cout << "Contenedor refrigerado solida\n";
    cout << "Capacidad del contenedor pequeño: " << refri.getCapacityS() << endl;
    cout << "Capacidad del contenedor grande: " << refri.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    refri.divCont();
    cout << "Cantidad de contenedores pequeños: " << refri.getCantContS() << endl;
    cout << "Cantidad de contenedores grandes: " << refri.getCantCont() << endl;

    cout << endl;

    /* se imprimen los atributos de cada clase */
    cout << "Contenedor de liquidos normales\n";
    cout << "Capacidad del contenedor: " << enorml.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    enorml.divCont();
    cout << "Cantidad del contenedor: " << enorml.getCapacity() << endl;

    cout << endl;

    /* se imprimen los atributos de cada clase */
    cout << "Contenedor de liquidos normales\n";
    cout << "Capacidad del contenedor: " << erefri.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    erefri.divCont();
    cout << "Cantidad del contenedor: " << erefri.getCapacity() << endl;

    cout << endl;

    /* se imprimen los atributos de cada clase */
    cout << "Contenedor de liquidos normales\n";
    cout << "Capacidad del contenedor: " << einfla.getCapacity() << endl;
    /* se modifica uno de los atributos de la clase */
    einfla.divCont();
    cout << "Cantidad del contenedor: " << einfla.getCapacity() << endl;

    return 0;
}
