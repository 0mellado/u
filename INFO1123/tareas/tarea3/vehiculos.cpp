#include <iostream>

using namespace std;

/* clase padre que representa a los vehiculos */
class Vehiculo {
    /* atributos de la clase */
    protected:
        long price;
        unsigned short capaci;
        unsigned cantCont;

    public:
        /* constructor de la clase padre */
        Vehiculo(unsigned c) {
            cantCont = c;
        }

        /* método para obtener la cantidad de vehiculos */
        unsigned getCantVehi() { return (cantCont / capaci) + 1; }

        int getCapa() { return capaci; }
        long getPrice() { return price; }
};

/* definición de una clase */
class Truck: public Vehiculo {
    public:
        /* constructor de la clase truck */ 
        Truck(unsigned c):Vehiculo(c) {
            price = 500000;
            capaci = 1;
        }
};

/* definición de una clase */
class Train: public Vehiculo {
    public:
        /* constructor de la clase train */ 
        Train(unsigned c):Vehiculo(c) {
            price = 10000000;
            capaci = 250;
        }
};

/* definición de una clase */
class Plane: public Vehiculo {
    public:
        /* constructor de la clase plane */ 
        Plane(unsigned c):Vehiculo(c) {
            price = 1000000;
            capaci = 10;
        }
};

/* definición de una clase */
class Freighter: public Vehiculo {
    public:
        /* constructor de la clase freighter */ 
        Freighter(unsigned c):Vehiculo(c) {
            price = 1000000000;
            capaci = 24000;
        }
};

int main() {

    /* se instancian los objetos */
    Truck truck = Truck(43892);
    Train train = Train(89298);
    Plane plane = Plane(89293);
    Freighter barco = Freighter(32983);

    /* se imprimen los atributos de las clases */
    cout << "Cantidad de vehiculos (camion): "<< truck.getCantVehi() << endl;
    cout << "Cantidad de vehiculos (tren): "<< train.getCantVehi() << endl;
    cout << "Cantidad de vehiculos (avión): "<< plane.getCantVehi() << endl;
    cout << "Cantidad de vehiculos (barco): "<< barco.getCantVehi() << endl;

    return 0;
}

