#include <stdio.h>
#include <stdlib.h>

/* Estructura nodo: en la estructura se nodo se lo */
/* define como un entero la variable data que va a */ 
/* guardar el elemento de la lista, y como el mismo */
/* tipo nodo el puntero a el siguiente elemento */
struct Node {
    int data;
    struct Node* next;
};


void push(struct Node** headRef, int newData) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));

    newNode->data = newData;

    newNode->next = *headRef;

    *headRef = newNode;
}


void printList(struct Node* node) {
    while (node != NULL) {
        printf("%d\n", node->data);
        node = node->next;
    }
}


int main() {

    struct Node* head = NULL;

    push(&head, 1);
    printf("%p\n", &head);
    push(&head, 2);
    printf("%p\n", &head);
    push(&head, 3);
    printf("%p\n", &head);


    return 0;
}

