#include <stdio.h>
#include <stdlib.h>
 
struct HashNode {
    int key;
    int value;
};
 
const int capacity = 20;
int size = 0;
 
struct HashNode** arr;
struct HashNode* dummy;
 
void insert(int key, int value) {
 
    struct HashNode* temp = (struct HashNode*)malloc(sizeof(struct HashNode));
    temp->key = key;
    temp->value = value;
 
    int hashIndex = key % capacity;
 
    while (arr[hashIndex] != NULL
           && arr[hashIndex]->key != key
           && arr[hashIndex]->key != -1) {

        hashIndex++;
        hashIndex %= capacity;
    }
 
    if (arr[hashIndex] == NULL
        || arr[hashIndex]->key == -1)
        size++;
 
    arr[hashIndex] = temp;
}


int delete (int key) {
    int hashIndex = key % capacity;
 
    while (arr[hashIndex] != NULL) {
        if (arr[hashIndex]->key == key) {
            arr[hashIndex] = dummy;
 
            size--;
 
            return 1;
        }
        hashIndex++;
        hashIndex %= capacity;
    }
 
    return 0;
}
 

int find(int key) {
    int hashIndex = (key % capacity);
 
    int counter = 0;
 
    while (arr[hashIndex] != NULL) {
 
        int counter = 0;
        if (counter++ > capacity)
            break;
 
        if (arr[hashIndex]->key == key)
            return arr[hashIndex]->value;
 
        hashIndex++;
        hashIndex %= capacity;
    }
 
    return -1;
}


int main() {
    arr = (struct HashNode**)malloc(sizeof(struct HashNode*) * capacity);

    for (int i = 0; i < capacity; i++)
        arr[i] = NULL;

    dummy = (struct HashNode*)malloc(sizeof(struct HashNode));

    dummy->key = -1;
    dummy->value = -1;

    insert(1, 892);
    insert(2, 1318);
    insert(3, 473);
    insert(4, 732);
    insert(5, 73);

    if (find(4) != -1) 
        printf("%d\n", find(4));
    else 
        printf("la clave 4 no existe\n");

    if (delete(4))
        printf("el valor de la clave 4 fue borrado\n");
    else
        printf("La clave no existe");

    if (find(4) != -1) 
        printf("%d\n", find(4));
    else 
        printf("la clave 4 no existe\n");
}

