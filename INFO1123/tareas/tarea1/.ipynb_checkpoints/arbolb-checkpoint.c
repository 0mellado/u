#include <stdio.h>
#include <stdlib.h>


struct node {
    int data;
    struct node* left;
    struct node* right;
};


struct node* newNode(int item) {
    struct node* node = (struct node*)malloc(sizeof(struct node));

    node->data = item;
    node->left = node->right = NULL;

    return(node);
}


void tree(struct node* root) {
    if (root != NULL) {
        tree(root->left);
        printf("%d\n", root->data);
        tree(root->right);
    }
}


struct node* insert(struct node* node, int data) {
    if (node == NULL)
        return newNode(data);

    if (data< node->data) 
        node->left = insert(node->left, data);
    else if (data> node->data)
        node->right = insert(node->right, data);

    return node;
}


struct node* minValue(struct node* node) {
    struct node* current = node;

    while (current && current->left != NULL) 
        current = current->left;

    return current;
}


struct node* delete(struct node* root, int data) {

    if (root == NULL)
        return root;

    if (data < root->data)
        root->left = delete(root->left, data);

    else if (data > root->data)
        root->right = delete(root->right, data);

    else {
        if (root->left == NULL) {
            struct node *temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL) {
            struct node *temp = root->left;
            free(root);
            return temp;
        }

        struct node* temp = minValue(root->right);

        root->data = temp->data;

        root->right = delete(root->right, temp->data);

    }

    return root;
}


int main() {

    struct node* root = NULL;

    root = insert(root, 438);
    root = insert(root, 234);
    root = insert(root, 423);
    root = insert(root, 189);
    root = insert(root, 788);

    tree(root);

    root = delete(root, 423);
    printf("\n");

    tree(root);

    return 0;
}
