#!/usr/bin/perl -w

use strict;

my $file = $ARGV[0] or die;
open my $data, "<", $file or die;

while (my $line = <$data>) {
    chomp $line;

    my @fields = split ",", $line;

    print "@fields\n";
}
