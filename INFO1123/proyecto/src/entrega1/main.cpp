#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include "vehiculos.h"
#include "containers.h"


using namespace std;


int main(int argc, char** argv) {
/* se crea un nodo para hacer una lista enlazada */
    Node<Product>* products = NULL;
    /* ruta del archivo .csv */
    string pathFile = argv[1];
    /* se crea manejador del archivo */
    ifstream file(pathFile.c_str());
    /* en la variable line se van a guardar las lineas del archivo */
    string line;
    /* se descarta la primera linea del archivo */
    getline(file, line);

    /* se define el delimitador del archivo .csv */
    char d = ',';
    /* con el bucle while se va leyendo cada linea */
    while (getline(file, line)) {
        /* con el tipo de dato stringstream se simula un archivo para el */
        /* manejo mas comodo de una cadena de caracteres */
        stringstream stream(line);
        /* en las siguientes variables se van a guardar los atributos de los productos */
        string id, name, type, typeweight, weight;

        /* se obtiene la linea, separando los atributos por comas */
        getline(stream, id, d);
        getline(stream, name, d);
        getline(stream, type, d);
        getline(stream, typeweight, d);
        getline(stream, weight, d);

        /* se transfoman los strings en números enteros cortos y largos */
        unsigned short idn = stoi(id);
        unsigned long weightn = stold(weight);

        /* se instancia la estructura producto */
        Product product;
        product.id = idn;
        product.name = name;
        product.type = type;
        product.weightType = typeweight;
        product.weight = weightn;

        /* se añade a la lista de productos */
        append(&products, product);
    }

    /* se cierra el archivo */
    file.close();

    /* se guarda en un arreglo las clases de los containers de 2 tipos */
    Solid solid[] = {
        Normal(),
        Refri()
    };

    /* se guarda en un arreglo las clases de los containers de un tipo */
    Estanques liquidos[] = {
        Enorml(),
        Erefri(),
        Einfla()
    };

    /* se recorre la lista de todos los productos para añadirlos a las clases */
    while (products != NULL) {
        for (char i = 0; i < 3; i++) {
            if (i < 2) 
                solid[i].addProd(products->data);
            liquidos[i].addProd(products->data);
        }
        products = products->next;
    }

    /* se definen arreglos para guardar los atributos que devuelven los metodos */ 
    /* de las clases */
    Node<Product>* prodConts[5];
    unsigned long weights[5];
    unsigned long cantConts[5];
    unsigned long cantContsS[2];

    /* en el bucle for se usan los metodos relacionados con ejecutar los calculos */ 
    /* dentro de la clases para guardar otros atributos */
    for (char i = 0; i < 3; i++) {
        if (i < 2) {
            /* se obtienen los productos de uno de los tipos de contenedores */
            prodConts[i] = solid[i].getProds();
            /* se ejecuta la sumatoria de los pesos de las clases */
            solid[i].sumTotalW();
            /* se obtine el peso total de los productos en las clases */
            weights[i] = solid[i].getTotalW();
            /* en el caso de los contenedores pequeños se dividen en un contenedor grande y un pequeño */
            solid[i].divCont();
            /* se obtiene la cantidad de contenedores grandes */
            cantConts[i] = solid[i].getCantCont();
            /* se obtiene la cantidad de contenedores pequeños */
            cantContsS[i] = solid[i].getCantContS();
        }
        prodConts[i+2] = liquidos[i].getProds();
        liquidos[i].sumTotalW();
        weights[i+2] = liquidos[i].getTotalW();
        liquidos[i].divCont();
        cantConts[i+2] = liquidos[i].getCantCont();
    }

    /* con la siguiente variable, además del bucle for y la condicional */ 
    /* se hace la sumatoria de todos los contenedores */
    unsigned cantTotalConts = 0;
    for (char i = 0; i < 5; i++)
        cantTotalConts = cantTotalConts + cantConts[i];
    if (cantContsS[0] != 0 || cantContsS[1] != 0)
        cantTotalConts++;

    /* en este arreglo se guardan los nombre de los contenedores */
    /* para luego imprimirlos por pantalla */
    string strs[] = {
        "Contenedores normales", 
        "Contenedores refrigerados o inflamables solidos",
        "Estanques normales", 
        "Estanques refrigerados",
        "Estanques inflamables"
    };

    /* con el for se imprimen datos como la cantidad de contenedores */
    for (char i = 0; i < 5; i++) {
        cout << endl;
        cout << strs[i] << ":\n";
        if (i < 2) {
            cout << "\tCantidad de contenedores grandes: " << cantConts[i] << endl;
            cout << "\tCantidad de contenedores pequeños: " << cantContsS[i] << endl;
        } else 
            cout << "\tCantidad de contenedores: " << cantConts[i] << endl;
    }

    /* se imprime la cantidad total de contenedores */
    cout << "\nCantidad total de contenedores: " << cantTotalConts << endl;

    /* se limpia la memoria */
    while (products != NULL) {
        dele(&products, products->data.id);
        products = products->next;
    }

    /* se instancian las clases de los vehiculos con sus precios ingresado por parámetro */
    Vehiculo vehicle[4] = {
        Truck(500000),
        Train(10000000),
        Plane(1000000),
        Freighter(1000000000)
    };

    /* con los siguientes bucle se ordenan por rentabilidad */ 
    for (char i = 0; i < 4; i++) 
        for (char j = 1; j < 4 - i; j++) 
            if (vehicle[j].getRent() < vehicle[j-1].getRent()) {
                Vehiculo temp = vehicle[j];
                vehicle[j] = vehicle[j-1];
                vehicle[j-1] = temp;
            }

    /* se definen arreglos para los precios, cantidad de vehiculos y los nombres tanto como los restos */
    unsigned long prices[] = {0, 0, 0, 0};
    unsigned cantVehicle[] = {0, 0, 0, 0};
    string namesV[4];
    unsigned long rests[4];
    /* aca se comprueban para un número de contenedores menor a 10 */
    if (cantTotalConts < 10) {
        /* se calcula la cantidad de vehiculos */
        cantVehicle[0] = cantTotalConts / vehicle[3].getCapa();
        /* se calcula el precio del vehiculo */
        prices[0] = cantVehicle[0] * vehicle[3].getPrice();

        /* se imprime el precio y la cantidad de vehiculos */
        cout << "Precio total: " << prices[0] << endl;
        cout << "Cantidad de contenedores en el vehiculo: " << cantVehicle[0] << endl;

        cout << "\n[*] Ejecución exitosa\n\n";
        /* se retorna el código de salida 0 */
        return 0;
    }

    /* se calcula la cantidad de vehiculos partiendo con el vehiculo de mayor rentabilidad */ 
    cantVehicle[0] = cantTotalConts / vehicle[0].getCapa();
    /* se calcula el precio por tipo vehiculo */ 
    prices[0] = cantVehicle[0] * vehicle[0].getPrice();
    /* se calcula el residuo de la división para seguir el mismo proceso con los restos de las divisiones */
    rests[0] = cantTotalConts % vehicle[0].getCapa();
    /* se le asigna el nombre al vehiculo */
    namesV[0] = vehicle[0].getName();

    /* se prueban todas las combinaciones posibles para encotrar la más óptima*/
    /* se compureba si el siguiente vehiculo cumple con la especificación de capacidad */
    /* osea que el resto que se calculo anteriormente sea mayor a la capacidad del vehiculo */
    if (rests[0] > vehicle[1].getCapa()) {
        cantVehicle[1] = rests[0] / vehicle[1].getCapa();
        prices[1] = cantVehicle[1] * vehicle[1].getPrice();
        rests[1] = rests[0] % vehicle[1].getCapa();
        namesV[1] = vehicle[1].getName();
        if (rests[1] > vehicle[2].getCapa()) {
            cantVehicle[2] = rests[1] / vehicle[2].getCapa();
            prices[2] = cantVehicle[2] * vehicle[2].getPrice();
            rests[2] = rests[1] % vehicle[2].getCapa();
            namesV[2] = vehicle[2].getName();
            if (rests[2] > vehicle[3].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[3].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[3].getPrice();
                namesV[3] = vehicle[3].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[2].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[2].getPrice();
            }
        } else if (rests[1] > vehicle[3].getCapa()) {
            cantVehicle[2] = rests[1] / vehicle[3].getCapa();
            prices[2] = cantVehicle[2] * vehicle[3].getPrice();
            rests[2] = rests[1] % vehicle[3].getCapa();
            namesV[2] = vehicle[3].getName();
            if (rests[2] > vehicle[2].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[2].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[2].getPrice();
                namesV[3] = vehicle[2].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[3].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[3].getPrice();
            }
        } else {
            cantVehicle[1] = (rests[0] / vehicle[1].getCapa()) + 1;
            prices[1] = cantVehicle[1] * vehicle[1].getPrice();
        }
    } else if (rests[0] > vehicle[2].getCapa()) {
        cantVehicle[1] = rests[0] / vehicle[2].getCapa();
        prices[1] = cantVehicle[1] * vehicle[2].getPrice();
        rests[1] = rests[0] % vehicle[2].getCapa();
        namesV[1] = vehicle[2].getName();
        if (rests[1] > vehicle[1].getCapa()) {
            cantVehicle[2] = rests[1] / vehicle[1].getCapa();
            prices[2] = cantVehicle[2] * vehicle[1].getPrice();
            rests[2] = rests[1] % vehicle[1].getCapa();
            namesV[2] = vehicle[1].getName();
            if (rests[2] > vehicle[3].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[3].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[3].getPrice();
                namesV[3] = vehicle[3].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[1].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[1].getPrice();
            }
        } else if (rests[1] > vehicle[3].getCapa()) {
            cantVehicle[2] = rests[1] / vehicle[3].getCapa();
            prices[2] = cantVehicle[2] * vehicle[3].getPrice();
            rests[2] = rests[1] % vehicle[3].getCapa();
            namesV[2] = vehicle[3].getName();
            if (rests[2] > vehicle[2].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[2].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[2].getPrice();
                namesV[3] = vehicle[2].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[3].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[3].getPrice();
            }
        } else {
            cantVehicle[1] = (rests[0] / vehicle[2].getCapa()) + 1;
            prices[1] = cantVehicle[1] * vehicle[2].getPrice();
        }
    } else if (rests[0] > vehicle[3].getCapa()) {
        cantVehicle[1] = rests[0] / vehicle[3].getCapa();
        prices[1] = cantVehicle[1] * vehicle[3].getPrice();
        rests[1] = rests[0] % vehicle[3].getCapa();
        namesV[1] = vehicle[3].getName();
        if (rests[1] > vehicle[1].getCapa()) {
            cantVehicle[2] = rests[1] / vehicle[1].getCapa();
            prices[2] = cantVehicle[2] * vehicle[1].getPrice();
            rests[2] = rests[1] % vehicle[1].getCapa();
            namesV[2] = vehicle[1].getName();
            if (rests[2] > vehicle[2].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[2].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[2].getPrice();
                namesV[3] = vehicle[2].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[1].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[1].getPrice();
            }
        } else if (rests[1] > vehicle[2].getCapa()) {
            cantVehicle[2] = rests[0] / vehicle[2].getCapa();
            prices[2] = cantVehicle[2] * vehicle[2].getPrice();
            rests[2] = rests[1] % vehicle[2].getCapa();
            namesV[2] = vehicle[2].getName();
            if (rests[2] > vehicle[1].getCapa()) {
                cantVehicle[3] = (rests[2] / vehicle[1].getCapa()) + 1;
                prices[3] = cantVehicle[3] * vehicle[1].getPrice();
                namesV[3] = vehicle[1].getName();
            } else {
                cantVehicle[2] = (rests[1] / vehicle[2].getCapa()) + 1;
                prices[2] = cantVehicle[2] * vehicle[2].getPrice();
            }
        } else {
            cantVehicle[1] = (rests[0] / vehicle[3].getCapa()) + 1;
            prices[1] = cantVehicle[1] * vehicle[3].getPrice();
        }
    }

    /* se definen variables en las cuales se guardaran datos como el precio total */ 
    /* de todos los vehiculos, y la cantida total de vehiculos */
    unsigned long totalPrice = 0;
    unsigned long totalVehicles = 0;
    /* con un bucle for se hace la sumatoria de las variables */
    for (char i = 0; i < 4; i++) {
        totalPrice = totalPrice + prices[i];
        totalVehicles = totalVehicles + cantVehicle[i];
    }

    /* se imprime el total de vehiculos */
    cout << endl;
    cout << "Cantidad total de vehiculos: " << totalVehicles << endl;
    cout << endl;

    /* se vuelve a ordenar el arreglo de los vehiculos */
    for (char i = 1; i < 5; i++) 
        for (char j = 2; j < 5-1; j++) 
            if (cantVehicle[j] < cantVehicle[j-1]) {
                Vehiculo temp = vehicle[j];
                vehicle[j] = vehicle[j-1];
                vehicle[j-1] = temp;
            }

    /* se calculan las capacidades totales de los conjuntos de vehiculos */
    long capaTotalV[4];
    for (char i = 0; i < 4; i++) {
        capaTotalV[i] = vehicle[i].getCapa() * cantVehicle[i];
    }

    /* con el siguiente bucle se imprime por pantalla el nombre del vehiculo, la */ 
    /* cantidad de los vehiculo y la capacidad de los contenedores */
    for (char i = 0; i < 4; i++)
        if (prices[i] != 0) {
            cout << "Cantidad total de " << namesV[i] << ": " << cantVehicle[i] << endl;
            cout << "\tCantidad de contenedores: " << capaTotalV[i] << endl;
        }
            
    /* se definen arreglos para guardar los pesos de las categorias normal, refrigerado, y inflamable */
    long weighttTypesV1[] = {0, 0, 0};
    long weighttTypesV2[] = {0, 0, 0};

    /* con un bucle se recorre la lista de los productos que retornaron las clases */
    short j = 0;
    /* el primer bucle recorre los elementos del arreglo de listas */
    for (char i = 0; i < 5; i++) {
        /* el segundo bucle recorre el las listas que son elementos */
        while (prodConts[i] != NULL) {
            if (j < cantVehicle[0]) {
                if (prodConts[i]->data.type == "normal")
                    weighttTypesV1[0] = weighttTypesV1[0] + prodConts[i]->data.weight;
                if (prodConts[i]->data.type == "refrigerado")
                    weighttTypesV1[1] = weighttTypesV1[1] + prodConts[i]->data.weight;
                if (prodConts[i]->data.type == "inflamable")
                    weighttTypesV1[2] = weighttTypesV1[2] + prodConts[i]->data.weight;
            } else if (j > cantVehicle[1]) {
                if (prodConts[i]->data.type == "normal")
                    weighttTypesV2[0] = weighttTypesV2[0] + prodConts[i]->data.weight;
                if (prodConts[i]->data.type == "refrigerado")
                    weighttTypesV2[1] = weighttTypesV2[1] + prodConts[i]->data.weight;
                if (prodConts[i]->data.type == "inflamable")
                    weighttTypesV2[2] = weighttTypesV2[2] + prodConts[i]->data.weight;
            } 
            prodConts[i] = prodConts[i]->next;
            j++;
        }
    }

    /* se imprimen los elementos de los arreglos */
    cout << endl;
    for (char i = 0; i < 3; i++) {
        cout << weighttTypesV1[i] << endl;
    }

    cout << endl;
    for (char i = 0; i < 3; i++) {
        cout << weighttTypesV2[i] << endl;
    }

    /* se imprime el precio total de todos los vehiculos */
    cout << endl;
    cout << "Precio total: " << totalPrice << endl;
    cout << endl;

    /* se imprimen los nombre de los vehiculos y sus precios parciales */
    for (char i = 0; i < 4; i++)
        if (prices[i] != 0)
            cout << "Precio total de los " << namesV[i] << ": " << prices[i] << endl;


    cout << "\n[*] Ejecución exitosa\n\n";
    return 0;
}
