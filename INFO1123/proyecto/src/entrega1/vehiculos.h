#include <iostream>
#include "containers.h"
using namespace std;

#ifndef VEHICULOS_H
#define VEHICULOS_H

/* se define una clase padre vehiculo */
class Vehiculo {
    /* la clase tiene los atributos precio total, precio unitario, nombre y
     * capacidad 
     * */ 
    protected:
        unsigned long totalP;
        float unitP;
        string name;
        unsigned short capaci;


        /* en los métodos publicos esta el constructor, el métodos para obtener el nombre */
        /* o la rentabilidad del vehiculo*/ 
    public:
        /* constructor */
        Vehiculo(unsigned long price) {
            unitP = price;
        }

        /* para obtener la rentabilidad */
        float getRent() { return unitP / capaci; }
        /* para obtener el nombre */ 
        string getName() { return name; }

        /* para obtener la capacidad de la class */
        unsigned short getCapa() { return capaci; }
        /* para obtener el precio unitariodel producto */
        long getPrice() { return unitP; }
};

/* clase camión */
class Truck: public Vehiculo {
    public:
        Truck(unsigned long p):Vehiculo(p) {
            name = "camiones";
            capaci = 1;
        }
};

/* clase tren */
class Train: public Vehiculo {
    public:
        Train(unsigned long p):Vehiculo(p) {
            name = "trenes";
            capaci = 250;
        }
};

/* clase pablo */
class Plane: public Vehiculo {
    public:
        Plane(unsigned long p):Vehiculo(p) {
            name = "aviones";
            capaci = 10;
        }
};

/* clase barco */
class Freighter: public Vehiculo {
    public:
        Freighter(unsigned long p):Vehiculo(p) {
            name = "barcos";
            capaci = 24000;
        }
};

#endif
