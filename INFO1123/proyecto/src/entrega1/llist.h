#include <iostream>

using namespace std;

#ifndef LLIST_H
#define LLIST_H

/* se define una clase nodo para poder trabajar con variables dinamicas */
template <typename T>
class Node {
    /* tiene atributos como data que es donde se guardan los datos */
    /* y el atributo next que es un puntero al siguiente elemento */
    public:
        T data;
        Node *next;
};


/* se crea una función para añadir elementos a la lista */
template <typename T>
void append(Node<T>** head_ref, T data) {
    
    /* se crea un objeto nodo */
    Node<T> *new_data = new Node<T>();
    /* se guarda la cabeza de la lita en el último dato */
    Node<T> *last = *head_ref;
    /* se guarda el dato en su atributo */ 
    new_data->data = data;
    /* se deja el puntero siguiente con un valor nulo */
    new_data->next = NULL;
    /* si la cabeza esta vacía se retorna un nuevo nodo */
    if (*head_ref == NULL) {
        *head_ref = new_data;
        return;
    }
    /* se recorre la lista hasta el último nodo */
    while (last->next != NULL)
        last = last->next;

    /* guarda el siguiente del último nodo en el nodo nuevo */
    last->next = new_data;
    return;
}


/* se define una función para eliminar nodos */
template <typename T>
void dele(Node<T>** head_ref, short id) {
    /* se inicializan un nodo cabeza y un nodo que es el anterior */
    Node<T> *temp = *head_ref, *prev;
    /* si el nodo temporal no es nulo y se encuentra la idque se paso por */ 
    /* parámetro se guarda el puntero del nodo temporal en la cabeza y se */ 
    /* elimina el nodo liberando la zona en memoria */
    if (temp != NULL && temp->data.id == id) {
        *head_ref = temp->next; 
        delete temp; 
        return;
    }
    /* si no lo encuentra en la condicional anterior lo busca hacia atras */
    while (temp != NULL && temp->data.id != id) {
        prev = temp;
        temp = temp->next;
    }
    /* si el nodo esta vacío se retorna */
    if (temp == NULL)
        return;
 
    /* se desviculan los nodos */
    prev->next = temp->next;
 
    /* se librera la memoria */
    delete temp;
}


/* se crea una función para listar una lista */
template <typename T>
void printList(Node<T>* products) {
    /* se imprime los productos */ 
    cout << "\tProductos:\n";
    while (products != NULL) {
        printf("\t\tid de producto: %4d\n", products->data.id);
        cout << "\t\tproducto: "<< products->data.name << endl;
        products = products->next;
    }
}


#endif
