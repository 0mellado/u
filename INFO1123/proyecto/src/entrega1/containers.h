#include <iostream>
#include "llist.h"
#ifndef CONTAINER_H
#define CONTAINER_H

using namespace std;

/* se define una estructura de tipo producto que nos ayuda a */ 
/* ordenar los datos de los productos en una misma lista */
struct Product {
    unsigned short id;   // identificador del producto
    string name;         // nombre del producto
    string type;         // tipo de producto
    string weightType;   // tipo de masa del producto
    unsigned long weight;// peso del producto
};


/* se define la clase padre Container */
class Container {
    /* en la clase Container se definen atributos protegidos */ 
    /* como: las capacidades de los contenedores pequeños y grandes, */
    /* la cantidad de contenedores, el tipo de contenedor, */ 
    /* el tipo de masa que puede llevar el contenedor, el */
    /* total de masa que lleva y una lista que tiene los productos que posee */
    protected:
        unsigned capaciContS;
        unsigned capaciCont;
        unsigned long cantCont[2] = {0, 0};
        string type[3];
        string weightType[3];
        long totalW = 0;
        Node<Product> *products = NULL;


    public:
        /* se obtiene la capacidad del contenedor */
        unsigned char getCapacity() { return capaciCont; }
        /* se obtiene la cantidad de contenedores que hay */
        unsigned long getCantCont() { return cantCont[0]; }
        /* se obtiene la lista de productos */
        Node<Product>* getProds() { return products; }
        /* se dividen los contenedores */ 
        virtual void divCont() {
            if (totalW != 0)
                cantCont[0] = (totalW / capaciCont) + 1;
        }
        /* se calcula la sumatoria de todas las masas que se encuentran en el contenedor */
        void sumTotalW() {
            while (products != NULL) {
                totalW = totalW + products->data.weight;
                products = products->next;
            }
        }
        /* este método es para añadir y filtrar los productos que entran al contenedor */
        void addProd(Product prod) {
            for (char i = 0; i < 3; i++)
                for (char j = 0; j < 3; j++)
                    if (prod.type == type[j] && prod.weightType == weightType[i])
                        append(&products, prod);
        }
        /* se obtiene la masa total de contenedor */
        long getTotalW() { return totalW; }
};


/* se define una subclase llamada Estanques que es hija de la clase Container */
class Estanques: public Container {
    public:
        /* en su constructor se configuran atributos especificos para esta clase */
        Estanques() {
            weightType[0] = "";
            weightType[1] = "liquida";
            weightType[2] = "gas";
        }
};


/* se define la subclase llamada Solid que es hija de la clase Container */
class Solid: public Container {
    public:
        /* al igual que en la subclase anterior se asignan algunos de los atributos */
        /* especificos de la subclase */
        Solid() {
            weightType[0] = "solida";
            weightType[1] = "";
            weightType[2] = "";
        }
        /* se hace un override del método divCont para que pueda dividir en 2 tipos de contenedores */
        void divCont() override {
            if (totalW != 0) {
                cantCont[0] = totalW / capaciCont;
                unsigned long mod = totalW % capaciCont;
                if (mod < capaciContS) 
                    cantCont[1] = 1;
                else 
                    cantCont[0]++;
            }
        }

        /* se obtiene la capacidad del contenedor pequeño */
        unsigned char getCapacityS() { return capaciContS; }
        /* se obtiene la cantidad de contenedores pequeños */
        unsigned long getCantContS() { return cantCont[1]; }
};


/* clase para los contenedores normales solidos */
class Normal: public Solid {

    public:
        Normal() { 
            capaciCont = 24000;
            capaciContS = 12000;
            type[0] = "normal";
            type[1] = "";
            type[2] = "";
        }
};


/* clase para los contenedores refrigerados y solidos */
class Refri: public Solid {

    public:
        Refri() { 
            capaciCont = 20000;
            capaciContS = 10000;
            type[0] = "refrigerado";
            type[1] = "inflamable";
            type[2] = "";
        }
};


/* clase para los estanques normales */
class Enorml: public Estanques {

    public:
        Enorml() {
            capaciCont = 24000;
            type[0] = "normal";
            type[1] = "";
            type[2] = "";
        };
};

/* clase para los estanques refrigerados */
class Erefri: public Estanques {

    public:
        Erefri() {
            capaciCont = 20000;
            type[0] = "refrigerado";
            type[1] = "";
            type[2] = "";
        };
};

/* clase para los estanques que pueden tener elementos inflamables */
class Einfla: public Estanques {

    public:
        Einfla() {
            capaciCont = 20000;
            type[0] = "inflamable";
            type[1] = "";
            type[2] = "";
        };
};

#endif
