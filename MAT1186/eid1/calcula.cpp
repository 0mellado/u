#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

long double paraboll(double const x, double const p, double const h, double const k);
long double get_p(double d, double Ht, double Hmin);

void error_input();
void help();


int main(int argc, char *argv[]) {

    if (argc != 4) error_input();

    double d = std::atof(argv[1]);
    double h_min = std::atoi(argv[2]);
    double h_towers = (double)(d / 4);
    unsigned int cant_wire = std::atoi(argv[3]);

    long double p;
    if (h_towers - h_min == 0) 
        p = MAXFLOAT;
    else
        p = get_p(d, h_towers, h_min);

    std::ofstream file_data("data.txt");

    file_data << std::fixed;
    file_data << std::setprecision(23);

    file_data << p << "\n";

    double d_wires = (double)(d / (cant_wire - 1));

    file_data << d_wires << "\n";

    long double h_wires[cant_wire];

    long double total_sum_wire = 0.0;

    for (int i = 0; i < cant_wire; i++) {
        h_wires[i] = paraboll(i * d_wires, p, (d / 2), h_min);
        total_sum_wire += h_wires[i];
        if (i+1 == cant_wire) {
            file_data << h_wires[i] << "\n";
            total_sum_wire -= 2 * h_towers;
        }
        else file_data << h_wires[i] << ",";
    }

    file_data << h_wires[1] << "\n";
    file_data << h_wires[(int)(cant_wire/2)] << "\n";

    file_data << total_sum_wire << "\n";

    return 0;
}


void help() {
    std::cerr << "Uso: ./calcula <d-entre-torres> <H-minima> <cantidad-cables>\n";
    return;
}

void error_input() {
    std::cerr << "Entrada invalida: faltan parámetros\n";
    help();
    std::exit(-1);
}

long double paraboll(double const x, double const p, double const h, double const k) {
    return (std::pow((x - h), 2.0) / (4 * p)) + k;
}

long double get_p(double const d, double Ht, double Hmin) {
    return std::pow(d, 2.0) / (16 * (Ht - Hmin));
}
