#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from sys import argv
import os

wire_func = lambda x, p, h, k : (((x - h) ** 2) / (4 * p)) + k

def chart_data(data: dict) -> None:
    plt.style.use('_mpl-gallery')

    x_wire = np.linspace(0, float(argv[1]), 1000)
    y_wire = wire_func(x_wire, data['p'], float(argv[1]) / 2, float(argv[2]))

    x_segment = np.arange(0, float(argv[1]) + 0.01, data['d_wires'])
    y_segment = wire_func(x_segment, data['p'], float(argv[1]) / 2, float(argv[2]))

    _, axe = plt.subplots(1, constrained_layout=True)

    axe.set_title('Gráfico que describe el puente', fontsize=30)
    axe.set_xlabel('Longitud (metros)', fontsize=20)
    axe.set_ylabel('Altura (metros)', fontsize=20)

    axe.text((float(argv[1]) / 2) - 10, float(argv[1]) / 5,
             r"$ f(x) = \frac{(x - " + f"{float(argv[1]) / 2}" +
             r")^2}{4 \cdot" + f"{data['p']}"+"} + " +
             f"{int(argv[2])}" + "$",
             fontsize=30)

    axe.text((float(argv[1]) / 2) - 10, (float(argv[1]) / 5) - 2,
             f"Distancía entre los cable: {data['d_wires']}m\n" +
             f"Altura de las torres: {float(argv[1])/4}m\n" +
             f"Altura del cable más largo: {data['wire_max']}m\n" +
             f"Altura del cable más corto: {data['wire_min']}m\n" +
             f"Cantidad de cables (sin contar las torres): {int(argv[3])-2}\n" +
             f"Longitud sumada de los cables (sin contar las torres): {data['wire_total']}",
             fontsize=20)

    axe.plot(x_wire, y_wire, color='red')
    axe.stem(x_segment, y_segment)

    axe.set_xlim((-(float(argv[1]) / 70), (float(argv[1]) / 70) + float(argv[1])))
    axe.set_ylim((-(float(argv[1]) / 200), (float(argv[1]) / 200) + (float(argv[1]) / 4)))

    plt.show()
    return


def parser_data(data: list) -> dict:

    h_wires = data[2].split(',')

    for i in range(len(h_wires)):
        h_wires[i] = float(h_wires[i])

    return {
        'p': float(data[0]),
        'd_wires': float(data[1]),
        'h_wires': h_wires,
        'wire_max': float(data[3]),
        'wire_min': float(data[4]),
        'wire_total': float(data[5]),
    }


def exec_backend() -> None:
    os.system(f"./calcula {argv[1]} {argv[2]} {argv[3]}")
    return


def read_file() -> list[str]:
    with open("data.txt", "r") as fh_data:
        return fh_data.readlines()


def error_input() -> None:
    if len(argv) == 4:
        return
    print("Error: Faltan parámetros")
    exit(-1)

def error_data() -> None:
    if (float(argv[1]) / 4) < 4 or \
        float(argv[2]) < 0 or \
        float(argv[3]) < 3 or \
        (float(argv[3]) % 1) != 0.0:
        print('Error: Uno de los datos ingresados es invalido')
        exit(-1)
    return


def main() -> None:
    error_input()
    error_data()
    exec_backend()
    parsed_data = parser_data(read_file())
    chart_data(parsed_data)


if __name__ == '__main__':
    main()
