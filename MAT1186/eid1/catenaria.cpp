#include <iostream>
#include <iomanip>
#include <limits>
#include <cmath>
#include <bits/stdc++.h>
#include <math.h>
#include <vector>


long double catenary(double x, double a, double d, double b);
long double casi_cero(double const d, long double const a, double b);
long double my_binary_search(double const d, std::vector<long double>& rango, long double const step, long double const b);


int main(int argc, char *argv[]) {

    if (argc != 3)
        return -1;

    int const d = std::atoi(argv[1]);
    int const b = std::atoi(argv[2]);

    std::vector<long double> rango {0, (long double)(d * 2)};

    long double step = std::numeric_limits<long double>::epsilon();

    std::cout << std::fixed;
    std::cout << std::setprecision(23);


    std::cout << step << "\n";

    std::cout << sizeof(long double) << "\n";

    /* long double a = my_binary_search(d, rango, step, b); */

    /* float i = 0; */

    /* while ( i <= d) { */
    /*     std::cout << catenary(i, a, d, b) << "\n"; */
    /*     i += 0.125; */
    /* } */

    return 0;
}


long double catenary(double x, double a, double d, double b) {
    return a * std::cosh((x - (d / 2)) / a) - a + b;
}

long double casi_cero(double const d, long double const a, double b) {
    return std::sqrt(std::pow(((d / 3) - a * std::cosh(d / (2*a)) + a - b), 2.0));
}

long double my_binary_search(double const d, std::vector<long double>& rango, long double const step, long double const b) {

    long double a = (rango[0] + rango[1]) / 2.0;

    long double left_side  = casi_cero(d, a - step, b);
    long double right_side = casi_cero(d, a + step, b);

    bool control = true;

    while (control) {
        if (left_side < right_side) rango[1] = a;
        else rango[0] = a;

        long double new_a = (rango[0] + rango[1]) / 2.0;

        if (a == new_a) {
            control = a != new_a;
            continue;
        }

        left_side  = casi_cero(d, new_a - step, b);
        right_side = casi_cero(d, new_a + step, b);
        a = new_a;
    }

    return a;
}
