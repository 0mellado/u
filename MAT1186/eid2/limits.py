#!/usr/bin/python3.11

import sympy
from sympy.plotting import plot


def limit_trigonomic():
    x = sympy.symbols('x')
    expr = (10 * sympy.sin(x)) / x

    print("ecuación: ")
    sympy.pprint(expr, use_unicode=True)

    limit_expr_to_0 = sympy.limit(expr, x, 0)
    limit_expr_to_oo = sympy.limit(expr, x, sympy.oo)

    print(f"x cuando tiende a 0 es: {limit_expr_to_0}")
    print(f"x cuando tiende a oo es: {limit_expr_to_oo}")
    print(f"tiene continuidad cuando x = 4: {sympy.limit(expr, x, 4, dir='-') == sympy.limit(expr, x, 4, dir='+')}\n")
    return expr


def limit_radicals():
    x = sympy.symbols('x')
    expr = x / sympy.sqrt(2*x**2 - x)

    print("ecuación: ")
    sympy.pprint(expr, use_unicode=True)

    limit_expr_to_0 = sympy.limit(expr, x, 0)
    limit_expr_to_oo = sympy.limit(expr, x, sympy.oo)

    print(f"x cuando tiende a 0 es: {limit_expr_to_0}")
    print(f"x cuando tiende a oo es: {limit_expr_to_oo}")
    print(f"tiene continuidad cuando x = 4: {sympy.limit(expr, x, 4, dir='-') == sympy.limit(expr, x, 4, dir='+')}\n")
    return expr


def limit_fraction():
    x = sympy.symbols('x')
    expr = (2*x**2 + 3*x)/x

    print("ecuación: ")
    sympy.pprint(expr, use_unicode=True)

    limit_expr_to_0 = sympy.limit(expr, x, 0)
    limit_expr_to_oo = sympy.limit(expr, x, sympy.oo)

    print(f"x cuando tiende a 0 es: {limit_expr_to_0}")
    print(f"x cuando tiende a oo es: {limit_expr_to_oo}")
    print(f"tiene continuidad cuando x = 4: {sympy.limit(expr, x, 4, dir='-') == sympy.limit(expr, x, 4, dir='+')}\n")
    return expr


def main():
    arr_expr = []
    arr_expr.append(limit_trigonomic())
    arr_expr.append(limit_radicals())
    arr_expr.append(limit_fraction())

    p1 = plot(arr_expr[0], show=False)
    p2 = plot(arr_expr[1], show=False)
    p3 = plot(arr_expr[2], show=False)

    p1.append(p2[0])
    p1.append(p3[0])
    p1.show()

if __name__ == '__main__':
    main()
