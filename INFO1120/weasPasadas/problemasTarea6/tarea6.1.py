#!/usr/bin/python3

def exist(file):
    try: open(file) ; return 1
    except: return 0


def readComidas(file: str):
    with open(file, "r") as comidas:
        lines = []
        for wea in comidas.readlines():
            lines.append(wea)

    return lines


def logRequest(comida, tipo):
    frutas = ["cerveza", "sandia", "manzana", "cebolla", "lechuga"]
    verduras = ["cocaina", "pasta base", "mariguana", "acido", "ajo"]
    with open("comidas.log", "w") as log:
        for fruta in frutas:
            log.write(f"frutas: {fruta}\n")
        for verdura in verduras:
            log.write(f"verduras: {verdura}\n")

        log.write(f"{tipo}: {comida}")

    return


def writeComidas():

    with open("comidas.txt", "w") as comidas:
        lines = readComidas("./comidas.log")
        comidas.write("verduras:\n")
        for i in range(len(lines)):
            lines[i] = lines[i].replace("\n", "").split(" ", 1)
            if lines[i][0] == "verduras:":
                comidas.write(lines[i][1]+"\n")

        lines = readComidas("./comidas.log")
        comidas.write("frutas:\n")
        for i in range(len(lines)):
            lines[i] = lines[i].replace("\n", "").split(" ", 1)
            if lines[i][0] == "frutas:":
                comidas.write(lines[i][1]+"\n")
    return


def inComidas():
    tipo = int(input("Ingrese si es una verdura o fruta (1 o 2): "))
    if tipo != 1 and tipo != 2:
        print("tu wea no existe owo")
        inComidas()
    elif tipo == 1:
        tipo = "verduras"
        comida = input("Ingrese algún alimento: ")

        return comida, tipo
    elif tipo == 2:
        tipo = "frutas"
        comida = input("Ingrese algún alimento: ")

        return comida, tipo


def main():
    comida, tipo = inComidas()
    logRequest(comida, tipo)
    writeComidas()
    # for wea in readComidas():
    #     print(wea.replace("\n", ""))


if __name__ == "__main__":
    main()

