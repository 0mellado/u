#!/usr/bin/perl -w

my @contacts = (
    ("Ignacio", 25, "19/06/1996", "+56911111111"),
    ("Cristian", 22, "14/05/1999", "+56922222222"),
    ("Claudio", 18, "15/03/2005", "+56933333333"),
    ("Sebastian", 17, "29/12/2006", "+56944444444"),
    ("Juan", 12, "18/09/2010", "+56955555555"),
);

open $contactsFile, ">>contactos.txt" 
    or die "No se pudo crear el archivo\n";

my $i = 0;
while ($i < 17) {
    print $contactsFile "$contacts[$i], $contacts[$i+1], $contacts[$i+2], $contacts[$i+3]\n";
    $i = $i + 4;
}

close $contactsFile;
