#!/usr/bin/python3

def fileToArray(file):
    with open(file, "r") as file:
        lines = file.readlines()
        array = []
        for line in lines:
            line = line.replace("\n", "")
            array.append(line)
    return array


def frutaYverduras():
    return fileToArray("./verduras.txt"), \
            fileToArray("./frutas.txt")

def checkReq(request):
    verduras, frutas = frutaYverduras()
    for verdura in verduras:
        if request == verdura.lower(): return "verdura"
    for fruta in frutas:
        if request == fruta.lower(): return "fruta"


def request():
    req = input("Ingrese una fruta o verdura: ").lower()
    with open("comidas.txt", "a") as comidas:
        tipo = checkReq(req)
        if tipo != "verdura" and tipo != "fruta":
            print("No parece una fruta o verdura")
            exit(1)
        comidas.write(f"{tipo}: {req}\n")


def readFile():
    with open("comidas.txt", "r") as comidas:
        return comidas.readlines()


def main():
    request()
    for line in readFile():
        print(line.replace("\n", ""))


if __name__ == '__main__':
    main()

