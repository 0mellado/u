#!/usr/bin/python3

def exist():
    try:
        open("comidas.txt")
        return 1
    except: return 0


def writeRequest():
    tipo = int(input("Ingrese si la comida es una verdura o una fruta (1 o 2): "))
    comida = input("Ingrese una comida: ")
    with open("comidas.txt", "a") as comidas:
        if tipo == 1:
            comidas.write(f"verdura: {comida}\n")
        elif tipo == 2:
            comidas.write(f"fruta: {comida}\n")
        else:
            print("Esa opción no existe")
            writeRequest()
    return


def main():
    if not exist():
        frutas = ["cerveza", "sandia", "manzana", "cebolla", "lechuga"]
        verduras = ["cocaina", "pasta base", "mariguana", "acido", "ajo"]
        with open("comidas.txt", "w") as comidas:
            for verdura in verduras:
                comidas.write(f"verdura: {verdura}\n")
            for fruta in frutas:
                comidas.write(f"fruta: {fruta}\n")
        writeRequest()
    else:
        writeRequest()


if __name__ == '__main__':
    main()
