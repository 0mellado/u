#!/usr/bin/python3
import curses
from curses import *
from random import randint


def initScreen():
    curses.initscr()
    screen = curses.newwin(30, 60, 1, 2)
    screen.keypad(True)
    curses.curs_set(0)
    screen.nodelay(True)
    return screen


def initSnake(x: int, y: int):
    return [[y, x], [y, x-1], [y, x-2]]


def initComida(x: int, y: int):
    return [y, x]


def movSnake(snake, key):
    snake.insert(0, [snake[0][0] + (key == KEY_DOWN and 1) + (key == KEY_UP and -1),
                     snake[0][1] + (key == KEY_LEFT and -1) + (key == KEY_RIGHT and 1)])
    return snake


def randomApple(comida, snake):
    while comida == []:
        comida = [randint(1, 28), randint(1, 58)]

        if comida in snake:
            comida = []
    return comida


def eatManzana(screen, snake, comida, puntaje):
    if snake[0] == comida:
        comida = []
        puntaje += 1
        comida = randomApple(comida, snake)
        screen.addch(comida[0], comida[1], 'O')
    else:
        last = snake.pop()
        screen.addch(last[0], last[1], ' ')
    return comida, puntaje


def main():
    screen = initScreen()

    key = KEY_RIGHT
    puntaje = 0

    snake = initSnake(5, 5)
    comida = initComida(25, 10)

    screen.addch(comida[0], comida[1], 'O')

    while key != 27:
        screen.border(0)

        screen.addstr(0, 2, ' Puntuación: ' + str(puntaje) + ' ')
        screen.addstr(0, 27, ' SNAKE! ')

        event = screen.getch()

        if event == -1:
            key = key
        else:
            key = event

        snake = movSnake(snake, key)

        if snake[0][0] == 0: snake[0][0] = 28
        if snake[0][0] == 29: snake[0][0] = 1

        if snake[0][1] == 0: snake[0][1] = 58
        if snake[0][1] == 59: snake[0][1] = 1

        if snake[0] in snake[1:]:
            break

        comida, puntaje = eatManzana(screen, snake, comida, puntaje)
        screen.addch(snake[0][0], snake[0][1], '#')

        screen.timeout(140)

    curses.endwin()

    print("\nPuntuación: " + str(puntaje))
    print('Moriste wuajaja')


if __name__ == '__main__':
  main()

