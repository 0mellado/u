from math import sqrt

class Rey:
    def __init__(self, x: int, y: int, img: int, equipo: int, ids: int):
        self.x = x
        self.y = y
        self.img = img
        self.eq = equipo
        self.id = ids


    def eat(self):
        pass


    def mov(self, coord: list, surface: list):
        modulDiag = sqrt(2)
        move = sqrt(((coord[0]-self.x)**2) + ((coord[1] - self.y)**2))
        if modulDiag == move:
            surface[self.x][self.y] = 0
            self.x = coord[0]
            self.y = coord[1]
            return 1
        if move == 1:
            surface[self.x][self.y] = 0
            self.x = coord[0]
            self.y = coord[1]
            return 1
        return 0

