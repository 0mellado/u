from math import sqrt

class Torre:
    def __init__(self, x: int, y: int, img: int, equipo: int, ids: int):
        self.x = x
        self.y = y
        self.img = img
        self.eq = equipo
        self.id = ids

    def mov(self, coord: list, surface: list):
        move = sqrt(((coord[0]-self.x)**2) + ((coord[1] - self.y)**2))
        for modPos in range(1,8):
            if move == modPos:
                surface[self.x][self.y] = 0
                self.x = coord[0]
                self.y = coord[1]
                return 1
        return 0
