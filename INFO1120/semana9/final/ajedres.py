#!/usr/bin/python3
from mapa import maps
import pygame as pg, os

sizeTile = 100
sizeRes = (10 * sizeTile, 10 * sizeTile)


class pieza():
    def __init__(self, x, y, img, e, i):
        self.id = i
        self.x = x
        self.y = y
        self.img = img
        self.equipo = e


def initPg():
    pg.init()
    pg.mouse.set_visible(True)
    pg.display.set_caption('Tarea 10')
    return pg.display.set_mode(sizeRes)


def loadImage(img, transp=False):
    try:
        image = pg.image.load(f"./imgs/{img}")
        image = pg.transform.scale(image,(100, 100))
        if transp:
            image = image.convert_alpha()
            color = image.get_at((0, 0))
            image.set_colorkey(color)
        return image
    except:
        print("Error: No se pudo cargar las imagenes, no encuentra el archivo o directorio")
        exit(1)


def initFig():
    aImg = []
    files = os.listdir('./imgs')
    files.sort()
    for img in files:
        aImg.append(loadImage(img))
    return aImg


def initPiezas():
    piezas = []
    imgRow = [12, 6, 1, 4, 10, 1, 6, 12]
    for i in range(8):
        piezas.append(pieza(1, i+1, imgRow[i], 0, i))
    for i in range(8):
        piezas.append(pieza(2, i+1, 8, 0, i+8))
    for i in range(8):
        piezas.append(pieza(8, i+1, imgRow[i]+1, 1, i+16))
    for i in range(8):
        piezas.append(pieza(7, i+1, 9, 1, i+8+16))
    return piezas


def drawMap(screen, sprt):
    mapa = maps()
    for fil in range(0, sizeRes[1] // sizeTile):
        for col in range(0, sizeRes[0] // sizeTile):
            screen.blit(sprt[mapa[fil][col]], (col * 100, fil * 100))
    return


def drawPiezas(screen, piezas, sprt):
    for pi in piezas:
        screen.blit(sprt[pi.img], (pi.y * 100, pi.x * 100))
    return


def selectPieza(piezas, posClic):
    for i in piezas:
        if i.x == posClic[1] and i.y == posClic[0]:
            return i
    return False


def movePieza(pieza, coord: list):
    pieza.x = coord[1]
    pieza.y = coord[0]
    return


def main():
    clickPos = [0, 0]
    screen = initPg()
    sprt = initFig()
    piezas = initPiezas()

    a = 1
    pieza = 0

    run = True
    while run:
        ev = pg.event.get()
        for event in ev:
            if event.type == pg.QUIT:
                run = False
            if event.type == pg.MOUSEBUTTONDOWN:
                clickPos = [event.pos[0] // 100, event.pos[1] // 100]
                if a:
                    pieza = selectPieza(piezas, clickPos)
                    a = 0
                else:
                    print(pieza)
                    movePieza(pieza, clickPos)
                    a = 1

        drawMap(screen, sprt)
        drawPiezas(screen, piezas, sprt)
        pg.display.flip()


if __name__ == '__main__':
    main()

