package main
// Realiza una función que genere una lista con todos los múltiplos del 3, desde el 0 hasta el
// 150 (contando el 150), luego mostrar el resultado de la lista por pantalla. (Nota: Se
// descontarán puntos si el print() se encuentra dentro la función.)
import "fmt"

const indices int = (150 / 3) +1

func funcionQueDevuelveUnaListaConLosMultiplosDeTres() [indices]int {
    var multDe3 [indices]int
    i := 0
    for i < 151 {
        if (i % 3) == 0 {
            multDe3[i/3] = i 
        }
        i++
    }
    return multDe3
}

func main() {
    fmt.Println(funcionQueDevuelveUnaListaConLosMultiplosDeTres())
}
