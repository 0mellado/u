-- Realiza un juego de adivinar un número, el programa genera aleatoriamente un número
-- entero entre el 1 y 100 sin mostrar al usuario, luego se solicita que ingrese un numero, si el
-- número no es igual al número generado aleatoriamente, se indicará si es mayor o menor al
-- número que buscamos. Una vez que sean iguales se finaliza el programa mostrando el número
-- de intentos.

nGana = math.random(1, 100)
intentos = 1
function entrada()
    print("Ingrese un número")
    n = io.read("*n")
    if n < nGana then
        print("el número que ingresaste es menor al que quieres adivinar")
        intentos = intentos + 1
        entrada()
    elseif n > nGana then
        print("el número que ingresaste es mayor al que quieres adivinar")
        intentos = intentos + 1
        entrada()
    else 
        print("Número de intentos: ", intentos)
        print("Ese es el número")
    end
end

entrada()
