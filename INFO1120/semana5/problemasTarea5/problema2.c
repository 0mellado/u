#include <stdio.h>

int min(int a, int b, int c) {
    if (a < b && a < c) return(a);
    if (b < a && b < c) return(b);
    if (c < b && c < a) return(c);
}

int main() {
    int nMin = min(1, 2, 3);

    printf("%d\n", nMin);
    
    return(0);
}
