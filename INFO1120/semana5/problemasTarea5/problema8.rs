// Escribir una función que recibe como parámetro un número natural y retorna su factorial,
// luego imprime su valor por pantalla. Sin la libreria math o otras (Nota: Se descontarán
// puntos si el print() se encuentra dentro la función.)

fn fac(n: u128) -> u128 {
    match n {
        0 => 1,
        1.. => (1..n+1).product(),
    }
}

fn main() {
    println!("{}", fac(34));
}
