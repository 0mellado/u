#!/usr/bin/python3

def factorial():
    n = 1000
    fac = 1
    for i in range(1, n + 1):
        fac = fac * i

    return fac

def main():
    print(factorial())

if __name__ == "__main__":
    main()
