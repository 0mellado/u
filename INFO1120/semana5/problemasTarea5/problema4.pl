#!/usr/bin/perl -w

# Realiza una función que retorne una lista con todos los números pares que existen entre el
# 1 hasta el 100 (Contando el 100) y luego imprimir por pantalla los números. (Nota: Se
# descontarán puntos si el print() se encuentra dentro la función.)

use strict;

sub pares {
    my @lista;
    for (my $i = 1; $i < 101; $i += 1) {
        if ($i % 2 == 0) {
            push @lista, $i;
        }
    }
    return @lista;
}

my @listaPares = pares();
$, = ' ';

print "[@listaPares]\n";
