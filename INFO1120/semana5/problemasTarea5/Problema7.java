// Realiza una función que retorne una lista con todos los números primos desde el 1 hasta el
// 100, luego mostrarlos por pantalla. Sin la libreria math o otras (Nota: Para realizar este
// cálculo se recomienda utilizar 2 ciclos “for” anidados.)
import java.util.List;
import java.util.Vector;

public class Problema7 {
    static List<Integer> primo() {
        List<Integer> nums = new Vector<Integer>();
        for (int i = 1; i < 100; i++) {
            if (!(i == 1)) {
                if (((i == 2) || !(i % 2 == 0)) &&
                    ((i == 3) || !(i % 3 == 0)) &&
                    ((i == 5) || !(i % 5 == 0)) &&
                    ((i == 7) || !(i % 7 == 0))) {
                    nums.add(i);
                }
            }
        }

        return nums;
    }
    
    public static void main(String []args) {
        List<Integer> lista = new Vector<Integer>();
        lista = primo();
        System.out.println(lista);
    }
}
