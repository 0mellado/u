# Realiza dos funciones, una función que calcule el máximo común divisor entre dos números y
# otra que calcule el mínimo común múltiplo entre dos números, luego sume estos valores y
# muestre por pantalla si pertenece a un número primo o no. Sin la libreria math o otras (Nota:
# Se descontarán puntos si el print() se encuentra dentro la función.)

nPrimos <- function() {
    primos <- c()
    n <- 10000
    x <- seq(1, n)
    for (i in seq(2, n)) {
        if (any(x == i)) {
            primos <- c(primos, i)
            x <- c(x[(x %% i) != 0], i)
        }
    }
    return(primos)
}

divisores <- function(x) {
    primos <- nPrimos()
    a <- x
    divisores <- c()
    for (primo in primos) {
        while (a %% primo == 0) {
            a <- a / primo
            divisores <- c(divisores, primo)
        }
    }
    return(divisores)
}

max <- function(x) {
    mayor <- x[c(1)]
    for (n in x) {
        if (n >= mayor) {
            mayor <- n
        }
    }
    return(mayor)
}

maximoComunDivisor <- function(x, y) {
    if (x == y) {
        return(x)
    }
    divx <- divisores(x)
    divy <- divisores(y)
    flag <- 0
    divComunes <- c()

    for (x in divx) {
        for (y in divy) {
            if (x == y) {
                divComunes <- c(divComunes, x)
                flag <- 1
            }
        }
    }

    if (flag == 0) {
        return(1)
    } else {
        return(max(divComunes))
    }
}

minimoComunMultiplo <- function(x, y) {
    return((x * y) / maximoComunDivisor(x, y))
}

mcm <- minimoComunMultiplo(17, 46)
mcd <- maximoComunDivisor(17, 46)

sumMcmMcd <- mcm + mcd

for (i in nPrimos()) {
    if (sumMcmMcd == i) {
        print("el número si es un primo")
    } else {
        print("el número no es un primo")
        return(0)
    }
}
