import System.Random
import Data.Time.Clock.System

sumaLista :: [Int] -> Int
sumaLista [] = 0 
sumaLista (x:xs) = x + sumaLista (xs)

main :: IO ()
main = do
    let wea = take 5 $ randoms (mkStdGen 4327894) :: [Int]
    
    print(sumaLista wea)
    
