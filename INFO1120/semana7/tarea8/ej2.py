#!/usr/bin/python3
from random import randint

def main():
    matrix = []

    for i in range(5):
        matrix.append([])
        for _ in range(5):
            matrix[i].append(randint(1, 100))

    for i in range(5):
        if i % 2 != 1:
            for j in range(5):
                matrix[i][j] = 0

    for fila in matrix:
        for col in fila:
            print("%4d" % col, end='')
        print()


if __name__ == '__main__':
    main()
