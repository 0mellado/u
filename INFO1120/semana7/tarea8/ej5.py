#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz y un número entero que
# debe ser mayor a 0 y menor a la cantidad de columnas que posee la matriz, la
# función debe devolver la suma de la columna indicada en el segundo parámetro.
# La matriz debe ser de n*m donde n y m se deben solicitar al usuario, la
# matriz será rellenada con números enteros aleatorios entre -10 hasta 10
# (-10 y 10 están incluidos). Mostrar por pantalla la suma final y la matriz.
# (10 Pts)

from random import randint

def sumaFila(matrix: list, index: int):
    if index < 0 and index > len(matrix[1]):
        print("El indice ingresado esta fuera de rango")
        exit(1)

    suma = 0
    try:
        for i in range(len(matrix)):
            suma += matrix[i][index]
        return suma
    except:
        print("Error: Debes ingresar una lista")


def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        print(" "*4, end="")
        for i in range(col):
            print("%4d" % (i+1), end='')
        print("\n----" + "-" * col * 4)

        for i in range(fil):
            i += 1
            print("%2d" % i, end=" |")
            i -= 1
            for j in matrix[i]:
                print("%4d" % j, end="")
            print()

        col = 2

        print(f"\nLa suma de la columna {col} es: {sumaFila(matrix, col)}")
    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()

