#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz y un número entero que
# debe ser mayor a 0 y menor a la cantidad de filas que posee la matriz, la
# función debe devolver la suma de fila indicada en el segundo parámetro. La
# matriz debe ser de n*m donde n y m se deben solicitar al usuario, la matriz
# será rellenada con número enteros aleatorios entre -10 hasta 10 (-10 y 10
# están incluidos). Mostrar por pantalla la suma final y la matriz. (10 Pts)

from random import randint

def sumaFila(matrix, index: int):
    if index < 0 and index > len(matrix):
        print("El indice ingresado esta fuera de rango")
        exit(1)

    suma = 0
    try:
        for i in matrix[index - 1]:
            suma += i
        return suma
    except:
        print("Error: Debes ingresar una lista")

def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for i in range(fil):
            i += 1
            print("%2d" % i, end=" |")
            i -= 1
            for j in matrix[i]:
                print("%4d" % j, end="")
            print()

        print(f"La suma de la fila 1 es: {sumaFila(matrix, 1)}")
    except:
        print("Error: Las entradas deben ser números")


if __name__ == '__main__':
    main()
