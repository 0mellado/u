#!/usr/bin/python3
from random import randint

def main():
    matrix = []

    for i in range(10):
        matrix.append([])
        for j in range(10):
            matrix[i].append(randint(0, 100))

    for fila in matrix:
        for col in fila:
            print("%4d" % col, end='')
        print()


if __name__ == '__main__':
    main()
