#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz, la función debe
# retornar la cantidad de números 1 que existen dentro de la matriz. La
# matriz debe ser de n*m donde n y m se deben solicitar al usuario, la
# matriz será rellenada con números enteros aleatorios entre -10 hasta 10
# (-10 y 10 están incluidos). Mostrar por pantalla la cantidad de números 1
# y la matriz. (10 Pts)

from random import randint

def cantUnos(matrix: list):
    cant = 0
    for fila in matrix:
        for col in fila:
            if col == 1:
                cant += 1
    return cant

def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for fila in matrix:
            for col in fila:
                print("%4d" % col, end='')
            print()

        print(f"La matriz tiene {cantUnos(matrix)} números 1")

    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()
