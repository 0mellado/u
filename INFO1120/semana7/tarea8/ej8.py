#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz, la función debe
# retornar un índice que indica cual es la fila que posee la suma más alta.
# La matriz debe ser de n*m donde n y m se deben solicitar al usuario, la
# matriz será rellenada con número enteros aleatorios entre -10 hasta 10
# (-10 y 10 están incluidos). Mostrar por pantalla el índice de la fila con
# suma más alta y la matriz. (10 Pts)

from random import randint

def elIndiceDeLaFilaConLaSumaMayor(matrix: list):
    sumas = []
    for fila in matrix:
        suma = 0
        for i in fila:
            suma += i
        sumas.append(suma)

    return sumas.index(max(sumas))

def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for fila in matrix:
            for col in fila:
                print("%4d" % col, end='')
            print()

        a = elIndiceDeLaFilaConLaSumaMayor(matrix)
        print(f"\nEl indice de la fila con la suma más alta es: {a}")

    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()
