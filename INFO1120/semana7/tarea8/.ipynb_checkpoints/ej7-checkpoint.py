#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz, la función debe
# retornar un índice que indica cual es la columna que posee la suma más alta.
# La matriz debe ser de n*m donde n y m se deben solicitar al usuario, la
# matriz será rellenada con números enteros aleatorios entre -10 hasta 10
# (-10 y 10 están incluidos). Mostrar por pantalla el índice de la columna
# con suma más alta y la matriz. (10 Pts)

from random import randint

def elIndiceDeLaColumnaConLaSumaMayor(matrix: list):
    sumas = []
    for j in range(len(matrix[0])):
        suma = 0
        for i in range(len(matrix)):
            suma += matrix[i][j]
        sumas.append(suma)

    return sumas.index(max(sumas))

def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for fila in matrix:
            for col in fila:
                print("%4d" % col, end='')
            print()

        a = elIndiceDeLaColumnaConLaSumaMayor(matrix)
        print(f"\nEl indice de la columna con la suma más alta es: {a}")

    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()
