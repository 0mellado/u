#!/usr/bin/python3

from random import randint

def menor(matrix: list):
    menor = matrix[0][0]
    coord = [0, 0]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if menor > matrix[i][j]:
                menor = matrix[i][j]
                coord[0] = i
                coord[1] = j
    return (coord[0], coord[1])


def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for fila in matrix:
            for col in fila:
                print("%4d" % col, end='')
            print()

        c = menor(matrix)
        print(f"\nEl número menor es el {matrix[c[0]][c[1]]} en las coordenadas {c}")

    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()
