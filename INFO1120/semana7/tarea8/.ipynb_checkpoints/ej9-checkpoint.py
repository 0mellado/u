#!/usr/bin/python3
# Genera una función que recibe como parámetro una matriz, la función debe
# retornar un tupla que en indica la posición de fila y columna donde encuentra
# el número más alto que existe en la matriz. La matriz debe ser de n*m donde
# n y m se deben solicitar al usuario, la matriz será rellenada con número
# enteros aleatorios entre -10 hasta 10 (-10 y 10 están incluidos).
# Finalmente mostrar por pantalla la tupla, el valor más alto y la matriz
# generada. (10 Pts)

from random import randint

def mayor(matrix: list):
    mayor = matrix[0][0]
    coord = [0, 0]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if mayor < matrix[i][j]:
                mayor = matrix[i][j]
                coord[0] = i
                coord[1] = j
    return (coord[0], coord[1])


def main():
    try:
        fil = int(input("Filas: "))
        col = int(input("Columnas: "))
        matrix = []
        for i in range(fil):
            matrix.append([])
            for _ in range(col):
                matrix[i].append(randint(-10, 10))

        for fila in matrix:
            for col in fila:
                print("%4d" % col, end='')
            print()

        c = mayor(matrix)
        print(f"\nEl número mayor es el {matrix[c[0]][c[1]]} en las coordenadas {c}")

    except:
        print("Error: Las entradas deben ser números")

if __name__ == '__main__':
    main()
