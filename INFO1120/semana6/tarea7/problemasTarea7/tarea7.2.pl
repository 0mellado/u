#!/usr/bin/perl -w

# Crea tu propia función que realiza lo mismo que la función join(), esto quiere decir que la función
# recibe 2 parámetros de entrada. El primer parámetro es una lista de cualquier tamaño que contiene
# las palabras que se quieren juntar. El segundo parámetro es un carácter por el cual se unen las
# variables de la lista, finalmente la función debe retornar solo una cadena de caracteres Sin usar la
# función join() Nota: Se descontarán puntos si el print() se encuentra dentro de la función. (10 Pts)
# Ejemplo:
# a=['Hola','Mundo']
# b ='-'
# print(mi_join(a,b))
# Al utilizar la función debe devolver "Hola-Mundo". Imprimir el valor en pantalla


# entrada: una lista de tipo string y el separador que también tiene que ser un string
# salida: retorna un string con los elementos de la lista concatenados pero separados por el segundo parametro
sub joyn {
    my $e = pop;
    my @strings = @_;

    my $rString = "";

    for my $str (@strings) {
        if ($str eq $strings[0]) {
            $rString = $str;
        } else {
            $rString = $rString.$e.$str;
        }
    }
    return $rString;
}

my @a = ("hola", "mundo");

my $b = "-";

print joyn(@a, $b), "\n";
