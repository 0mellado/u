#!/usr/bin/perl -w

# Crear una función que posee dos parámetros de entrada, el primer parámetro es una cadena de
# caracteres y el segundo parámetro es un carácter, donde la función deberá retornar True si el
# carácter se encuentra dentro de la cadena de caracteres y False en caso que no exista dentro de la
# cadena de caracteres. Nota: No pueden ocupar la instrucción “in” junto con el "if", ejemplo "if
# caracter in lista". (10 Pts)

# entrada: recive un string con primer y segundo parametro
# salida: retorna 1 o verdadero si el caracter se encuentra en la cadena y 0 cuando 
# el caracter no se encuentra en la cadena
sub in {
    my $chr = shift;
    my $string = shift;

    my @string = split "", $string;

    for my $c (@string) {
        if ($c eq $chr) {
            return 1;
        }
    }
    return 0;
}

if (in("s", "lagos")) {
    print "s esta en lagos\n";
} else {
    print "s no esta en lagos\n";
}

if (in("v", "lagos")) {
    print "v esta en lagos\n";
} else {
    print "v no esta en lagos\n";
}
