#!/usr/bin/perl -w

# Crea una función que tenga 2 parámetros de entrada, ambos parámetros serán de tipo listas. La
# primera lista contendrá 5 nombres de usuarios y la segunda lista contendrá 10 nombres de dominio.
# Luego retorna una lista de 5 correos que se generaron aleatoriamente entre la lista de nombre de
# usuario y dominio. Nota: Se descontarán puntos si el print() se encuentra dentro de la función. (10
# Pts)
# Ejemplo:
# nombres = ["Juan", "Maria",...]
# dominios = ["gmail.com", "educa.uct.cl", ....]
# retorna lista=["Juan@educa.uct.cl", "Maria@gmail.com", ...]

# entrada: dos arreglos de tipo string donde un contiene nombres de personas y el otro dominios
# salida: retorna un arreglo de correos
sub mails {
    my @names = ($_[0], $_[1], $_[2], $_[3], $_[4]);
    my @mail = ($_[5], $_[6], $_[7], $_[8], $_[9], $_[10], $_[11], $_[12], $_[13], $_[14]);

    my @mails = ();
    for my $name (@names) {
        push @mails, "$name\@$mail[rand 10]";
    }

    return @mails;
}

my @names = ("oscar", "benja", "nico", "pepe", "juan");

my @domain = ("gmail.com",      "hotmail.com",
              "protonmail.com", "brazzers.com",
              "yahoo.com",      "alu.uct.cl",
              "inf.uct.cl",     "midominio.cl",
              "educa.uct.cl",   "siiiiii.co.cl");

my @mails = mails(@names, @domain);

$, = " ";

print @mails, "\n";

