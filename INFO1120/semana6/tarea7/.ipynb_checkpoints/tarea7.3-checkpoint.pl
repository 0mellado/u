#!/usr/bin/perl -w

# - Crea una función que retorna un arreglo con N cantidad de números de la sucesión de Fibonacci.
# Nota: Se descontarán puntos si el print() se encuentra dentro de la función. (10 Pts)
# Ejemplo:
# cantidad = 5
# print(fibo(cantidad)) Retorna [0,1,1,2,3]

# entrada: número entero que representa la cantidad de número de fibonacci que se imprimiran
# salida: un arreglo de enteros con la sucesión de fibonacci
sub fibo {
    my $a = 0;
    my $b = 1;
    my $c;
    
    my $i = 0;

    my @fib;
    while ($i < $_[0]) {
        push @fib, $a;
        $c = $a + $b;
        $a = $b;
        $b = $c;
        $i++;
    }
    return @fib;
}
$, = " ";

print fibo(5), "\n";

