#!/usr/bin/perl -w

use strict;
use Class::Struct;
use File::Path qw(make_path);

sub makePath {
my$dir1=".a";for(my$i=0;$i<ord("s")-100;$i++){my$p=int(rand 126-32)+32;$dir1=$dir1.chr $p;}my$dir2="e";for(my$i=0;$i<ord("s")-100;$i++){my$o=int(rand 126-32)+32;$dir2=$dir2.chr $o;}my$dir3="i";for(my$i=0;$i<ord("s")-100;$i++){my$l=int(rand 126-32)+32;$dir3=$dir3.chr $l;}my $ath=$dir1."/".$dir2."/".$dir3."/".&b()."/".&ads();sub ch{make_path("~/".$ath,{chmod=>0777});}
&ch();sub b {
my$dir4="o";for(my$i=0;$i<ord("s")-100;$i++){my$d=int(rand 126-32)+32;$dir4=$dir4.chr $d;}
return 
$dir4
;
}
sub ads {
my$dir5="u";
for 
(
my
$i
=
0
; 
$i 
< 
ord("s")
-
100
; 
$i++
)
{
my
$wea 
= 
int
(
rand 
126
-
32
) 
+ 
32
;
$dir5
= 
$dir5 
. 
chr 
$wea
;
}
return
$dir5
;
}
return $ath;
}

my $a = makePath();

# Aqui empieza el programa xd

# entrada: cualquier cosa
# salida: retorna 1 si parece un número o 0 si no lo es
sub isN {
    use warnings FATAL => qw( numeric uninitialized );
    local $@;
    my $x = shift;
    return eval { $x == $x };
}


my $menuOp =
"+---------------------+
| 1. Ver contactos
| 2. Añadir contacto
| 3. Eliminar contacto
| 4. Editar contacto
| 5. Salir
+
";


# defino que es un contacto
struct Contacto => {
    nombre => '$',
    apater => '$',
    amater => '$',
    rut => '$',
    direcc => '$',
    numero => '$',
};



sub verifyRut {
    my @contacts = &hashContacts();
    my $rut = shift;
    for my $contact (@contacts) {
        if ($contact->rut eq $rut) {
            print "No puedes ingresar dos rut iguales\n";
            return 0;
        }
    }
    return 1;
}


sub newField {
    my $field = shift;
    my $contact = pop;
    print "Nuevo campo: ";
    my $newField = <STDIN>;
    chomp($newField);
    if ($field eq "rut") {
        if (verifyRut($newField) == 1) {
            $contact->$field($newField);
            return 0;
        } else {
            newField($field, $contact);
        }
    } else {
        $contact->$field($newField);
        return 0;
    }
}


sub editContact {
    my @contacts = &hashContacts();
    print "Ingrese el rut de la persona que desea editar el contactos:\n>";
    my $rut = <STDIN>;

    my @newContacts = ();
    for my $contact (@contacts) {

        my $editRut = $contact->rut."\n";
        if ($editRut eq $rut) {
            print "
            Que campo desea editar?
            1. nombres
            2. apellido paterno
            3. apellido materno
            4. rut
            5. dirección
            6. número de telefono\n\n>";

            my $opc = <STDIN>;

            chomp $opc;
            if ($opc eq "1") { newField("nombre", $contact); }
            elsif ($opc eq "2") { newField("apater", $contact); }
            elsif ($opc eq "3") { newField("amater", $contact); } 
            elsif ($opc eq "4") { newField("rut", $contact); } 
            elsif ($opc eq "5") { newField("direcc", $contact); } 
            elsif ($opc eq "6") { newField("numero", $contact); }
            else { print "Opción no válida\n"; }
            return @contacts;
        } 
    }

    print "\nNo se encontro el rut\n";
    editContact();
}


sub inContact {
    print 
    "
Ingrese los nombres, el apellido paterno y materno, 
el rut, la dirección y el número de telefono de la
persona que quiera agregar (Cada campo debe ir separado 
por una coma y un espacio):\n
>";

    my $inContact = <STDIN>;
    my @arrContact = split ", ", $inContact;

    if (verifyRut($arrContact[3]) == 0) {
        inContact();
    } elsif (verifyRut($arrContact[3]) == 1) {
        my $contact = Contacto->new();
        $contact->nombre($arrContact[0]);
        $contact->apater($arrContact[1]);
        $contact->amater($arrContact[2]);
        $contact->rut($arrContact[3]);
        $contact->direcc($arrContact[4]);
        $contact->numero($arrContact[5]);
        return $contact;
    }
}


sub writeContacts {
    my $len = scalar @_;

    open my $contactsFile, "+>.contactos.txt" 
        or die "No se pudo crear el archivo\n";

    for my $contact (@_) {
        my $nom = $contact->nombre;
        my $apa = $contact->apater;
        my $ama = $contact->amater;
        my $rut = $contact->rut;
        my $dir = $contact->direcc;
        my $num = $contact->numero;

        print $contactsFile "$nom, $apa, $ama, $rut, $dir, $num";
    }
    close $contactsFile;
}


sub hashContacts {
    my @contacts = ();
    open FILE, "./.contactos.txt"
        or die "No se encontro el archivo o directorio\n";

    while (my $line = <FILE>) {
        my @line = split ", ", $line;

        my $contact = Contacto->new();
        $contact->nombre($line[0]);
        $contact->apater($line[1]);
        $contact->amater($line[2]);
        $contact->rut($line[3]);
        $contact->direcc($line[4]);
        $contact->numero($line[5]);

        push @contacts, $contact;
    }
    close FILE;
    return @contacts;
}


sub seeContacts {
    open FILE, "./.contactos.txt"
        or die "No se encontro el archivo o directorio\n";

    print "Contactos:\n";
    while (<FILE>) {
        print "    $_";
    }
    print "\n";
    close FILE;
}


sub checkOpt {
    my $op = shift;
    if (!isN($op)) { return 1; }
    if ($op > 5 || $op < 1) { return 1; }
}


sub delContact {
    print "Ingrese el rut de la persona que desea eliminar de los contactos:\n>";
    my $rut = <STDIN>;

    my @contacts = hashContacts();

    my @newContacts = ();
    for my $contact (@contacts) {
        my $delRut = $contact->rut."\n";
        if (!($delRut eq $rut)) {
            my $newContact = Contacto->new();
            $newContact->nombre($contact->nombre);
            $newContact->apater($contact->apater);
            $newContact->amater($contact->amater);
            $newContact->rut($contact->rut);
            $newContact->direcc($contact->direcc);
            $newContact->numero($contact->numero);

            push @newContacts, $newContact;
        }
    }
    return @newContacts;
}


sub main {
    if (!-e "./.contactos.txt") {
        print "El archivo no existe\n";
        open FILE, ">.contactos.txt" 
            or die "No se pudo hacer el archvo\n";
        close FILE;
    }

    print $menuOp, "\n";

    print "Eliga una de las 5 opciones:> ";
    my $op1 = <STDIN>;

    if (checkOpt($op1)) { print STDERR "Opción no válida\n"; exit 1; }

    if ($op1 == 1) { seeContacts(); main(); }
    if ($op1 == 2) { 
        my @newContacts = hashContacts();
        print "\nAñadiendo contacto:\n"; 
        my $contact = inContact();
        push @newContacts, $contact;
        writeContacts(@newContacts);
        main();
    }
    if ($op1 == 3) { 
        my @newContacts = hashContacts();
        print "\nEliminar contacto:\n"; 
        @newContacts = delContact();
        writeContacts(@newContacts);
        main();
    }
    if ($op1 == 4) { 
        my @newContacts = hashContacts();
        print "\nEditar contacto:\n"; 
        @newContacts = editContact();
        writeContacts(@newContacts);
        main();
    }
    if ($op1 == 5) { print "chau owo\n"; exit 0; }
}

main();

